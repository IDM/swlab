# Shared files for magnetic measurement benches

## Introduction
This repository contains shared C++ source code for undulator and multipole magnet measurement benches.
The following interfaces are implemented:

- Igor XOP 6, 7 and 8 Win32
- The *axis* libraries are not yet available for Win64
## Contents
### axis

Classes for driving axis and stages, eg Newport XPS. See *axis/README.md* for details.

### comm

Sources files for communication with instruments, e.g. socket class, GPIB, etc. See *comm/README.md* for details.

### keithley
Implementation of the keithley class for controlling Keithley instruments. See *keithley/README.md* for details.

### math
Mathematical functions needed for data analysis. 

## Dependencies
The following packages are needed:
- Igor XOPToolkits are needed for the Igor interfaces (www.wavemetrics.com)
- NI 488-2 drivers

The project files assume that the external libraries are organized as follows:

- *external_libs*
  - *XOPToolkit*
    - *XOPToolkit6*
    - *XOPToolkit7.01/IgorXOPs7*
    - *XOPToolkit8/IgorXOPs*
- *Benches*
  - *BenchesShared (this project)*

## Compilation

The projects were compiled with Microsoft Visual Studio 2017. 

After cloning the repository, please update the include directories according to the location of the external libraries (Igor XOPToolkit, etc.) on your computer.

### Common errors

In case of compilation or link errors, please check the following items:

- It may be necessary to recompile the XOPSupport libs
- The NI 488 drivers must be installed
- Be sure that the link in the project files corresponds to the directory structure above, or update the links

**Igor XOP 7 and 8 note:** It was necessary to replace the XOPMain() by the legacy main() in the xop.cpp files, to avoid a bug at Igor start. The cause of this problem is not well understood but it seems to work as it is now.