// Gpib.cpp: implementation of the Gpib class.
//
//////////////////////////////////////////////////////////////////////

//#include <iostream.h>
#include "Gpib.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------
// Basic constructor where only the GPIB address is given as 
// parameter.


Gpib::Gpib(int addr)
{
	CHAR rspb;
	int  i;
	
	Handler = ibdev (0, addr, 0, T3s, 1, 0);
		
	if (Handler < 0)
	{
		Really_Connected = false;
		return;
	}
	else
		Really_Connected = true;
		
	ibclr(Handler);
		
	/*----------------------------*/
		
//	ibeos(Handler, 0x140A);
		
//	ibconfig(Handler, IbcAUTOPOLL, 0);



for(i=0; i<10; i++)
		ibrsp(Handler, &rspb);
}

//-------------------------------------------------------------------
// This constructor can be used when the timeout has to be set to
// a particular value (1, 3, 10 or 30 seconds). If none of the
// previous values is given then 3 is applied by default.

Gpib::Gpib(int addr, int timeout)
{
	CHAR rspb;
	int  NI_Timeout;
	int  i;

	switch(timeout)
	{
		case 1:
			NI_Timeout = T1s;
			break;
		case 3:
			NI_Timeout = T3s;
			break;
		case 10:
			NI_Timeout = T10s;
			break;
		case 30:
			NI_Timeout = T30s;
			break;
		default:
			NI_Timeout = T3s;
			break;
	}

	Handler = ibdev (0, addr, 0, NI_Timeout, 1, 0);
		
	if (Handler < 0)
	{
		Really_Connected = false;
		return;
	}
	else
		Really_Connected = true;
		
	ibclr(Handler);
		
	/*----------------------------*/
		
//	ibeos(Handler, 0x140A);
		
//	ibconfig(Handler, IbcAUTOPOLL, 0);
		
	for(i=0; i<10; i++)
		ibrsp(Handler, &rspb);
}

//-------------------------------------------------------------------
// Constructor called with GPIB device name as parameter.

Gpib::Gpib(char *dev_name)
{
GetConfiguration(dev_name);
	Handler = ibdev (0, Adress, 0, T3s, 1, 0);
		
	if (Handler < 0)
	{
		Really_Connected = false;
		return;
	}
	else
		Really_Connected = true;
		
	ibclr(Handler);
int i;
CHAR rspb;
for(i=0; i<10; i++)
		ibrsp(Handler, &rspb);

SetTimeout(Timeout);
}

//-------------------------------------------------------------------
Gpib::~Gpib()
{
	if(Really_Connected)
		ibonl(0, 0);  // <= This is very VERY required !!!!!!!
}

//-------------------------------------------------------------------
bool Gpib::Write(char *str)
{
	bool status;
	
	if(Really_Connected)
	{
		ibwrt(Handler, str, (long) strlen(str));
		
		if (ibsta & ERR)
		{
			LastError = GPIB_ERROR_AT_WRITE;
			status = false;
		}
		else
			status = true;
		
		return(status);
		}
	else
	{
		LastError = GPIB_NO_OPEN_CONNECTION;
		return(false);
	}
}

//-------------------------------------------------------------------
bool Gpib::IsConnected()
{
	return(Really_Connected);
}

//-------------------------------------------------------------------
bool Gpib::Read(char *str)
{
	static char resp[500000];

	if(Really_Connected)
	{
		ibrd(Handler, (void *) resp, 500000L);

		if(ibsta & ERR)
		{
			LastError = GPIB_ERROR_AT_READ;
			return(false);
		}
		else
		{
			resp[ibcntl-1] = 0;
			strcpy(str, resp);
			return(true);
		}
		
	}
	else
	{
		LastError = GPIB_NO_OPEN_CONNECTION;
		return(false);
	}
}

//-------------------------------------------------------------------
bool Gpib::Read(char *str, int nb)
{


	if(Really_Connected)
	{
		ibrd(Handler, (void *) str, nb);

		if(ibsta & ERR)
		{
			LastError = GPIB_ERROR_AT_READ;
			return(false);
		}
		else
		{
			str[ibcntl] = 0;
			return(true);
		}		
	}
	else
	{
		LastError = GPIB_NO_OPEN_CONNECTION;
		return(false);
	}
}

//-------------------------------------------------------------------
bool	Gpib::WaitForData()
{
	char rspb;
	int ret;

	if(Really_Connected)
	{
		ret=ibwait(Handler, TIMO|RQS);
		
		if(ibsta & RQS)
		{
			ibrsp(Handler, &rspb);
			return(true);
		}
		
		if(ibsta & TIMO)
		{
			LastError = GPIB_WAIT_TIMEOUT;
			return(false);
		}
		
		return(true);
		}
	else
	{
		LastError = GPIB_NO_OPEN_CONNECTION;
		return(false);
	}
}


//-------------------------------------------------------------------

bool Gpib::SetTimeout(double t)
{
	int t_tosend;

	if(t==0)
		t_tosend=TNONE;
	else if(t<=0.1)
		t_tosend=T100ms;
	else if(t<=0.3)
		t_tosend=T300ms;
	else if(t<=1)
		t_tosend=T1s;
	else if(t<=3)
		t_tosend=T3s;
	else if(t<=10)
		t_tosend=T10s;
	else if(t<=30)
		t_tosend=T30s;
	else if(t<=100)
		t_tosend=T100s;
	else if(t<=300)
		t_tosend=T300s;
	else if(t<=1000)
		t_tosend=T1000s;
	else
		t_tosend=TNONE;

	ibsta=ibtmo(Handler,t_tosend);
	if (ibsta&ERR)
	{
		LastError = GPIB_ERROR_TIMEOUT_SET;
		return (false);
	}
	return (true);

}

int Gpib::GetConfiguration(char *devname)
{
typedef struct _GpibRes
{
	char name[30];
	char val[30];
}GpibRes;

enum Devices 
{
  ADRESS,
  TIMEOUT
};

GpibRes GpibResTab[] = 
{
  {"ADRESS",       ""},
  {"TIMEOUT",       ""},
};
	
	FILE   *Fin;
	char   *Temp;
	char    ConfBuf[1000];
	char    ConfStr[128];
	char    FileName[256];
	int     i, NbRes;
	int     Status;
	


	Status = GetEnvironmentVariable("ConfPath", ConfStr, 100);

  if(Status == 0)  // Environment variable not defined.
    sprintf(FileName, "%s\\%s.conf", "c:\\ConfigurationFolder", devname);
  else
    sprintf(FileName, "%s\\%s.conf", ConfStr, devname);
	
  Fin= fopen(FileName, "r");
  if(Fin == NULL)
	{

		return(-1);
	}

	//--------------------------------------------
	// Get resource names from configuration file.

  fread(ConfBuf, sizeof(ConfBuf), 1, Fin);
	NbRes = sizeof(GpibResTab) / sizeof(GpibRes);
	
  for(i=0; i<NbRes; i++)
  {
		if((Temp = strstr(ConfBuf, GpibResTab[i].name)) == NULL)
		{
			fclose(Fin);
			return(-1);
		}
		else
		{
			sscanf(Temp, "%*s%s", &(GpibResTab[i].val));

		}
  }

	Timeout=atof(GpibResTab[TIMEOUT].val);
	Adress=	atoi(GpibResTab[ADRESS].val);
	fclose(Fin);
	return(0);
}

Gpib::Gpib()
{

}
