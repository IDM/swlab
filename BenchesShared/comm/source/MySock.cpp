// MySock.cpp: implementation of the MySock class.
//
//////////////////////////////////////////////////////////////////////

//#include <iostream.h>

#include "MySock.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------
// Basic constructor where only the MySock address is given as 
// parameter.
MySock::MySock(int addr)
{

}

//-------------------------------------------------------------------
// This constructor can be used when the timeout has to be set to
// a particular value (1, 3, 10 or 30 seconds). If none of the
// previous values is given then 3 is applied by default.

MySock::MySock(int addr, int timeout)
{

}

//-------------------------------------------------------------------
// Constructor called with MySock device name as parameter.

MySock::MySock(char *dev_name)

{
	GetConfiguration(dev_name);
	int iResult ;
	iResult= WSAStartup( MAKEWORD(2,2), &wsaData );
	if ( iResult != NO_ERROR )
	{
		sprintf(error,"Error at WSAStartup()\nMySock::Init Error at WSAStartup\n");
		Really_Connected=false;
		m_socket=0;
		return;
	}
	m_socket = socket( AF_INET, SOCK_STREAM, IPPROTO_TCP );
	if ( m_socket == INVALID_SOCKET ) 
	{
	
		sprintf(error,"Error at socket():\nSocket::Init bad socket\n");
		WSACleanup();
		Really_Connected=false;
		m_socket = 0;		
		return;
	}
	else
	{
		clientService.sin_family = AF_INET;
		clientService.sin_addr.s_addr = inet_addr(&Adress[0]);
		clientService.sin_port = htons( (short)port );
		if ( connect( m_socket, (SOCKADDR*) &clientService, sizeof(clientService) ) == SOCKET_ERROR) 
		{
			closesocket(m_socket);	
			if ( connect( m_socket, (SOCKADDR*) &clientService, sizeof(clientService) ) == SOCKET_ERROR) 
			{
				sprintf(error,"Failed to connect after close:\nSocket::Init Failed to connect after close\n");
				Really_Connected=false;
				m_socket = 0;		

			}
			return;
		}
		int tm;
		tm=(int)(timeout*1000);
		int status;
		status=setsockopt(m_socket, SOL_SOCKET, SO_RCVTIMEO, (char*)&tm, sizeof(int));

	return;

  }
}

//-------------------------------------------------------------------
MySock::~MySock()
{
	if ( m_socket != 0 ) 
	{
		WSACleanup();
		closesocket(m_socket);
	}
}

//-------------------------------------------------------------------
bool MySock::Write(char *str)
{
int bytesSent;

char *tosend;

tosend=(char*)malloc(strlen(str)+5);
sprintf(tosend,"%s\n",str);

bytesSent = send( m_socket, tosend, strlen(tosend), 0 );
if (bytesSent!=(int)strlen(tosend))
{
		sprintf(error,"error in send to socket !\nSocket::Write Function\n");
		free(tosend);
		return false;
}
free(tosend);
return true;
}

//-------------------------------------------------------------------
bool MySock::IsConnected()
{
	return(Really_Connected);
}

//-------------------------------------------------------------------
bool MySock::Read(char *str)
{
	char *argout  = new char[1030];
	strcpy(argout, "");
	if (!Really_Connected)
	{
		sprintf(error," Not Connected!\nSocket::read()\n");
		return false;
	}
	int bytesRecv;
	char tmp[2005];
	int count=0;
	int buffsize=2005;
	do
	{
		if ( count > buffsize-1024)
		{
			if( (argout = (char *)realloc( argout, buffsize + (1024) ))==  NULL )
			{
					sprintf(error," Realloc return NULL!\nSocket::read()\n");
					return false;
			}
			buffsize+=1024;
		}
		bytesRecv = recv( m_socket, tmp, 1024, 0 );
		if (bytesRecv<0)
		{
			if (count==0)
			{
				sprintf(error," Socket::Read Function!\nSocket::read()\n");
				return false;
			}
		}
		else
		{
			tmp[bytesRecv]=0;	
			strcat(argout,tmp);
			count+=bytesRecv;
		}

	}
	while (bytesRecv>1023);
	argout[count]=0;
	strcpy(str, argout);
	delete (argout);
	return true;
}

//-------------------------------------------------------------------
bool MySock::Read(char *str, int nb)
{
	char *argout  = new char[nb];
	strcpy(argout, "");
	if (!Really_Connected)
	{
		sprintf(error," Not Connected!\nSocket::read()\n");
		return false;
	}
	int bytesRecv;
	bytesRecv = recv( m_socket, argout, nb, 0 );
		if (bytesRecv<0)
		{
				sprintf(error," Socket::Read Function!\nSocket::read()\n");
				delete (argout);
				return false;
		}
		else
		{
			argout[bytesRecv]=0;	
		}

	strcpy(str, argout);
	delete (argout);
	return true;
}

//-------------------------------------------------------------------
bool	MySock::WaitForData()
{
	if (!Really_Connected)
	{
		sprintf(error," Not Connected!\nSocket::read()\n");
		return false;
	}
	return true;
}


//-------------------------------------------------------------------

bool MySock::SetTimeout(double t)
{
	int tm,err;
	tm=(int)(t*1000);
	err=setsockopt(m_socket, SOL_SOCKET, SO_RCVTIMEO, (char*)&tm, sizeof(int));

	return (true);

}

// // get configuration parameter for port
int MySock::GetConfiguration(char *Name)
{
typedef struct _MySockRes
{
	char name[30];
	char val[30];
}MySockRes;

enum Devices 
{
  ADRESS,
  PORT,
  TIMEOUT
};

MySockRes MySockResTab[] = 
{
  {"ADRESS",       ""},
  {"PORT",       ""},
  {"TIMEOUT",       ""},
};
	
	FILE   *Fin;
	char   *Temp;
	char    ConfBuf[1000];
	char    ConfStr[128];
	char    FileName[256];
	int     i, NbRes;
	int     Status;
	


	Status = GetEnvironmentVariable("ConfPath", ConfStr, 100);

  if(Status == 0)  // Environment variable not defined.
    sprintf(FileName, "%s\\%s.conf", "c:\\ConfigurationFolder", Name);
  else
    sprintf(FileName, "%s\\%s.conf", ConfStr, Name);
	
  Fin= fopen(FileName, "r");
  if(Fin == NULL)
	{

		return(-1);
	}

	//--------------------------------------------
	// Get resource names from configuration file.

  fread(ConfBuf, sizeof(ConfBuf), 1, Fin);
	NbRes = sizeof(MySockResTab) / sizeof(MySockRes);
	
  for(i=0; i<NbRes; i++)
  {
		if((Temp = strstr(ConfBuf, MySockResTab[i].name)) == NULL)
		{
			fclose(Fin);
			return(-1);
		}
		else
		{
			sscanf(Temp, "%*s%s", &(MySockResTab[i].val));

		}
  }
	strcpy(Adress, MySockResTab[ADRESS].val);
	port=atoi(MySockResTab[PORT].val);
	timeout=atoi(MySockResTab[TIMEOUT].val);
	
	fclose(Fin);
	return(0);
}

MySock::MySock()
{

}
