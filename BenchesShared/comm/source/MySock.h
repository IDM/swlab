// MySock.h: interface for the MySock class.

//

//////////////////////////////////////////////////////////////////////



#if !defined(AFX_MySock_H__D622A1B4_3D74_11D2_A391_00805F3A78EC__INCLUDED_)

#define AFX_MySock_H__D622A1B4_3D74_11D2_A391_00805F3A78EC__INCLUDED_



#if _MSC_VER >= 1000

#pragma once

#endif // _MSC_VER >= 1000


//#include "winsock2.h"

#include <windows.h>
#include <stdio.h>

//-------------------------------------------------------------------

enum MySockErrors{
	MySock_NO_OPEN_CONNECTION,
		MySock_WAIT_TIMEOUT,
		MySock_ERROR_AT_WRITE,
		MySock_ERROR_AT_READ,
		MySock_ERROR_TIMEOUT_SET,
};

//-------------------------------------------------------------------

#include "ComPath.h"


class MySock : public ComPath
{ 
public:
	MySock();
	
	bool SetTimeout(double t);
	bool Read(char *, int);
	MySock    (char *dev_name);
	MySock    (int addr);
	MySock    (int addr, int timeout);
	~MySock   ();
	 bool IsConnected  ();                    // To check connection
	 bool     Write        (char *str);           // Send command to MySock device
	 bool     Read         (char *str);
	 bool	    WaitForData  ();

	int GetConfiguration(char *);
private:
	bool Really_Connected;
	int     Handler;
	int     LastError;
	WSADATA wsaData;
	SOCKET m_socket;
	char Adress[30];
	int port;
	double timeout;
	sockaddr_in clientService;
	char error[256];


};



#endif // !defined(AFX_MySock_H__D622A1B4_3D74_11D2_A391_00805F3A78EC__INCLUDED_)

