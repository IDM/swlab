// Gpib.h: interface for the Gpib class.

//

//////////////////////////////////////////////////////////////////////



#if !defined(AFX_GPIB_H__D622A1B4_3D74_11D2_A391_00805F3A78EC__INCLUDED_)

#define AFX_GPIB_H__D622A1B4_3D74_11D2_A391_00805F3A78EC__INCLUDED_



#if _MSC_VER >= 1000

#pragma once

#endif // _MSC_VER >= 1000



#include <windows.h>
#if !defined(_WIN64)
#include "decl-32.h"
#else
#include "ni4882.h"
#endif



//-------------------------------------------------------------------

enum GpibErrors{
	GPIB_NO_OPEN_CONNECTION,
		GPIB_WAIT_TIMEOUT,
		GPIB_ERROR_AT_WRITE,
		GPIB_ERROR_AT_READ,
		GPIB_ERROR_TIMEOUT_SET,
};



//-------------------------------------------------------------------

#include "ComPath.h"

class Gpib : public ComPath 
{
private:
	bool Really_Connected;
	int     Handler;
	int     LastError;
public:
	double Timeout;
	int Adress;
	Gpib();
	int GetConfiguration(char *devname);
	bool SetTimeout(double t);
	bool Read(char *, int);
	Gpib    (char *);
	Gpib    (int addr);
	Gpib    (int addr, int timeout);
	~Gpib   ();

	 bool IsConnected  ();                    // To check connection
	 bool     Write        (char *str);           // Send command to GPIB device
	 bool     Read         (char *str);
	 bool	    WaitForData  ();

};



#endif // !defined(AFX_GPIB_H__D622A1B4_3D74_11D2_A391_00805F3A78EC__INCLUDED_)

