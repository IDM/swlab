// ComPath.cpp: implementation of the ComPath class.
//
//////////////////////////////////////////////////////////////////////

//#include <iostream.h>

#include "ComPath.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
//-------------------------------------------------------------------
// Constructor called with ComPath device name as parameter.

ComPath::ComPath(char *dev_name)

{
	int status;
	status=GetConfiguration(dev_name);
	if (status==0)
	{
		if (strcmp(Type,"GPIB")==0)
		{
//			GenAxis = new Anorad(Name);
//			delete GenAxis;
			GenComPath = new Gpib(atoi(Adress));
			Valid = true;
		}
		else 	if (strcmp(Type,"ETH")==0)
		{
			GenComPath = new MySock(dev_name);
			Valid = true;
		}
	}
	return;
}

ComPath::ComPath()
{

}
//-------------------------------------------------------------------
// This constructor can be used when the timeout has to be set to
// a particular value (1, 3, 10 or 30 seconds). If none of the
// previous values is given then 3 is applied by default.

ComPath::ComPath(int addr, int timeout)
{

}

//-------------------------------------------------------------------
// Basic constructor where only the ComPath address is given as 
// parameter.
ComPath::ComPath(int addr)
{

}




//-------------------------------------------------------------------
ComPath::~ComPath()
{
		if (strcmp(Type,"GPIB")==0)
		{
			delete ((Gpib *)GenComPath);
		}
		else 	if (strcmp(Type,"ETH")==0)
		{
			delete ((MySock *)GenComPath);
		}		Valid = false;	
}

//-------------------------------------------------------------------
bool ComPath::Write(char *str)
{
	if  (Valid)
		return GenComPath->Write(str);
	else
		return true;

}

//-------------------------------------------------------------------
bool ComPath::IsConnected()
{
	if  (Valid)
		return GenComPath->IsConnected();
	else
		return true;

}

//-------------------------------------------------------------------
bool ComPath::Read(char *str)
{
	if  (Valid)
		return GenComPath->Read(str);
	else
		return true;
}

//-------------------------------------------------------------------
bool ComPath::Read(char *str, int nb)
{
	if  (Valid)
		return GenComPath->Read(str,nb);
	else
		return true;

}

//-------------------------------------------------------------------
bool	ComPath::WaitForData()
{
	if  (Valid)
		return GenComPath->WaitForData();
	else
		return true;
}


//-------------------------------------------------------------------

bool ComPath::SetTimeout(double t)
{
	if  (Valid)
		return GenComPath->SetTimeout(t);
	else
		return true;

}

// // get configuration parameter for port
int ComPath::GetConfiguration(char *Name)
{
typedef struct _ComPathRes
{
	char name[30];
	char val[30];
}ComPathRes;

enum Devices 
{
  TYPE,
  ADRESS,
  PORT,
  TIMEOUT
};

ComPathRes ComPathResTab[] = 
{
	
	{"COM_TYPE",       ""},
	{"ADRESS",       ""},
	{"COM_PORT",       ""},
	{"TIMEOUT",       ""},
};
	
	FILE   *Fin;
	char   *Temp;
	char    ConfBuf[1000];
	char    ConfStr[128];
	char    FileName[256];
	int     i, NbRes;
	int     Status;
	


	Status = GetEnvironmentVariable("ConfPath", ConfStr, 100);

  if(Status == 0)  // Environment variable not defined.
    sprintf(FileName, "%s\\%s.conf", "c:\\ConfigurationFolder", Name);
  else
    sprintf(FileName, "%s\\%s.conf", ConfStr, Name);
	
  Fin= fopen(FileName, "r");
  if(Fin == NULL)
	{

		return(-1);
	}

	//--------------------------------------------
	// Get resource names from configuration file.

  fread(ConfBuf, sizeof(ConfBuf), 1, Fin);
	NbRes = sizeof(ComPathResTab) / sizeof(ComPathRes);
	
  for(i=0; i<NbRes; i++)
  {
		if((Temp = strstr(ConfBuf, ComPathResTab[i].name)) == NULL)
		{
			fclose(Fin);
			return(-1);
		}
		else
		{
			sscanf(Temp, "%*s%s", &(ComPathResTab[i].val));

		}
  }
	
	strcpy(Adress, ComPathResTab[ADRESS].val);
	port=atoi(ComPathResTab[PORT].val);
	timeout=atoi(ComPathResTab[TIMEOUT].val);
	strcpy(Type, ComPathResTab[TYPE].val);

	fclose(Fin);
	return(0);
}

