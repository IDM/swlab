// ComPath.h: interface for the ComPath class.

//

//////////////////////////////////////////////////////////////////////
#include "Gpib.h"
#include "MySock.h"


#if !defined(AFX_COMPATH_H__D622A1B4_3D74_11D2_A391_00805F3A78EC__INCLUDED_)

#define AFX_COMPATH_H__D622A1B4_3D74_11D2_A391_00805F3A78EC__INCLUDED_


#if _MSC_VER >= 1000

#pragma once

#endif // _MSC_VER >= 1000


#include <windows.h>
#include <stdio.h>



//-------------------------------------------------------------------

enum ComPathErrors{
	COMPATH_NO_OPEN_CONNECTION,
		COMPATH_WAIT_TIMEOUT,
		COMPATH_ERROR_AT_WRITE,
		COMPATH_ERROR_AT_READ,
		COMPATH_ERROR_TIMEOUT_SET,
};

//-------------------------------------------------------------------

class ComPath 
{
public:
	ComPath();
	 ComPath    (char *dev_name);
	 ComPath    (int addr);
	 ComPath    (int addr, int timeout);
	 ~ComPath   ();
	bool Valid;
	ComPath *GenComPath;	
	virtual bool SetTimeout(double t);
	virtual bool Read(char *, int);
	 virtual bool IsConnected  ();                    // To check connection
	 virtual bool     Write        (char *str);           // Send command to ComPath device
	 virtual bool     Read         (char *str);
	 virtual bool	    WaitForData  ();

	int GetConfiguration(char *);
private:
	char Adress[30];
	int port;
	double timeout;
	char error[256];
	char Type[10];

};



#endif // !defined(AFX_COMPATH_H__D622A1B4_3D74_11D2_A391_00805F3A78EC__INCLUDED_)

