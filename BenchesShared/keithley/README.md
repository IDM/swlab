# Keithley

## Introduction
This subproject contains the source code for driving Keithley instruments.
The following interfaces are implemented:

- Igor XOP6 Win32 
- Igor XOP7 Win 32 and x64
- Igor XOP8 Win32 and x64
## Directory
- *config:* configuration files needed for initialization
- *source:* C++ source codes
- *vc:* Visual Studio 2017 solution
  - *keithley_dll:* project files for DLL (to be adapted for e.g. Python interface using SWIG)
  - *keithley_xop6:* project files for Igor XOP6
  - *keithley_xop7:* project files for Igor XOP7
  - *keithley_xop8:* project files for Igor XOP8
- *xop*: Igor XOP files
  - *res:* resource files
  - *xop6:* binaries Igor XOP6
  - *xop7:* binaries for Igor XOP7
  - *xop8:* binaries for Igor XOP8

## Dependencies
The following tools are needed
- Needs miscellaneous communication files: *comm*
- Igor XOPToolkits are needed for the Igor interfaces (www.wavemetrics.com)

- The repository must be structured as follows (otherwise please update the links in the project files):

  - *external_libs*
    - *XOPToolkit*
      - *XOPToolkit6*
      - *XOPToolkit7.01/IgorXOPs7*
      - *XOPToolkit8/IgorXOPs8*
  - *Benches*
    - BenchesShared
      - *comm*
      - *keithley (this project)*
- NI 488-2 drivers

## Compilation
The project can be compiled with Microsoft Visual Studio 2017 and MSBuild 2017. 

After cloning the repository, please update the include directories according to the location of the external libraries (Igor XOPToolkit, etc.) on your computer.

### Visual Studio Code

Open the *keithley* folder and type Ctrl+Shift+B, then select the configuration you want.

### Visual Studio

Open the *keithley/vc/keithley.sln* file and switch to the configuration you want.

### Common errors

In case of compilation or link errors, please check the following items:

- The XOPs directory must contain IGOR.lib, IGOR64.lib, XOPSupport.lib and XOPSupport64.lib.
- It may be necessary to recompile the XOPSupport libs
- The NI 488 drivers must be installed
- Be sure that the link in the project files corresponds to the directory structure above, or update the links

**Igor XOP 7 and 8 note:** It was necessary to replace the XOPMain() by the legacy main() in the xop.cpp files, to avoid a bug at Igor start. The cause of this problem is not well understood but it seems to work as it is now.

## Configuration
Configuration files are needed for initialization. The default path to configuration files is "c:\\ConfigurationFolder". It can be set to another directory with the *ConfPath* environment variable.