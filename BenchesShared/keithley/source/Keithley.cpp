
//*********************************************************
//@doc Keithley
//@module Keithley.cpp | Implementation of the Keithley class.
//
// ESRF 2000
// C. Penel
//@index | Keithley
//*********************************************************

#include <math.h>

#include <string.h>
#include <stdio.h>
//#include <windows.h>

#include "Keithley.h"
//**********************************************************
// Define some structures used for resources extraction.
//**********************************************************



//**********************************************************
// @mfunc Destructor.
//**********************************************************

Keithley::~Keithley()
{
#ifdef _DEBUG
	printf("Keithley destructor called.\n" );
#endif
	delete (comu_path);
	comu_path=NULL;
}
//**********************************************************
// @mfunc Called by the constructor.
// Open the relevant configuration file, extract the 
// settings (ASCII format) and build private parameters. 
// @parm The name of the Keithley.
//**********************************************************
int Keithley::GetConfiguration(char *Name)
{
	typedef struct _Res
	{
		char name[30];
		char val[30];
	}Res;
	Res ResTab[] = 
	{
 		{"TIMEOUT",			""},
		{"TRIGSRC",			""},
		{"SCANTYPE",		""}, 
		{"SCANLIST",		""}, 
		{"SAMPLE",			""}, 
		{"RANGE",			""},
		{"UNITMODE",		""},
		{"ITIME",			""},
		{"INTEGRATION",		""},
		{"FORMAT",			""},
		{"DIGITS",			""},
		{"DELAY",			""},
		{"CHANNEL",			""},
		{"AUTOZERO",		""},
		{"SCANCARD",		""},
		{"ALL_CHANNEL",		""},
		{"DATAFORMAT",		""},
		{"TRIGCOUNT",		""},
		{"IT_ADJUST",		""},
		{"ADRESS",			""},

	};
	enum Devices 
	{
		TIMEOUT, 
		TRIGSRC, 
		SCANTYPE, 
		SCANLIST, 
		SAMPLE, 
		RANGE,
		UNITMODE,
		ITIME, 
		INTEGRATION, 
		FORMAT,
		DIGITS,
		DELAY,
		CHANNEL,
		AUTOZERO,
		SCANCARD,
		ALL_CHANNEL,
		DATAFORMAT,
		TRIGCOUNT,
		IT_ADJUST,
		ADRESS,
	};	
	FILE   *Fin;
	char   *Temp;
	char    Buf[1000];
	char    ConfStr[128];
	char    FileName[256];
	int     i, NbRes;
	int     status;
	int iparam;


	status = GetEnvironmentVariable("ConfPath", ConfStr, 100);

	if(status == 0)  // Environment variable not defined.
		sprintf(FileName, "%s\\%s.conf", "c:\\ConfigurationFolder", Name);
	else
		sprintf(FileName, "%s\\%s.conf", ConfStr, Name);
	
  Fin = fopen(FileName, "r");
  if(Fin == NULL)
	{
		return(false);
	}

	//--------------------------------------------
	// Get resource names from configuration file.

  fread(Buf, sizeof(Buf), 1, Fin);
	NbRes = sizeof(ResTab) / sizeof(Res);
	
  for(i=0; i<NbRes; i++)
  {
		if((Temp = strstr(Buf, ResTab[i].name)) == NULL)
		{
			return(false);
		}
		else
		{
			sscanf(Temp, "%*s%s", &(ResTab[i].val));
		}
  }
  fclose(Fin);

	double fparam;
	iparam = atoi(ResTab[ADRESS].val);
	if (comu_path==NULL)
		comu_path=new ComPath(Name);

	fparam = (double)atof(ResTab[TIMEOUT].val);
	SetTimeout(fparam);

//	comu_path->Write("syst:pres");
//	comu_path->Write("stat:pres;*cls");
//	comu_path->Write("*sre 0");
	reset();
  
  if (GetModel()==false)
		return false;
	if (strstr(ResTab[SCANCARD].val,"YES")==NULL)
		ScanCard=false;
	else
		ScanCard=true;
	if (Model==2000)
		strcpy (all_channel,ResTab[ALL_CHANNEL].val);

	
	SetDataFormat(ResTab[DATAFORMAT].val);
 	SetMode(ResTab[UNITMODE].val);
	fparam = (double)atof(ResTab[DELAY].val);
	SetDelay(fparam);
	fparam = (double)atof(ResTab[RANGE].val);
	SetRange(fparam);
	iparam = atoi(ResTab[CHANNEL].val);
	SetChannel(iparam);
	it_adjust=(double)atof(ResTab[IT_ADJUST].val);
	fparam = (double)atof(ResTab[ITIME].val);
	SetIntegrationTime(fparam);
	iparam = atoi(ResTab[AUTOZERO].val);
	SetAutoZero(iparam);
	iparam = atoi(ResTab[DIGITS].val);
	SetDigits(iparam);
	iparam = atoi(ResTab[INTEGRATION].val);
	SetIntegration(iparam);
	SetTrigSource(ResTab[TRIGSRC].val);
	iparam = atoi(ResTab[SAMPLE].val);
	SetSample(iparam);
	iparam = atoi(ResTab[TRIGCOUNT].val);
	SetTriggerCount(iparam);
	SetFormat(ResTab[FORMAT].val);
	SetScanList(ResTab[SCANLIST].val);
	SetScanMode(ResTab[SCANTYPE].val);

	comu_path->Write("SYST:TST:TYPE REL");
	comu_path->Write("TRAC:TST:FORM ABS");
	LeaveIddle();

	return(true);
}


//**********************************************************
// @mfunc 
// - Constructor.
// - Set configuration parameters.
// @parm The name of the keithley.
//**********************************************************
Keithley::Keithley(char *dev_name)
{

	sprintf (Mode,"???");
	Range=-1;
	Digits=-1;
	sprintf (Format,"???");
	Sample =-1;
	Integrate=-1;
	IntegrationTime=-1;
	Delay=-1;
	Channel=-1;
	sprintf (TrigSrc,"???");
	sprintf (ScanList,"???");
	sprintf (ScanMode,"???");
	sprintf (dataformat,"???");
	strcpy (myname,dev_name);
	comu_path=NULL;
	if (GetConfiguration(dev_name))
	{
	}
	else
	{
		delete comu_path;
		comu_path=NULL;
	}
//	SetTrigSource("IMM");
	

}

//---------------------------------------------------------
// @mfunc ScannerGetTemperature(int channel, double *val)
//  Get the temperature from a thermocouple connected to
// the input referenced by channel.
// @parm Channel number.
// @parm an adresse to store read value
// @rdesc  return true if OK or false if NOTOK
//---------------------------------------------------------
int Keithley::ScannerGetTemperature(int channel, double *val)
{
  char cmd[256];
  char resp[256];
  bool  status;

  sprintf(cmd, "rout:clos (@%d)", channel);
  status = comu_path->Write(cmd);
	if(status != true)
		return(false);

  status = comu_path->Write("FUNC 'TEMP'");
	if(status != true)
		return(false);

  status = comu_path->Write(":READ?");
	if(status != true)
		return(false);

  status = comu_path->Read(resp);
  if(status == true)
    sscanf(resp, "%lf", val);

  return(true);
}

//---------------------------------------------------------
// @mfunc 
//  When using the scanner card there is no direct 
//	measurement of the current vailable.
//  Measuring the voltage across the resistance will
//	give, after calculation, the current value through 
//	the resistor.
//  This is only available for scanner inputs #1 and #5.
// @parm	 Channel number.
// @parm	 an adresse to store read value
// @rdesc  return true if OK or false if NOTOK
//---------------------------------------------------------
int Keithley::ScannerGetCurrent(int channel, double *val)
{
  char cmd[256];
  char resp[256];
  bool  status;
  
  if((channel == 1) || (channel == 5))  // Check channel value.
  {
    sprintf(cmd, "rout:clos (@%d)", channel);
    status = comu_path->Write(cmd);
		if(status != true)
			return(false);
		
    status = comu_path->Write("FUNC 'VOLT:DC'");
		if(status != true)
			return(false);
		
    status = comu_path->Write(":READ?");
		if(status != true)
			return(false);
		
    status = comu_path->Read(resp);
    if(status == true)
    {
      sscanf(resp, "%lf", val);       // Get DC voltage.
      *val = *val / (double) 1000.0;  // Resistor is 1 kohm
    }
    return(true);
  }
  else
    return(false);
}

//---------------------------------------------------------
// @mfunc 
// Read voltage on specified channel 
// @parm Channel number.
// @parm an adress to store read value
// @rdesc  return true if OK or false if NOTOK
//---------------------------------------------------------
int Keithley::ScannerGetVoltage(int channel, double *val)
{
  char cmd[256];
  char resp[256];
  bool  status;
  
  sprintf(cmd, "rout:clos (@%d)", channel);
  status = comu_path->Write(cmd);
		if(status != true)
			return(false);

  status = comu_path->Write("FUNC 'VOLT:DC'");
		if(status != true)
			return(false);

  status = comu_path->Write(":READ?");
		if(status != true)
			return(false);

  status = comu_path->Read(resp);
  if(status == true)
    sscanf(resp, "%lf", val);
  return(true);
}

//---------------------------------------------------------
// @mfunc 
// configure the measurement functions.
// @parm  a string that contain the desired mode<nl>	
// setmode could be : <nl>
//		'CURRent:AC'________for current AC reading <nl>
//		'CURRent:DC'________for current DC reading <nl>
//		'VOLT:DC'___________for Volt DC reading <nl>
//		'VOLT:'AC'__________for Volt AC reading <nl>
//		'RES'_______________for 2 wire resistance reading<nl>
//		'FRES'______________for 4 wires resistance reading<nl>
//		'PERIOD'____________for period reading	<nl>
//		'FREQ'______________for Frequency reading<nl>
//		'TEMP'______________for Temperature reading<nl>
//		'DIOD'______________for diode testing<nl>
//		'CONT'______________for continuity testing<nl>			
// @rdesc  return true if OK or false if NOTOK
//---------------------------------------------------------

int Keithley::SetMode(char * setmode)
{

	char cmd[100];
	char ret [30];
	int status;

	strcpy(cmd,":SENS:FUNC \"");
	strcat (cmd,setmode);
	strcat (cmd,"\"");
	
	if((status=comu_path->Write(&cmd[0]))==true)
	{
		sprintf(Mode,"???");
		return (ReadMode(&ret[0]));

	}
	else
		return(false);	
}
//---------------------------------------------------------
// @mfunc 
// Read the measuremnt functions.
// @parm a string adress to store the read mode	<nl>
// readmode could be : <nl>
//		'CURRent:AC'________for current AC reading <nl>
//		'CURRent:DC'________for current DC reading <nl>
//		'VOLT:DC'___________for Volt DC reading <nl>
//		'VOLT:'AC'__________for Volt AC reading <nl>
//		'RES'_______________for 2 wire resistance reading<nl>
//		'FRES'______________for 4 wires resistance reading<nl>
//		'PERIOD'____________for period reading	<nl>
//		'FREQ'______________for Frequency reading<nl>
//		'TEMP'______________for Temperature reading<nl>
//		'DIOD'______________for diode testing<nl>
//		'CONT'______________for continuity testing<nl>		
// @rdesc  return true if OK or false if NOTOK
//---------------------------------------------------------

int Keithley::ReadMode(char * readmode)
{
	char ret [30];
	int status;

	if (strstr(Mode,"???")==NULL)
	{
		strcpy (readmode,Mode);
		return (true);
	}
	else
		if((status=comu_path->Write(":SENS:FUNC?"))==true)
			if(comu_path->Read(&ret[0])==true)
				if (sscanf(&ret[0],"\"%[0-9A-Za-z:.]",Mode)!=1)
					return (false);
				else
				{
					strcpy (readmode,Mode);
					return (true);
				}
			else
				return (false);
		else
			return (false);


}
//---------------------------------------------------------
// @mfunc
// configure the measuremnt range of the current functions mode.
// @parm expected reading in unit of current function mode <nl>
//	range could be :<nl>
//		0 to 3,03_____for ACI and DCI<nl>
//	    0 to 757.5____for ACV<nl>
//      0 to 1010_____for DCV<nl>
//      0 to 101 E6___for RES				
// @rdesc  return true if OK or false if NOTOK
//---------------------------------------------------------

int Keithley::SetRange(double setrange)
{
	char cmd[100];
	char ret[20];
	int status;
	double retrange;

	if (ReadMode(&ret[0])==false)
		return(false);
	if (strstr(ret,"TEMP")==NULL)
	{
		sprintf(cmd,":SENS:%s:RANG %lf",Mode,setrange);
		if((status=comu_path->Write(&cmd[0]))==true)
		{
			Range=-1;
			return (ReadRange(&retrange));
		}
	else
		return(true);
	}
	return true;
}
//---------------------------------------------------------
// @mfunc  read the measuremnt range of the current functions mode.
//	@parm  range  reading in unit of current function mode<nl>
//	range could be :<nl>
//		0 to 3,03_____for ACI and DCI<nl>
//	    0 to 757.5____for ACV<nl>
//      0 to 1010_____for DCV<nl>
//      0 to 101 E6___for RES				
// @rdesc  return true if OK or false if NOTOK
//---------------------------------------------------------

int Keithley::ReadRange(double * readrange)
{
	char cmd[100];
	char ret[20];


	if (Range!=-1)
		*readrange=Range;
	else
	{
		if (ReadMode(&ret[0])==false)
			return(false);		
		sprintf(cmd,":SENS:%s:RANG?",Mode);
		if((comu_path->Write(&cmd[0])==true))
			if(comu_path->Read(&ret[0])==true)
			{
				if (sscanf(&ret[0],"%lf",&Range)!=1)
					return (false);
				else
				{
					*readrange=Range;
					return (true);
				}
			}
			else
				return (false);
		else
			return (false);

	}
	return (true);
}
//---------------------------------------------------------
// @mfunc  configure the measuremnt range of the selected functions mode.
// @parm  a string that represent expected setting for range<nl>
//			"AUTO 0"____manual ranging <nl>
//			"AUTO 1"____auto ranging <nl>
//			"MIN"________lowest range (0) <nl>
//			"MAX"________highest range 	 <nl>	
// @rdesc  return true if OK or false if NOTOK
//---------------------------------------------------------

int Keithley::SetRange(char * setrange)
{
	char cmd[100];
	char ret[20];
	int status;
	double retrange;

	if (ReadMode(&ret[0])==false)
		return(false);
	sprintf(cmd,":SENS:%s:RANG:%s",Mode,setrange);
	if((status=comu_path->Write(&cmd[0]))==true)
	{
		Range=-1;
		return (ReadRange(&retrange));		
	}
	else
		return (false);

}
//---------------------------------------------------------
// @mfunc  select the display resolution for the specified measurement function
// @parm expected digit between	4  and	7 										
// @rdesc  return true if OK or false if NOTOK
//---------------------------------------------------------

int Keithley::SetDigits(int setdigit)
{

	char cmd[100];
	char ret[20];
	int status;

	if (ReadMode(&ret[0])==false)
		return(false);	
	sprintf(cmd,":SENS:%s:DIG %d",Mode,setdigit);
	if((status=comu_path->Write(&cmd[0]))==true)
	{
		Digits=-1;
		return(ReadDigits(&status));
	}
	else
		return(false);		

}
//---------------------------------------------------------
// @mfunc	read the display resolution for the specified measurement function
// @parm	an adress to store  number of digit read 										
// @rdesc  return true if OK or false if NOTOK
//---------------------------------------------------------

int Keithley::ReadDigits(int * readdigits)
{
	char cmd[50];
	char ret[20];

	if (Digits!=-1)
	{
		*readdigits=Digits;
		return (true);
	}
	else
	{
		if (ReadMode(&ret[0])==false)
			return(false);
		sprintf(cmd,":SENS:%s:DIG?",Mode);
		if((comu_path->Write(&cmd[0])==true))
			if(comu_path->Read(&ret[0])==true)
			{
				if (sscanf(&ret[0],"%d",&Digits)!=1)
					return (false);
				else
				{
					*readdigits=Digits;
					return (true);
				}
			}
			else
				return (false);
		else
			return (false);
	}
}
//---------------------------------------------------------
// @mfunc 
// specify the reading format of instrument
// @parm	a string that contain what you need on each reading <nl>
//	UNIT__________to have unit with each reading<nl>
//	CHAN__________to have Channel number with each reading<nl>
//  READ__________to have the read value<nl>
//	<nl>
//	param could contain 1 2 or 3 Setting  separated by comma (ex "UNIT,READ")
//			
// @rdesc  return true if OK or false if NOTOK
//---------------------------------------------------------

int Keithley::SetFormat(char * setformat)
{
	char cmd[100];
	char ret[30];
	int status;

	sprintf(cmd,":FORM:ELEM %s",setformat);
	if((status=comu_path->Write(&cmd[0]))==true)
	{
		sprintf (Format,"???");
		return (ReadFormat(&ret[0]));
	}
	else
		return(false);		
}
//---------------------------------------------------------
// @mfunc 
// read the reading format of instrument
// @parm 	a string adress to store reading format <nl>
//  return value could be :<nl>
//	UNIT__________to have unit with each reading<nl>
//	CHAN__________to have Channel number with each reading<nl>
//  READ__________to have the read value<nl>
//	<nl>
//	return string could contain 1 2 or 3 Setting  separated by comma (ex "UNIT,READ")		
// @rdesc  return true if OK or false if NOTOK
//---------------------------------------------------------
int Keithley::ReadFormat(char * readformat)
{
	char ret[20];
	int status;
	if (strstr(Format,"???")==NULL)
	{
		strcpy (readformat,Format);
		return (true);
	}
	else
		if((status=comu_path->Write(":FORM:ELEM?"))==true)
			if(comu_path->Read(&ret[0])==true)
				if (sscanf(&ret[0],"%[0-9A-Za-z:.,]",&Format)!=1)
					return (false);
				else
				{
					strcpy (readformat,Format);
					return (true);
				}
			else
				return (false);
		else
			return (false);

}
//---------------------------------------------------------
// @mfunc	Set the acquisition number per trig
// @parm	number of aquisition per trig 										
// @rdesc  return true if OK or false if NOTOK
//---------------------------------------------------------

int Keithley::SetSample(int setsample)
{
	char cmd[100];
	int status;
	int readsample;
	sprintf(cmd,":SAMP:COUN %d",setsample);
	if((status=comu_path->Write(&cmd[0]))==true)
	{
		if (setsample >1)
			SetBuffer(setsample);
		Sample=-1;
		return (ReadSample(&readsample));
	}
	else
		return(false);		
}
//---------------------------------------------------------
// @mfunc	read the number of aquisition per trig
// @parm	an adress to store  aqquisition number per trig										
// @rdesc  return true if OK or false if NOTOK
//---------------------------------------------------------

int Keithley::ReadSample(int *readsample)
{
	char ret[20];
	int status;
	if (Sample != -1)
	{
		*readsample=Sample;
		return (true);
	}
	else
		if((status=comu_path->Write(":SAMP:COUN?"))==true)
			if(comu_path->Read(&ret[0])==true)
				if (sscanf(&ret[0],"%d",&Sample)!=1)
					return (false);
				else
				{
					*readsample=Sample;
					return (true);
				}
			else
				return (false);
		else
			return (false);
}

//---------------------------------------------------------
// @mfunc	Set the integration mode 
// @parm	1 or 0 to set or reset integration mode 										
// @rdesc  return true if OK or false if NOTOK
//---------------------------------------------------------


int Keithley::SetIntegration(int integrate)
{
	char cmd[100];
	char ret[20];
	int status;

	if (ReadMode(&ret[0])==false)
		return (false);
	if (strstr(ret,"VOLT")!=NULL)
	{
		switch(Model)
		{
		case 2700:case 2701:
			sprintf(cmd,":CALC:FORM MXB;:CALC:KMAT:MUN 'S';:CALC:STAT %d",integrate);
			break;
		case 2182:
			sprintf(cmd,":CALC:FORM MXB;:CALC:KMAT:MUN 'SC';:CALC:STAT %d",integrate);
			break;
		case 2000:
			sprintf(cmd,":CALC:FORM MXB;:CALC:KMAT:MUN 'VSC';:CALC:STAT %d",integrate);
			break;
		default:
			return false;
		}
	}
	else if (strstr(ret,"CURR")!=NULL)
	{
		switch(Model)
		{
		case 2700:case 2701:
			sprintf(cmd,":CALC:FORM MXB;:CALC:KMAT:MUN 'S';:CALC:STAT %d",integrate);
			break;
		case 2182:
			sprintf(cmd,":CALC:FORM MXB;:CALC:KMAT:MUN 'SC';:CALC:STAT %d",integrate);
			break;		case 2000:
			sprintf(cmd,":CALC:FORM MXB;:CALC:KMAT:MUN 'ISC';:CALC:STAT %d",integrate);
			break;
		default:
			return false;
		}
	}		
	else if (strstr(ret,"RES")!=NULL)
	{
		switch(Model)
		{
		case 2700:case 2701:
			sprintf(cmd,":CALC:FORM MXB;:CALC:KMAT:MUN 'S';:CALC:STAT %d",integrate);
			break;
		case 2182:
			sprintf(cmd,":CALC:FORM MXB;:CALC:KMAT:MUN 'SC';:CALC:STAT %d",integrate);
			break;
		case 2000:
			sprintf(cmd,":CALC:FORM MXB;:CALC:KMAT:MUN 'RSC';:CALC:STAT %d",integrate);
			break;
		default:
			return false;
		}
	}		
	else
		sprintf(cmd,":CALC:FORM MXB;:CALC:STAT %d",integrate);
	

	if((status=comu_path->Write(&cmd[0]))==true)
	{
		Integrate=-1;
		return (ReadIntegration(&status));
	}
	else
		return(false);		
}
//---------------------------------------------------------
// @mfunc	read the intgration mode
// @parm	the integration mode<nl>
//			1 if integration ON <nl>
//			0 if integration OFF 										
// @rdesc  return true if OK or false if NOTOK
//---------------------------------------------------------
int Keithley::ReadIntegration(int * integrate)
{
	char cmd[100];
	char ret[20];
	if (Integrate != -1)
	{
		*integrate=Integrate;
		return (true);
	}
	else
	{
		sprintf(cmd,":CALC:STAT?");
		if((comu_path->Write(&cmd[0])==true))
			if(comu_path->Read(&ret[0])==true)
			{
				if (sscanf(&ret[0],"%d",&Integrate)!=1)
					return (false);
				else
				{
					*integrate=Integrate;
					return (true);
				}
			}
			else
				return (false);
		else
			return (false);
	}

}
//---------------------------------------------------------
// @mfunc	Trig the multimeter 
// 										
// @rdesc  return true if OK or false if NOTOK
//---------------------------------------------------------

int Keithley::Trig()
{

//		if((status=comu_path->Write(":TRAC:CLE;:INIT;*TRG")==0))
		if((comu_path->Write("*TRG")==true))
			return (true);
		else 
			return(false);
}
//---------------------------------------------------------
// @mfunc	Timeout setting for Keithley
// @parm	timeout in second
// @rdesc  return true if OK or false if NOTOK
//---------------------------------------------------------

int Keithley::SetTimeout(double t)
{
	
	if (comu_path->SetTimeout(t)==true)
	{
		Timeout=t;
		return true;
	}
	else
	{
		return (false);
	}
}
//---------------------------------------------------------
// @mfunc	Integration time setting
// @parm	integration time in second
// @rdesc  return true if OK or false if NOTOK
//---------------------------------------------------------
int Keithley::SetIntegrationTime(double itime)
{
	double plcval,minval,tmptime,nbplc;
	int i,minplc;
	double frac[11],diff[11],nbcount[101];
	char cmd[50];
	double mytime;

	tmptime=itime;
//	tmptime=itime+0.0001;
	if(strstr(Mode,"VOLT:DC")!=NULL)
	{
		plcval=0.02;
	}
	else
	{
		plcval=0.2;
	}

	switch (Model)
	{
	case 2700:case 2701:
		{
				double aper;
				int cnt;
				cnt=(int)ceil(itime);
				aper=itime/cnt;
				sprintf (cmd,":SENS:%s:AVER:STAT 1",Mode);
				if(comu_path->Write(cmd)!=true)
					return (false);
				sprintf (cmd,":SENS:%s:AVER:TCON REP",Mode);
				if(comu_path->Write(cmd)!=true)
					return (false);
				sprintf (cmd,":SENS:%s:AVER:COUN %i",Mode,cnt);
				if(comu_path->Write(cmd)!=true)
					return (false);
				sprintf (cmd,":SENS:%s:APER %.5f",Mode,aper);
				if(comu_path->Write(cmd)!=true)
					return (false);


			break;
		}
	case 2182:
		{
			{
				double aper;
				int cnt;
				cnt=(int)ceil(itime);
				aper=itime/cnt;
				int stf=1;
				sprintf (cmd,":SENS:%s:DFIL:WIND 0",Mode);
				if(comu_path->Write(cmd)!=true)
					return (false);
				if (cnt==1)
					stf=0;
				sprintf (cmd,":SENS:%s:DFIL:STAT %i",Mode,stf);
				if(comu_path->Write(cmd)!=true)
					return (false);
				sprintf (cmd,":SENS:%s:DFIL:TCON REP",Mode);
				if(comu_path->Write(cmd)!=true)
					return (false);
				sprintf (cmd,":SENS:%s:DFIL:COUN %i",Mode,cnt);
				if(comu_path->Write(cmd)!=true)
					return (false);
				sprintf (cmd,":SENS:%s:APER %.5f",Mode,aper);
				if(comu_path->Write(cmd)!=true)
					return (false);
		
				
			}

			break;
		}

	case 2000:
		{
		tmptime=plcval*ceil(itime/plcval);
		if(tmptime>=plcval)
			{
			nbplc=tmptime/plcval;
			i=11;
			minval=100;
			minplc=10;
			do
			{
				i--;
				frac[i]=modf(nbplc/i,&nbcount[i]);
				diff[i]=nbplc-(nbcount[i]*i);
				if (diff[i]<minval)
				{
					if (nbcount[i]<100)
					{
					minval=diff[i];
					minplc=i;
					}
				}
			

			}while((diff[i]!=0)&&(i>1));
			tmptime=minplc * nbcount[(int)minplc] * plcval;

			{
				sprintf (cmd,":SENS:%s:AVER:STAT ON",Mode);
				if(comu_path->Write(cmd)!=true)
					return (false); 
				sprintf (cmd,":SENS:%s:AVER:TCON REP",Mode);
				if(comu_path->Write(cmd)!=true)
					return (false);
				if(Model!=2000)
				{
					sprintf (cmd,":SENS:%s:AVER:WIND 0",Mode);
					if(comu_path->Write(cmd)!=true)
						return (false); 
				}
				sprintf (cmd,":SENS:%s:AVER:COUN %i",Mode,(int)nbcount[minplc]);
				if(comu_path->Write(cmd)!=true)
					return (false);
			}
			sprintf (cmd,":SENS:%s:NPLC %i",Mode,minplc);
			if(comu_path->Write(cmd)!=true)
			return (false);
			sprintf (cmd,":TRIG:DELAY 0;:SYST:AZER:STAT 0");
			if(comu_path->Write(cmd)!=true)
				return (false);
		}
		else
		{
			minval=itime/plcval;
			sprintf (cmd,":SENS:%s:AVER:STAT OFF",Mode);
			if(comu_path->Write(cmd)!=true)
				return (false); 
			sprintf (cmd,":SENS:%s:NPLC %.2f",Mode,minval);
			if(comu_path->Write(cmd)!=true)
			return (false);
		}
		break;
		}
	}

	sprintf (cmd,":TRIG:DELAY 0;:SYST:AZER:STAT 0");
	if(comu_path->Write(cmd)!=true)
		return (false);

	IntegrationTime=-1;
	ReadIntegrationTime(&mytime);

	sprintf (cmd,":CALC:KMAT:MMF %.5f",mytime);
	if(comu_path->Write(cmd)!=true)
		return (false);

	
return (true);
}
//---------------------------------------------------------
// @mfunc	Integration time reading
// @parm	an adress to store integration time in second
// @rdesc  return true if OK or false if NOTOK
//---------------------------------------------------------
int Keithley::ReadIntegrationTime(double *itime)
{
char cmd[50];
char ret[20];
int  aver ,count,mult;
double nplc;

	if (IntegrationTime!=-1)
		*itime=IntegrationTime;
	else
	{
	switch (Model)
	{
	case 2700:case 2701:
		{
			sprintf (cmd,":SENS:%s:APER?",Mode);
			if(comu_path->Write(cmd)!=true)
				return (false);
			if(comu_path->Read(&ret[0])!=true)
				return (false);
			if (sscanf(&ret[0],"%lf",&IntegrationTime)!=1)
				return (false);
			sprintf (cmd,":SENS:%s:AVER:COUN?",Mode);
			if(comu_path->Write(cmd)!=true)
				return (false);
			if(comu_path->Read(&ret[0])!=true)
				return (false);
			if (sscanf(&ret[0],"%i",&count)!=1)
				return (false);
			IntegrationTime*=count;
			IntegrationTime+=it_adjust;
			*itime=IntegrationTime;
			break;
		}
	case 2182:
		{
			sprintf (cmd,":SENS:%s:APER?",Mode);
			if(comu_path->Write(cmd)!=true)
				return (false);
			if(comu_path->Read(&ret[0])!=true)
				return (false);
			if (sscanf(&ret[0],"%lf",&IntegrationTime)!=1)
				return (false);
			sprintf (cmd,":SENS:%s:DFIL:COUN?",Mode);
			if(comu_path->Write(cmd)!=true)
				return (false);
			if(comu_path->Read(&ret[0])!=true)
				return (false);
			if (sscanf(&ret[0],"%i",&count)!=1)
				return (false);
			IntegrationTime*=count;
			IntegrationTime+=it_adjust;
			*itime=IntegrationTime;
			break;
		}
	case 2000:
		{
		sprintf(cmd,":SENS:%s:AVER:STAT?",Mode);
		if(comu_path->Write(cmd)!=true)
			return (false);
		if(comu_path->Read(&ret[0])!=true)
			return (false);
		if (sscanf(&ret[0],"%d",&aver)!=1)
			return (false);
		if (aver==1)
		{
			sprintf(cmd,":SENS:%s:AVER:COUN?",Mode);
			if(comu_path->Write(cmd)!=true)
				return (false);
			if(comu_path->Read(&ret[0])!=true)
				return (false);
			if (sscanf(&ret[0],"%d",&count)!=1)
				return (false);
			mult=count;
		}
		else
			mult=1;
		
		sprintf(cmd,":SENS:%s:NPLC?",Mode);
		if(comu_path->Write(cmd)!=true)
			return (false);
		if(comu_path->Read(&ret[0])!=true)
			return (false);
		if (sscanf(&ret[0],"%lf",&nplc)!=1)
			return (false);
		if(strstr(Mode,"VOLT:AC")!=NULL)
			nplc*=(double).2;
		else
			nplc*=(double).02;
		
		IntegrationTime=nplc*mult;
		IntegrationTime+=it_adjust;
		*itime=IntegrationTime;
		}
		break;
	}
	}
	return (true);
}
//---------------------------------------------------------
// @mfunc	 trig delay  time setting
// @parm	Delay in  second
// @rdesc  return true if OK or false if NOTOK
//---------------------------------------------------------
int Keithley::SetDelay(double delay)
{
	char cmd[100];
	int status;
	double retval;

	sprintf(cmd,":TRIG:DEL %lf",delay);
	if((status=comu_path->Write(&cmd[0]))==true)
	{
		Delay=-1;
		return(ReadDelay(&retval));

	}
	else
		return (false);	

}
//---------------------------------------------------------
// @mfunc	trig delay time reading
// @parm	an adress to store trig delay time in second
// @rdesc  return true if OK or false if NOTOK
//---------------------------------------------------------
int Keithley::ReadDelay(double * delay)
{
	char cmd[50];
	char ret[20];


	if (Delay!=-1)
	{
		*delay=Delay;
		return (true);
	}
	else
	{
		if (ReadMode(&ret[0])==false)
			return (false);
		sprintf(cmd,":TRIG:DEL?");
		if((comu_path->Write(&cmd[0])==true))
			if(comu_path->Read(&ret[0])==true)
			{
				if (sscanf(&ret[0],"%lf",&Delay)!=1)
					return (false);
				else
				{
					*delay=Delay;
					return (true);
				}
			}
			else
				return (false);
		else
			return (false);
	}
}
//---------------------------------------------------------
// @mfunc	 Set or Reset Auto Zero mode (zero recalibration  beetween each reading)
// @parm	Auto zero 0 for reset 1 for Set
// @rdesc  return true if OK or false if NOTOK
//---------------------------------------------------------
int Keithley::SetAutoZero(int autozero)
{
	char cmd[100];
	int status;

	if (Model==2182)
	{	
		sprintf(cmd,":SYST:FAZ:STAT 0");
		status=comu_path->Write(&cmd[0]);
		sprintf(cmd,":OUTP:STAT OFF");
		status=comu_path->Write(&cmd[0]);
	}
	sprintf(cmd,":SYST:AZER:STAT %d",autozero);
	if((status=comu_path->Write(&cmd[0]))==true)
	{
		AutoZero=-1;
		return(ReadAutoZero(&AutoZero));

	}
	else
		return (false);	
	
}
//---------------------------------------------------------
// @mfunc	 Read  Auto Zero mode 
// @parm	adress to store Auto zero mode  (0 for reset 1 for Set)
// @rdesc  return true if OK or false if NOTOK
//---------------------------------------------------------
int Keithley::ReadAutoZero(int * autozero)
{
	char cmd[50];
	char ret[20];


	if (AutoZero!=-1)
	{
		*autozero=AutoZero;
		return (true);
	}
	else
	{
		sprintf(cmd,":SYST:AZER:STAT?");
		if((comu_path->Write(&cmd[0])==true))
			if(comu_path->Read(&ret[0])==true)
			{
				if (sscanf(&ret[0],"%d",&AutoZero)!=1)
					return (false);
				else
				{
					*autozero=AutoZero;
					return (true);
				}
			}
			else
				return (false);
		else
			return (false);
	}
}

//---------------------------------------------------------
// @mfunc	 Test if keithley have a mesure running 
// 
// @rdesc	return true if mesure running <nl>
//			return false if KEITHLEY READY
//---------------------------------------------------------
bool Keithley::IsReading(void)
{;
	char ret[30];

	if (comu_path->Write("STAT:OPER:COND?")==true)
	{
		if(comu_path->Read(&ret[0])==true)
		{
			if ((atoi(ret)&(1024+32))!=0)
				return false;
			else
				return true;
		}
		else
			return true;
	}
	else
		return true;

}
//---------------------------------------------------------
// @mfunc	 Get data read by keithley 
// @parm	adress to pass deired read number and to store nb of read
// @parm	adress to store reading	
// @parm	ardees to store reading channels	
// @rdesc	return -1 if mesure running <nl>
//			return 0 if can return double table
//---------------------------------------------------------

int Keithley::ReadData(int * len, double *data,int *channel)
{

	char ret[30000];
	double st_time;
	int nbvar;
	char *ret_pt;
	double *datapt;
	int i,*chpt,chread;
	char cmd[50];

	if (IsReading()&&((strcmp(TrigSrc,"EXT")!=0)!=true))
		return (false);

	if (Sample>=1)
	{
		if (Sample==1 )
		{
			if (strcmp(TrigSrc,"EXT")==0)
				sprintf (cmd,"SENS:DATA:FRESH?");
			else
				sprintf (cmd,":FETCH?");
		}
		else
			sprintf (cmd,":DATA:DATA?");
		if (comu_path->Write(cmd)==true)
		{
			st_time=Timeout;
			SetTimeout(10);
			if(comu_path->Read(&ret[0],30000)==true)
			{
				SetTimeout(st_time);
				nbvar=0;
				if (strstr(Format,"READ")!=NULL)
					nbvar+=1;
				else
					return (false);
				if (strstr(Format,"UNIT")!=NULL)
					nbvar+=1;
				if (strstr(Format,"CHAN")!=NULL)
					nbvar+=2;
	// only reading or reding unit  present
				if ((nbvar==1)||(nbvar==2))
				{
					ret_pt=&ret[0];
					ret_pt--;
					i=0;
					datapt=data;
					chpt=channel;
					chread=Channel;
					do
					{
						ret_pt++;
						sscanf(ret_pt,"%lf",datapt);
						*chpt=chread;
						datapt++;
						chpt++;
						i++;
					}while (((ret_pt=strstr(ret_pt,","))!=NULL)&&(i<*len));
					*len=i;
				}
	// reading  and channel present on return
				if ((nbvar==3)|| (nbvar==4))
				{
					ret_pt=&ret[0];
					ret_pt--;
					i=0;
					datapt=data;
					chpt=channel;
					do
					{
						ret_pt++;
						sscanf(ret_pt,"%lf",datapt);
						datapt++; 						
						ret_pt=strstr(ret_pt,",");
						ret_pt++;
						sscanf(ret_pt,"%d",&chread);
						*chpt=chread;
						chpt++;
						i++;
					}while (((ret_pt=strstr(ret_pt,","))!=NULL)&&(i<*len));
					*len=i;
			
				}
				return (true);
			}
			SetTimeout(st_time);
			return (false);
		}
		return (false);
	}
	else
	{
		return false;
	}
	
}

//---------------------------------------------------------
// @mfunc	Reading channel setting  
// @parm	channel number to close
// @rdesc	return false NOT OK <nl>
//			return true NOT OK 
//---------------------------------------------------------

int Keithley::SetChannel(int channel)
{
	char cmd[100];
	int status;
	int retval;

	if(!ScanCard)
	{
		return (true);
	}
switch (Model)
{
	case 2000:case 2700:case 2701:
		if (channel==0)
			sprintf(cmd,":ROUT:MULT:OPEN (@%s)",all_channel);
		else
			sprintf(cmd,":ROUT:CLOS  (@%i)",channel);
		if((status=comu_path->Write(&cmd[0]))==true)
		{
			Channel=-1;
			return(ReadChannel(&retval));
		}
		else
			return (false);	
		break;
	case 2182:
		sprintf(cmd,":SENS:CHAN %i",channel);
		if((status=comu_path->Write(&cmd[0]))==true)
		{
			Channel=-1;
			return(ReadChannel(&retval));
		}
		else
			return (false);	
		
		break;
	default:
			return false;
			break;
}
}
//---------------------------------------------------------
// @mfunc	Read the selected channel (close one)  
// @parm	an adress to store channel number
// @rdesc	return false NOT OK <nl>
//			return true NOT OK 
//---------------------------------------------------------

int Keithley::ReadChannel(int * channel)
{
	char cmd[50];
	char ret[20];

	if(!ScanCard)
	{
		*channel=0;
		return (true);
	}


	if (Channel!=-1)
	{
		*channel=Channel;
		return (true);
	}
	else
	{
		switch(Model)
		{
		case 2182:
			sprintf(cmd,":SENS:CHAN?");
			break;
		case 2000:
			sprintf(cmd,":ROUT:CLOS:STAT?");
			break;
		case 2700:case 2701:
			sprintf(cmd,":ROUT:CLOS?");
			break;
		default:
			return (false);
		}
		if((comu_path->Write(&cmd[0])==true))
			if(comu_path->Read(&ret[0])==true)
			{
				switch (Model)
				{
				case 2000:case 2700:case 2701:
					if (sscanf(&ret[0],"(@%i)",&Channel)!=1)
						return (false);
					else
					{
						*channel=Channel;
						return (true);
					}
					break;
				case 2182:
					if (sscanf(&ret[0],"%i",&Channel)!=1)
						return (false);
					else
					{
						*channel=Channel;
						return (true);
					}
					break;
				default:
					return (false);
				}

			}
			else
				return (false);
		else
			return (false);
	}
}

//---------------------------------------------------------
// @mfunc	trigger source  setting  
// @parm	trig source description <nl>
//			IMM for immediat<nl>
//			TIM	for Timer <nl>
//			MAN	for MAnual<nl>
//			BUS	for soft trig<nl>
//			EXT for external trig
// @rdesc	return false NOT OK <nl>
//			return true NOT OK 
//---------------------------------------------------------

int Keithley::SetTrigSource(char * src)
{
	char cmd[100];
	int status;
	char retval[20];

	sprintf(cmd,":TRIG:SOUR %s",src);
	if((status=comu_path->Write(&cmd[0]))==true)
	{
		sprintf (TrigSrc,"???");
		return(ReadTrigSource(&retval[0]));
	}
	else
		return (false);		
}
//---------------------------------------------------------
// @mfunc	trigger source  reading  
// @parm	an adress to store trig source description <nl>
//			IMM for immediat<nl>
//			TIM	for Timer <nl>
//			MAN	for MAnual<nl>
//			BUS	for soft trig<nl>
//			EXT for external trig
// @rdesc	return false NOT OK <nl>
//			return true NOT OK 
//---------------------------------------------------------
int Keithley::ReadTrigSource(char * src)
{
	char ret[20];
	int status;
	if (strstr(TrigSrc,"???")==NULL)
	{
		strcpy (src,TrigSrc);
		return (true);
	}
	else
		if((status=comu_path->Write(":TRIG:SOUR?"))==true)
			if(comu_path->Read(&ret[0])==true)
				if (sscanf(&ret[0],"%[0-9A-Za-z:.,]",&TrigSrc)!=1)
					return (false);
				else
				{
					strcpy (src,TrigSrc);
					return (true);
				}
			else
				return (false);
		else
			return (false);
}

//---------------------------------------------------------
// @mfunc	scan channel list setting  
// @parm	a channel list on the followings formats<nl>
//			each channel separated with comma <nl>
//			example : 1,3,5,2 <nl>
//			a range of channel first and last separated with :<nl>
//			example 1:5 <nl>
//			
// @rdesc   return false NOT OK <nl>
//			return true OK
//---------------------------------------------------------
int Keithley::SetScanList(char * list)
{
	char cmd[100];
	int status;
	
	if (!ScanCard)
	{
		strcpy(list,"0");
		return (true);
	}

	sprintf(cmd,":ROUT:SCAN (@%s)",list);
	if((status=comu_path->Write(&cmd[0]))==true)
	{
		sprintf (ScanList,"???");
		return(ReadScanList(&ScanList[0]));
	}
	else
		return (false);			
}
//---------------------------------------------------------
// @mfunc	scan channel list reading  
// @parm	an adress tos store a channel list on the followings formats<nl>
//			each channel separated with comma <nl>
//			example : 1,3,5,2 <nl>
//			a range of channel first and last separated with :<nl>
//			example 1:5 <nl>
//			
// @rdesc	return false NOT OK <nl>
//			return true NOT OK 
//---------------------------------------------------------
int Keithley::ReadScanList(char * list)
{
	char ret[20];
	int status;

	if (!ScanCard)
	{
		strcpy(list,"0");
		return (true);
	}
	if (strstr(ScanList,"???")==NULL)
	{
		strcpy (list,ScanList);
		return (true);
	}
	else
		if((status=comu_path->Write(":ROUT:SCAN?"))==true)
			if(comu_path->Read(&ret[0])==true)
				if (sscanf(&ret[0],"(@%[0-9A-Za-z:.,]",&ScanList)!=1)
					return (false);
				else
				{
					strcpy (list,ScanList);
					return (true);
				}
			else
				return (false);
		else
			return (false);
}
//---------------------------------------------------------
// @mfunc	SCAN MODE  setting  
// @parm	a string that describe the scan MODE<nl>
//			INT		Scan on internal scanne <nl>
//			NONE	NO SCAN ON TRIG<nl>
// @rdesc	return false NOT OK <nl>
//			return true NOT OK 
//---------------------------------------------------------
int Keithley::SetScanMode(char * mode)
{
	char cmd[100];
	int status;
	char retval[20];
	
	if(!ScanCard)
	{
		return (true);
	}

	sprintf(cmd,":ROUT:SCAN:LSEL %s",mode);
	if((status=comu_path->Write(&cmd[0]))==true)
	{
		sprintf (ScanMode,"???");
		if(ReadScanMode(&retval[0]))
		{
			return ( strcmp(mode,retval)==0);
		}
		else
		{
			return false;
		}
	}
	else
		return (false);		
}
//---------------------------------------------------------
// @mfunc	SCAN MODE   reading  
// @parm	an adress to store SCAN MODE description <nl>
//			INT		if scan will run on trig on internal scanner<nl>
//			NONE	no channel scan on trig <nl>
// @rdesc	return false NOT OK <nl>
//			return true NOT OK 
//---------------------------------------------------------
int Keithley::ReadScanMode(char * mode)
{
	char ret[20];
	int status;
	if(!ScanCard)
	{
		strcpy(mode,"NONE");
		return (true);
	}

	if (strstr(ScanMode,"???")==NULL)
	{
		strcpy (mode,ScanMode);
		return (true);
	}
	else
		if((status=comu_path->Write(":ROUT:SCAN:LSEL?"))==true)
			if(comu_path->Read(&ret[0])==true)
				if (sscanf(&ret[0],"%[0-9A-Za-z:.,]",&ScanMode)!=1)
					return (false);
				else
				{
					strcpy (mode,ScanMode);
					return (true);
				}
			else
				return (false);
		else
			return (false);
}






int Keithley::GetModel()
{
	char ret[100];
	int status;
	char trash[50];

	Model=0;
	if((status=comu_path->Write("*IDN?"))==true)
		if(comu_path->Read(&ret[0])==true)
			if (sscanf(&ret[0],"%[A-Za-z:., ] %i",&trash[0],&Model)!=2)
				return (false);
			else
			{
				return (Model);
			}
		else
			return (false);
	else
		return (false);

}

int Keithley::SetDataFormat(char * form)
{
	char cmd[100];
	int status;
	char retval[20];


	sprintf(cmd,":FORM:DATA %s",form);
	if((status=comu_path->Write(&cmd[0]))==true)
	{
		sprintf(cmd,":FORM:BORD SWAP");
		if((status=comu_path->Write(&cmd[0]))==true)
		{
			sprintf (dataformat,"???");
			return(ReadDataFormat(&retval[0]));
		}	
		sprintf (dataformat,"???");
		return(ReadDataFormat(&retval[0]));
	}
	else
		return (false);	
}

int Keithley::ReadDataFormat(char * form)
{
	char ret[20];
	int status;
	if (strstr(dataformat,"???")==NULL)
	{
		strcpy (form,dataformat);
		return (true);
	}
	else
		if((status=comu_path->Write(":FORM:DATA?"))==true)
			if(comu_path->Read(&ret[0])==true)
				if (sscanf(&ret[0],"%[0-9A-Za-z:.,]",&dataformat)!=1)
					return (false);
				else
				{
					strcpy (form,dataformat);
					return (true);
				}
			else
				return (false);
		else
			return (false);
}

double Keithley::QuickRead()
{
double retval;
char ret[105];
int status;
char cmd [30];
double *pt=(double *)&(ret[2]);
float *fl=(float *)&(ret[2]);
retval=9.9e37;
//if (IsReading()&&(strcmp(TrigSrc,"EXT")!=0))
//		return retval;
sprintf (cmd,":FETCH?");
if((status=comu_path->Write(cmd))!=true)
{
	return retval;
}

switch(dataformat[0])
{
case 'A':
	if(comu_path->Read(&ret[0],100)==true)
		sscanf(&ret[0],"%lf",&retval);
	break;
case 'S':
	if (comu_path->Read(&ret[0],20)!=true)
		retval=9.9e37;
	else
		retval=(double)*fl;
	break;
	break;
case 'D':
	if (comu_path->Read(&ret[0],20)!=true)
		retval=9.9e37;
	retval=*pt;
	break;
default:
	break;

}
		
return retval;	
}

int Keithley::SetTriggerCount(int count)
{
	char cmd[100];
	int status;


	sprintf(cmd,":TRIGGER:COUNT %d",count);

	if((status=comu_path->Write(&cmd[0]))==true)
	{
		TriggerCount=-1;
		if(ReadtriggerCount(&TriggerCount))
		{
/*			if (comu_path->Write(":INIT")==0)
				return (true);
			else
				return false;
				*/
			return true;
		}
		else
			return false;

	}
	else
		return (false);	
}

int Keithley::ReadtriggerCount(int * count)
{
	char cmd[50];
	char ret[20];

	if (*count!=-1)
	{
		*count=TriggerCount;
		return (true);
	}
	else
	{
		if (ReadMode(&ret[0])==false)
			return(false);
		sprintf(cmd,":TRIGGER:COUNT?",Mode);
		if((comu_path->Write(&cmd[0])==true))
			if(comu_path->Read(&ret[0])==true)
			{
				if (sscanf(&ret[0],"%d",&TriggerCount)!=1)
					return (false);
				else
				{
					*count=TriggerCount;
					return (true);
				}
			}
			else
				return (false);
		else
			return (false);
	}
}

int Keithley::LoadDefaults()
{
	return GetConfiguration(myname);
}

int Keithley::SetBuffer(int nb_pt)
{
	char cmd[100];
	int integ;
if (Model==2000 || Model==2182)
{
	if(comu_path->Write(":TRAC:CLE")==false)
			return false;
	if (ReadIntegration(&integ)==false)
			return false;
	else
	{
		sprintf(cmd,":TRAC:POIN %d",nb_pt);
		if(comu_path->Write(cmd)==false)
			return false;	

		if (integ==1)
		{
			if(comu_path->Write(":TRAC:FEED CALC")==false)
				return false;
		}
		else
		{
			if(comu_path->Write(":TRAC:FEED SENS1")==false)
				return false;

		}
		if(comu_path->Write(":TRAC:FEED:CONT NEXT")==false)
			return false;
	}

}
else
{
	if(comu_path->Write(":TRAC:CLE:AUTO ON")==false)
			return false;

	sprintf(cmd,":TRAC:POIN %d",nb_pt);
	if(comu_path->Write(cmd)==false)
			return false;	
	if(comu_path->Write(":TRAC:FEED CALC")==false)
			return false;
	if(comu_path->Write(":TRAC:FEED:CONT NEXT")==false)
			return false;
}
	return true;	

}



int Keithley::GetBufferSize()
{
	char ret[20];
	int toread;
	if(comu_path->Write(":TRAC:POIN?")==false)
		return 0;
	if(comu_path->Read(&ret[0])==true)	
		{
			if (sscanf(&ret[0],"%d",&toread)!=1)
					return (0);
				return (toread);
		}
		else
		{
			return 0;
		}


	/*
else
{
	if(comu_path->Write(":TRAC:NEXT?")==false)
			return 0;

	if(comu_path->Read(&ret[0])==true)
	{
		if (sscanf(&ret[0],"%d",&count)!=1)
			return (0);
		else
			{
			if (count==0)
			{
			if(comu_path->Write(":TRAC:POIN?")==false)
				return 0;
				if(comu_path->Read(&ret[0])==true)	
				{
					if (sscanf(&ret[0],"%d",&toread)!=1)
						return (0);
					return (toread);
				}
				else
				{
					return 0;
				}
				
			}
			else
				return count;
				
			}
	}
	else return 0;
}
*/
	
}

int Keithley::GetBufferReadNumber()
{
	char ret[20];
	int count, toread;
	if ((Model == 2000) || (Model == 2182))
	{
		if (comu_path->Write(":TRAC:POIN?") == false)
			return 0;
		if (comu_path->Read(&ret[0]) == true)
		{
			if (sscanf(&ret[0], "%d", &toread) != 1)
				return (0);
			return (toread);
		}
		else
		{
			return 0;
		}

	}
	else
	{
		if (comu_path->Write(":TRAC:NEXT?") == false)
			return 0;

		if (comu_path->Read(&ret[0]) == true)
		{
			if (sscanf(&ret[0], "%d", &count) != 1)
				return (0);
			else
			{
				if (count == 0)
				{
					if (comu_path->Write(":TRAC:POIN?") == false)
						return 0;
					if (comu_path->Read(&ret[0]) == true)
					{
						if (sscanf(&ret[0], "%d", &toread) != 1)
							return (0);
						return (toread);
					}
					else
					{
						return 0;
					}

				}
				else
					return count;

			}
		}
		else return 0;
	}

}

int Keithley::GetBufferData(double *data,int len)
{
double retval;
//float flval;
char ret[500000];
int status,i;
char cmd [30];
double *pt,*dbpt;
char *pt2;
float *flpt;

retval=9.9e37;
//if (IsReading()&&(strcmp(TrigSrc,"EXT")!=0))
//		return retval;

len=min(GetReadBufferPoint(),len);

sprintf (cmd,"FORM:ELEM READ");
if((status=comu_path->Write(cmd))!=true)
	return (false);
sprintf (cmd,":TRAC:DATA?");
if((status=comu_path->Write(cmd))!=true)
	return (false);
Sleep(len);

switch(dataformat[0])
{
case 'A':
		if(comu_path->Read(&ret[0])!=true)
		{
			return false;
		}
		pt2=&ret[0];
		pt=data;
		for(i=0;i<len;i++)
		{
			sscanf(pt2,"%lf",pt);
			pt2=strstr(pt2,",")+1;
			pt++;
		}
	break;
case 'S':
		if(comu_path->Read(&ret[0],499990)!=true)
		{
			return false;
		}
		flpt=(float *)&ret[2];
		pt=data;
		for(i=0;i<len;i++)
		{
			*pt=(double )*flpt;
			flpt++;
			pt++;
		}
	break;
case 'D':
		if(comu_path->Read(&ret[0],499990)!=true)
		{
			return false;
		}
		dbpt=(double *)&ret[2];
		pt=data;
		for(i=0;i<len;i++)
		{
			*pt=*dbpt;
			dbpt++;
			pt++;
		}

	break;
default:	
	break;
}
return (true);
}

int Keithley::GetBufferTime(double *data,int len)
{

double retval;
//float flval;
char ret[500000];
int status,i;
char cmd [30];
double *pt,*dbpt;
char *pt2;
float *flpt;

retval=9.9e37;
//if (IsReading()&&(strcmp(TrigSrc,"EXT")!=0))
//		return retval;

len=min(GetReadBufferPoint(),len);

sprintf (cmd,"FORM:ELEM TST");
if((status=comu_path->Write(cmd))!=true)
	return (false);
sprintf (cmd,":TRAC:DATA?");
if((status=comu_path->Write(cmd))!=true)
	return (false);
Sleep(len);

switch(dataformat[0])
{
case 'A':
		if(comu_path->Read(&ret[0])!=true)
		{
			return false;
		}
		pt2=&ret[0];
		pt=data;
		for(i=0;i<len;i++)
		{
			sscanf(pt2,"%lf",pt);
			pt2=strstr(pt2,",")+1;
			pt++;
		}
	break;
case 'S':
		if(comu_path->Read(&ret[0],499990)!=true)
		{
			return false;
		}
		flpt=(float *)&ret[2];
		pt=data;
		for(i=0;i<len;i++)
		{
			*pt=(double )*flpt;
			flpt++;
			pt++;
		}
	break;
case 'D':
		if(comu_path->Read(&ret[0],499990)!=true)
		{
			return false;
		}
		dbpt=(double *)&ret[2];
		pt=data;
		for(i=0;i<len;i++)
		{
			*pt=*dbpt;
			dbpt++;
			pt++;
		}

	break;
default:	
	break;
}
return (true);
}


int Keithley::GetSizeNeedForBuffer()
{
	int tot, count;
	char ret[20];
	if (comu_path->Write(":TRAC:FREE?") == false)
		return 0;
	if (comu_path->Read(&ret[0]) == true)
	{
		if (sscanf(&ret[0], "%d,%d", &tot, &count) != 2)
			return (0);
		else
			return count;
	}
	else
		return 0;

}

int Keithley::GetReadBufferPoint()
{
	long tot,count;
    char ret[20];
	if(comu_path->Write(":TRAC:FREE?")==false)
			return 0;
	if(comu_path->Read(&ret[0])==true)
	{
		if (sscanf(&ret[0],"%d,%d",&tot,&count)!=2)
			return (0);
		else
		{
			if (Model==2000 || Model==2182)
				return (int)(1024*count/(tot+count));
			else if (Model==2700 || Model==2701)
			{
				double rap=(double)count/((double)tot+(double)count);
				int retval=(int)(55000*rap);
				return retval;
			}
			else
				return (0);
		}
	}
	else
		return 0;
		
}

int Keithley::EnterIddle()
{
	if(comu_path->Write(":INIT:CONT OFF")==false)
			return false;
	Sleep(100);
	if(comu_path->Write(":ABORT")==false)
			return false;

	return true;
}


int Keithley::LeaveIddle()
{
	if(comu_path->Write(":INIT")==false)
			return false;
	return true;
}

int Keithley::reset()
{
	if(comu_path->Write(":*RST")==false)
			return false;
	return true;
	
}

int Keithley::SetDisplay(bool disp)
{
	if (disp)
	{	
		if(comu_path->Write(":DISP:ENAB ON")==false)
			return false;

	}
	else
	{
		if(comu_path->Write(":DISP:ENAB OFF")==false)
			return false;
	}
	return true;
}

int Keithley::FreeRun()
{
	EnterIddle();
	SetSample(1);
	if(comu_path->Write(":INIT:CONT ON")==false)
			return false;
	return true;

}

int Keithley::SetLowPass(int filt)
{
	char cmd [30];
	switch (Model)
	{
		case 2182:
		{
			sprintf (cmd,":SENS:%s:LPASS %d",Mode,filt);
			if(comu_path->Write(cmd)!=true)
				return (false);
				break;
		}
		default :
			return true;
	}
	return true;
}
