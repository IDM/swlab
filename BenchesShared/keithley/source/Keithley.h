//*********************************************************

// Keithley.h: Declaration of the class Keithley.
//
// ESRF 2000
//C Penel
// @doc
//*********************************************************


#if !defined(AFX_KEITHLEY_H__0EC0B334_3D8D_11D2_A392_00805F3A78EC__INCLUDED_)
#define AFX_KEITHLEY_H__0EC0B334_3D8D_11D2_A392_00805F3A78EC__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <math.h>
#include "ComPath.h"


//@class This is the class for Keithley multimeter.
class  Keithley
{
//@access Public members
public:
	int SetLowPass(int filt);
	int FreeRun();
	int SetDisplay( bool disp);
	int reset();
	int LeaveIddle();
	int EnterIddle();
	ComPath *comu_path;
	int adress;
	int GetReadBufferPoint();
	int GetBufferReadNumber();
	int GetSizeNeedForBuffer();
	int GetBufferData(double *data,int len);
	int GetBufferTime(double *data,int len);
	int GetBufferSize();
	int SetBuffer(int nb_pt);
	int LoadDefaults();
	int ReadtriggerCount(int * count);
	int SetTriggerCount(int count);
	double QuickRead();
	int ReadDataFormat(char *form);
	int SetDataFormat(char *form);
	int GetModel();
	//@cmember get config value from file 
	int GetConfiguration(char *);
	//@cmember ReadScanMode 
	int ReadScanMode(char *);
	//@cmember SetScanMode
	int SetScanMode(char * );
	//@cmember Read the scan list used
	int ReadScanList(char * );
	//@cmember Set a scan list for future scan
	int SetScanList(char *);
	//@cmember  Read The trigger source
	int ReadTrigSource(char *);
	//@cmember Set The trigger source 
	int SetTrigSource(char *);
	//@cmember Get the Reading channel number
	int ReadChannel(int *);
	//@cmember Select channel for reading
	int SetChannel(int );
	//@cmember Read trigged value
	int ReadData(int * ,double *,int *);
	//@cmember get reading status (true or False)
	bool IsReading();
	//@cmember Read Auto Zero mode
	int ReadAutoZero(int *);
	//@cmember Set AutoZero mode ( recalibrate zero beetween each reading)
	int SetAutoZero(int );
	//@cmember Trig delay reading
	int ReadDelay(double *);
	//@cmember  Trig Delay setting
	int SetDelay(double );
	//@cmember Integration time reading
	int ReadIntegrationTime(double *);
	//@cmember   Integration time setting
	int SetIntegrationTime(double );
	//@cmember  timeout setting function
	int SetTimeout(double );
	//@cmember  start measure function
	int Trig();
	//@cmember  Read integration mode 
	int ReadIntegration(int *);
	//@cmember  Set Integration mode 
	int SetIntegration(int );
	//@cmember Read number of sample by trig
	int ReadSample(int *);
	//@cmember	Set sample number by trig
	int SetSample(int );
	//@cmember	Get format of reading
	int ReadFormat(char *);
	//@cmember	Set format of reading
	int SetFormat(char *);
	//@cmember	get the number of digit 
	int ReadDigits(int * );
	//@cmember	Set the digits number
	int SetDigits(int );
	//@cmember	Set the reading range AUTO 
	int SetRange(char *);
	//@cmember	get the reading range
	int ReadRange(double *);
	//@cmember	Set the range in manual mode
	int SetRange(double );
	//@cmember	Get the  reading mode (V,I,DC,AC ...)
	int ReadMode( char *);
	//@cmember	Set the reading Mode
	int SetMode(char *);
	//@cmember	Get temperature on a channel
	int ScannerGetTemperature(int, double *);
	//@cmember	Get voltage on a channel
	int ScannerGetVoltage(int, double *);
	//@cmember	Get Current on a channel
	int ScannerGetCurrent(int, double *);
	//@cmember	Constructor
	Keithley(char*);
	//@cmember	Destructor
	virtual ~Keithley();
		//Timeout Value in second
	double Timeout;

  //@access Private members
private:
	double it_adjust;
	char myname[30];
	int TriggerCount;
	char dataformat[20];
	int Model;
	bool ScanCard;
	//@cmember scan mode NONE or INTernale
	char ScanMode[10];
	//@cmember Scan list description
	char ScanList[30];
	//@cmember	trigger source description
	char TrigSrc[20];
	// @cmember used channel
	int Channel;
	//@cmember AutoZero Mode (zero recalibration  beetween each reading)
	int AutoZero;
	//@cmember trig delay 
	double Delay;
	//@cmember	selected meanning time value
	double IntegrationTime;
	//@cmember	selected integrate mode (0 or 1)
	int Integrate;
	//@cmember	selected sample number by trig
	int Sample;
	//@cmember	selected  format
	char Format[30];
	//@cmember	selected number of digits
	int Digits;
	//@cmember	selected range 
	double Range;
	//@cmember   selected mode 
	char Mode[20];
	//@cmember all channel drive by card:
	char all_channel[20];

};

#endif // !defined(AFX_KEITHLEY_H__0EC0B334_3D8D_11D2_A392_00805F3A78EC__INCLUDED_)
