/*	XFUNC1.c -- illustrates Igor external functions.

*/
//#include <afxinet.h>
#include "XOPStandardHeaders.h"			// Include ANSI headers, Mac headers, IgorXOP.h, XOP.h and XOPSupport.h
#include "keithley_xop6.h"

/* Global Variables (none) */
#define False 0
#define True 1
char *Kname[10];
Keithley *Kobj[10];
DataFolderHandle KeithDFH[10];

static int
AddCStringToHandle(						// Concatenates C string to handle.
	char *theStr,
	Handle theHand)
{
	return PtrAndHand(theStr, theHand, strlen(theStr));
}

static bool testpath(int i)
{	
	if ( i<0 || i>9)
	{
		XOPNotice("Keith::Path must be betwwen 0 and 9" CR_STR);
		return (false);		
	}
	if(Kobj[i]==NULL)
	{
		XOPNotice("Keith::Path num is a free one I can't drive it" CR_STR);
		return (false);		
	}
	return (true);
}

//**********************OPEN FUNCTION*******************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
	struct KeithOpenParams  {
	Handle strH;
	double result;
};
typedef struct KeithOpenParams KeithOpenParams;
#include "XOPStructureAlignmentReset.h"

static int KeithOpen(KeithOpenParams* p)
{
	char name[50];
	int len2 = GetHandleSize(p->strH);
	memcpy(&name[0], *p->strH, len2);	
	name[len2]=0;
	// verify that this Keithley wasn't already create
	for (int k=0;k<10;k++)
	{if(Kname[k]!=NULL)
		{if (strcmp(Kname[k],name)==0)
			{// name already exit return Error
				XOPNotice("Keith::already open" CR_STR);
				SetNaN64(&(p->result));
				return (0);
			}
		
		}
	}
	// search a free indice
	int i=0;
	do
		i=i+1;
	while(Kname[i]!=NULL && i!=9);
	if (i==9)
	{
		XOPNotice("Keith::no free indice to create Keithley" CR_STR);
		SetNaN64(&(p->result));
		return (0);		
	}
	DataFolderHandle currentDF;
	int err;
	DataFolderHandle rootDFH;
	if(err=GetCurrentDataFolder(&currentDF))
		return err;
	if (err = GetRootDataFolder(0, &rootDFH))
		return err;
	if (err = GetNamedDataFolder(rootDFH, name, &KeithDFH[i])) 
	{
		// We need to createDataFolder.
		if (err = NewDataFolder(rootDFH, name, &KeithDFH[i]))
		return err;
	}
	// i contain the first free index copy name in table and create the Keithley
//	Kname[i]=new char(strlen (name)+1);
	Kname[i]=(char *)malloc(strlen(name)+1);
	strcpy(Kname[i],name);
	Kobj[i]=new Keithley(name);
	if(Kobj[i]->comu_path==NULL)
	{
		delete Kname[i];
		Kname[i]=NULL;
		delete Kobj[i];
		Kobj[i]=NULL;
		SetNaN64(&p->result);
		return 0;

	}
	else
	{
	SetCurrentDataFolder(KeithDFH[i]);
	Variable("path",NT_FP64);
	double j=0;
	double path;
	path=(double)i;
	StoreNumVar("path",&path,&j);
	SetCurrentDataFolder(currentDF);
	p->result=i;
	return(0);	
	}
}

//************************CLOSE FUNCTION********************************************************
#include "XOPStructureAlignmentTwoByte.h"// All structures passed to Igor are two-byte aligned.
struct KeithCloseParams  {
	double index;
	double result;
	

};
typedef struct KeithCloseParams KeithCloseParams;
#include "XOPStructureAlignmentReset.h"
static int KeithClose(KeithCloseParams* p)
{
// verify that index isn'nt a free one 
	int i=(int)p->index;
	if ( !testpath(i))
	{
		p->result=False;
		return (0)	;
	}
	else
	{
		int err;
		DataFolderHandle currentDF;
		if(err=GetCurrentDataFolder(&currentDF))
			return err;
		SetCurrentDataFolder(KeithDFH[i]);
		Variable("path",NT_FP64);
		double path;
		double j;
		SetNaN64(&path);
		SetNaN64(&j);
		StoreNumVar("path",&path,&j);

		delete Kname[i];
		Kname[i]=NULL;
		delete Kobj[i];
		Kobj[i]=NULL;
		p->result=True;
		SetCurrentDataFolder(currentDF);

		return(0);	
	}
			/* XFunc error code */
}





//************************FREE_RUN*************************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithFreeRunParams  {
	double index;					// Complex parameter
	double result;				// Complex result
};
typedef struct KeithFreeRunParams KeithFreeRunParams;
#include "XOPStructureAlignmentReset.h"
static int KeithFreeRun(KeithFreeRunParams* p)
{
	int i=(int)p->index;
	if ( !testpath(i))
	{
		p->result=False;
		return (0);	
	}
	else
	{
		if(Kobj[i]->FreeRun())
		{
			p->result=True;
			return(0);	
		}
		else
		{
			XOPNotice("K->freeRun return False" CR_STR);
			p->result=False;
			return (0);
		}
	}	
}



//**************************DET DISPLAY*******************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithSetDisplayParams  {
	double display;
	double index;
	double result;				// Complex result
};
typedef struct KeithSetDisplayParams KeithSetDisplayParams;
#include "XOPStructureAlignmentReset.h"
static int
KeithSetDisplay(KeithSetDisplayParams* p)
{
	int i=(int)p->index;
	if ( !testpath(i))
	{
		p->result=False;
		return (0);	
	}
	else
	{
		bool display;
		if (p->display==0)
			display=false;
		else
			display=true;
		if(Kobj[i]->SetDisplay(display))
		{
			p->result=True;
			return(0);	
		}
		else
		{
			XOPNotice("K->SetDisplay return False" CR_STR);
			p->result=False;
			return (0);

		}
	}

}

//******************************RESET******************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithResetParams  {
	double index;
	double result;				// Complex result
};
typedef struct KeithResetParams KeithResetParams;
#include "XOPStructureAlignmentReset.h"

static int KeithReset(KeithResetParams* p)
{
	int i=(int)p->index;
	if ( !testpath(i))
	{
		p->result=False;
		return (0);	
	}
	else
	{
		if(Kobj[i]->reset())
		{
			p->result=True;
			return(0);	
		}
		else
		{
			XOPNotice("K->Reset return False" CR_STR);
			p->result=False;
			return (0);
		}
	}
}
//******************************SETLOWPASS*******************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithSetLowPassParams  {
	double filter;
	double index;
	double result;				// Complex result
};
typedef struct KeithSetLowPassParams KeithSetLowPassParams;
#include "XOPStructureAlignmentReset.h"

static int KeithSetLowPass(KeithSetLowPassParams* p)
{
	int i=(int)p->index;
	if ( !testpath(i))
	{
		p->result=False;
		return (0);	
	}
	else
	{	if(Kobj[i]->SetLowPass(p->filter))
		{	p->result=True;
			return(0);
		}
		else
		{	XOPNotice("K->SetLowPass return False" CR_STR);
			p->result=False;
			return (0); 
		}
	}

}
//******************************LEAVE  IDDLE*******************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithLeaveIddleParams  {
	double index;
	double result;				// Complex result
};
typedef struct KeithLeaveIddleParams KeithLeaveIddleParams;
#include "XOPStructureAlignmentReset.h"
static int KeithLeaveIddle(KeithLeaveIddleParams* p)
{
	int i=(int)p->index;
	if ( !testpath(i))
	{
		p->result=False;
		return (0);	
	}	else
	{	if(Kobj[i]->LeaveIddle())
		{	p->result=True;
			return(0);
		}
		else
		{	XOPNotice("K->LeaveIddle return False" CR_STR);
			p->result=False;
			return (0); 
		}
	}
	
}
//******************************ENTER  IDDLE*******************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithEnterIddleParams  {
	double index;
	double result;				// Complex result
};
typedef struct KeithEnterIddleParams KeithEnterIddleParams;
#include "XOPStructureAlignmentReset.h"
static int KeithEnterIddle(KeithEnterIddleParams* p)
{
	int i=(int)p->index;
	if ( !testpath(i))
	{
		p->result=False;
		return (0);	
	}	else
	{	if(Kobj[i]->EnterIddle())
		{	p->result=True;
			return(0);
		}
		else
		{	XOPNotice("K->EnterIddle return False" CR_STR);
			p->result=False;
			return (0); 
		}
	}

}

//******************************GET BUFFER FREE*******************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithGetReadBufferPointParams  {
	double index;
	double result;				// Complex result
};
typedef struct KeithGetReadBufferPointParams KeithGetReadBufferPointParams;
#include "XOPStructureAlignmentReset.h"
static int KeithGetReadBufferPoint(KeithGetReadBufferPointParams* p)
{
	int i=(int)p->index;
	if ( !testpath(i))
	{
		p->result=False;
		return (0);	
	}	else
	{	p->result=Kobj[i]->GetReadBufferPoint();
		return (0);
	}

}

//******************************GET BUFFER DATA******************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithGetBufferDataParams  {
	double len;
	double index;
	double result;				// Complex result
};
typedef struct KeithGetBufferDataParams KeithGetBufferDataParams;
#include "XOPStructureAlignmentReset.h"
static int KeithGetBufferData(KeithGetBufferDataParams* p)
{
	double *data;
	waveHndl wavH;
	//long DimensionSizes[MAX_DIMENSIONS+1];
	CountInt DimensionSizes[MAX_DIMENSIONS + 1];
	int i=(int)p->index;
	if ( !testpath(i))
	{
		p->result=False;
		return (0);	
	}
	else
	{//	data =new double((int)p->len+1);
		data=(double *)malloc(p->len*sizeof(double));
		if(p->result=Kobj[i]->GetBufferData(data,(int)p->len))
		{
			MemClear(DimensionSizes,sizeof(DimensionSizes));
			DimensionSizes[ROWS]=p->len;
			int err=MDMakeWave(&wavH,"BData",KeithDFH[(int)p->index],DimensionSizes,NT_FP64,1);
			if (err==0)
			{
				double *pt=data;
				//long indices[MAX_DIMENSIONS];
				IndexInt indices[MAX_DIMENSIONS];
				double val[2];
				MemClear(indices,sizeof(indices));
				for (long j=0;j<p->len;j++)
				{
					val[0]=*pt;	
					indices[0]=j;
					MDSetNumericWavePointValue(wavH,indices,val);
					pt++;
				}
				free(data);
				p->result=True;
				return (0);
			}
			else 
			{
				p->result=False;
				free (data);
				return (err);
			}
		}
		else
		{
			p->result=False;
			return (0);	
		}
	}

}
//******************************GET BUFFER DATA******************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithGetBufferTimeParams  {
	double len;
	double index;
	double result;				// Complex result
};
typedef struct KeithGetBufferTimeParams KeithGetBufferTimeParams;
#include "XOPStructureAlignmentReset.h"
static int KeithGetBufferTime(KeithGetBufferTimeParams* p)
{
	double *data;
	waveHndl wavH;
	//long DimensionSizes[MAX_DIMENSIONS+1];
	CountInt DimensionSizes[MAX_DIMENSIONS + 1];;
	int i=(int)p->index;
	if ( !testpath(i))
	{
		p->result=False;
		return (0);	
	}
	else
	{//	data =new double((int)p->len+1);
		data=(double *)malloc(p->len*sizeof(double));
		if(p->result=Kobj[i]->GetBufferTime(data,(int)p->len))
		{
			MemClear(DimensionSizes,sizeof(DimensionSizes));
			DimensionSizes[ROWS]=p->len;
			int err=MDMakeWave(&wavH,"BTime",KeithDFH[(int)p->index],DimensionSizes,NT_FP64,1);
			if (err==0)
			{
				double *pt=data;
				// long indices[MAX_DIMENSIONS];
				IndexInt indices[MAX_DIMENSIONS];
				double val[2];
				MemClear(indices,sizeof(indices));
				for (long j=0;j<p->len;j++)
				{
					val[0]=*pt;	
					indices[0]=j;
					MDSetNumericWavePointValue(wavH,indices,val);
					pt++;
				}
				free(data);
				p->result=True;
				return (0);
			}
			else 
			{
				p->result=False;
				free (data);
				return (err);
			}
		}
		else
		{
			p->result=False;
			return (0);	
		}
	}

}
//******************************GET BUFFER Size******************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithGetBufferSizeParams  {
	double index;
	double result;				// Complex result
};
typedef struct KeithGetBufferSizeParams KeithGetBufferSizeParams;
#include "XOPStructureAlignmentReset.h"
static int KeithGetBufferSize(KeithGetBufferSizeParams* p)
{
	int i=(int)p->index;
	if ( !testpath(i))
	{
		p->result=False;
		return (0);	
	}	else
	{	
		p->result=Kobj[i]->GetBufferSize();
		return (0);
	}
}
//******************************SET BUFFER ******************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithSetBufferParams  {
	double len;
	double index;
	double result;				// Complex result
};
typedef struct KeithSetBufferParams KeithSetBufferParams;
#include "XOPStructureAlignmentReset.h"
static int KeithSetBuffer(KeithSetBufferParams* p)
{
	int i=(int)p->index;
	if ( !testpath(i))
	{
		p->result=False;
		return (0);	
	}
	else
	{	
		if(Kobj[i]->SetBuffer((int)p->len))
		{
			int retval=Kobj[(int)p->index]->GetBufferSize();
			if (retval!=0)
			{
				if (retval==(int)p->len)
				{
					p->result=True;
					return (0);
				}
				else
				{
					XOPNotice("Keith::error in SetBuffer:Set#Read" CR_STR);
					p->result=False;
					return (0);
				}
			}
			else
			{
					XOPNotice("Keith::error in SetBuffer:Can't Read BufferSize" CR_STR);
					p->result=False;
					return (0);

			}		
		}
		else
			XOPNotice("Keith::error in SetBuffer" CR_STR);
			p->result=False;
		return (0);
	}

}

//******************************LOAD Default ******************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithLoadDefaultParams  {
	double index;
	double result;				// Complex result
};
typedef struct KeithLoadDefaultParams KeithLoadDefaultParams;
#include "XOPStructureAlignmentReset.h"
static int KeithLoadDefault(KeithLoadDefaultParams* p)
{
	int i=(int)p->index;
	if ( !testpath(i))
	{
		p->result=False;
		return (0);	
	}
	else
	{	
		if(Kobj[i]->LoadDefaults())
			p->result=True;
		else
			p->result=False;
		return (0);
	}

}



//******************************ReadTriggerCount ******************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithReadtriggerCountParams  {
	double index;
	double result;				// Complex result
};
typedef struct KeithReadtriggerCountParams KeithReadtriggerCountParams;
#include "XOPStructureAlignmentReset.h"
static int KeithReadtriggerCount(KeithReadtriggerCountParams* p)
{
	int i=(int)p->index;
	if ( !testpath(i))
	{
		p->result=False;
		return (0);	
	}
	else
	{	
		
		if(Kobj[i]->ReadtriggerCount(&i))
			p->result=i;
		else
		{	
			SetNaN64(&p->result);
			XOPNotice("Keith::error in ReadTrigger Count" CR_STR);
		}	
		return (0);
	}

}

//******************************SetTriggerCount ******************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithSetTriggerCountParams  {
	double cnt;
	double index;
	double result;				// Complex result
};
typedef struct KeithSetTriggerCountParams KeithSetTriggerCountParams;
#include "XOPStructureAlignmentReset.h"
static int KeithSetTriggerCount(KeithSetTriggerCountParams* p)
{
	int i=(int)p->index;
	if ( !testpath(i))
	{
		p->result=False;
		return (0);	
	}
	else
	{	int n;
		
		if(Kobj[i]->SetTriggerCount((int)(p->cnt) ))
			p->result=True;
		else
		{	
			p->result=False;
			XOPNotice("Keith::error in SetTrigger Count" CR_STR);
		}	
		return (0);
	}

}


//******************************QuickRead ******************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithQuickReadParams  {
	double index;
	double result;				// Complex result
};
typedef struct KeithQuickReadParams KeithQuickReadParams;
#include "XOPStructureAlignmentReset.h"
static int KeithQuickRead(KeithQuickReadParams* p)
{
	int i=(int)p->index;
	if ( !testpath(i))
	{
		p->result=False;
		return (0);	
	}
	else
	{		
		p->result=Kobj[i]->QuickRead();
		return (0);
	}

}


//******************************ReadDatFormat ******************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithReadDataFormatParams  {
	double index;
	Handle result;				// Complex result
};
typedef struct KeithReadDataFormatParams KeithReadDataFormatParams;
#include "XOPStructureAlignmentReset.h"
static int KeithReadDataFormat(KeithReadDataFormatParams* p)
{
	Handle retformat;
	int i=(int)p->index;

	if ( !testpath(i))
	{
		p->result=False;
		return (0);	
	}
	else
	{	int n;
		char format[20];
		if ((Kobj[i]->ReadDataFormat(&format[0]))==(int)true)
		{
			retformat=NewHandle(strlen(format));
			memcpy(*retformat, format, strlen(format));
			p->result=retformat;
		}
		else
		{
			sprintf (format,"????");
			retformat=NewHandle(strlen(format));
			memcpy(*retformat, format, strlen(format));
			p->result=retformat;
			return 0;
		}
		return (0);
	}

}


//**********************SetDataFormat FUNCTION*******************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
	struct KeithSetDataFormatParams  {
	Handle strH;
	double index;
	double result;
};
typedef struct KeithSetDataFormatParams KeithSetDataFormatParams;
#include "XOPStructureAlignmentReset.h"

static int KeithSetDataFormat(KeithSetDataFormatParams* p)
{
	if ( !testpath((int)p->index))
	{
		p->result=False;
		return (0);
	}
	else
	{	int n;
		char format[50];
		int len2 = GetHandleSize(p->strH);
		memcpy(&format[0], *p->strH, len2);	
		format[len2]=0;

		if(Kobj[(int)p->index]->SetDataFormat(format))
			p->result=True;
		else
		{	
			p->result=False;
			XOPNotice("Keith::error in SetDataFormat" CR_STR);
		}	
		return (0);
	}
	// verify that this Keithley wasn't already create

					
}

//**********************GetModel FUNCTION*******************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
	struct KeithGetModelParams  {
	double index;
	double result;
};
typedef struct KeithGetModelParams KeithGetModelParams;
#include "XOPStructureAlignmentReset.h"

static int KeithGetModel(KeithGetModelParams* p)
{
	if ( !testpath((int)p->index))
	{
		SetNaN64(&p->result);
		return (0);
	}
	else
	{
		p->result=Kobj[(int)p->index]->GetModel();
		if ((int)p->result!=false)
			return (0);

		else
		{	
			SetNaN64(&p->result);
			XOPNotice("Keith::error in GetModel" CR_STR);
		}	
		return (0);
	}
	// verify that this Keithley wasn't already create
					
}

//**********************GetConfiguration FUNCTION*******************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
	struct KeithGetConfigurationParams  {
	Handle filename;
	double index;
	double result;
};
typedef struct KeithGetConfigurationParams KeithGetConfigurationParams;
#include "XOPStructureAlignmentReset.h"

static int KeithGetConfiguration(KeithGetConfigurationParams* p)
{
	if ( !testpath((int)p->index))
	{
		p->result=False;
		return (0);
	}
	else
	{
		char name[50];
		int len2 = GetHandleSize(p->filename);
		memcpy(&name[0], *p->filename, len2);	
		name[len2]=0;

		p->result=Kobj[(int)p->index]->GetConfiguration(name);
		if ((int)p->result!=false)
			return (0);

		else
		{	
			XOPNotice("Keith::error in GetConfiguration" CR_STR);
		}	
		return (0);
	}
	// verify that this Keithley wasn't already create

				
}

//**********************SetScanMode FUNCTION*******************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
	struct KeithSetScanModeParams  {
	Handle mode;
	double index;
	double result;
};
typedef struct KeithSetScanModeParams KeithSetScanModeParams;
#include "XOPStructureAlignmentReset.h"

static int KeithSetScanMode(KeithSetScanModeParams* p)
{
	if ( !testpath((int)p->index))
	{
		p->result=False;
		return (0);
	}
	else
	{
		char name[50];
		int len2 = GetHandleSize(p->mode);
		memcpy(&name[0], *p->mode, len2);	
		name[len2]=0;

		p->result=Kobj[(int)p->index]->SetScanMode(name);
		if ((int)p->result!=false)
			return (0);

		else
		{	
			XOPNotice("Keith::error in SetScanMode" CR_STR);
		}	
		return (0);
	}
			
}

//******************************ReadScanMode ******************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithReadScanModeParams  {
	double index;
	Handle result;				// Complex result
};
typedef struct KeithReadScanModeParams KeithReadScanModeParams;
#include "XOPStructureAlignmentReset.h"
static int KeithReadScanMode(KeithReadScanModeParams* p)
{
	Handle retformat;
	char format[20];
	int i=(int)p->index;

	if ( !testpath(i))
	{
			sprintf (format,"????");
			retformat=NewHandle(strlen(format));
			memcpy(*retformat, format, strlen(format));
			p->result=retformat;

		return (0);
	}
	else
	{	int n;
		
		if ((Kobj[i]->ReadScanMode(&format[0]))==(int)true)
		{
			retformat=NewHandle(strlen(format));
			memcpy(*retformat, format, strlen(format));
			p->result=retformat;
		}
		else
		{
			XOPNotice("Keith::error in ReadScanMode" CR_STR);
			sprintf (format,"????");
			retformat=NewHandle(strlen(format));
			memcpy(*retformat, format, strlen(format));
			p->result=retformat;
			return 0;
		}
		return (0);
	}

}


//**********************SetScanList FUNCTION*******************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
	struct KeithSetScanListParams  {
	Handle list;
	double index;
	double result;
};
typedef struct KeithSetScanListParams KeithSetScanListParams;
#include "XOPStructureAlignmentReset.h"

static int KeithSetScanList(KeithSetScanListParams* p)
{
	if ( !testpath((int)p->index))
	{
		p->result=False;
		return (0);
	}
	else
	{
		char name[50];
		int len2 = GetHandleSize(p->list);
		memcpy(&name[0], *p->list, len2);	
		name[len2]=0;

		p->result=Kobj[(int)p->index]->SetScanList(name);
		if ((int)p->result!=false)
			
			return (0);

		else
		{	
			XOPNotice("Keith::error in SetScanList" CR_STR);
		}	
		return (0);
	}
			
}

//******************************ReadScanList ******************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithReadScanListParams  {
	double index;
	Handle result;				
};
typedef struct KeithReadScanListParams KeithReadScanListParams;
#include "XOPStructureAlignmentReset.h"
static int KeithReadScanList(KeithReadScanListParams* p)
{
	Handle HStr;
	int i=(int)p->index;
	char tmpstr[20];
	if ( !testpath(i))
	{
			sprintf (tmpstr,"????");
			HStr=NewHandle(strlen(tmpstr));
			memcpy(*HStr, tmpstr, strlen(tmpstr));
			p->result=HStr;
			return (0);
	}
	else
	{	int n;
		
		if ((Kobj[i]->ReadScanList(&tmpstr[0]))==(int)true)
		{
			HStr=NewHandle(strlen(tmpstr));
			memcpy(*HStr, tmpstr, strlen(tmpstr));
			p->result=HStr;
		}
		else
		{
			XOPNotice("Keith::error in ReadScanList" CR_STR);
			sprintf (tmpstr,"????");
			HStr=NewHandle(strlen(tmpstr));
			memcpy(*HStr, tmpstr, strlen(tmpstr));
			p->result=HStr;

			return 0;
		}
		return (0);
	}

}


//**********************SetTrigSource FUNCTION*******************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
	struct KeithSetTrigSourceParams  {
	Handle source;
	double index;
	double result;
};
typedef struct KeithSetTrigSourceParams KeithSetTrigSourceParams;
#include "XOPStructureAlignmentReset.h"

static int KeithSetTrigSource(KeithSetTrigSourceParams* p)
{
	if ( !testpath((int)p->index))
	{
		p->result=-1;
		return (0);
	}
	else
	{
		char tmpstr[50];
		int len2 = GetHandleSize(p->source);
		memcpy(&tmpstr[0], *p->source, len2);	
		tmpstr[len2]=0;

		p->result=Kobj[(int)p->index]->SetTrigSource(tmpstr);
		if ((int)p->result!=false)
			return (0);

		else
		{	
			XOPNotice("Keith::error in SetTrigsource" CR_STR);
			return (0);
		}	
		return (0);
	}
			
}

//******************************ReadTrigSource ******************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithReadTrigSourceParams  {
	double index;
	Handle result;				
};
typedef struct KeithReadTrigSourceParams KeithReadTrigSourceParams;
#include "XOPStructureAlignmentReset.h"
static int KeithReadTrigSource(KeithReadTrigSourceParams* p)
{
	Handle HStr;
	int i=(int)p->index;
	char tmpstr[20];
	if ( !testpath(i))
	{
			sprintf (tmpstr,"????");
			HStr=NewHandle(strlen(tmpstr));
			memcpy(*HStr, tmpstr, strlen(tmpstr));
			p->result=HStr;
			return (0);	
	}
	else
	{	int n;
		
		if ((Kobj[i]->ReadTrigSource(&tmpstr[0]))==(int)true)
		{
			HStr=NewHandle(strlen(tmpstr));
			memcpy(*HStr, tmpstr, strlen(tmpstr));
			p->result=HStr;
		}
		else
		{
			XOPNotice("Keith::error in ReadTrigSource" CR_STR);
			sprintf (tmpstr,"????");
			HStr=NewHandle(strlen(tmpstr));
			memcpy(*HStr, tmpstr, strlen(tmpstr));
			p->result=HStr;

			return 0;
		}
		return (0);
	}

}


//**********************SetChannel FUNCTION*******************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
	struct KeithSetChannelParams  {
	double channel;
	double index;
	double result;
};
typedef struct KeithSetChannelParams KeithSetChannelParams;
#include "XOPStructureAlignmentReset.h"

static int KeithSetChannel(KeithSetChannelParams* p)
{
	if ( !testpath((int)p->index))
	{
		p->result=False;
		return (0);
	}
	else
	{

		p->result=Kobj[(int)p->index]->SetChannel((int)p->channel);
		if ((int)p->result!=false)
			return (0);

		else
		{	
			XOPNotice("Keith::error in SetChannel");
			return (0);
		}	
		return (0);
	}
			
}

//******************************ReadChannel ******************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithReadChannelParams  {
	double index;
	double result;				
};
typedef struct KeithReadChannelParams KeithReadChannelParams;
#include "XOPStructureAlignmentReset.h"
static int KeithReadChannel(KeithReadChannelParams* p)
{
	int i=(int)p->index;
	if ( !testpath((int)p->index))
	{
		SetNaN64(&p->result);
		return (0);
	}
	else
	{	int n;
		char tmpstr[20];
		if ((Kobj[i]->ReadChannel(&n))==(int)true)
		{
			p->result=(double)n;
			return (0);
		}
		else
		{
			XOPNotice("Keith::error in ReadChannel" CR_STR);
			SetNaN64(&p->result);
			return 0;
		}
		return (0);
	}
}

//******************************ReadData******************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithReadDataParams  {
	double len;
	double index;
	double result;				// Complex result
};
typedef struct KeithReadDataParams KeithReadDataParams;
#include "XOPStructureAlignmentReset.h"
static int KeithReadData(KeithReadDataParams* p)
{
	double *data;
	int *chan;
	waveHndl wavH,chanH;
	//long DimensionSizes[MAX_DIMENSIONS+1];
	CountInt DimensionSizes[MAX_DIMENSIONS + 1];
	int i=(int)p->index;
	if ( !testpath(i))
	{
		SetNaN64(&p->result);
		return (0);	
	}
	else
	{//	data =new double((int)p->len+1);
		data=(double *)malloc(p->len*sizeof(double));
		chan=(int *)malloc(p->len*sizeof(double));
		p->result=p->len;
		int llen=p->len;
		if(p->result=Kobj[i]->ReadData(&llen,data,chan))
		{
			p->len=llen;
			MemClear(DimensionSizes,sizeof(DimensionSizes));
			DimensionSizes[ROWS]=p->len;
			int err=MDMakeWave(&wavH,"Data",KeithDFH[(int)p->index],DimensionSizes,NT_FP64,1);
			int err2=MDMakeWave(&chanH,"Chan",KeithDFH[(int)p->index],DimensionSizes,NT_FP64,1);
			if (err==0 && err2==0)
			{
				double *pt=data;
				int *pt2=chan;
				//long indices[MAX_DIMENSIONS];
				IndexInt indices[MAX_DIMENSIONS];
				double val[2];
				MemClear(indices,sizeof(indices));
				for (long j=0;j<p->len;j++)
				{
					val[0]=*pt;	
					indices[0]=j;
					MDSetNumericWavePointValue(wavH,indices,val);
					pt++;
					val[0]=(double)*pt2;
					MDSetNumericWavePointValue(chanH,indices,val);
					pt2++;
				}
				free(data);
				free(chan);
				return (0);
			}
			else 
			{
				p->result=False;
				free (data);
				free (chan);
				return (err);
			}
		}
		else
		{
			p->result=False;
			return (0);	
		}
	}

}
//************************FREE_RUN*************************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithIsReadingParams  {
	double index;					// Complex parameter
	double result;				// Complex result
};
typedef struct KeithIsReadingParams KeithIsReadingParams;
#include "XOPStructureAlignmentReset.h"
static int KeithIsReading(KeithIsReadingParams* p)
{
	int i=(int)p->index;
	if ( !testpath(i))
	{
		p->result=False;
		return (0);	
	}
	else
	{
		if(Kobj[i]->IsReading())
		{
			p->result=True;
			return(0);	
		}
		else
		{
			p->result=False;
			return (0);
		}
	}	
}



//**********************SetAutoZero FUNCTION*******************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
	struct KeithSetAutoZeroParams  {
	double autozero;
	double index;
	double result;
};
typedef struct KeithSetAutoZeroParams KeithSetAutoZeroParams;
#include "XOPStructureAlignmentReset.h"

static int KeithSetAutoZero(KeithSetAutoZeroParams* p)
{
	int retval;
	if ( !testpath((int)p->index))
	{
		p->result=False;
		return (0);
	}
	else
	{

		p->result=Kobj[(int)p->index]->SetAutoZero((int)p->autozero);
		if ((int)p->result!=false)
		{
			if (Kobj[(int)p->index]->ReadAutoZero(&retval))
			{
				if (retval==(int)p->autozero)
				{
					p->result=True;
					return (0);
				}
				else
				{
					XOPNotice("Keith::error in SetAutoZero:Set#Read" CR_STR);
					p->result=False;
					return (0);
				}
			}
			else
			{
					XOPNotice("Keith::error in SetAutoZero:Can't Read Autozero" CR_STR);
					p->result=False;
					return (0);

			}
		}
		else
		{	
			XOPNotice("Keith::error in SetAutoZero" CR_STR);
			p->result=False;
			return (0);
		}	
		return (0);
	}
			
}

//******************************ReadAutoZero ******************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithReadAutoZeroParams  {
	double index;
	double result;				
};
typedef struct KeithReadAutoZeroParams KeithReadAutoZeroParams;
#include "XOPStructureAlignmentReset.h"
static int KeithReadAutoZero(KeithReadAutoZeroParams* p)
{
	int i=(int)p->index;
	if ( !testpath((int)p->index))
	{
		SetNaN64(&p->result);
		return (0);
	}
	else
	{	int n;
		char tmpstr[20];
		if ((Kobj[i]->ReadAutoZero(&n))==(int)true)
		{
			p->result=(double)n;
			return (0);
		}
		else
		{
			XOPNotice("Keith::error in ReadAutoZero" CR_STR);
			SetNaN64(&p->result);
			return 0;
		}
		return (0);
	}
}

//**********************SetDelay FUNCTION*******************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
	struct KeithSetDelayParams  {
	double delay;
	double index;
	double result;
};
typedef struct KeithSetDelayParams KeithSetDelayParams;
#include "XOPStructureAlignmentReset.h"

static int KeithSetDelay(KeithSetDelayParams* p)
{
	if ( !testpath((int)p->index))
	{
		p->result=False;
		return (0);
	}
	else
	{

		p->result=Kobj[(int)p->index]->SetDelay(p->delay);
		if ((int)p->result!=false)
			return (0);

		else
		{	
			XOPNotice("Keith::error in SetDelay" CR_STR);
			return (0);
		}	
		return (0);
	}
			
}

//******************************ReadDelay ******************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithReadDelayParams  {
	double index;
	double result;				
};
typedef struct KeithReadDelayParams KeithReadDelayParams;
#include "XOPStructureAlignmentReset.h"
static int KeithReadDelay(KeithReadDelayParams* p)
{
	int i=(int)p->index;
	if ( !testpath((int)p->index))
	{
		SetNaN64(&p->result);
		return (0);
	}
	else
	{
		char tmpstr[20];
		if ((Kobj[i]->ReadDelay(&(p->result))==(int)true))
		{
			return (0);
		}
		else
		{
			XOPNotice("Keith::error in ReadDelay" CR_STR);
			SetNaN64(&p->result);
			return 0;
		}
		return (0);
	}
}

//**********************SetIntegrationTime FUNCTION*******************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
	struct KeithSetIntegrationTimeParams  {
	double IntegrationTime;
	double index;
	double result;
};
typedef struct KeithSetIntegrationTimeParams KeithSetIntegrationTimeParams;
#include "XOPStructureAlignmentReset.h"

static int KeithSetIntegrationTime(KeithSetIntegrationTimeParams* p)
{
	if ( !testpath((int)p->index))
	{
		p->result=False;
		return (0);
	}
	else
	{

		p->result=Kobj[(int)p->index]->SetIntegrationTime(p->IntegrationTime);
		if ((int)p->result!=false)
			return (0);

		else
		{	
			XOPNotice("Keith::error in SetIntegrationTime" CR_STR);
			return (0);
		}	
		return (0);
	}
			
}

//******************************ReadIntegrationTime ******************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithReadIntegrationTimeParams  {
	double index;
	double result;				
};
typedef struct KeithReadIntegrationTimeParams KeithReadIntegrationTimeParams;
#include "XOPStructureAlignmentReset.h"
static int KeithReadIntegrationTime(KeithReadIntegrationTimeParams* p)
{
	int i=(int)p->index;
	if ( !testpath((int)p->index))
	{
		SetNaN64(&p->result);
		return (0);
	}
	else
	{
		char tmpstr[20];
		if ((Kobj[i]->ReadIntegrationTime(&(p->result))==(int)true))
		{
			return (0);
		}
		else
		{
			XOPNotice("Keith::error in ReadIntegrationTime" CR_STR);
			SetNaN64(&p->result);
			return 0;
		}
		return (0);
	}
}

//**********************SetTimeout FUNCTION*******************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
	struct KeithSetTimeoutParams  {
	double Timeout;
	double index;
	double result;
};
typedef struct KeithSetTimeoutParams KeithSetTimeoutParams;
#include "XOPStructureAlignmentReset.h"
static int KeithSetTimeout(KeithSetTimeoutParams* p)
{
	if ( !testpath((int)p->index))
	{
		p->result=False;
		return (0);
	}
	else
	{

		p->result=Kobj[(int)p->index]->SetTimeout(p->Timeout);
		if ((int)p->result!=false)
			return (0);

		else
		{	
			XOPNotice("Keith::error in SetTimeout" CR_STR);
			return (0);
		}	
		return (0);
	}
			
}

//**********************Trig FUNCTION*******************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
	struct KeithTrigParams  {
	double index;
	double result;
};
typedef struct KeithTrigParams KeithTrigParams;
#include "XOPStructureAlignmentReset.h"
static int KeithTrig(KeithTrigParams* p)
{
	if ( !testpath((int)p->index))
	{
		p->result=False;
		return (0);
	}
	else
	{

		p->result=Kobj[(int)p->index]->Trig();
		if ((int)p->result!=false)
			return (0);

		else
		{	
			XOPNotice("Keith::error in Trig" CR_STR);
			return (0);
		}	
		return (0);
	}
			
}

//**********************SetIntegration FUNCTION*******************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
	struct KeithSetIntegrationParams  {
	double Integration;
	double index;
	double result;
};
typedef struct KeithSetIntegrationParams KeithSetIntegrationParams;
#include "XOPStructureAlignmentReset.h"

static int KeithSetIntegration(KeithSetIntegrationParams* p)
{
	if ( !testpath((int)p->index))
	{
		p->result=False;
		return (0);
	}
	else
	{

		p->result=Kobj[(int)p->index]->SetIntegration((int)p->Integration);
		if ((int)p->result!=false)
			return (0);

		else
		{	
			XOPNotice("Keith::error in SetIntegration" CR_STR);
			return (0);
		}	
		return (0);
	}
			
}

//******************************ReadIntegration ******************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithReadIntegrationParams  {
	double index;
	double result;				
};
typedef struct KeithReadIntegrationParams KeithReadIntegrationParams;
#include "XOPStructureAlignmentReset.h"
static int KeithReadIntegration(KeithReadIntegrationParams* p)
{
	int i=(int)p->index;
	if ( !testpath((int)p->index))
	{
		SetNaN64(&p->result);
		return (0);
	}
	else
	{	int n;
		char tmpstr[20];
		if ((Kobj[i]->ReadIntegration(&n))==(int)true)
		{
			p->result=(double)n;
			return (0);
		}
		else
		{
			XOPNotice("Keith::error in ReadIntegration" CR_STR);
			SetNaN64(&p->result);
			return 0;
		}
		return (0);
	}
}

//**********************SetSample FUNCTION*******************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
	struct KeithSetSampleParams  {
	double Sample;
	double index;
	double result;
};
typedef struct KeithSetSampleParams KeithSetSampleParams;
#include "XOPStructureAlignmentReset.h"

static int KeithSetSample(KeithSetSampleParams* p)
{
	if ( !testpath((int)p->index))
	{
		p->result=False;
		return (0);
	}
	else
	{

		p->result=Kobj[(int)p->index]->SetSample((int)p->Sample);
		if ((int)p->result!=false)
			return (0);

		else
		{	
			XOPNotice("Keith::error in SetSample" CR_STR);
			return (0);
		}	
		return (0);
	}
			
}

//******************************ReadSample ******************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithReadSampleParams  {
	double index;
	double result;				
};
typedef struct KeithReadSampleParams KeithReadSampleParams;
#include "XOPStructureAlignmentReset.h"
static int KeithReadSample(KeithReadSampleParams* p)
{
	int i=(int)p->index;
	if ( !testpath((int)p->index))
	{
		SetNaN64(&p->result);
		return (0);
	}
	else
	{	int n;
		char tmpstr[20];
		if ((Kobj[i]->ReadSample(&n))==(int)true)
		{
			p->result=(double)n;
			return (0);
		}
		else
		{
			XOPNotice("Keith::error in ReadSample" CR_STR);
			SetNaN64(&p->result);
			return 0;
		}
		return (0);
	}
}

//**********************SetFormat FUNCTION*******************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
	struct KeithSetFormatParams  {
	Handle Format;
	double index;
	double result;
};
typedef struct KeithSetFormatParams KeithSetFormatParams;
#include "XOPStructureAlignmentReset.h"

static int KeithSetFormat(KeithSetFormatParams* p)
{
	if ( !testpath((int)p->index))
	{
		p->result=False;
		return (0);
	}
	else
	{
		char tmpstr[50];
		int len2 = GetHandleSize(p->Format);
		memcpy(&tmpstr[0], *p->Format, len2);	
		tmpstr[len2]=0;

		p->result=Kobj[(int)p->index]->SetFormat(tmpstr);
		if ((int)p->result!=false)
			return (0);

		else
		{	
			XOPNotice("Keith::error in SetFormat" CR_STR);
			return (0);
		}	
		return (0);
	}
			
}

//******************************ReadFormat ******************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithReadFormatParams  {
	double index;
	Handle result;				
};
typedef struct KeithReadFormatParams KeithReadFormatParams;
#include "XOPStructureAlignmentReset.h"
static int KeithReadFormat(KeithReadFormatParams* p)
{
	Handle HStr;
	int i=(int)p->index;
	char tmpstr[20];
	if ( !testpath(i))
	{
			sprintf (tmpstr,"????");
			HStr=NewHandle(strlen(tmpstr));
			memcpy(*HStr, tmpstr, strlen(tmpstr));
			p->result=HStr;
			return (0);	
	}
	else
	{	int n;
		
		if ((Kobj[i]->ReadFormat(&tmpstr[0]))==(int)true)
		{
			HStr=NewHandle(strlen(tmpstr));
			memcpy(*HStr, tmpstr, strlen(tmpstr));
			p->result=HStr;
		}
		else
		{
			XOPNotice("Keith::error in ReadFormat" CR_STR);
			sprintf (tmpstr,"????");
			HStr=NewHandle(strlen(tmpstr));
			memcpy(*HStr, tmpstr, strlen(tmpstr));
			p->result=HStr;

			return 0;
		}
		return (0);
	}

}



//**********************SetDigits FUNCTION*******************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
	struct KeithSetDigitsParams  {
	double Digits;
	double index;
	double result;
};
typedef struct KeithSetDigitsParams KeithSetDigitsParams;
#include "XOPStructureAlignmentReset.h"

static int KeithSetDigits(KeithSetDigitsParams* p)
{
	if ( !testpath((int)p->index))
	{
		p->result=False;
		return (0);
	}
	else
	{

		p->result=Kobj[(int)p->index]->SetDigits((int)p->Digits);
		if ((int)p->result!=false)
			return (0);

		else
		{	
			XOPNotice("Keith::error in SetDigits" CR_STR);
			return (0);
		}	
		return (0);
	}
			
}

//******************************ReadDigits ******************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithReadDigitsParams  {
	double index;
	double result;				
};
typedef struct KeithReadDigitsParams KeithReadDigitsParams;
#include "XOPStructureAlignmentReset.h"
static int KeithReadDigits(KeithReadDigitsParams* p)
{
	int i=(int)p->index;
	if ( !testpath((int)p->index))
	{
		SetNaN64(&p->result);
		return (0);
	}
	else
	{	int n;
		char tmpstr[20];
		if ((Kobj[i]->ReadDigits(&n))==(int)true)
		{
			p->result=(double)n;
			return (0);
		}
		else
		{
			XOPNotice("Keith::error in ReadSample" CR_STR);
			SetNaN64(&p->result);
			return 0;
		}
		return (0);
	}
}

//**********************SetRange FUNCTION*******************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
	struct KeithSetRangeParams  {
	double Range;
	double index;
	double result;
};
typedef struct KeithSetRangeParams KeithSetRangeParams;
#include "XOPStructureAlignmentReset.h"
static int KeithSetRange(KeithSetRangeParams* p)
{
	if ( !testpath((int)p->index))
	{
		p->result=False;
		return (0);
	}
	else
	{

		p->result=Kobj[(int)p->index]->SetRange(p->Range);
		if ((int)p->result!=false)
			return (0);

		else
		{	
			XOPNotice("Keith::error in SetRange" CR_STR);
			return (0);
		}	
		return (0);
	}
			
}

//******************************ReadRange ******************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithReadRangeParams  {
	double index;
	double result;				
};
typedef struct KeithReadRangeParams KeithReadRangeParams;
#include "XOPStructureAlignmentReset.h"
static int KeithReadRange(KeithReadRangeParams* p)
{
	int i=(int)p->index;
	if ( !testpath((int)p->index))
	{
		SetNaN64(&p->result);
		return (0);
	}
	else
	{
		char tmpstr[20];
		if ((Kobj[i]->ReadRange(&(p->result))==(int)true))
		{
			return (0);
		}
		else
		{
			XOPNotice("Keith::error in ReadRange" CR_STR);
			SetNaN64(&p->result);
			return 0;
		}
		return (0);
	}
}



//**********************SetMode FUNCTION*******************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
	struct KeithSetModeParams  {
	Handle Mode;
	double index;
	double result;
};
typedef struct KeithSetModeParams KeithSetModeParams;
#include "XOPStructureAlignmentReset.h"
static int KeithSetMode(KeithSetModeParams* p)
{
	if ( !testpath((int)p->index))
	{
		p->result=False;
		return (0);
	}
	else
	{
		char tmpstr[50];
		int len2 = GetHandleSize(p->Mode);
		memcpy(&tmpstr[0], *p->Mode, len2);	
		tmpstr[len2]=0;

		p->result=Kobj[(int)p->index]->SetMode(tmpstr);
		if ((int)p->result!=false)
			return (0);

		else
		{	
			XOPNotice("Keith::error in SetMode" CR_STR);
			return (0);
		}	
		return (0);
	}
			
}

//******************************ReadMode ******************************************************
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct KeithReadModeParams  {
	double index;
	Handle result;				
};
typedef struct KeithReadModeParams KeithReadModeParams;
#include "XOPStructureAlignmentReset.h"
static int KeithReadMode(KeithReadModeParams* p)
{
	Handle HStr;
	int i=(int)p->index;
	char tmpstr[20];
	if ( !testpath(i))
	{
			sprintf (tmpstr,"????");
			HStr=NewHandle(strlen(tmpstr));
			memcpy(*HStr, tmpstr, strlen(tmpstr));
			p->result=HStr;
			return (0);	
	}
	else
	{	int n;
		
		if ((Kobj[i]->ReadMode(&tmpstr[0]))==(int)true)
		{
			HStr=NewHandle(strlen(tmpstr));
			memcpy(*HStr, tmpstr, strlen(tmpstr));
			p->result=HStr;
		}
		else
		{
			XOPNotice("Keith::error in ReadMode" CR_STR);
			sprintf (tmpstr,"????");
			HStr=NewHandle(strlen(tmpstr));
			memcpy(*HStr, tmpstr, strlen(tmpstr));
			p->result=HStr;

			return 0;
		}
		return (0);
	}

}




//*****************************************************************************************************

static long RegisterFunction()
{
	int funcIndex;

	/*	NOTE:
		Some XOPs should return a result of NIL in response to the FUNCADDRS message.
		See XOP manual "Restrictions on Direct XFUNCs" section.
	*/

	funcIndex = GetXOPItem(0);		/* which function invoked ? */
	switch (funcIndex) {
		case 0:						/* XFUNC1Add(p1, p2) */
			return((long)KeithOpen);
			break;
		case 1:						/* XFUNC1Div(p1, p2) */
			return((long)KeithClose);
			break;
		case 2:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithFreeRun);
			break;
		case 3:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithSetDisplay);
			break;
		case 4:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithReset);
			break;
		case 5:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithSetLowPass);
			break;
		case 6:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithLeaveIddle);
			break;
		case 7:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithEnterIddle);
			break;
		case 8:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithGetReadBufferPoint);
			break;
		case 9:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithGetBufferData);
			break;
		case 10:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithGetBufferSize);
			break;
		case 11:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithSetBuffer);
			break;
		case 12:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithLoadDefault);
			break;
		case 13:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithReadtriggerCount);
			break;
		case 14:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithSetTriggerCount);
			break;
		case 15:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithQuickRead);
			break;
		case 16:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithReadDataFormat);
			break;
		case 17:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithSetDataFormat);
			break;
		case 18:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithGetModel);
			break;
		case 19:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithGetConfiguration);
			break;
		case 20:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithSetScanMode);
			break;
		case 21:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithReadScanMode);
			break;
		case 22:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithSetScanList);
			break;
		case 23:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithReadScanList);
			break;
		case 24:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithSetTrigSource);
			break;
		case 25:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithReadTrigSource);
			break;
		case 26:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithSetChannel);
			break;
		case 27:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithReadChannel);
			break;
		case 28:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithReadData);
			break;
		case 29:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithIsReading);
			break;
		case 30:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithSetAutoZero);
			break;
		case 31:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithReadAutoZero);
			break;
		case 32:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithSetDelay);
			break;
		case 33:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithReadDelay);
			break;
		case 34:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithSetIntegrationTime);
			break;
		case 35:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithReadIntegrationTime);
			break;
		case 36:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithSetTimeout);
			break;
		case 37:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithTrig);
			break;
		case 38:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithSetIntegration);
			break;
		case 39:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithReadIntegration);
			break;
		case 40:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithSetSample);
			break;
		case 41:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithReadSample);
			break;
		case 42:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithSetFormat);
			break;
		case 43:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithReadFormat);
			break;
		case 44:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithSetDigits);
			break;
		case 45:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithReadDigits);
			break;
		case 46:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithSetRange);
			break;
		case 47:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithReadRange);
			break;
		case 48:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithSetMode);
			break;
		case 49:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithReadMode);
			break;
		case 50:						/* XFUNC1ComplexConjugate(p1) */
			return((long)KeithGetBufferTime);
			break;


	} 
	return(NIL);
}

/*	DoFunction()

	This will actually never be called because all of the functions use the direct method.
	It would be called if a function used the message method. See the XOP manual for
	a discussion of direct versus message XFUNCs.
*/
static int DoFunction()
{
	int funcIndex;
	void *p;				/* pointer to structure containing function parameters and result */
	int err;

	funcIndex = GetXOPItem(0);		/* which function invoked ? */
	p = (void *)GetXOPItem(1);		/* get pointer to params/result */
	switch (funcIndex) {
		case 0:						/* XFUNC1Add(p1, p2) */
			err = KeithOpen((KeithOpenParams *)p);
			break;
		case 1:						/* XFUNC1Div(p1, p2) */
			err = KeithClose((KeithCloseParams *)p);
			break;
		case 2:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithFreeRun((KeithFreeRunParams *)p);
			break;
		case 3:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithSetDisplay((KeithSetDisplayParams *)p);
			break;
		case 4:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithReset((KeithResetParams *)p);
			break;
		case 5:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithSetLowPass((KeithSetLowPassParams *)p);
			break;
		case 6:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithLeaveIddle((KeithLeaveIddleParams *)p);
			break;
		case 7:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithEnterIddle((KeithEnterIddleParams *)p);
			break;
		case 8:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithGetReadBufferPoint((KeithGetReadBufferPointParams *)p);
			break;
		case 9:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithGetBufferData((KeithGetBufferDataParams *)p);
			break;
		case 10:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithGetReadBufferPoint((KeithGetReadBufferPointParams *)p);
			break;
		case 11:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithSetBuffer((KeithSetBufferParams *)p);
			break;
		case 12:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithLoadDefault((KeithLoadDefaultParams *)p);
			break;
		case 13:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithReadtriggerCount((KeithReadtriggerCountParams *)p);
			break;
		case 14:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithSetTriggerCount((KeithSetTriggerCountParams *)p);
			break;
		case 15:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithQuickRead((KeithQuickReadParams *)p);
			break;
		case 16:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithReadDataFormat((KeithReadDataFormatParams *)p);
			break;
		case 17:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithSetDataFormat((KeithSetDataFormatParams *)p);
			break;
		case 18:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithGetModel((KeithGetModelParams *)p);
			break;
		case 19:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithGetConfiguration((KeithGetConfigurationParams *)p);
			break;
		case 20:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithSetScanMode((KeithSetScanModeParams *)p);
			break;
		case 21:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithReadScanMode((KeithReadScanModeParams *)p);
			break;
		case 22:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithSetScanList((KeithSetScanListParams *)p);
			break;
		case 23:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithReadScanList((KeithReadScanListParams *)p);
			break;
		case 24:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithSetTrigSource((KeithSetTrigSourceParams *)p);
			break;
		case 25:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithReadTrigSource((KeithReadTrigSourceParams *)p);
			break;
		case 26:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithSetChannel((KeithSetChannelParams *)p);
			break;
		case 27:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithReadChannel((KeithReadChannelParams *)p);
			break;
		case 28:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithReadData((KeithReadDataParams *)p);
			break;
		case 29:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithIsReading((KeithIsReadingParams *)p);
			break;
		case 30:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithSetAutoZero((KeithSetAutoZeroParams *)p);
			break;
		case 31:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithReadAutoZero((KeithReadAutoZeroParams *)p);
			break;
		case 32:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithSetDelay((KeithSetDelayParams *)p);
			break;
		case 33:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithReadDelay((KeithReadDelayParams *)p);
			break;
		case 34:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithSetIntegrationTime((KeithSetIntegrationTimeParams *)p);
			break;
		case 35:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithReadIntegrationTime((KeithReadIntegrationTimeParams *)p);
			break;
		case 36:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithSetTimeout((KeithSetTimeoutParams *)p);
			break;
		case 37:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithTrig((KeithTrigParams *)p);
			break;
		case 38:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithSetIntegration((KeithSetIntegrationParams *)p);
			break;
		case 39:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithReadIntegration((KeithReadIntegrationParams *)p);
			break;
		case 40:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithSetSample((KeithSetSampleParams *)p);
			break;
		case 41:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithReadSample((KeithReadSampleParams *)p);
			break;
		case 42:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithSetFormat((KeithSetFormatParams *)p);
			break;
		case 43:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithReadFormat((KeithReadFormatParams *)p);
			break;
		case 44:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithSetDigits((KeithSetDigitsParams *)p);
			break;
		case 45:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithReadDigits((KeithReadDigitsParams *)p);
			break;
		case 46:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithSetRange((KeithSetRangeParams *)p);
			break;
		case 47:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithReadRange((KeithReadRangeParams *)p);
			break;
		case 48:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithSetMode((KeithSetModeParams *)p);
			break;
		case 49:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithReadMode((KeithReadModeParams *)p);
			break;
		case 50:						/* XFUNC1ComplexConjugate(p1) */
			err = KeithGetBufferTime((KeithGetBufferTimeParams *)p);
			break;
						
	}  
	return(err);
}

/*	XOPEntry()

	This is the entry point from the host application to the XOP for all messages after the
	INIT message.
*/

static void XOPEntry(void)
{	
	long result = 0;

	switch (GetXOPMessage()) {
		case FUNCTION:								/* our external function being invoked ? */
			result = DoFunction();
			break;

		case FUNCADDRS:
			result = RegisterFunction();
			break;
	}
	SetXOPResult(result);
}

/*	main(ioRecHandle)

	This is the initial entry point at which the host application calls XOP.
	The message sent by the host must be INIT.
	main() does any necessary initialization and then sets the XOPEntry field of the
	ioRecHandle to the address to be called for future messages.
*/

HOST_IMPORT void main(IORecHandle ioRecHandle)
{	
	XOPInit(ioRecHandle);							/* do standard XOP initialization */
	SetXOPEntry(XOPEntry);							/* set entry point for future calls */
	
	if (igorVersion < 200)
		SetXOPResult(REQUIRES_IGOR_200);
	else
		SetXOPResult(0L);
	for ( int i=0;i<10;i++)
	{
		Kname[i]=NULL;
		Kobj[i]=NULL;
	}
}
