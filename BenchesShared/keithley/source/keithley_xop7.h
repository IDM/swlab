/*
	keithley_xop7.h
*/
#include "XOPStandardHeaders.h"	

// Custom error codes
#define OLD_IGOR 1 + FIRST_XOP_ERR

/* Prototypes */
//HOST_IMPORT int XOPmain(IORecHandle ioRecHandle);
HOST_IMPORT void main(IORecHandle ioRecHandle);
