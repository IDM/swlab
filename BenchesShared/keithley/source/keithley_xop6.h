/*
	XFUNC1.h -- equates for XFUNC1 XOP
*/
#include "Keithley.h"



/* XFUNC1 custom error codes */

#define REQUIRES_IGOR_200 1 + FIRST_XOP_ERR

/* Prototypes */
HOST_IMPORT void main(IORecHandle ioRecHandle);
