# Axis
## Introduction
Classes for driving axis and stages, e.g. XPS, Anorad linear motors, etc.
The following interfaces are implemented:

- Igor XOP 6, 7 and 8 Win32
- x64 XPS drivers not available
## Directory
Classes for driving axis and stages, e.g. XPS. This directory is organized as follows:

- *.vscode:* Visual Studio Code configuration files
- *config:* configuration files needed for initialization
- *source:* C++ sources (note that /anorad and /mm4006 are for out-of-date drivers)
- *drivers:* drivers
- *igor_proc:* Igor procedure files and *Axis2D* menu
- *vc:* Visual Studio 2017 subprojects and solution files
- *xop:* Igor extension files
  - *res:* Igor resource files
  - *xop6, xop7, xop8:* binaries for Igor extensions

## Dependencies
The following tools are needed:
- Igor XOPToolkits are needed for the Igor interfaces (www.wavemetrics.com)

The repository must be structured as follows:

- *external_libs*
  - *XOPToolkit*
    - *XOPToolkit6*
    - *XOPToolkit7.01*
    - *XOPToolkit8*
- *Benches*
  - BenchesShared
    - *axis (this project)* 

## Compilation

The project can be compiled with Microsoft Visual Studio 2017 and MSBuild 2017. 

After cloning the repository, please update the include directories according to the location of the external libraries (Igor XOPToolkit, etc.) on your computer.

### Visual Studio Code
Open the *axis* folder and type Ctrl+Shift+B, then select the configuration you want.

### Visual Studio
Open the *axis/vc/axis.sln* file and switch to the configuration you want.

### Common errors

In case of compilation or link errors, please check the following items:

- The XOPs directory must contain IGOR.lib and XOPSupport.lib.
- It may be necessary to recompile the XOPSupport libs
- Be sure that the link in the project files corresponds to the directory structure above, or update the links

**Igor XOP 7 and 8 note:** It was necessary to replace the XOPMain() by the legacy main() in the xop.cpp files, to avoid a bug at Igor start. The cause of this problem is not well understood but it seems to work as it is now.

## Configuration

Configuration files are needed for initialization. The default path to configuration files is "c:\\ConfigurationFolder". It can be set to another directory with the *ConfPath* environment variable.

### Igor menus

Please use the *igor_proc\menus_axis_config* procedure file to enable or disable the *Axis2D* and *Axis3D* menus.