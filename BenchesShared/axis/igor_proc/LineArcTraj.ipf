#pragma rtGlobals=1		// Use modern global access method.

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------
//
//	XPS LINE ARC TRAJECTORIES
//	
// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------


// ------------------------------------------------------------------
//  BuildRefCircle(r0, nPoints)
// ------------------------------------------------------------------
Function BuildRefCircle(r0, nPoints)
	Variable r0, nPoints
	
	// Make waves
	Make/o/n=(nPoints) xwRef, zwRef
	// Compute positions
	xwRef = r0*cos(2*pi*x/nPoints)
	zwRef = r0*sin(2*pi*x/nPoints)
	
End

// ------------------------------------------------------------------
// Build4PArcTrajectory(r0, r1, r2, nPts1, nPts2, nPtsTrig, TI, measRatio, nAve)
// 
// Build XPS arc trajectories
//
// INPUTS
//	r0			main circle radius			[mm]
// 	r1			C1 radius				[mm]
//	r2 			C2 radius				[mm]
//	nPts1		number of points for C1 plot
//	nPts2		number of points for C2 plot
//	nPtsTrig		number of trig
//	TI			integration time 			[s] 
//	measRatio 	Meas Length / StepLength
// 	nAve		Number of averages
//
// OUTPUTS
//	xwArc, zwArc 	waves for trajectory plot
//	Trajectory		XPS commands (Text)
//	TrajParam[0]		x0					[mm]
//	TrajParam[1]		z0					[mm]
//	TrajParam[2]		Total Length	(1 turn)	[mm]
//	TrajParam[3]		C2 Length			[mm]
//	TrajParam[4]		C1 Length			[mm]
//	TrajParam[5]		Step Length			[mm]
//	TrajParam[6]		Speed				[mm/s]
//	TrajParam[7]		C2 radius			[mm]
//	TrajParam[8]		C1 radius			[mm]
//	TrajParam[9]		Main radius			[mm]
//	TrajParam[10	]	C2 centre (x)			[mm]
//	TrajParam[11]	Number of trigs
//	TrajParam[12]	Integration time		[s]
//	TrajParam[13]	Meas Length / StepLength
//	TrajParam[14]	Number of averages
//	TrajParam[15] 	Initial Angle			[rad]
// ------------------------------------------------------------------
Function Build4PArcTrajectory(r0, r1, r2, nPts1, nPts2, nPtsTrig, TI, measRatio, nAve)
	Variable r0, r1, r2, nPts1, nPts2, nPtsTrig, TI, measRatio, nAve

	// --- Centre cercle 2
	Variable b = -sqrt(2)*( r0+r1 )
	Variable c = ( r0+r1 )^2 -  (r1 + r2)^2
	Variable delt = b^2 - 4*c
	Variable c2 =  ( -b-sqrt(delt) )/2
	// --- Intersection angle
	Variable theta = atan( (r0+r1 )/( r0+r1-c2*sqrt(2) ) )
	// --- Init Waves and Variables
	Make/o/T/n=1 Trajectory
	Make/o/n=0 xwArc, zwArc
	Make/o/n=(nPts2)  xw1, zw1
	Make/o/n=(nPts1)  xw2, zw2
	Variable k, thetak, q, n
	Variable/c z_
	Variable TrajLength = 0
	// --- First element for acceleration...
	// !!!
	// Before running the trajectory, the wire must be placed a point (r2+c2, 0)
	// !!!
	String cmdStr
	variable i = 0
	// header
	Trajectory[i] = "FirstTangent = 90 ; Degrees"
	InsertPoints i+1, 1, Trajectory; i+=1
	Trajectory[i] = "DiscontinuityAngle = 0.01 ; Degrees"
	// Approach
	InsertPoints i+1, 1, Trajectory; i+=1	
	cmdStr = "Arc = "+num2str(r2)+","+num2str(360-theta*180/pi)
	Trajectory[i] = cmdStr
	// For n averages
	For (n=0; n<nAve; n+=1)
		// --- For Each Quarter
		For (q = 0; q<4; q+=1)
			// ---- C2 ----
			// Write Trajectory Wave
			cmdStr = "Arc = "+num2str(r2)+","+num2str( 2*theta*180/pi )
			InsertPoints i+1, 1, Trajectory; i+=1	
			Trajectory[i] = cmdStr
			// Compute positions for trajectory plot
			For (k = 0; k<nPts2; k+=1)
				thetak = -theta+ 2*theta/(nPts2-1)*k
				z_ = cmplx(c2 + r2* cos(thetak), r2 * sin(thetak) ) * exp(cmplx( 0, q*pi/2 ))
				xw1[k] = real(z_)
				zw1[k] = imag(z_)
			EndFor
			// Length
			TrajLength += abs(r2*2*theta)
			//
			// ---- C1 ----
			// Write Trajectory Wave
			cmdStr = "Arc = "+num2str(r1)+","+num2str(  (pi/2-2*theta) * 180/pi )
			InsertPoints i+1, 1, Trajectory; i+=1	
			Trajectory[i] = cmdStr
			// Compute positions for trajectory plot
			For (k = 0; k<nPts1; k+=1)
				thetak = -pi+theta + (pi/2-2*theta)/(nPts1-1)*k
				z_ = cmplx(sqrt(2)/2*(r0+r1) + r1 * cos(thetak), sqrt(2)/2*(r0+r1) + r1 * sin(thetak) ) * exp(cmplx( 0, q*pi/2 ))
				xw2[k] = real(z_)
				zw2[k] = imag(z_)
			EndFor
			// Length
			TrajLength += abs(r1*(pi/2-2*theta))
			// Store trajectory arc
			Concatenate/o/NP {xwArc, xw1, xw2}, xwArc
			Concatenate/o/NP {zwArc, zw1, zw2}, zwArc	
		EndFor
	EndFor
	// Write Trajectory Wave
	cmdStr = "Arc = "+num2str(r2)+",360"
	InsertPoints i+1, 1, Trajectory; i+=1	
	Trajectory[i] = cmdStr
	killwaves xw1, xw2
	killwaves zw1, zw2
	// Trajectory parameters
	Make/O/n = 15 TrajParam
	TrajParam[0] = c2+r2							// x0
	TrajParam[1] = 0								// z0
	TrajParam[2] = TrajLength/nAve				// Total Length
	TrajParam[3] = abs(r2*2*theta) 					// C2 Length
	TrajParam[4] = abs(r1*(pi/2-2*theta)) 			// C1 Length
	TrajParam[5] = TrajParam[2]/nPtsTrig 			// Step Length
	TrajParam[6] = measRatio*TrajParam[5] / TI		// Speed
	TrajParam[7] = r2								// C2 radius
	TrajParam[8] = r1								// C1 radius
	TrajParam[9] = r0								// C0 radius
	TrajParam[10] = c2							// C2 centre
	TrajParam[11] = nPtsTrig						// number of trig
	TrajParam[12] = TI							// Integration time
	TrajParam[13] = measRatio					// Meas Length / StepLength
	TrajParam[14] = nAve							// Number of averages
	TrajParam[15] = theta							// Initial angle 
	
End

// ------------------------------------------------------------------
// Build4PLineArcTrajectory( r0, r1, r2, d3, nPts1, nPts2, nPts3)
// ------------------------------------------------------------------
Function Build4PLineArcTrajectory( r0, r1, r2, d3, nPts1, nPts2, nPts3)
	Variable r0, r1, r2, d3, nPts1, nPts2, nPts3
	
	// Position cercle 2
	Variable b = -sqrt(2)*(r0+r1)
	Variable c = (r0+r1 )^2 -  (r1+r2)^2 + sqrt(2)*(r0+r1)*(r2-d3) + (r2-d3)^2
	Variable delt = b^2 - 4*c
	Variable y2 =  ( -b -sqrt(delt) )/2

	// Intersection angle
	Variable theta = atan( (r0+r1+sqrt(2)*(r2-d3) )/( r0+r1-sqrt(2)*y2 ) )
	// Init Wave and Variables
	Make/o/n=0 xwLineArc, zwLineArc
	Variable k, thetak, q
	Variable/c z_
	Variable TrajLength = 0
	// Trajectoire
	Make/o/T/n=1 Trajectory
	Make/o/n=(nPts3) xw1, zw1
	Make/o/n=(nPts2) xw2, zw2
	Make/o/n=(nPts1) xw3, zw3
	Make/o/n=(nPts2) xw4, zw4
	Make/o/n=(nPts3) xw5, zw5	
	// --- First element for acceleration...
	// !!!
	// Before running the trajectory, the wire must be placed a point (d3, -y2)
	// !!!
	String cmdStr
	variable i = 0
	// header
	Trajectory[i] = "FirstTangent = -90 ; Degrees"
	InsertPoints i+1, 1, Trajectory; i+=1
	Trajectory[i] = "DiscontinuityAngle = 0.01 ; Degrees"
	// Approach
	InsertPoints i+1, 1, Trajectory; i+=1	
	cmdStr = "Arc = "+num2str(r2)+",360"
	Trajectory[i] = cmdStr
	// --- Pour chaque quadrant
	For	(q =0; q<4; q+=1) 
		// --- Line 1
		// Write Trajectory Wave
		z_ = cmplx(d3,y2)*exp(cmplx(0, q*pi/2)) - cmplx(d3,-y2)
		cmdStr = "Line = "+num2str(real(z_))+","+num2str( imag(z_) )
		InsertPoints i+1, 1, Trajectory; i+=1	
		Trajectory[i] = cmdStr
		// Compute positions for trajectory plot
		For(k=0; k<nPts3; k+=1)
			z_ = cmplx(d3, y2/(nPts3-1)*k ) * exp( cmplx(0, q*pi/2) )
			xw1[k] = real(z_)
			zw1[k] = imag(z_)
		EndFor
		// Length
		TrajLength += 2*y2
		// --- Arc 2
		// Write Trajectory Wave
		cmdStr = "Arc = "+num2str(r2)+","+num2str(  - (pi/2-theta) * 180/pi )
		InsertPoints i+1, 1, Trajectory; i+=1	
		Trajectory[i] = cmdStr
		// Compute positions for trajectory plot		
		For(k=0; k<nPts2; k+=1)
			thetak =  (pi/2 - theta) /(nPts2-1)*k
			z_ = cmplx(d3-r2 + r2 * cos(thetak), y2 + r2 * sin(thetak) ) * exp( cmplx(0, q*pi/2) )
			xw2[k] = real(z_)
			zw2[k] = imag(z_)
		EndFor
		// Length
		TrajLength += r2 * (pi/2 - theta)
		// --- Arc 1
		// Write Trajectory Wave
		cmdStr = "Arc = "+num2str(r1)+","+num2str(  (pi/2-2*theta) * 180/pi )
		InsertPoints i+1, 1, Trajectory; i+=1	
		Trajectory[i] = cmdStr
		// Compute positions for trajectory plot	
		For(k=0; k<nPts1; k+=1)
			thetak = -pi/2-theta - (pi/2-2*theta)/(nPts1-1)*k
			z_ = cmplx( sqrt(2)/2*(r0+r1) + r1 * cos(thetak), sqrt(2)/2*(r0+r1) + r1 * sin(thetak) ) * exp( cmplx(0, q*pi/2) )
			xw3[k] = real(z_)
			zw3[k] = imag(z_)		
		EndFor
		// Length
		TrajLength += r1*(pi/2-2*theta)
		// --- Arc 2 bis
		// Write Trajectory Wave
		cmdStr = "Arc = "+num2str(r2)+","+num2str(  - (pi/2-theta) * 180/pi )
		InsertPoints i+1, 1, Trajectory; i+=1	
		Trajectory[i] = cmdStr
		// Compute positions for trajectory plot		
		For(k=0; k<nPts2; k+=1)
			thetak = theta+ (pi/2-theta) /(nPts2-1)*k
			z_ = cmplx(y2 + r2 * cos(thetak), d3-r2 + r2 * sin(thetak) ) * exp( cmplx(0, q*pi/2) )
			xw4[k] = real(z_)
			zw4[k] = imag(z_)
		EndFor
		// Length
		TrajLength += r2 * (pi/2-theta)
		// --- Line 1 bis
		// Write Trajectory Wave
		// ..... Deja trace, car "Line1" va de (d3, -y2) a (d3, y2)
		// et les calculs de positions sont faits pour les 1/2 segments 	
		// (d3, 0) a (d3, y2) et (d3, -y2) a (d3, 0)
		// Compute positions for trajectory plot	
		For(k=0; k<nPts3; k+=1)
			z_ = cmplx(y2 - y2/(nPts3-1)*k , d3 ) * exp( cmplx(0, q*pi/2) )
			xw5[k] = real(z_)
			zw5[k] = imag(z_)
		EndFor
		Concatenate/o/NP {xwLineArc, xw1, xw2, xw3, xw4, xw5}, xwLineArc
		Concatenate/o/NP {zwLineArc, zw1, zw2, zw3, zw4, zw5}, zwLineArc	
	EndFor // Pour chaque quadrant
	killwaves xw1, xw2, xw3, xw4, xw5
	killwaves zw1, zw2, zw3, zw4, zw5
	// Outputs
	Make/O/n = 3 TrajParam
	TrajParam[0] = d3
	TrajParam[1] = 0
	TrajParam[2] = TrajLength
	
End

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------
//
//	DATA PROCESSING
//	
// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------
Function	Build4PArcMatrix( TrajParam, xPos, zPos )
	Wave TrajParam, xPos, zPos
	
	//-----------------------------------------------------------
	// Read Trajectory Parameters
	//-----------------------------------------------------------
	Variable TrajLength 	= TrajParam[2]	// Total Length
	Variable C2Length  	= TrajParam[3]	// C2 Length
	Variable	C1Length  	= TrajParam[4]	// C1 Length
	Variable StepLength	= TrajParam[5]	// Step Length
	Variable r2		   	= TrajParam[7]	// C2 radius
	Variable r1		   	= TrajParam[8]	// C1 radius
	Variable r0		  	= TrajParam[9]	// Main radius
	Variable c2			= TrajParam[10]	// C2 centre (c2, 0)
	Variable nPtsTrig	   	= TrajParam[11]	// number of trigs
	Variable measRatio	= TrajParam[13]	// Meas Length / StepLength

	//-----------------------------------------------------------
	// Arc Parameters
	//-----------------------------------------------------------
	Make/O/n=(nPtsTrig)  CircleType 
	Make/O/n=(nPtsTrig)  CircleID 	
	Make/O/C/n=(nPtsTrig) CircleCentre 
	Make/O/n=(nPtsTrig)	CircleRadius 
	Make/O/n=(nPtsTrig)	dTheta	
	Variable k
	Variable n1 = ceil(C1Length / StepLength)
	Variable n2 = floor(C2Length / StepLength)
	Variable dTheta1 = - C1Length / (2*pi*r1*n1)
	Variable dTheta2 = C2Length / (2*pi*r2*n2)	
	For(k = 0; k<nPtsTrig; k+=1)
		If (k<n2)
			CircleType[k] = 0		// Arc 20
			CircleID[k] = 20
			CircleRadius[k] = r2
			CircleCentre[k] = cmplx(c2, 0)	
			dTheta[k] = dTheta2
		ElseIf ( (k-n2) < n1 )
			CircleType[k] = 1		// Arc 10
			CircleID[k] = 10
			CircleRadius[k] = r1
			CircleCentre[k] = sqrt(2)/2*cmplx(r0+r1, r0+r1)	
			dTheta[k] = dTheta1
		ElseIf  ( (k-n2-n1) < n2 )
			CircleType[k] = 0		// Arc 21
			CircleID[k] = 21
			CircleRadius[k] = r2
			CircleCentre[k] = cmplx(0, c2)	
			dTheta[k] = dTheta2
		ElseIf  ( (k-2*n2-n1) < n1 )
			CircleType[k] = 1		// Arc 11
			CircleID[k] = 11
			CircleRadius[k] = r1
			CircleCentre[k] = sqrt(2)/2*cmplx(-r0-r1, r0+r1)	
			dTheta[k] = dTheta1
		ElseIf  ( (k-2*n2-2*n1) < n2 )
			CircleType[k] = 0		// Arc 22
			CircleID[k] = 22
			CircleRadius[k] = r2
			CircleCentre[k] = cmplx(-c2, 0)		
			dTheta[k] = dTheta2		
		ElseIf  ( (k-3*n2-2*n1) < n1 )
			CircleType[k] = 1		// Arc 12	
			CircleID[k] = 12
			CircleRadius[k] = r1
			CircleCentre[k] = sqrt(2)/2*cmplx(-r0-r1, -r0-r1)	
			dTheta[k] = dTheta1				
		ElseIf  ( (k-3*n2-3*n1) < n2 )
			CircleType[k] =0		// Arc 23
			CircleID[k] = 23
			CircleRadius[k] = r2
			CircleCentre[k] = cmplx(0, -c2)		
			dTheta[k] = dTheta2		
		ElseIf  ( (k-4*n2-3*n1) < n1 )
			CircleType[k] =1		// Arc 13
			CircleID[k] = 13	
			CircleRadius[k] = r1
			CircleCentre[k] = sqrt(2)/2*cmplx(r0+r1, -r0-r1)	
			dTheta[k] = dTheta1									
		EndIf
	EndFor

	//-----------------------------------------------------------
	// Compute measurement points from trig points
	//-----------------------------------------------------------
	Make/C/O/n=(nPtsTrig) zMeas
	Make/C/O/n=(nPtsTrig) zetaw
	Make/O/n=(nPtsTrig) thetaMeas =0
	Variable thetak
	For(k=0; k<nPtsTrig; k+=1)
		zMeas[k] = ( cmplx(xPos[k], zPos[k]) - CircleCentre[k] ) * exp( cmplx( 0, measRatio*dTheta[k]/2 ) ) + CircleCentre[k]
		zetaw[k] = ( cmplx(xPos[k], zPos[k]) - CircleCentre[k] ) * exp( cmplx( 0, (1+measRatio)*dTheta[k] /2) ) + CircleCentre[k]
		//thetak = imag( r2polar( zMeas[k] - CircleCentre[k] ) ) 
		//thetaMeas[k] = thetak * (1-CircleType[k])+ (thetak) * CircleType[k]
	EndFor
	
	
End