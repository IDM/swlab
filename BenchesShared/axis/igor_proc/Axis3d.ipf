#pragma rtGlobals=3		// Use modern global access method and strict wave access.

#if defined(HALL3D_ENABLE)
Menu "HALL3d"
	"initHall3d"
	"CloseHall3D"
	"-"
	Submenu "Axis3d"
		"Open",AXIS3DOpen("XPS3D")
		"Close" ,Axis3dClose(root:XPS3d:path)
		"Is Ready ?",F_axis3dIsready()
		"Abort",axis3dAbort(root:XPS3d:path)
		"Readpos",F_Axis3dReadPos()
		"Setpos",P_Axis3dSetpos()
		"SetVelocity",P_Axis3dSetvelocity()
		"ReadVelocity",F_Axis3dReadvelocity()
		"SetAcceleration",P_Axis3dSetAcceleration()
		"ReadAcceleration",F_Axis3dReadAcceleration()
		"-"
		"ExecSpline",P_Axis3dExecSplin()
		"Get GatheringData",F_GetGathering()
	End
	"RunScanHall3D",RunScanHall3D(,)
end

#endif

function F_Axis3dReadPos()
	NVAR path=root:XPS3d:path
	Axis3dReadpos(path)
	Wave Pos=root:XPS3d:Pos
	print Pos

end

proc P_Axis3dSetPos(X,Z,S)
	variable X=Nan
	variable Z=Nan
	variable S=Nan	
	make /O/N=(3)  root:XPS3D:SetPos

	
	root:XPS3D:Setpos[0]=X;root:XPS3D:SetPos[1]=Z;root:XPS3D:SetPos[2]=S
	Axis3DSetPos(root:XPS3d:path,root:XPS3D:SetPos)
	killwaves root:XPS3D:SetPos
end

proc P_Axis3dSetvelocity(V)
	variable V	
		
	Axis3DSetVelocity(root:XPS3d:path,V)
end

function F_Axis3dReadVelocity()
	NVAR path=root:XPS3d:path
	variable vel
	vel= Axis3dReadVelocity(path)
	print vel
	return vel
end

proc P_Axis3dSetAcceleration(Acc)
	variable Acc	
		
	Axis3DSetAcceleration(root:XPS3d:path,Acc)
end

function F_Axis3dReadAcceleration()
	NVAR path=root:XPS3d:path
	variable acc
	acc= Axis3dReadAcceleration(path)
	print acc
	return acc
end

function F_axis3dIsready()
NVAR path=root:XPS3d:path
if (axis3disready(path))
	print "Ready "
else
	print "Not ready"
endif
	
end

proc P_Axis3dExecSplin(WX,WZ,WY,vel,acc,trigstep)
string WX
string WZ
string WY
variable vel
variable acc
variable trigStep

//NVAR path=root:XPS3d:path

	if(!Axis3dCreateTraj(root:XPS3d:path,$WX,$WZ,$WY))
		return	
	endif
	 Axis3DRunTrajAndTrig(root:XPS3d:path,vel,acc,trigstep)
 
end

Function F_GetGathering()

NVAR path=root:XPS3d:path
	Axis3DGetGatheringData(path)
end


// HALL3D Function and procs

Function initHall3d()
	AXIS3DOpen("XPS3D")
	keithopen ("KX")
	keithopen ("KZ")
	keithopen ("KS")
	newdatafolder /O root:HALL
	String /G root:HAll:Hall_file
end

proc  CloseHall3D()
	Axis3dClose ( root:XPS3d:Path)
	KeithClose(root:KX:Path)
	KeithClose(root:KZ:Path)
	KeithClose(root:KS:Path)
end

function SetKeithHall3D(path, NB)
	variable path
	variable NB
	

	KeithEnterIddle(path)
	KeithSetTrigSource(path,"EXT")
	KeithSetTriggerCount(path,NB)
	KeithSetBuffer(path,NB)
	keithSetTimeout(path,10)
	KeithSetDisplay(path,1)
	KeithLeaveIddle(path)
	

end


Function TrajLen(XW,ZW,YW)
wave XW,ZW,YW
if( (numpnts(XW)!=numpnts(ZW)) || (numpnts(XW)!=numpnts(YW)) )
	print "the wave must have the same length"
	return Nan
endif
variable i

variable npt=numpnts(ZW)
variable len=0
for (i=1;i<npt;i+=1)
	len=len+ sqrt((XW[i]-XW[i-1])^2+(ZW[i]-ZW[i-1])^2+(YW[i]-YW[i-1])^2)
endfor 

return len
end

Function H_POS_Calc(XW,ZW,YW)
wave XW,ZW,YW
if( (numpnts(XW)!=numpnts(ZW)) || (numpnts(XW)!=numpnts(YW)) )
	print "the wave must have the same length"
	return Nan
endif
variable i
wave H_pos=root:Hall:h_pos
variable npt=numpnts(ZW)
variable len=YW[0]
for (i=1;i<npt;i+=1)
	len=len+ sqrt((XW[i]-XW[i-1])^2+(ZW[i]-ZW[i-1])^2+(YW[i]-YW[i-1])^2)
	H_pos[i]=len
endfor 

return len-YW[0]
end




proc RunScanHall3D(Step,Vel)
variable Step
variable Vel

	variable  len=TrajLen(root:HALL:XPOS,root:Hall:ZPOS,root:Hall:YPOS)*1.1
	if(!Axis3dCreateTraj(root:XPS3d:path,root:Hall:XPOS,root:Hall:ZPOS,root:Hall:YPOS))
			print "Create 3D Trajectory error"
			return	
		endif
	variable trgpt=ceil(len/step)
	
	SetKeithHall3D(root:KS:Path, trgpt)
	SetKeithHall3D(root:KX:Path, trgpt)
	SetKeithHall3D(root:KZ:Path, trgpt)

	 Axis3DRunTrajAndTrig(root:XPS3d:path,Vel,150,step)

	CtrlNamedBackground HScan3d, period=120, proc=HScan3d_BG
	CtrlNamedBackground HScan3d, start
end

Function HScan3d_BG(s)		// This is the function that will be called periodically
	STRUCT WMBackgroundStruct &s
	NVAR PATH=root:XPS3d:Path
	if(!Axis3disReady(Path))
		return 0	
	else
//		Execute "P_Axis3dSetpos(0,0,0)"
		Execute "GetHallScan3dRes()"
		return (-1)
		
	endif
End

proc GetHallScan3dRes()

	Axis3DGetGatheringData(root:XPS3D:path)
	variable npt=numpnts(root:XPS3D:XPOS)
	KeithGetBufferData(root:KX:Path,npt)
	KeithGetBufferData(root:KZ:Path,npt)
	KeithGetBufferData(root:KS:Path,npt)
	

	duplicate /O root:XPS3D:XPOS, root:Hall:XPOS
	duplicate /O root:XPS3D:ZPOS, root:Hall:ZPOS
	duplicate /O root:XPS3D:YPOS, root:Hall:YPOS
	duplicate /O root:XPS3D:YPOS, root:Hall:h_pos
	
	duplicate /O root:KX:BDATA, root:Hall:H_BX
	duplicate /O root:KZ:BDATA, root:Hall:H_BZ
	duplicate /O root:KS:BDATA, root:Hall:H_BS

	
	print "len =",H_pos_calc(root:Hall:XPOS,root:Hall:ZPOS,root:Hall:YPOS)
end

Proc ScanPrepare(indice,folder)
	variable indice
	string folder

STRING TrajX,TrajZ
	TrajX="root:"+folder+":BZ"+num2str(indice)+"CAT"
	TrajZ="root:"+folder+":BX"+num2str(indice)+"CAT"
	root:HALL:XPOS=$TrajX(root:HALL:YPOS[p]/1000)*1000	
	root:HALL:ZPOS=$TrajZ(root:HALL:YPOS[p]/1000)*1000
//	root:HALL:XPOS/=2
//	root:HALL:ZPOS/=2	
end

Proc XSCAN(Y,Z,XB,XE,XStep,XVelocity)
variable Y,Z,XB,XE,XStep,XVelocity

make /o/N=2 root:HALL:XPOS, root:HALL:ZPOS, root:HALL:YPOS,
 root:HALL:XPOS[0]=XB
 root:HALL:XPOS[1]=XE
  root:HALL:ZPOS=Z
  root:HALL:YPOS=Y
  RunScanHall3D(XStep,XVelocity)

end

Proc getXSCAN(indice)
variable indice

string DestW


DestW="H_BX"+num2str(indice)
duplicate/O root:HALL:H_BX $DESTW
DestW="H_BZ"+num2str(indice)
duplicate/O root:HALL:H_BZ $DESTW
DestW="H_BS"+num2str(indice)
duplicate/O root:HALL:H_BS $DESTW
DestW="XPOS"+num2str(indice)
duplicate/O root:HALL:XPOS $DESTW
DestW="ZPOS"+num2str(indice)
duplicate/O root:HALL:ZPOS $DESTW
DestW="YPOS"+num2str(indice)
duplicate/O root:HALL:YPOS $DESTW

end



function SSCANPOSCALC(L0,LS,DL)
	variable l0,ls,dl
	
	variable i
	make /O/N=10 YSCANPOS
	for (i=0;i<5;i+=1)
		YSCANPOS[2*i]=L0+i*DL
		YSCANPOS[2*i+1]=YSCANPOS[2*i]+LS
	endfor
end

function findcenter(indice,lev)
variable indice, lev

string bzstr="H_BZ" +num2str(indice)
string bz_L= "H_BZ" +num2str(indice)+"_L" 
string XP="XPOS"+num2str(indice)
Interpolate2/T=1/N=1000/Y=$BZ_L $XP, $bzstr
wavestats /Q $BZ_L

variable tstlev=v_min*lev
findlevels /Q /edge=0 $BZ_L,tstlev 
wave FL=w_FindLevels
// print FL
// print " Center : " ,(w_FindLevels[0]+w_FindLevels[1])/2
 return (FL[0]+FL[1])/2
end

function findZCenter(lastind)
variable lastind
variable ind
make/O  /N=(lastind+1) SDEV
wave sdev=sdev
for (ind=0;ind<lastind+1;ind+=1)
	string bXstr="H_BX" +num2str(ind)
	wavestats /Q $BXstr
	SDEV[ind]=V_sdev
endfor
end