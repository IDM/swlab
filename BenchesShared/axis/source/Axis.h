// Axis.h: interface for the Axis class.
//
//////////////////////////////////////////////////////////////////////
//#include "Anorad.h"
//#include "MM4006.h"
#include "XPS.h"
#if !defined(AFX_AXIS_H__2C036225_2D77_11D4_BFF6_00A02459FF70__INCLUDED_)
#define AFX_AXIS_H__2C036225_2D77_11D4_BFF6_00A02459FF70__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


class Axis  
{
public:
	virtual int RelMoveOne(int ax,double depl);
	virtual int ReadRef( double *refpos);
	virtual int ReadAcceleration(double *accel);
	virtual int MovingTime(double depl,double* t);
	virtual int MoveAndTrig(double beg,double fin,double step,double over);
	virtual int Move_1Trig(double Pos,double TrigPos);
	virtual int SetTaper(double Taper);
	virtual int SetDefaultTaper(double Taper);
	virtual int SetSpeed(double Speed);
	virtual int SetDefaultPosition(double Pos);
	virtual int SetAcceleration(double accel);
	virtual int Reference();
	virtual int ReadTaper(double *Taper);
	virtual int ReadSpeed(double * speed);
	virtual int SetExternalLatching();
	virtual int RelMove(double Disp);
	virtual int ReadPosition(double *Pos);
	virtual int ReadExternallyLatchedPosition(double *Pos);
	virtual int PositiveLimit();
	virtual int NegativeLimit();
	virtual bool IsValid();
	virtual bool IsMoving();
	virtual int InitOpticalRule();
	virtual int AbsMove(double RequestedPosition);
	 Axis(char *Name);
	virtual int Abort();
	Axis();
	virtual ~Axis();
	Axis * GenAxis;

private:
	int GetConfiguration(char *Name);
	bool Valid;
	char axis_type[20];
	
//@cmember A debug function to output string to the debug console.
//	void WriteToConsole(char *);
//	char DebugStr[256];

};

#endif // !defined(AFX_AXIS_H__2C036225_2D77_11D4_BFF6_00A02459FF70__INCLUDED_)
