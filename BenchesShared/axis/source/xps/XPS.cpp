// XPS.cpp: implementation of the XPS class.
//
//////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include "XPS.h"
#include <process.h>
#include <string.h>
#include <math.h>
#include <windows.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

XPS::XPS(char *Name)
{

if(!GetConfiguration(Name))
{
	printf ("can't get configuartion for %s\n",Name);
}

comupath=TCP_ConnectToServer(comu_path_name,comport,timeout);
if (-1==comupath)
{
	printf ( "error in connect\n");
	return;
}
refsock=TCP_ConnectToServer(comu_path_name,comport,20000);
if (-1==comupath)
{
	printf ( "error in connect\n");
	return;
}
mvsock=TCP_ConnectToServer(comu_path_name,comport,20000);
if (-1==comupath)
{
	printf ( "error in connect\n");
	return;
}
int error,status;
error=GroupStatusGet(comupath,AxisNum,&status);
{

}
refmove=false;
if (!READY(status))
{
	Reference();
	while(refmove)
		Sleep(100);
}

//Set_KFactors();
	
SetSpeed(DefaultSpeed);
SetAcceleration(DefaultAcceleration);
SetDefaultTaper(RefTaper);

Valid=true;
}

XPS::~XPS()
{
//	comupath->Write("MF\r");
//	delete comupath;
	TCP_CloseSocket(comupath);
	TCP_CloseSocket(refsock);
	TCP_CloseSocket(mvsock);

}

int XPS::GetConfiguration(char *Name)
{
//**********************************************************
// Define some structures used for resources extraction.
//**********************************************************
typedef struct _Res
{
	char name[50];
	char val[50];
}Res;

enum Devices 
{
  AXE_TYPE, 
    AXE_NUM, 
    COMU_PATH,
	COMU_PORT,
	COMU_TIMEOUT,
    DEF_SPEED,
	DIRECTION,
    DEF_ACC, 
    DEF_POS,
	DEF_TAPER,
	KI,
	KD,
	KP
};

Res CtrlResTab[] = 
{
  {"AXE_TYPE",       ""},
  {"AXE_NUM",        ""}, 
  {"COMU_PATH",      ""},
  {"COMU_PORT",      ""},
  {"COMU_TIMEOUT",      ""},
  {"DEF_SPEED",      ""},
  {"DIRECTION",		 ""},
  {"DEF_ACC",        ""},
  {"DEF_POS",        ""},
  {"DEF_TAPER",		 ""},
  {"KI_FACT",		 ""},
  {"KD_FACT",		 ""},
  {"KP_FACT",		 ""}
};
	
	FILE   *Fin;
	char   *Temp;
	char    Buf[1000];
	char    ConfStr[128];
	char    FileName[256];
	int     i, NbRes;
	int     status;
	

	status = GetEnvironmentVariable("ConfPath", ConfStr, 100);

  if(status == 0)  // Environment variable not defined.
    sprintf(FileName, "%s\\%s.conf", "c:\\ConfigurationFolder", Name);
  else
    sprintf(FileName, "%s\\%s.conf", ConfStr, Name);
	
  Fin = fopen(FileName, "r");
  if(Fin == NULL)
	{
		return(false);
	}

	//--------------------------------------------
	// Get resource names from configuration file.

  fread(Buf, sizeof(Buf), 1, Fin);
	NbRes = sizeof(CtrlResTab) / sizeof(Res);

	fclose(Fin);
  for(i=0; i<NbRes; i++)
  {
		if((Temp = strstr(Buf, CtrlResTab[i].name)) == NULL)
		{
			return(false);
		}
		else
		{
			sscanf(Temp, "%*s%s", &(CtrlResTab[i].val));
		}
  }

	strcpy(comu_path_name, CtrlResTab[COMU_PATH].val);
	comport=atoi(CtrlResTab[COMU_PORT].val);
	timeout=1000*atof(CtrlResTab[COMU_TIMEOUT].val);
	strcpy(AxisNum, CtrlResTab[AXE_NUM].val);
	nb_axis=strlen(AxisNum);
	DefaultSpeed        =  atof(CtrlResTab[DEF_SPEED].val);
	DefaultAcceleration =  atof(CtrlResTab[DEF_ACC].val);
	RefPosition     =  atof(CtrlResTab[DEF_POS].val);
	RefTaper     =  atof(CtrlResTab[DEF_TAPER].val);
	SetTap=0;
	P_KD=atof(CtrlResTab[KD].val);
	P_KI=atof(CtrlResTab[KI].val);
	P_KP=atof(CtrlResTab[KP].val);
	for(i=0;i<nb_axis;i++)
	{
		if(CtrlResTab[DIRECTION].val[i]=='-')
			direction[i]=-1;
		else
			direction[i]=1;
	}
	AxisType=atoi(CtrlResTab[AXE_TYPE].val);
	
	return(true);
}

static void backRef(void *p)
{


	XPS *pt;
	pt=(XPS *)p;
	int error;


	error =GroupKill(pt->refsock,pt->AxisNum);
	if (0!=error)
	{
		printf ( "error in kill\n");
		pt->refmove=false;
		_endthread();
	}
	error =GroupInitialize (pt->refsock,pt->AxisNum);
	if (0!=error)
	{
		printf ( "error in Initialize\n");
		pt->refmove=false;
		_endthread();
	}
	error =GroupHomeSearch (pt->refsock,pt->AxisNum);
	if (0!=error)
	{
		printf ( "error in Initialize\n");
	
	}
	pt->refmove=false;
	_endthread();

}


int XPS::Reference()
{
	//Reference done in thread because there is some time 
	//where motor are stop during reference
	if (IsMoving())
		return false;

	refmove=true;
	if(_beginthread(backRef,0,(void *)this)==-1)
	{
		refmove=false;
		return (false);
	}	
	return (true);
}

bool XPS::IsMoving()
{


	if (refmove)
		return true;

		return ReallyMove();

}


bool XPS::ReallyMove()
{
	int error,status;
	error=GroupStatusGet(comupath,AxisNum,&status);
	if(0!=error)
	{
		return true;
	}

	return (status==44);
//	return (!READY(status));
}
static void backMove(void *p)
{


	XPS *pt;
	pt=(XPS *)p;



	double TargetPosition[8];
	int i,err;
	for (i=0;i<pt->nb_axis;i++)
		TargetPosition[i]=(pt->SetPos-pt->RefPosition)*pt->direction[i];	

	if (pt->nb_axis==2)
	{	
		TargetPosition[0]+=(pt->RefTaper/2+pt->SetTap/2)*pt->direction[0];
		TargetPosition[1]-=(pt->RefTaper/2+pt->SetTap/2)*pt->direction[1];		
	}

	err=GroupMoveAbsolute (pt->mvsock, pt->AxisNum, pt->nb_axis,TargetPosition);
	char master[10];
	sprintf (master,"%s.0",pt->AxisNum);
	err=PositionerPositionCompareDisable (pt->comupath, master);// Disable position compare
	
	_endthread();

}
int XPS::AbsMove(double Pos)
{
	if (IsMoving())
		return false;
	SetPos=Pos;
	if(_beginthread(backMove,0,(void *)this)==-1)
	{
		return (false);
	}	
	return (true);
	
}

int XPS::ReadPosition(double *Pos)
{

	double CurrentEncoderPosition[8];
	int i,err;

	err=GroupPositionCurrentGet (comupath, AxisNum, nb_axis,CurrentEncoderPosition);

	*Pos=0;
	for (i=0;i<nb_axis;i++)
		*Pos+=CurrentEncoderPosition[i]*direction[i];	
	
	*Pos/=nb_axis;
	*Pos+=RefPosition;
	return (true);
}

int XPS::SetSpeed(double Speed)
{
	double Jerk1,Jerk2;
	int err,i;

	char axis[10];
	double A,S;
	sprintf (axis,"%s.0",AxisNum);


	err=PositionerSGammaParametersGet (comupath, axis, &S,&A,&Jerk1,&Jerk2);
	if(0!=err)
	{
		return false;
	}
	DefaultSpeed=Speed;
	S=Speed;

	for (i=0;i<nb_axis;i++)
	{
		sprintf (axis,"%s.%i",AxisNum,i);
		err=PositionerSGammaParametersSet(comupath, axis, S,A,Jerk1,Jerk2);
		if(0!=err)
		{
			return false;
		}
	}
	return (true);
}

int XPS::Set_KFactors()
{


	return (true);
		
}

int XPS::Abort()
{
 
	int err;
	err=GroupMoveAbort (comupath, AxisNum);
	if(0!=err)
	{
		return false;
	}
	refmove=false;

return true;

}

int XPS::Move_1Trig(double Pos, double TrigPos)
{
	//double beg, double end, double step,double over
		char master[10];
	int err;
	sprintf (master,"%s.0",AxisNum);
	if (IsMoving())
		return false;
	SetPos=Pos;
	double corrbeg,corrend;
	
	corrbeg=(TrigPos-RefPosition)*direction[0];
	corrend=(Pos-RefPosition)*direction[0];
	corrbeg=corrbeg+(RefTaper/2+SetTap/2)*direction[0];
	corrend=corrend+(RefTaper/2+SetTap/2)*direction[0];
	err=PositionerPositionCompareDisable (comupath, master);// Disable position compare	
	if (corrbeg >corrend)
	{
	err=PositionerPositionCompareDisable (comupath, master);// Disable position compare
	err=PositionerPositionCompareSet (comupath, master, corrbeg -.003,(corrbeg ), .002); // Set position compare parameters

	err=PositionerPositionCompareEnable (comupath, master);// Enable position compare
	
	AbsMove(Pos);
	}
	else
	{
	err=PositionerPositionCompareDisable (comupath, master);// Disable position compare
	err=PositionerPositionCompareSet (comupath, master, (corrbeg-.003), corrbeg,.002); // Set position compare parameters
	err=PositionerPositionCompareEnable (comupath, master);// Enable position compare
	
	AbsMove(Pos);

	}
	
	return (true);
}

bool XPS::IsValid()
{
	return Valid;
}


int XPS::NegativeLimit()
{

	return (true);
}

int XPS::ReadSpeed(double *speed)
{
	double Jerk1,Jerk2;
	char master[10];
	int err;
	sprintf (master,"%s.0",AxisNum);

	err=PositionerSGammaParametersGet (comupath, master, &DefaultSpeed,&DefaultAcceleration,&Jerk1,&Jerk2);
	*speed=DefaultSpeed;
	return (true);
}

int XPS::ReadTaper(double *Taper)
{

	double CurrentEncoderPosition[8];
	int err;

	err=GroupPositionCurrentGet (comupath, AxisNum, nb_axis,CurrentEncoderPosition);

	*Taper=CurrentEncoderPosition[0]*direction[0]-CurrentEncoderPosition[1]*direction[1];
	*Taper-=RefTaper;
	return (true);
}
static void backRelMove(void *p)
{


	XPS *pt;
	pt=(XPS *)p;

	double TargetPosition[8];
	int i,err;
	for (i=0;i<pt->nb_axis;i++)
		TargetPosition[i]=pt->SetPos*pt->direction[i];	

	err=GroupMoveRelative (pt->mvsock, pt->AxisNum, pt->nb_axis,TargetPosition);

	_endthread();

}
int XPS::RelMove(double mo)
{

	if (IsMoving())
		return false;
	SetPos=mo;
	if(_beginthread(backRelMove,0,(void *)this)==-1)
	{
		return (false);
	}	
	return (true);
	
}

int XPS::SetAcceleration(double accel)
{
	double Jerk1,Jerk2;
	int err,i;

	char axis[10];


	sprintf (axis,"%s.0",AxisNum);
	double S,A;

	err=PositionerSGammaParametersGet (comupath, axis, &S,&A,&Jerk1,&Jerk2);
	if(0!=err)
	{
		return false;
	}
	DefaultAcceleration=accel;
	A=accel;
	for (i=0;i<nb_axis;i++)
	{
		sprintf (axis,"%s.%i",AxisNum,i);
		err=PositionerSGammaParametersSet(comupath, axis, S,A,Jerk1,Jerk2);
		if(0!=err)
		{
			return false;
		}
	}

	return (true);
}

int XPS::SetDefaultPosition(double Pos)
{
	RefPosition=direction[0]*Pos;
	return true;
}

int XPS::SetDefaultTaper(double Taper)
{
double pos;
	ReadPosition(&pos);
	RefTaper=Taper;
	AbsMove(pos);
	return true;	


}

int XPS::SetTaper(double Taper)
{
double pos;
	
	ReadPosition(&pos);
	SetTap=Taper;
	AbsMove(pos);
	return true;

}

int XPS::MovingTime(double depl, double *t)
{

	double v,a;

	if (!Valid)
		return (false);


	if(!ReadAcceleration(&a))
	{
		*t=0;
		return false;
	}
	if(!ReadSpeed(&v))
	{
		*t=0;
		return false;
	}
	if (depl>v*v/a)
		*t=(depl+v*v/a)/v;
	else
		*t=2*sqrt(a*depl)/a;
	return true;

}

int XPS::ReadAcceleration(double *accel)
{
	double Jerk1,Jerk2;
	char master[10];
	int err;
	sprintf (master,"%s.0",AxisNum);

	err=PositionerSGammaParametersGet (comupath, master, &DefaultSpeed,&DefaultAcceleration,&Jerk1,&Jerk2);
	*accel=DefaultAcceleration;
	return (true);

}

int XPS::ReadRef(double *refpos)
{
	*refpos=RefPosition;
	return true;
}

bool XPS::READY(int status)
{
	return ( status>9 && status<19);
}

int XPS::MoveAndTrig(double beg, double end, double step,double over)
{
	char master[10];
	int err;
	sprintf (master,"%s.0",AxisNum);
	if (IsMoving())
		return false;
	SetPos=end;
	double corrbeg,corrend;
	
	corrbeg=(beg-RefPosition)*direction[0];
	corrend=(end-RefPosition)*direction[0];
	corrbeg=corrbeg+(RefTaper/2+SetTap/2)*direction[0];
	corrend=corrend+(RefTaper/2+SetTap/2)*direction[0];
	

	
	err=PositionerPositionCompareDisable (comupath, master); // Disable position compare	
	if (end >beg)
	{
	AbsMove(beg-over);
	do
		Sleep(50);
	while(IsMoving());
	//set pulse length to 10us to trig keithley.
	err=PositionerPositionComparePulseParametersSet (comupath, master, 10, 0.075);
	err=PositionerPositionCompareSet (comupath, master, min(corrbeg,corrend),max(corrbeg, corrend), step); // Set position compare parameters
	err=PositionerPositionCompareEnable (comupath, master);// Enable position compare
	
	AbsMove(end+over);
	}
	else
	{
	AbsMove(beg+over);
	do
		Sleep(50);
	while(IsMoving());
	err=PositionerPositionCompareDisable (comupath, master);// Disable position compare
	err=PositionerPositionCompareSet (comupath, master, corrend	, corrbeg, step); // Set position compare parameters
	err=PositionerPositionCompareEnable (comupath, master);// Enable position compare
	
	AbsMove(end-over);

	}
	
	return (true);
}

static void backRelMoveOne(void *p)
{

	XPS *pt;
	pt=(XPS *)p;

	double TargetPosition[8];
	int i,err;
	for (i=0;i<pt->nb_axis;i++)
		TargetPosition[i]=0;	
	TargetPosition[pt->AxisToMove]=pt->SetPos*pt->direction[pt->AxisToMove];	
	
	err=GroupMoveRelative (pt->mvsock, pt->AxisNum, pt->nb_axis,TargetPosition);

	_endthread();


}
// Relative move of one axis (0 =master 1=slave)
int XPS::RelMoveOne(int num, double Depl)
{
	if (IsMoving())
		return false;
	SetPos=Depl;
	AxisToMove=num;
	if(_beginthread(backRelMoveOne,0,(void *)this)==-1)
	{
		return (false);
	}	
	return (true);
}
