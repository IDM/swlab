// XPS.h: interface for the XPS class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_XPS_H__C32247E4_2D22_11D7_A2BE_00AA36753667__INCLUDED_)
#define AFX_XPS_H__C32247E4_2D22_11D7_A2BE_00AA36753667__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "Axis.h"
#include "Gpib.h"
#include "XPS_Q8_drivers.h"

class XPS :public Axis
{
public:
	// Relative move of one axis (0 =master 1=slave)
	int AxisToMove;
	int RelMoveOne(int num, double Depl);
	int MoveAndTrig(double beg,double end,double step,double over);
	double SetPos;
	int refsock;
	int mvsock;
	int ReadRef(double *refpos);
	int AxisType;
	double direction[8];
	int ReadAcceleration(double * accel);
	int MovingTime(double depl,double *t);
	int SetTaper(double Taper);
	int SetDefaultTaper(double Taper);
	int SetDefaultPosition(double Pos);
	int SetAcceleration(double accel);
	int RelMove(double  mo);
	int ReadTaper(double *Taper);
	int ReadSpeed(double *speed);
	int NegativeLimit();
	bool IsValid();
	int Move_1Trig(double Pos, double TrigPos);
	int Abort();
	int SetSpeed(double Speed);
	int ReadPosition(double *Pos);
	int AbsMove(double Pos);
	bool ReallyMove();
	bool IsMoving();
	int Reference();
	bool refmove;
	char AxisNum[8];
	int nb_axis;
	int comupath;
	double RefPosition;
	double RefTaper;
	char comu_path_name[20];
	double SetTap;	


	XPS(char *Name);
	virtual ~XPS();
	int comport;

private:
	
	bool READY(int status);
	double timeout;
	double TrigLength;
	bool Valid;
	int Set_KFactors();
	double P_KD;
	double P_KP;
	double P_KI;

	double DefaultAcceleration;
	double DefaultSpeed;
	int ReductorRatio;
	int GetConfiguration(char *Name);
};

#endif // !defined(AFX_XPS_H__C32247E4_2D22_11D7_A2BE_00AA36753667__INCLUDED_)
