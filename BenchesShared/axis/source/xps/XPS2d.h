// XPS2d.h: interface for the XPS2d class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_XPS2D_H__59F3556B_46AF_4EDC_901A_AC04181019E4__INCLUDED_)
#define AFX_XPS2D_H__59F3556B_46AF_4EDC_901A_AC04181019E4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include "XPS_C8_drivers.h"
#include "XPS_Q8_drivers.h"
#include <Windows.h>
#include <Wininet.h>

class XPS2d  
{
public:
	XPS2d(char* Name);
	virtual ~XPS2d();
	int GetConfiguration(char *Name);
	// creta a file named name that contain content in adress
	int CreateTraj(char* name, char* content);
	bool Valid;
	char comu_path_name[20];
	int comport;
	char AxisNum[8];
	int comupath;
	int mvsock;
	int refsock;
	int AbortSock;
	char XMaster[20];
	char ZMaster[20];
	char XSlave[20];
	char ZSlave[20];
	char XZ[20];
	char last_err[200];
	HINTERNET xpsint;
	HINTERNET xpsftp;
	HINTERNET ftpfile;
	DWORD_PTR dwContext;
	double treatangle(double angle);
	char *XPS2d::treat_traj(char *gbtraj);
	int MoveAbs(double X, double Z,int bg);
	int mvthread;
	int ReadPos(double * X, double * Z);
	int ReadMasterPos(double * X, double * Z);

	int SetAcceleration(double XAccel, double Zaccel);
	int ReadAcceleration(double *XAccel, double *Zaccel);
	int SetVelocity(double XVelocity, double ZVelocity);
	int ReadVelocity(double *XVelocity, double *ZVelocity);
	double Xset;
	double Zset;
	int Reference(void);
	bool READY(char *Group);
	bool SLAVE(char *Group);
	bool NOTINIT(char *Group);
	bool NOTREF(char *Group);
	char * Group(char* Positionner);
	bool IsMoving(void);
	bool GatheringDataReady (void);
	int ExecTraj(char * fname,double V ,double Accel,int n,int backgr);
	int TrajAndTrig( double xbeg,double zbeg,char *fname,double len,double FirstTrig, double TrigLen, int TrigNb, double Velocity, double Acceleration);
	int ReadTaper(double * XTaper, double *ZTaper);
	char TT_name[40];
	// trajectory parameter
	char trajname[50];
	double trajV;
	double trajA;
	int trajN;
	
	int Direction[2];
	int SetTaper(double  Xtaper,double ZTaper);
	int SetSlaveMode(double factor);
	double XTset,ZTset;
	int LineTo(double X, double Z,double Vel,double accel,int bg);
	int Abort(void);
	int LineAndTrig(double XStart,double ZStart , double FirstTrigLen , double TrigStep , int TrigNb, double XEndPos,double ZEndPos, double Velocity, double Acceleration);
	double LT_XStart,LT_ZStart;
	double LT_FirstTrigLen;
	double LT_TrigStep;
	int LT_TrigNb;
	double LT_XEndPos,LT_ZEndPos;
	double LT_Velocity,LT_Acceleration;
	int SampleN;
	int GetGatheringData(double **XPos,double **ZPos,double **VX,double **VZ,int *nb);
	double *XGath,*ZGath,*VxGath,*VzGath;
	int Circle(double XCentre, double ZCenter, double Teta2,double velocity,double accel,int bg);
	int MasterCircle(double XCentre, double ZCenter, double Teta2, double velocity, double accel, int bg);
	int CircleAndTrig(double XCenter, double ZCenter, double R, double OBeg, double OFirstTrig, double OTrigLen, int OTrigNb, double OEnd, double Velocity, double Acceleration);
	int MasterCircleAndTrig(double XCenter, double ZCenter, double R, double OBeg, double OFirstTrig, double OTrigLen, int OTrigNb, double OEnd, double Velocity, double Acceleration);

	double CT_XCenter,CT_ZCenter,CT_R,CT_OBeg,CT_OFirstTrig,CT_OTrigLen,CT_OEnd,CT_Velocity,CT_accel;
	double TT_XBeg,TT_ZBeg,TT_FirstTrig,TT_TrigLen,TT_TrigNb,TT_Velocity,TT_accel,TT_len;
	int CT_TrigNb;
	bool TrajectoryRunning;
	bool GathDataReady;
	bool TrajRunning(void);
	int MoveRel(double Xd, double Zd,int bg);
	int MasterMoveAbs(double X, double Z,int bg);
	int MasterMoveRel(double X, double Z,int bg);
	int SlaveMoveAbs(double X, double Z,int bg);
	int SlaveMoveRel(double X, double Z,int bg);
	double TimeToMove(double disp,double V,double A);
double timeout;
private:
	
	
	double DefaultAcceleration[2];
	double DefaultVelocity[2];
	double RefPos[2];
	double RefTaper[2];
	double MaxJerk[2];
	double MinJerk[2];
	int ReductorRatio;

	int ReadMoveParam(void);
	int SetMoveParam(void);


};

#endif // !defined(AFX_XPS2D_H__59F3556B_46AF_4EDC_901A_AC04181019E4__INCLUDED_)
