
#include <stdio.h>
#include <process.h>
#include <string.h>
#include <math.h>
#include <Windows.h>
#include <float.h>

#include "XPS3d.h"

XPS3D::XPS3D(char *Name)
{
	SampleN=0;
	TrajectoryRunning=false;
//GathDataReady=false;
	GathDataReady=true;
if (GetConfiguration(Name)	== (int)true)
{

	comupath=TCP_ConnectToServer(comu_path_name,comport,timeout);
	if (-1==comupath)
	{
		sprintf(last_err,"XPS:can't open a comunication path socket on %s ",comu_path_name);
		Valid=false;
		return;
	}
	refsock=TCP_ConnectToServer(comu_path_name,comport,timeout);
	if (-1==refsock)
	{
		sprintf(last_err,"XPS:can't open a reference path socket on %s ",comu_path_name);
		Valid=false;
		return;
	}
	mvsock=TCP_ConnectToServer(comu_path_name,comport,timeout);
	if (-1==mvsock)
	{
		sprintf(last_err,"XPS:can't open a moving path socket on %s ",comu_path_name);
		Valid=false;
		return;
	}
	AbortSock=TCP_ConnectToServer(comu_path_name,comport,timeout);
	if (-1==AbortSock)
	{
		sprintf(last_err,"XPS:can't open an Abort path socket on %s ",comu_path_name);
		Valid=false;
		return;
	}

	if (!READY())
		if(!Reference())
		{
			sprintf(last_err,"XPS:can't Make Reference ");
			Valid=false;
			return;
		}
		
	if (!SetMoveParam())
	{
		strcat(last_err,"\nXPS:can't Set Moving parameter ");
		Valid=false;
		return;
	}
	TrajectoryRunning=false;
	Valid=true;

}
else
	Valid=false;
}

XPS3D::~XPS3D(void)
{
}

int XPS3D::GetConfiguration(char *Name)
{
//**********************************************************
// Define some structures used for resources extraction.
//**********************************************************
typedef struct _Res
{
	char name[50];
	char val[50];
}Res;

enum Devices 
{
	AXE_TYPE, 
    COM_PATH,
	COM_PORT,
	COM_TIMEOUT,
	DIRECTION,
    DEF_SPEED,
    DEF_ACCEL,
	MIN_JERK,
	MAX_JERK,
    REF_POS,
	REF_TAPER,
	XM,
	ZM,
	SM,
//	XS,
//	ZS,
//	SS
};

Res CtrlResTab[] = 
{
  {"AXE_TYPE",       ""},
  {"COM_PATH",        ""}, 
  {"COM_PORT",      ""},
  {"COM_TIMEOUT",      ""},
  {"DIRECTION",      ""},
  {"DEF_SPEED",      ""},
  {"DEF_ACCEL",        ""},
  {"MIN_JERK",		""},
  {"MAX_JERK",		""},
  {"REF_POS",        ""},
  {"REF_TAPER",		 ""},
  {"XM",		 ""},
  {"ZM",		 ""},
  {"SM",		 ""}

 // {"XS",		 ""},
//  {"ZS",		 ""}
};
	
	FILE   *Fin;
	char   *Temp;
	char    Buf[1000];
	char    ConfStr[128];
	char    FileName[256];
	int     i, NbRes;
	int     status;
	

	status = GetEnvironmentVariable("ConfPath", ConfStr, 100);

  if(status == 0)  // Environment variable not defined.
    sprintf(FileName, "%s\\%s.conf", "c:\\ConfigurationFolder", Name);
  else
    sprintf(FileName, "%s\\%s.conf", ConfStr, Name);
	
  Fin = fopen(FileName, "r");
  if(Fin == NULL)
	{
		sprintf (last_err,"GetConfiguration: can't open file: %s",FileName);
		return(false);
	}

	//--------------------------------------------
	// Get resource names from configuration file.

  fread(Buf, sizeof(Buf), 1, Fin);
	NbRes = sizeof(CtrlResTab) / sizeof(Res);
  for(i=0; i<NbRes; i++)
  {
		if((Temp = strstr(Buf, CtrlResTab[i].name)) == NULL)
		{
			sprintf(last_err,"GetConfiguration: can't find %s in conf file",CtrlResTab[i].name);
			fclose (Fin);
			return(false);
		}
		else
		{
			sscanf(Temp, "%*s%s", &(CtrlResTab[i].val));
		}
  }
	fclose(Fin);

	strcpy(comu_path_name, CtrlResTab[COM_PATH].val);
	strcpy(XMotor,CtrlResTab[XM].val);
	strcpy(ZMotor,CtrlResTab[ZM].val);
	strcpy(SMotor,CtrlResTab[SM].val);
//	strcpy(XSlave,CtrlResTab[XS].val);
//	strcpy(ZSlave,CtrlResTab[ZS].val);

//	if(Group(&XMaster[0])!=Group(&ZMaster[0]))
//	{
//		sprintf(last_err,"GetConfiguration: The two master have different group");
//		Valid=false;
//		return(false);

//	}
	sprintf(XZS,Group(&XMotor[0]));
	comport=atoi(CtrlResTab[COM_PORT].val);
	timeout=atof(CtrlResTab[COM_TIMEOUT].val);
/*	strcpy(AxisNum, CtrlResTab[AXE_NUM].val);
	nb_axis=strlen(AxisNum);*/
	if (sscanf(CtrlResTab[DEF_SPEED].val,"%lf",&DefaultVelocity)!=1)
	{
		sprintf(last_err,"GetConfiguration: Can't read the  speed for axis");
		Valid=false;
		return(false);
	}
	if (sscanf(CtrlResTab[DEF_ACCEL].val,"%lf",&DefaultAcceleration)!=1)
	{
		sprintf(last_err,"GetConfiguration: Can't read the  acceleration for axis");
		Valid=false;
		return(false);
	}
	if (sscanf(CtrlResTab[REF_POS].val,"%lf,%lf,%lf",&RefPos[0],&RefPos[1],&RefPos[2])!=3)
	{
		sprintf(last_err,"GetConfiguration: Can't read the 3 refPos for axis");
		Valid=false;
		return(false);
	}
//	if (sscanf(CtrlResTab[REF_TAPER].val,"%lf,%lf",&RefTaper[0],&RefTaper[1])!=2)
//	{
//		sprintf(last_err,"GetConfiguration: Can't read the 2 refTaper for axis");
//		return(false);
//	}
	if (sscanf(CtrlResTab[DIRECTION].val,"%d,%d,%d",&Direction[0],&Direction[1],&Direction[2])!=3)
	{
		sprintf(last_err,"GetConfiguration: Can't read the 2 direction for axis");
		Valid=false;
		return(false);
	}
	if (sscanf(CtrlResTab[MIN_JERK].val,"%d,%d,%d",&MinJerk[0],&MinJerk[1],&MinJerk[2])!=3)
	{
		MinJerk[0]=.05;MinJerk[1]=0.05;MinJerk[2]=0.05;
	}
	if (sscanf(CtrlResTab[MAX_JERK].val,"%d,%d,%d",&MaxJerk[0],&MaxJerk[1],&MaxJerk[2])!=3)
	{
		MaxJerk[0]=.05;MaxJerk[1]=0.05;MaxJerk[2]=0.05;
	}
	
	Valid=true;
	moving=false;
	return(true);
}

char * XPS3D::Group(char* Positionner)
{
	static char  group[20];
	sprintf(group,Positionner);
	char sep[]=".";
	char *token;
	token = strtok( &group[0], sep );
	return &group[0];
}

bool XPS3D::READY()
{
	int error,status;
	if( moving)
		return false;
	error=GroupStatusGet(comupath,Group(XMotor),&status);
	if (error!=0)
	{
		sprintf(last_err,"XPS:can't getstatus of Group ");
		return false;
	}
	return ( status>9 && status < 19);
}

static void backRef(void *p)
{
	XPS3D *pt;
	pt=(XPS3D *)p;
	int error;

	//
	TCP_SetTimeout(pt->refsock,10000);
	error=KillAll(pt->refsock);
	if (error!=0)
	{
		ErrorStringGet(pt->refsock,error,pt->last_err);
		_endthread();;
	}
	error=GroupInitialize(pt->refsock,pt->Group(pt->XMotor));
	if (error!=0)
	{
		ErrorStringGet(pt->refsock,error,pt->last_err);
		_endthread();;
	}
	error=GroupHomeSearch(pt->refsock,pt->Group(pt->XMotor));
	if (error!=0)
	{
		ErrorStringGet(pt->refsock,error,pt->last_err);
		_endthread();;
	}
	_endthread();
}


int XPS3D::Reference(void)
{
	
	if(_beginthread(backRef,0,(void *)this)==-1)
	{
		return (false);
	}	
	return (true);
}

int XPS3D::SetMoveParam(void)
{
	int error;
	error=PositionerSGammaParametersSet (comupath, XMotor , DefaultVelocity,DefaultAcceleration,MinJerk[0], MaxJerk[0]);
	if(error!=0)
	{
		ErrorStringGet(comupath,error,last_err);
		return false;
	}

	error=PositionerSGammaParametersSet (comupath, ZMotor , DefaultVelocity,DefaultAcceleration,MinJerk[1], MaxJerk[1]);
	if(error!=0)
	{
		ErrorStringGet(comupath,error,last_err);
		return false;
	}
	error=PositionerSGammaParametersSet (comupath, SMotor , DefaultVelocity,DefaultAcceleration,MinJerk[2], MaxJerk[2]);
	if(error!=0)
	{
		ErrorStringGet(comupath,error,last_err);
		return false;
	}


	return true;
}

int XPS3D::ReadPos(double * X, double * Z, double *S)
{
	int error,error2;
	double pos[3];
	
	error=GroupPositionCurrentGet(comupath,XZS,3,&(pos[0]));
	if (error!=0)
	{	
		error2=ErrorStringGet(comupath,error,last_err);
		if (error2!=0)
			sprintf (last_err, "No connection --> Timeout");
		return false;
	}
	*X=pos[0]*Direction[0]+RefPos[0];
	*Z=pos[1]*Direction[1]+RefPos[1];
	*S=pos[2]*Direction[2]+RefPos[2];
	return true;
}


static void backSetPos(void *p)
{
	XPS3D *pt;
	pt=(XPS3D *)p;
	int error,error2;
	TCP_SetTimeout(pt->mvsock,30000);
	

	error=GroupMoveAbsolute(pt->mvsock,pt->XZS,3,pt->SetPt);
	if (error!=0)
	{	
		error2=ErrorStringGet(pt->mvsock,error,pt->last_err);
		if (error2!=0)
			sprintf (pt->last_err, "No connection --> Timeout");
	}
	pt->moving=false;
	_endthread();
}
int XPS3D::SetPos(double  X, double  Z, double S)
{
	if(!READY())
	{
		sprintf(last_err,"CAn't start a trajectorie device is moving\n");
		return false;
	}
	double tmppos[3];
	if (isnan(X) || isnan(Z) || isnan(S))
	{
		if (!ReadPos(&tmppos[0], &tmppos[1], &tmppos[2]))
		{
			sprintf(last_err, "can't read Position with Nan Parameter ");
			return (false);
		}
	}
	if (isnan(X))
		X = tmppos[0];
	if (isnan(Z))
		Z = tmppos[1];
	if (isnan(S))
		S = tmppos[2];


	SetPt[0]=(X-RefPos[0])*Direction[0];
	SetPt[1]=(Z-RefPos[1])*Direction[1];
	SetPt[2]=(S-RefPos[2])*Direction[2];
	
	moving=true;
	if(_beginthread(backSetPos,0,(void *)this)==-1)
		return (false);
	return (true);	
}

int XPS3D::SetVelocity(double  V)
{
	double SavedVelocity=DefaultVelocity;
	if(! _isnan(V))
		DefaultVelocity=V;
	
	if( SetMoveParam())
		return true;
	else
	{
		DefaultVelocity=SavedVelocity;
		SetMoveParam();
		return false;
	}
	
}
int XPS3D::ReadVelocity(double  *V)
{
	*V=	DefaultVelocity;
	return (true);	
}

int XPS3D::SetAcceleration(double  A)
{
	double SavedAcceleration=DefaultAcceleration;
	if(! _isnan(A))
		DefaultAcceleration=A;
	
	if( SetMoveParam())
		return true;
	else
	{
		DefaultAcceleration=SavedAcceleration;
		SetMoveParam();
		return false;
	}
	
}
int XPS3D::ReadAcceleration(double  *A)
{

	*A=	DefaultAcceleration;
	return (true);	
}

int XPS3D::CreateStartOfTraj(double X,double Z,double S)
{
	XStartTraj=X;
	ZStartTraj=Z;
	SStartTraj=S;
	return true;
}

int XPS3D::CreateTraj( float *XPtr_d,float *ZPtr_d,float *SPtr_d ,long numpt)
{

	char *content;
	char fname[30];
	sprintf(fname,"XPS3Dtraj.txt");
	content=(char*)malloc(36*(numpt+2));
	sprintf(content,"");
	char OneLine[32];
	float XS,ZS,SS;
	float XA,ZA,SA;
	XS=XPtr_d[0];
	ZS=ZPtr_d[0];
	SS=SPtr_d[0];
	
	sprintf (OneLine,"%.4f,%.4f,%.4f\n",(2*XPtr_d[0]-XPtr_d[1]-XS)*Direction[0],(2*ZPtr_d[0]-ZPtr_d[1]-ZS)*Direction[1],(2*SPtr_d[0]-SPtr_d[1]-SS)*Direction[2]);
	CreateStartOfTraj(XS,ZS,SS);
	strcat(content,OneLine);
	for (int i=0;i<numpt;i++)
	{
		
		sprintf (OneLine,"%.4f,%.4f,%.4f\n",(*XPtr_d-XS)*Direction[0],(*ZPtr_d-ZS)*Direction[1],(*SPtr_d-SS)*Direction[2]);
		XPtr_d++;ZPtr_d++;SPtr_d++;
		strcat(content,OneLine);
	}
	XPtr_d--;ZPtr_d--;SPtr_d--;
	XA=2*(*XPtr_d);XA-=(*(--XPtr_d));XA-=XS;XA*=Direction[0];
	ZA=2*(*ZPtr_d);ZA-=(*(--ZPtr_d));ZA-=ZS;ZA*=Direction[1];
	SA=2*(*SPtr_d);SA-=(*(--SPtr_d));SA-=SS;SA*=Direction[2];
	sprintf (OneLine,"%.4f,%.4f,%.4f\n",XA,ZA,SA);
	strcat(content,OneLine);

	xpsint=InternetOpen("Igor",INTERNET_OPEN_TYPE_DIRECT,NULL,NULL,NULL);
	xpsftp=InternetConnect(xpsint,comu_path_name,INTERNET_DEFAULT_FTP_PORT,
	"Administrator","Administrator",INTERNET_SERVICE_FTP, INTERNET_FLAG_PASSIVE ,dwContext);
	if (xpsftp==NULL)
	{
		sprintf(last_err,"Internetconnect NULL errN %d   | \n",GetLastError());
		return false;
	}
	if (!FtpSetCurrentDirectory(xpsftp,"Public/Trajectories/"))
	{
		sprintf(last_err,"XPS2d::CreateTraj : can't set directory %d   | \n",GetLastError());
		return false;
	}
//	char *trtraj=treat_traj(content);
	if (content==NULL)
	{
		sprintf(last_err,"XPS2d::CreateTraj : error in traj file from treat   | \n");
		return false;
	}
	// is the file already present in the directory.
	ftpfile=FtpOpenFile(xpsftp,fname,GENERIC_WRITE,FTP_TRANSFER_TYPE_ASCII,NULL);
	if (ftpfile==NULL)
	{
	DWORD lasterr=GetLastError();
		sprintf(last_err,"FtpOpenFile NULL errN %d   | \n",GetLastError());
			free (content);
		return false;
	}
	DWORD writenb;
	if(!InternetWriteFile(ftpfile,content,strlen(content),&writenb))
	{
		sprintf(last_err,"InternetWriteFile NULL errN %d   | \n",GetLastError());
		free (content);
		return false;
	}
	
	while(InternetCloseHandle(ftpfile)==false)
	{
	}
	//Sleep(400);

		free (content);
	InternetCloseHandle(xpsftp);
	InternetCloseHandle(xpsint);
	
	return true;
}

static void backRunTraj(void *p)
{
	XPS3D *pt;
	pt=(XPS3D *)p;
	int error,error2;
	TCP_SetTimeout(pt->mvsock,40000);	
	error=GroupMoveAbsolute(pt->mvsock,pt->XZS,3,pt->SetPt);
	if (error!=0)
	{	
		error2=ErrorStringGet(pt->mvsock,error,pt->last_err);
		if (error2!=0)
		{
			sprintf (pt->last_err, "No connection --> Timeout");
			pt->moving=false;
			_endthread();
		}
	}
	Sleep(100);
		double X,Z,S;
	pt->ReadPos(& X,& Z,&S);
	if(XYZSplineVerification(pt->mvsock,pt->XZS,"XPS3Dtraj.txt")!=0)
	{
		sprintf(pt->last_err,"XPS3d::CreateTraj : Spline verification fail %d   | \n",GetLastError());
		pt->moving=false;
		_endthread();
	}
	if (XYZSplineExecution(pt->mvsock,pt->XZS,"XPS3Dtraj.txt",pt->TV,pt->TAcc)!=0)
	{
		sprintf(pt->last_err,"XPS3d::CreateTraj : Spline execution %d   | \n",GetLastError());
		pt->moving=false;
		_endthread();
	}


	pt->moving=false;
	_endthread();
}
int XPS3D::RunTraj(double V , double Acc)
{
	if(!READY())
	{
		sprintf(last_err,"CAn't start a trajectorie device is moving\n");
		return false;
	}
	SetPt[0]=(XStartTraj-RefPos[0])*Direction[0];
	SetPt[1]=(ZStartTraj-RefPos[1])*Direction[1];
	SetPt[2]=(SStartTraj-RefPos[2])*Direction[2];
	TV=V;TAcc=Acc;
	moving=true;
	if(_beginthread(backRunTraj,0,(void *)this)==-1)
		return (false);
	return ( true);
}

int XPS3D::Abort()
{
	int error;
	error=GroupMoveAbort(AbortSock,XZS);
	if (error!=0)
	{
		char reason[100];
		ErrorStringGet(AbortSock,error,&reason[0]);
		sprintf(last_err,"Can't Abort %s \n",reason);
		return false;
	}
	return true;
}
int XPS3D::GetGatheringData(double **XPos,double **ZPos,double **SPos,int *nb)
{
	int error;
	int maxsample;

	if (GathDataReady==false)
	{
		sprintf(last_err,"No Position Data ready ");
		return (false);
	}

	error = GatheringCurrentNumberGet(comupath, &SampleN, &maxsample);
		if (error!=0)
		{
			ErrorStringGet(comupath, error, last_err);
			return (0);
		}
	SampleN/=2; // 2 for toggle --> 1 trig point evry 2 sample
//	double *newZ,*newX,*newS;

	ZGath=(double *)malloc((SampleN+1)*sizeof(double));
	if (ZGath==NULL)
	{
		sprintf(last_err,"error in malloc Z");
		return false;
	}
	XGath=(double *)malloc((SampleN+1)*sizeof(double));
	if (XGath==NULL)
	{
		sprintf(last_err,"error in malloc X");
		return false;
	}
	
	SGath=(double *)malloc((SampleN+1)*sizeof(double));
	if (SGath==NULL)
	{
		sprintf(last_err,"error in realloc X");
		return false;
	}
	int i;
	double *Xpt,*Zpt,*Spt;

	Xpt=XGath;Zpt=ZGath;Spt=SGath;
	double XM,ZM,SM;
	char tmp[50];
	for(i=0;i<2*SampleN;i+=2)
	{
		error = GatheringDataGet(comupath, i, &tmp[0]);
		if ( sscanf(&tmp[0],"%lf;%lf;%lf",&XM,&ZM,&SM)!=3)
		{
			sprintf(last_err,"error reading Data Gathering     ");
			strcat (last_err,&tmp[0]);

			*nb=i-1;
			return (false);
		}
		*Xpt=(XM)*Direction[0]+RefPos[0];
		*Zpt=(ZM)*Direction[1]+RefPos[1];
		*Spt=(SM)*Direction[2]+RefPos[2];
		Xpt++;
		Zpt++;
		Spt++;
		
	}
	*XPos=XGath;
	*ZPos=ZGath;
	*SPos=SGath;
	*nb=SampleN;
	return true;
}
static void backRunTrajAndTrig(void *p)
{
	XPS3D *pt;
	pt=(XPS3D *)p;
	int error,error2;
	TCP_SetTimeout(pt->mvsock,40000);	
	error=GroupMoveAbsolute(pt->mvsock,pt->XZS,3,pt->SetPt);
	if (error!=0)
	{	
		error2=ErrorStringGet(pt->mvsock,error,pt->last_err);
		if (error2!=0)
		{
			sprintf (pt->last_err, "No connection --> Timeout");
			pt->moving=false;_endthread();
		}
	}
	Sleep(500);
	if(XYZSplineVerification(pt->mvsock,pt->XZS,"XPS3Dtraj.txt")!=0)
	{
		sprintf(pt->last_err,"XPS3d::CreateTraj : Spline verification fail %d   | \n",GetLastError());
		pt->moving=false;_endthread();
	}
	if (TimerSet(pt->mvsock, "Timer1", (int)(0)) != 0)
	{
		sprintf(pt->last_err, "XPS3d::TrajAndTrig : TimerSet %d   | \n", GetLastError());
		pt->moving = false; _endthread();
	}

	if (TimerSet(pt->mvsock,"Timer1",(int)(pt->TimerVal*1e4))!=0)
	{
		sprintf(pt->last_err,"XPS3d::TrajAndTrig : TimerSet %d   | \n",GetLastError());
		pt->moving=false;_endthread();
	}
	char EvList[30];
	sprintf(EvList,"");
	error=EventExtendedAllGet(pt->mvsock,EvList);
	if( strlen(EvList)!=0)
		{
			int i;
			for(i=0;i<50;i++)
				EventExtendedRemove(pt->mvsock,i);
		}
	if(GatheringReset(pt->mvsock)!=0)
	{
		sprintf(pt->last_err,"XPS3d::TrajAndTrig : GatheringReset %d   | \n",GetLastError());
		pt->moving=false;_endthread();
	}
	char gathstr[300];
	sprintf (gathstr,"%s.X.CurrentPosition;%s.Z.CurrentPosition;%s.S.CurrentPosition",
		pt->XZS,pt->XZS,pt->XZS);

	if(GatheringConfigurationSet(pt->mvsock,3,gathstr)!=0)
	{
		sprintf(pt->last_err,"XPS3d::TrajAndTrig : GatheringConfigurationSet %d   | \n",GetLastError());
		pt->moving=false;_endthread();
	}
	char trigcmd[300];
	sprintf (trigcmd,"%s%s%s","Timer1;",pt->XZS,".Spline.TrajectoryState");

	if(EventExtendedConfigurationTriggerSet(pt->mvsock,2,trigcmd,"0;0","0;0","0;0","0;0")!=0)
	{
		sprintf(pt->last_err,"XPS3d::TrajAndTrig :EventExtendedConfigurationTriggerSet  %d   | \n",GetLastError());
		pt->moving=false;_endthread();
	}

	if(EventExtendedConfigurationActionSet(pt->mvsock,2,"GPIO1.DO.DOToggle;GatheringOneData","2;0","0;0","0;0","0;0")!=0)
	{
		sprintf(pt->last_err,"XPS3d::TrajAndTrig EventExtendedConfigurationActionSet:  %d   | \n",GetLastError());
		pt->moving=false;_endthread();
	}

	int ev;
	if( EventExtendedStart(pt->mvsock,&ev)!=0)
	{
		sprintf(pt->last_err,"XPS3d::TrajAndTrig EventExtendedStart:  %d   | \n",GetLastError());
		pt->moving=false;_endthread();
	}


	if (XYZSplineExecution(pt->mvsock,pt->XZS,"XPS3Dtraj.txt",pt->TV,pt->TAcc)!=0)
	{
		sprintf(pt->last_err,"XPS3d::RunTrajAndTrig : Spline execution %d   | \n",GetLastError());
		pt->moving=false;_endthread();
	}
	if(GatheringStopAndSave(pt->mvsock)!=0)
	{
		sprintf(pt->last_err,"XPS3d::TrajAndTrig GatheringStopAndSave:  %d   | \n",GetLastError());
		pt->moving=false;_endthread();
	}
	if(EventExtendedRemove(pt->mvsock,ev)!=0)
	{
		sprintf(pt->last_err,"XPS3d::TrajAndTrig EventExtendedRemove:  %d   | \n",GetLastError());
		pt->moving=false;_endthread();
	}
	GPIODigitalSet(pt->mvsock,"GPIO1.DO",2,1);
	
	pt->GathDataReady=true;
	pt->moving=false;
	_endthread();
}

int XPS3D::RunTrajAndtrig(double V , double Acc,double TrigLen)
{
	if(!READY())
	{
		sprintf(last_err,"CAn't start a trajectorie device is moving\n");
		return false;
	}
	
	SetPt[0]=(XStartTraj-RefPos[0])*Direction[0];
	SetPt[1]=(ZStartTraj-RefPos[1])*Direction[1];
	SetPt[2]=(SStartTraj-RefPos[2])*Direction[2];

	TV=V;TAcc=Acc;
	TimerVal=TrigLen/V/2;
	GathDataReady=false;
	int error=GPIODigitalSet(mvsock,"GPIO1.DO",2,1);
	moving=true;
	if(_beginthread(backRunTrajAndTrig,0,(void *)this)==-1)
		return (false);
	return ( true);
}