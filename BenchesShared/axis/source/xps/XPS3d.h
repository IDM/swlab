#pragma once

#include <Wininet.h>

#include "XPS_C8_drivers.h"

class XPS3D
{
public:
	XPS3D(char *Name);
	~XPS3D(void);

	int		SampleN;
	bool	TrajectoryRunning;
	bool	GathDataReady;
	int		comupath;
	char	comu_path_name[25];
	int		comport;
	double	timeout;
	char	last_err[200];
	bool	Valid;
	int		refsock;
	int		mvsock;
	int		AbortSock;
	char	XZS[20];
	char	XMotor[20];
	char	ZMotor[20];
	char	SMotor[20];
	double	DefaultVelocity;
	double	DefaultAcceleration;
	double	RefPos[3];
	int		Direction[3];
	double	MinJerk[3];
	double	MaxJerk[3];
	double  SetPt[3];
	HINTERNET xpsint;
	HINTERNET xpsftp;
	HINTERNET ftpfile;
	DWORD_PTR dwContext;
	bool moving;
	bool traj_running;
	double XStartTraj;
	double ZStartTraj;
	double SStartTraj;
	double TV;
	double TAcc;
	double *XGath,*ZGath,*SGath;
	double TimerVal;



	int		GetConfiguration(char *Name);
	char	*Group(char* Positionner);
	bool	READY();
	int		Reference(void);
	int		SetMoveParam(void);
	int		ReadPos(double * X, double * Z, double *S);
	int		SetPos(double  X, double  Z, double S);
	int		SetVelocity(double  V);
	int		ReadVelocity(double  *V);
	int		SetAcceleration(double  A);
	int		ReadAcceleration(double  *A);
	int		CreateStartOfTraj(double X,double Z,double S);
//	int		CreateTraj(char* name, char* content);
	int		CreateTraj( float *X,float *Z,float *S ,long numpt);
	int		RunTraj(double V,double Acc);
	int		Abort();
	int		GetGatheringData(double **XPos,double **ZPos,double **SPos,int *nb);
	int		RunTrajAndtrig(double V , double Acc,double TrigLen);
};
