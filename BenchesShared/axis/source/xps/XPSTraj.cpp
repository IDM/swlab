#include "XPSTraj.h"


XPSTraj::XPSTraj(char *adress)
{
	sprintf (last_err,"OK");
	if (pConnect!=NULL)
	{
		sprintf(last_err,"already open");
		
	}
	else
	{
	 try
	 {
		pConnect = sess.GetFtpConnection(_T(adress),"Administrator","Administrator");
	
	 }
	catch (CInternetException* pEx)
    {
        TCHAR sz[1024];
        pEx->GetErrorMessage(sz, 1024);
		sprintf(last_err,sz);
        pEx->Delete();
		Valid=false;
     }
		
}
}

XPSTraj::~XPSTraj(void)
{
	   if (pConnect != NULL) 
    {
        pConnect->Close();
        delete pConnect;
		pConnect=NULL;
		Valid=false;

    }
   else
   {
	   sprintf(last_err,"already Close");
	   Valid=false;
   }

}
