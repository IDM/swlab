// This file is part of SWLab Stretched Wire Magnetic Measurement Program 
// 	
// Copyright 2015, ESRF - The European Synchroton, Christophe Penel
//
// SWLab is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// The XOP Toolkit is copyright Wavemetrics, see <http://www.wavemetrics.com>
// The Newport XPS libraries are copyright Newport, see <http://www.newport.com>
// The Eigen library is licensed under the MPL2, see <http://eigen.tuxfamily.org>
// 

//
//////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <process.h>
#include <string.h>
#include <math.h>
#include <Windows.h>
#include <float.h>

#include "XPS2d.h"

#define PI 3.1415926535897932384626433832795


int XPS2d::ReadMoveParam(void)
{
	int error;
	error=PositionerSGammaParametersGet (comupath, XMaster , &DefaultVelocity[0],&DefaultAcceleration[0],&MinJerk[0], &MaxJerk[0]);
	if(error!=0)
	{
		ErrorStringGet(comupath,error,last_err);
		return false;
	}
	error=PositionerSGammaParametersGet (comupath, ZMaster , &DefaultVelocity[1],&DefaultAcceleration[1],&MinJerk[1], &MaxJerk[1]);
	if(error!=0)
	{
		ErrorStringGet(comupath,error,last_err);
		return false;
	}
	return true;
}

int XPS2d::SetMoveParam(void)
{
	int error;
	error=PositionerSGammaParametersSet (comupath, XMaster , DefaultVelocity[0],DefaultAcceleration[0],MinJerk[0], MaxJerk[0]);
	if(error!=0)
	{
		ErrorStringGet(comupath,error,last_err);
		return false;
	}
	error=PositionerSGammaParametersSet (comupath, XSlave , DefaultVelocity[0],DefaultAcceleration[0],MinJerk[0], MaxJerk[0]);
	if(error!=0)
	{
		ErrorStringGet(comupath,error,last_err);
		return false;
	}
	error=PositionerSGammaParametersSet (comupath, ZMaster , DefaultVelocity[1],DefaultAcceleration[1],MinJerk[1], MaxJerk[1]);
	if(error!=0)
	{
		ErrorStringGet(comupath,error,last_err);
		return false;
	}
	error=PositionerSGammaParametersSet (comupath, ZSlave , DefaultVelocity[1],DefaultAcceleration[1],MinJerk[1], MaxJerk[1]);
	if(error!=0)
	{
		ErrorStringGet(comupath,error,last_err);
		return false;
	}


	return true;
}

bool XPS2d::READY(char *Group)
{
	int error,status;
	error=GroupStatusGet(comupath,Group,&status);
	if (error!=0)
	{
		sprintf(last_err,"XPS:can't getstatus of Group ");
		return false;
	}
	return ( status>9 && status < 19);
}

bool XPS2d::SLAVE(char *Group)
{
	int error,status;
	error=GroupStatusGet(comupath,Group,&status);
	if (error!=0)
	{
		sprintf(last_err,"XPS:can't getstatus of XSlave ");
		return false;
	}
	return ( status==46);
}

bool XPS2d::NOTINIT(char *Group)
{
	int error,status;
	error=GroupStatusGet(comupath,Group,&status);
	if (error!=0)
	{
		sprintf(last_err,"XPS:can't getstatus of XSlave ");
		return false;
	}
	return ( status==0 || status==4 || status==9|| status==63|| status==6|| status==3|| status==50);
}
bool XPS2d::NOTREF(char *Group)
{
	int error,status;
	error=GroupStatusGet(comupath,Group,&status);
	if (error!=0)
	{
		sprintf(last_err,"XPS:can't getstatus of XSlave ");
		return false;
	}
	return ( status==42);
}
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////




XPS2d::XPS2d(char *Name)

{
	SampleN=0;
	TrajectoryRunning=false;
	GathDataReady=false;
if ( GetConfiguration(Name)	== (int)true)
{



	comupath=TCP_ConnectToServer(comu_path_name,comport,timeout);
	if (-1==comupath)
	{
		sprintf(last_err,"XPS:can't open a comunication path socket on %s ",comu_path_name);
		Valid=false;
		return;
	}
	refsock=TCP_ConnectToServer(comu_path_name,comport,timeout);
	if (-1==refsock)
	{
		sprintf(last_err,"XPS:can't open a reference path socket on %s ",comu_path_name);
		Valid=false;
		return;
	}
	mvsock=TCP_ConnectToServer(comu_path_name,comport,timeout);
	if (-1==mvsock)
	{
		sprintf(last_err,"XPS:can't open a moving path socket on %s ",comu_path_name);
		Valid=false;
		return;
	}
	AbortSock=TCP_ConnectToServer(comu_path_name,comport,timeout);
	if (-1==AbortSock)
	{
		sprintf(last_err,"XPS:can't open an Abort path socket on %s ",comu_path_name);
		Valid=false;
		return;
	}

	if (!READY(XZ) || !SLAVE(Group(XSlave)) || !SLAVE(Group(ZSlave)))
		if(!Reference())
		{
			sprintf(last_err,"XPS:can't Make Reference ");
			Valid=false;
			return;
		}
		
	if (!SetMoveParam())
	{
		strcat(last_err,"\nXPS:can't Set Moving parameter ");
		Valid=false;
		return;
	}
	TrajectoryRunning=false;
	Valid=true;
}
else
	Valid=false;
}

XPS2d::~XPS2d()
{
	TCP_CloseSocket(comupath);
	TCP_CloseSocket(refsock);
	TCP_CloseSocket(mvsock);
	TCP_CloseSocket(AbortSock);
}


int XPS2d::GetConfiguration(char *Name)
{
//**********************************************************
// Define some structures used for resources extraction.
//**********************************************************
typedef struct _Res
{
	char name[50];
	char val[50];
}Res;

enum Devices 
{
	AXE_TYPE, 
    COM_PATH,
	COM_PORT,
	COM_TIMEOUT,
	DIRECTION,
    DEF_SPEED,
    DEF_ACCEL,
	MIN_JERK,
	MAX_JERK,
    REF_POS,
	REF_TAPER,
	XM,
	ZM,
	XS,
	ZS
};

Res CtrlResTab[] = 
{
  {"AXE_TYPE",       ""},
  {"COM_PATH",        ""}, 
  {"COM_PORT",      ""},
  {"COM_TIMEOUT",      ""},
  {"DIRECTION",      ""},
  {"DEF_SPEED",      ""},
  {"DEF_ACCEL",        ""},
  {"MIN_JERK",		""},
  {"MAX_JERK",		""},
  {"REF_POS",        ""},
  {"REF_TAPER",		 ""},
  {"XM",		 ""},
  {"ZM",		 ""},
  {"XS",		 ""},
  {"ZS",		 ""}
};
	
	FILE   *Fin;
	char   *Temp;
	char    Buf[1000];
	char    ConfStr[128];
	char    FileName[256];
	int     i, NbRes;
	int     status;
	

	status = GetEnvironmentVariable("ConfPath", ConfStr, 100);

  if(status == 0)  // Environment variable not defined.
    sprintf(FileName, "%s\\%s.conf", "c:\\ConfigurationFolder", Name);
  else
    sprintf(FileName, "%s\\%s.conf", ConfStr, Name);
	
  Fin = fopen(FileName, "r");
  if(Fin == NULL)
	{
		sprintf (last_err,"GetConfiguration: can't open file: %s",FileName);
		return(false);
	}

	//--------------------------------------------
	// Get resource names from configuration file.

  fread(Buf, sizeof(Buf), 1, Fin);
	NbRes = sizeof(CtrlResTab) / sizeof(Res);
  for(i=0; i<NbRes; i++)
  {
		if((Temp = strstr(Buf, CtrlResTab[i].name)) == NULL)
		{
			sprintf(last_err,"GetConfiguration: can't find %s in conf file",CtrlResTab[i].name);
			fclose (Fin);
			return(false);
		}
		else
		{
			sscanf(Temp, "%*s%s", &(CtrlResTab[i].val));
		}
  }
	fclose(Fin);

	strcpy(comu_path_name, CtrlResTab[COM_PATH].val);
	strcpy(XMaster,CtrlResTab[XM].val);
	strcpy(ZMaster,CtrlResTab[ZM].val);
	strcpy(XSlave,CtrlResTab[XS].val);
	strcpy(ZSlave,CtrlResTab[ZS].val);

	if(Group(&XMaster[0])!=Group(&ZMaster[0]))
	{
		sprintf(last_err,"GetConfiguration: The two master have different group");
		Valid=false;
		return(false);

	}
	sprintf(XZ,Group(&XMaster[0]));
	comport=atoi(CtrlResTab[COM_PORT].val);
	timeout=atof(CtrlResTab[COM_TIMEOUT].val);
/*	strcpy(AxisNum, CtrlResTab[AXE_NUM].val);
	nb_axis=strlen(AxisNum);*/
	if (sscanf(CtrlResTab[DEF_SPEED].val,"%lf,%lf",&DefaultVelocity[0],&DefaultVelocity[1])!=2)
	{
		sprintf(last_err,"GetConfiguration: Can't read the 2 speed for axis");
		Valid=false;
		return(false);
	}
	if (sscanf(CtrlResTab[DEF_ACCEL].val,"%lf,%lf",&DefaultAcceleration[0],&DefaultAcceleration[1])!=2)
	{
		sprintf(last_err,"GetConfiguration: Can't read the 2 acceleration for axis");
		Valid=false;
		return(false);
	}
	if (sscanf(CtrlResTab[REF_POS].val,"%lf,%lf",&RefPos[0],&RefPos[1])!=2)
	{
		sprintf(last_err,"GetConfiguration: Can't read the 2 refPos for axis");
		Valid=false;
		return(false);
	}
	if (sscanf(CtrlResTab[REF_TAPER].val,"%lf,%lf",&RefTaper[0],&RefTaper[1])!=2)
	{
		sprintf(last_err,"GetConfiguration: Can't read the 2 refTaper for axis");
		Valid=false;
		return(false);
	}
	if (sscanf(CtrlResTab[DIRECTION].val,"%d,%d",&Direction[0],&Direction[1])!=2)
	{
		sprintf(last_err,"GetConfiguration: Can't read the 2 direction for axis");
		Valid=false;
		return(false);
	}
	if (sscanf(CtrlResTab[MIN_JERK].val,"%lf,%lf",&MinJerk[0],&MinJerk[1])!=2)
	{
		MinJerk[0]=.05;MinJerk[1]=0.05;
	}
	if (sscanf(CtrlResTab[MAX_JERK].val,"%lf,%lf",&MaxJerk[0],&MaxJerk[1])!=2)
	{
		MaxJerk[0]=.05;MaxJerk[1]=0.05;
	}
	
	Valid=true;
	return(true);
}





// create a file named name that contain content in adress
double XPS2d::treatangle(double angle)
{
double langle;
	langle=angle;
	if (Direction[0]<0)
		langle=180.-langle;
	if (Direction[1]<0)
		langle=360.-langle;
		
	return langle;
}
char *XPS2d::treat_traj(char *gbtraj)
{
	char *rettraj;
	char delim[10];
	char *current;
	char *next;
	char cmd[100];
	char unit[100];
	char tmpstr[100];

	char *trajpt=new char[strlen(gbtraj)+2];
	char *traj=trajpt;
	rettraj=new char[(int)(strlen(traj)*2)];

	sprintf(traj,"%s",gbtraj);	
	float FT;
	float P1,P2;	
	sprintf (delim,"\n");
	
	current = strtok_s( traj, delim, &next);
	if ( current==NULL)
	{
		delete (rettraj);
		return NULL;
	}
	else
	{
		if (sscanf(current,"FirstTangent = %f ; %s\n",&FT,unit)!=2)	
		{
			printf ( "error line 1\n");
			delete (rettraj);
			return NULL;
		}
		else
			sprintf (rettraj,"FirstTangent = %.3f ; %s\n",treatangle(FT),unit);
	}
	current = strtok_s( NULL, delim, &next);
	if ( current==NULL)
	{
		delete (rettraj);
		return NULL;
	}
	else
	{
		if (sscanf(current,"DiscontinuityAngle = %f ; %s\n",&FT,unit)!=2)	
		{
			delete (rettraj);
			return NULL;
		}
		else
		{
			sprintf (tmpstr,"DiscontinuityAngle = %.3f ; %s\n",FT,unit);
			strcat (rettraj,tmpstr);
		}
	}	
	strcat (rettraj,"\n");
	while((current=strtok_s( NULL, delim, &next))!=NULL)
	{
		if (sscanf(current,"%s = %f,%f",cmd,&P1,&P2)!=3)
		{
			delete (rettraj);
			return NULL;
		}	
		else
		{
			if (strstr(cmd,"Arc")!=NULL)
							sprintf (tmpstr,"%s = %.3f,%.3f\n",cmd,P1,P2*Direction[0]*Direction[1]);
			else if 	(strstr(cmd,"Line")!=NULL)
							sprintf (tmpstr,"%s = %.3f,%.3f\n",cmd,P1*Direction[0],P2*Direction[1]);
			else
					{
							delete[] (rettraj);
							return NULL;
					}
			strcat (rettraj,tmpstr);
		}
	}
	return rettraj;
}
int XPS2d::CreateTraj(char* name, char* content)
{
	char err[100];int error;
	xpsint=InternetOpen("Igor",INTERNET_OPEN_TYPE_DIRECT,NULL,NULL,NULL);
	xpsftp=InternetConnect(xpsint,comu_path_name,INTERNET_DEFAULT_FTP_PORT,
	"Administrator","Administrator",INTERNET_SERVICE_FTP, INTERNET_FLAG_PASSIVE ,dwContext);
	if (xpsftp==NULL)
	{
		sprintf(last_err,"Internetconnect NULL errN %d   | \n",GetLastError());
		return false;
	}
	if (!FtpSetCurrentDirectory(xpsftp,"Public/Trajectories/"))
	{
		sprintf(last_err,"XPS2d::CreateTraj : can't set directory %d   | \n",GetLastError());
		return false;
	}
	char *trtraj=treat_traj(content);
	if (trtraj==NULL)
	{
		sprintf(last_err,"XPS2d::CreateTraj : error in traj file from treat   | \n");
		return false;
	}
	// is the file already present in the directory.
	ftpfile=FtpOpenFile(xpsftp,name,GENERIC_WRITE,FTP_TRANSFER_TYPE_ASCII,NULL);
	if (ftpfile==NULL)
	{
		sprintf(last_err,"FtpOpenFile NULL errN %d   | \n",GetLastError());
		return false;
	}
	DWORD writenb;
	if(!InternetWriteFile(ftpfile,trtraj,strlen(trtraj),&writenb))
	{
		sprintf(last_err,"InternetWriteFile NULL errN %d   | \n",GetLastError());
		return false;
	}
	
	while(InternetCloseHandle(ftpfile)==false)
	{
	}
	//Sleep(400);
	int cpt=0;
	while ((XYLineArcVerification (comupath,XZ, name )!=0)&& cpt<5)
	{
		Sleep(100);
		cpt+=1;
	}

	if(error=XYLineArcVerification (comupath,XZ, name )!=0)
	{
		ErrorStringGet(comupath,error,err);
		sprintf(last_err,"Bad trajectory content err |%s|\n",err);
//		FtpDeleteFile(xpsftp,name);
		InternetCloseHandle(xpsftp);
		InternetCloseHandle(xpsint);
		return true;
	
	}

	InternetCloseHandle(xpsftp);
	InternetCloseHandle(xpsint);
	
	return true;
}

int XPS2d::ReadMasterPos(double * X, double * Z)
{
	int error;
	double pos[2];

	error = GroupPositionCurrentGet(comupath, XZ, 2, &(pos[0]));
	if (error != 0)
	{
		ErrorStringGet(comupath, error, last_err);
		return false;
	}
	*X = pos[0] * Direction[0]; *Z = pos[1] * Direction[1];

	return true;
}

int XPS2d::ReadPos(double * X, double * Z)
{
	int error;
	double pos[2];

	error=GroupPositionCurrentGet(comupath,XZ,2,&(pos[0]));
	if (error!=0)
	{
		ErrorStringGet(comupath,error,last_err);
		return false;
	}
	*X=pos[0]*Direction[0];*Z=pos[1]*Direction[1];
	error=GroupPositionCurrentGet(comupath,Group(XSlave),1,&(pos[0]));
	if (error!=0)
	{
		ErrorStringGet(comupath,error,last_err);
		return false;
	}
	error=GroupPositionCurrentGet(comupath,Group(ZSlave),1,&(pos[1]));
	if (error!=0)
	{
		ErrorStringGet(comupath,error,last_err);
		return false;
	}
	*X+=pos[0]*Direction[0];*Z+=pos[1]*Direction[1];
	*X/=2;*Z/=2;

	return true;
}

static void backMoveAbs(void *p)
{
	XPS2d *pt;
	pt=(XPS2d *)p;
	int error;
	double pos[2],nowpos[2],vel[2],accel[2];
	double taper[2];
	if (!pt->ReadTaper(&taper[0],&taper[1]))
	{
		pt->mvthread=0;
		_endthread();
	}
	if (!pt->ReadPos(&(nowpos[0]),&(nowpos[1])))
	{
		pt->mvthread=0;
		_endthread();
	}
	if (!pt->ReadVelocity(&(vel[0]),&(vel[1])))
	{
		pt->mvthread=0;
		_endthread();
	}
	if (!pt->ReadAcceleration(&(accel[0]),&(accel[1])))
	{
		pt->mvthread=0;
		_endthread();
	}
	
	double T0,T1,T;
	T0=fabs((nowpos[0]-pt->Xset)/vel[0])+vel[0]/accel[0];
	T1=fabs((nowpos[1]-pt->Zset)/vel[1])+vel[1]/accel[1];
	T=max(T0,T1);
	TCP_SetTimeout (pt->mvsock,T*1200 );
	pos[0]=pt->Xset-taper[0]/2;pos[1]=pt->Zset-taper[1]/2;
	pos[0]*=pt->Direction[0];
	pos[1]*=pt->Direction[1];
	if(_isnan(pt->Xset))
	{
		if(_isnan(pt->Zset))
		{//nothing to move
			pt->mvthread=0;
			_endthread();;
		}
		else
		{// move only Z
			
			error=GroupMoveAbsolute (pt->mvsock, pt->ZMaster, 1, &pos[1]);
			if (error!=0)
			{
				ErrorStringGet(pt->mvsock,error,pt->last_err);
				pt->mvthread=0;
				_endthread();;
			}
			pt->mvthread=0;
			_endthread();;
			
		}
	}
	else
	{
		if(_isnan(pt->Zset))
		{//move only X
			
			error=GroupMoveAbsolute (pt->mvsock, pt->XMaster, 1, &pos[0]);
			if (error!=0)
			{
				ErrorStringGet(pt->mvsock,error,pt->last_err);
				pt->mvthread=0;
				_endthread();;
			}
			pt->mvthread=0;
			_endthread();;
			
		}
		else
		{// move X and Z
			error=GroupMoveAbsolute (pt->mvsock, pt->XZ, 2, &pos[0]);
			if (error!=0)
			{
				ErrorStringGet(pt->mvsock,error,pt->last_err);
				pt->mvthread=0;
				_endthread();;
			}
			pt->mvthread=0;
			_endthread();
			
		}
	}
}
int XPS2d::MoveAbs(double X, double Z,int bg)
{

	Xset=X;Zset=Z;
	if (IsMoving())
	{
		sprintf (last_err,"MoveAbs : XZ Group Not Ready");
		return false;
	}
	mvthread=1;
	if(_beginthread(backMoveAbs,0,(void *)this)==-1)
	{
		return (false);
	}
	if( bg==0)
		return (true);
	do
	{
		Sleep(200);
	}while (mvthread!=0);
	
	return true;
}

int XPS2d::SetAcceleration(double XAccel, double ZAccel)
{
	if (!ReadMoveParam())
		return false;
	if(!_isnan(XAccel))
		DefaultAcceleration[0]=XAccel;
	if(!_isnan(ZAccel))
		DefaultAcceleration[1]=ZAccel;
	if (!SetMoveParam())
		return false;
	return true;
}
int XPS2d::ReadAcceleration(double *XAccel, double *ZAccel)
{

	if(ReadMoveParam())
	{
		*XAccel=DefaultAcceleration[0];
		*ZAccel=DefaultAcceleration[1];
	}
	else
		return false;

	return true;
}
int XPS2d::SetVelocity(double XVelocity, double ZVelocity)
{
	if (!ReadMoveParam())
		return false;
	if(!_isnan(XVelocity))
		DefaultVelocity[0]=XVelocity;
	if(!_isnan(ZVelocity))
		DefaultVelocity[1]=ZVelocity;
	if (!SetMoveParam())
		return false;
	return true;
}
int XPS2d::ReadVelocity(double *XVelocity, double *ZVelocity)
{

	if(ReadMoveParam())
	{
		*XVelocity=DefaultVelocity[0];
		*ZVelocity=DefaultVelocity[1];
	}
	else
		return false;

	return true;
}

static void backRef(void *p)
{
	XPS2d *pt;
	pt=(XPS2d *)p;
	int error;

	//
	TCP_SetTimeout(pt->refsock,10000);
	//KILL All Wire2d Group
	error = GroupKill(pt->refsock, pt->Group(pt->XSlave));
	if (error != 0)
	{
		ErrorStringGet(pt->refsock, error, pt->last_err);
		_endthread();;
	}
	error = GroupKill(pt->refsock, pt->Group(pt->ZSlave));
	if (error != 0)
	{
		ErrorStringGet(pt->refsock, error, pt->last_err);
		_endthread();;
	}
	error = GroupKill(pt->refsock, pt->Group(pt->XZ));
	if (error != 0)
	{
		ErrorStringGet(pt->refsock, error, pt->last_err);
		_endthread();;
	}

	//error=GroupInitialize(pt->refsock,pt->Group(pt->XSlave));
	//if (error!=0)
	//{
	//	ErrorStringGet(pt->refsock,error,pt->last_err);
	//	_endthread();;
	//}
	//error=GroupInitialize(pt->refsock,pt->Group(pt->ZSlave));
	//if (error!=0)
	//{
	//	ErrorStringGet(pt->refsock,error,pt->last_err);
	//	_endthread();;
	//}
	error=GroupInitialize(pt->refsock,pt->Group(pt->XZ));
	if (error!=0)
	{
		ErrorStringGet(pt->refsock,error,pt->last_err);
		_endthread();;
	}
	//set position to XSlave axis to 0
	//error=GroupReferencingStart(pt->refsock,pt->Group(pt->XSlave));
	//if (error!=0)
	//{
	//	ErrorStringGet(pt->refsock,error,pt->last_err);
	//	_endthread();;
	//}
	//error=GroupReferencingActionExecute(pt->refsock,(pt->XSlave),"SetPosition","None",0);
	//if (error!=0)
	//{
	//	ErrorStringGet(pt->refsock,error,pt->last_err);
	//	_endthread();;
	//}
	//error=GroupReferencingStop(pt->refsock,pt->Group(pt->XSlave));
	//if (error!=0)
	//{
	//	ErrorStringGet(pt->refsock,error,pt->last_err);
	//	_endthread();;
	//}
 //   // set position of ZSlave to 0
	//error=GroupReferencingStart(pt->refsock,pt->Group(pt->ZSlave));
	//if (error!=0)
	//{
	//	ErrorStringGet(pt->refsock,error,pt->last_err);
	//	_endthread();;
	//}
	//error=GroupReferencingActionExecute(pt->refsock,(pt->ZSlave),"SetPosition","None",0);
	//if (error!=0)
	//{
	//	ErrorStringGet(pt->refsock,error,pt->last_err);
	//	_endthread();;
	//}	
	//error=GroupReferencingStop(pt->refsock,pt->Group(pt->ZSlave));
	//if (error!=0)
	//{
	//	ErrorStringGet(pt->refsock,error,pt->last_err);
	//	_endthread();;
	//}
	// make XSlave and ZSlave in slave mode
	//error=SingleAxisSlaveParametersSet(pt->refsock,pt->Group(pt->XSlave),pt->XMaster,1);
	//if (error!=0)
	//{
	//	ErrorStringGet(pt->refsock,error,pt->last_err);
	//	_endthread();;
	//}
	//error=SingleAxisSlaveModeEnable(pt->refsock,pt->Group(pt->XSlave));
	//if (error!=0)
	//{
	//	ErrorStringGet(pt->refsock,error,pt->last_err);
	//	_endthread();;
	//}
	//error=SingleAxisSlaveParametersSet(pt->refsock,pt->Group(pt->ZSlave),pt->ZMaster,1);
	//if (error!=0)
	//{
	//	ErrorStringGet(pt->refsock,error,pt->last_err);
	//	_endthread();;
	//}
	//error=SingleAxisSlaveModeEnable(pt->refsock,pt->Group(pt->ZSlave));
	//if (error!=0)
	//{
	//	ErrorStringGet(pt->refsock,error,pt->last_err);
	//	_endthread();;
	//}
	//Make Reference on Master ( with slave following)
	error=GroupHomeSearch(pt->refsock,pt->XZ);
	if (error!=0)
	{
		ErrorStringGet(pt->refsock,error,pt->last_err);
		_endthread();;
	}
	//MAke Home on SLAVES
	//error=SingleAxisSlaveModeDisable(pt->refsock,pt->Group(pt->XSlave));
	//if (error!=0)
	//{
	//	ErrorStringGet(pt->refsock,error,pt->last_err);
	//	_endthread();;
	//}
	//error=SingleAxisSlaveModeDisable(pt->refsock,pt->Group(pt->ZSlave));
	//if (error!=0)
	//{
	//	ErrorStringGet(pt->refsock,error,pt->last_err);
	//	_endthread();;
	//}
	//error=GroupKill(pt->refsock,pt->Group(pt->XSlave));
	//if (error!=0)
	//{
	//	ErrorStringGet(pt->refsock,error,pt->last_err);
	//	_endthread();;
	//}
	//error=GroupKill(pt->refsock,pt->Group(pt->ZSlave));
	//if (error!=0)
	//{
	//	ErrorStringGet(pt->refsock,error,pt->last_err);
	//	_endthread();;
	//}
	//
	error=GroupInitialize(pt->refsock,pt->Group(pt->XSlave));
	if (error!=0)
	{
		ErrorStringGet(pt->refsock,error,pt->last_err);
		_endthread();;
	}
	error=GroupInitialize(pt->refsock,pt->Group(pt->ZSlave));
	if (error!=0)
	{
		ErrorStringGet(pt->refsock,error,pt->last_err);
		_endthread();;
	}
	error=GroupHomeSearch(pt->refsock,pt->Group(pt->XSlave));
	if (error!=0)
	{
		ErrorStringGet(pt->refsock,error,pt->last_err);
		_endthread();;
	}
	error=GroupHomeSearch(pt->refsock,pt->Group(pt->ZSlave));
	if (error!=0)
	{
		ErrorStringGet(pt->refsock,error,pt->last_err);
		_endthread();;
	}

//	 make XSlave and ZSlave in slave mode
	error=SingleAxisSlaveParametersSet(pt->refsock,pt->Group(pt->XSlave),pt->XMaster,1);
	if (error!=0)
	{
		ErrorStringGet(pt->refsock,error,pt->last_err);
		_endthread();;
	}
	error=SingleAxisSlaveModeEnable(pt->refsock,pt->Group(pt->XSlave));
	if (error!=0)
	{
		ErrorStringGet(pt->refsock,error,pt->last_err);
		_endthread();;
	}
	error=SingleAxisSlaveParametersSet(pt->refsock,pt->Group(pt->ZSlave),pt->ZMaster,1);
	if (error!=0)
	{
		ErrorStringGet(pt->refsock,error,pt->last_err);
		_endthread();;
	}
	error=SingleAxisSlaveModeEnable(pt->refsock,pt->Group(pt->ZSlave));
	if (error!=0)
	{
		ErrorStringGet(pt->refsock,error,pt->last_err);
		_endthread();;
	}
	//error=SingleAxisSlaveModeEnable(pt->refsock,pt->Group(pt->XSlave));
	//if (error!=0)
	//{
	//	ErrorStringGet(pt->refsock,error,pt->last_err);
	//	_endthread();;
	//}
	//error=SingleAxisSlaveModeEnable(pt->refsock,pt->Group(pt->ZSlave));
	//if (error!=0)
	//{
	//	ErrorStringGet(pt->refsock,error,pt->last_err);
	//	_endthread();;
	//}
	//

 
	_endthread();
}
int XPS2d::Reference(void)
{
	
	if(_beginthread(backRef,0,(void *)this)==-1)
	{
		return (false);
	}	
	return (true);

	

}

char * XPS2d::Group(char* Positionner)
{
	static char  group[20];
	sprintf(group,Positionner);
	char sep[]=".";
	char *token;
	token = strtok( &group[0], sep );
	return &group[0];
}

bool  XPS2d::GatheringDataReady (void)
{
	return GathDataReady;
}
bool XPS2d::IsMoving(void)
{
	int error,status2;

	if (mvthread==1)
		return true;
	error=GroupStatusGet(comupath,XZ,&status2);
	if (error!=0)
	{
		sprintf(last_err,"XPS:can't getstatus of XZ ");
		return true;
	}
	return ( (status2==44) || (status2==45));
}

static void backTrajExec(void *p)
{
	XPS2d *pt;
	pt=(XPS2d *)p;
	int error;

	error=XYLineArcExecution (pt->mvsock, pt->XZ, pt->trajname ,pt->trajV,pt->trajA,pt->trajN);
	if (error!=0)
	{
		pt->TrajectoryRunning=false;
		ErrorStringGet(pt->mvsock,error,pt->last_err);
		_endthread();;
	}
	pt->TrajectoryRunning=false;
	_endthread();;

}
int XPS2d::ExecTraj(char *fname,double Velocity,double acceleration,int ExecutionNumber,int backgr)
{

	sprintf (trajname,fname);
	trajV=Velocity;
	trajA=acceleration;
	trajN=ExecutionNumber;
	TrajectoryRunning=true;
	int error;
	if ( backgr != 0)
	{
		if(_beginthread(backTrajExec,0,(void *)this)==-1)
		{
			TrajectoryRunning=false;
			return (false);
		}
		else
		{
			return (true);
		}
	}
	else
	{
		error=XYLineArcExecution (mvsock, XZ, trajname ,trajV,trajA,trajN);
		if (error!=0)
		{
			TrajectoryRunning=false;
			ErrorStringGet(mvsock,error,last_err);
			return (false);
		}
		TrajectoryRunning=false;  // temporary remove to allow gathering finish on the same socket.
		return true;
	}

	
}

int XPS2d::ReadTaper(double * XTaper,double *ZTaper)
{
	int error;
	double pos[2];

	error=GroupPositionCurrentGet(comupath,XZ,2,&(pos[0]));
	if (error!=0)
	{
		ErrorStringGet(comupath,error,last_err);
		return false;
	}
	*XTaper=pos[0]*Direction[0];*ZTaper=pos[1]*Direction[1];
	error=GroupPositionCurrentGet(comupath,Group(XSlave),1,&(pos[0]));
	if (error!=0)
	{
		ErrorStringGet(comupath,error,last_err);
		return false;
	}
	error=GroupPositionCurrentGet(comupath,Group(ZSlave),1,&(pos[1]));
	if (error!=0)
	{
		ErrorStringGet(comupath,error,last_err);
		return false;
	}
	*XTaper-=pos[0]*Direction[0];*ZTaper-=pos[1]*Direction[1];
	
	*ZTaper*=-1;
	*XTaper*=-1;


	return true;
}


static void backSetTaper(void *p)
{
	XPS2d *pt;
	pt=(XPS2d *)p;
	double  CurrentPos[2],NewPos[2],CurrentTaper[2];
	int error;
	if(!pt->ReadPos(&CurrentPos[0],&CurrentPos[1]))
	{
		strcat (pt->last_err,"error in read Pos from SetTaper\n");
		pt->mvthread=0;
		_endthread();
	}
	if(!pt->ReadTaper(&CurrentTaper[0],&CurrentTaper[1]))
	{
		strcat (pt->last_err,"error in read Pos from SetTaper\n");
		pt->mvthread=0;
		_endthread();
	}
	if(_isnan(pt->XTset))
		NewPos[0]=CurrentPos[0]-CurrentTaper[0]/2;
	else
		NewPos[0]=CurrentPos[0]-pt->XTset/2;
	if(_isnan(pt->ZTset))
		NewPos[1]=CurrentPos[1]-CurrentTaper[1]/2;
	else
		NewPos[1]=CurrentPos[1]-pt->ZTset/2;
	if (!pt->SetSlaveMode(-1))
	{
		pt->mvthread=0;
		_endthread();
	}
	
	NewPos[0]*=pt->Direction[0];
	NewPos[1]*=pt->Direction[1];

	TCP_SetTimeout (pt->mvsock,5000);
	error=GroupMoveAbsolute (pt->mvsock, pt->XZ, 2, &NewPos[0]);
	if (error!=0)
	{
		ErrorStringGet(pt->mvsock,error,pt->last_err);
		pt->SetSlaveMode(1);
		pt->mvthread=0;
		_endthread();;
	}
	//Sleep(100);
	TCP_SetTimeout (pt->mvsock,pt->timeout);
	pt->SetSlaveMode(1);
		
	
	pt->mvthread=0;
	_endthread();
}
int XPS2d::SetTaper(double XTaper, double ZTaper)
{
	XTset=XTaper;ZTset=ZTaper;
	if (IsMoving())
	{
		sprintf (last_err,"Set Taper : XZ Group Not Ready");
		return false;
	}
	mvthread=1;
	if(_beginthread(backSetTaper,0,(void *)this)==-1)
	{
		return (false);
	}	
	return (true);

	return 0;
}

int XPS2d::SetSlaveMode(double factor)
{
	int error;
	error=SingleAxisSlaveModeDisable(mvsock,Group(XSlave));
	if (error!=0)
	{
		ErrorStringGet(mvsock,error,last_err);
		return false;
	}
	error=SingleAxisSlaveParametersSet(mvsock,Group(XSlave),XMaster,factor);
	if (error!=0)
	{
		ErrorStringGet(mvsock,error,last_err);
		return false;
	}
	error=SingleAxisSlaveModeEnable(mvsock,Group(XSlave));
	if (error!=0)
	{
		ErrorStringGet(mvsock,error,last_err);
		return false;
	}	
	error=SingleAxisSlaveModeDisable(mvsock,Group(ZSlave));
	if (error!=0)
	{
		ErrorStringGet(mvsock,error,last_err);
		return false;
	}

	error=SingleAxisSlaveParametersSet(mvsock,Group(ZSlave),ZMaster,factor);
	if (error!=0)
	{
		ErrorStringGet(mvsock,error,last_err);
		return false;
	}
	error=SingleAxisSlaveModeEnable(mvsock,Group(ZSlave));
	if (error!=0)
	{
		ErrorStringGet(mvsock,error,last_err);
		return false;
	}	


	return true;
}

int XPS2d::LineTo(double X, double Z,double velocity, double accel,int bg)
{
	char name[20];
	char Content[1000];
	char tmp[100];
	double CX,CZ;
	TrajectoryRunning=true;
	if(!ReadPos(&CX,&CZ))
		return false;

	sprintf(name,"LineTo.trj");
	sprintf(Content,"FirstTangent = 0 ;Degrees\n");
	strcat (Content,"DiscontinuityAngle = .01 ; Degrees\n\n");
	CX=X-CX; CZ=Z-CZ;
//	CX*=Direction[0];CZ*=Direction[1];
	if ((CX==0)&&(CZ==0))
	{
		sprintf(last_err,"�lready at pos\n");
		TrajectoryRunning=false;
		return false;
	}
	sprintf (tmp,"Line = %lf,%lf\n",CX,CZ);
	strcat(Content,tmp);

	if(!CreateTraj(name, Content))
	{
		TrajectoryRunning=false;
		return false;
	}
	double distance=sqrt(CX*CX+CZ*CZ);
	double tparcours=TimeToMove(distance,velocity,accel);
	TCP_SetTimeout (mvsock,tparcours*3000 );
	if(!ExecTraj(name,velocity,accel,1,bg))
	{
		TrajectoryRunning=false;
		TCP_SetTimeout (mvsock,timeout );
		return false;
	}
	TCP_SetTimeout (mvsock,timeout );
	return true;
}

int XPS2d::Abort(void)
{
	int error;

	error=GroupMoveAbort(AbortSock,XZ);
	if (error!=0)
	{
		ErrorStringGet(AbortSock,error,last_err);
		return false;
	}
	TrajectoryRunning=false;
	return true;
}


static void BackLineAndTrig(void *p)
{
	XPS2d *pt;
	pt=(XPS2d *)p;
	int error;
	int ret;
	double CPos[2];
	pt->ReadPos(&CPos[0],&CPos[1]);
	if((CPos[0]!=pt->LT_XStart)||(CPos[1]!=pt->LT_ZStart))
	{
//		Pos[0]=pt->LT_XStart;Pos[1]=pt->LT_ZStart;
//		error=GroupMoveAbsolute (pt->mvsock, pt->XZ, 2, &Pos[0]);
		if(!pt->MoveAbs(pt->LT_XStart,pt->LT_ZStart,1))
		{
			pt->TrajectoryRunning=false;
			_endthread();
		}
	}
	//Sleep(100);
	char EvList[30];
	sprintf(EvList,"");
	error=EventExtendedAllGet(pt->mvsock,EvList);
	if( strlen(EvList)!=0)
		{
			int i;
			for(i=0;i<50;i++)
				EventExtendedRemove(pt->mvsock,i);
		}
	//Gathering donnee 
	error=GatheringReset(pt->mvsock);
	if (error!=0)
	{
		pt->TrajectoryRunning=false;
			ErrorStringGet(pt->mvsock,error,pt->last_err);
			_endthread();
	}
	double EndLen=pt->LT_FirstTrigLen+(pt->LT_TrigNb*pt->LT_TrigStep)-0.25*pt->LT_TrigStep;
	error=XYLineArcPulseOutputSet(pt->mvsock,"XZ",pt->LT_FirstTrigLen,EndLen,pt->LT_TrigStep/2);
	if (error!=0)
	{
		pt->TrajectoryRunning=false;
			ErrorStringGet(pt->mvsock,error,pt->last_err);
			_endthread();
	}
	char gathstr[300];
	sprintf (gathstr,"%s.CurrentPosition;%s.CurrentPosition;%s.CurrentPosition;%s.CurrentPosition,%s.CurrentVelocity,%s.CurrentVelocity",
		pt->XMaster,pt->ZMaster,pt->XSlave,pt->ZSlave,pt->XMaster,pt->ZMaster);
	error=GatheringConfigurationSet(pt->mvsock,4,gathstr);
	if (error!=0)
	{
		pt->TrajectoryRunning=false;
			ErrorStringGet(pt->mvsock,error,pt->last_err);
			_endthread();
	}
	// Trigger surpulse trajectoire.
	error=EventExtendedConfigurationTriggerSet(pt->mvsock,2,"Always;XZ.XYLineArc.TrajectoryPulse","0;0","0;0","0;0","0;0");
		if (error!=0)
		{
			pt->TrajectoryRunning=false;
			ErrorStringGet(pt->mvsock,error,pt->last_err);
			_endthread();;
		}

	error=EventExtendedConfigurationActionSet(pt->mvsock,2,"GPIO1.DO.DOToggle;GatheringOneData","1;0","0;0","0;0","0;0");
		if (error!=0)
		{
			pt->TrajectoryRunning=false;
			ErrorStringGet(pt->mvsock,error,pt->last_err);
			_endthread();;
		}
		int ev;
	error= EventExtendedStart(pt->mvsock,&ev);
		if (error!=0)
		{
			pt->TrajectoryRunning=false;
			ErrorStringGet(pt->mvsock,error,pt->last_err);
			_endthread();;
		}
	ret=pt->LineTo(pt->LT_XEndPos,pt->LT_ZEndPos,pt->LT_Velocity,pt->LT_Acceleration,0);
		if (ret!=(int)true)
		{
			pt->TrajectoryRunning=false;
			_endthread();;
		}
	
	error=	GatheringStopAndSave(pt->mvsock);
	if (error!=0)
		{
			pt->TrajectoryRunning=false;
			ErrorStringGet(pt->mvsock,error,pt->last_err);
			_endthread();;
		}
	
	error= EventExtendedRemove(pt->mvsock,ev);
		if (error!=0)
		{
			pt->TrajectoryRunning=false;
			ErrorStringGet(pt->mvsock,error,pt->last_err);
			_endthread();;
		}
	pt->GathDataReady=true;
	pt->TrajectoryRunning=false;
	_endthread();
}
int XPS2d::LineAndTrig(double XStartPos,double ZStartPos , double FirstTrigLen , double TrigStep , int TrigNb, double XEndPos,double ZEndPos, double Velocity, double Acceleration)
{

	if (IsMoving())
	{
		sprintf (last_err,"Line And Trig : XZ Group Not Ready");
		return false;
	}
	GathDataReady=false;
	TrajectoryRunning=false;	
	int error=GPIODigitalSet(mvsock,"GPIO1.DO",1,0);
	if (error!=0)
		{
			TrajectoryRunning=false;
			ErrorStringGet(mvsock,error,last_err);
			return (false);
		}
	TrajectoryRunning=true;
	LT_XStart=XStartPos;
	LT_ZStart=ZStartPos;
	LT_FirstTrigLen =FirstTrigLen ;
	LT_TrigStep = TrigStep;
	LT_TrigNb = TrigNb;
	LT_XEndPos = XEndPos;
	LT_XEndPos =XEndPos ;
	LT_ZEndPos = ZEndPos;
	LT_Velocity = Velocity;
	LT_Acceleration = Acceleration;

	if(_beginthread(BackLineAndTrig,0,(void *)this)==-1)
	{
		TrajectoryRunning=false;
		return (false);
	}	
	return (true);

}

int XPS2d::GetGatheringData(double **XPos,double **ZPos,double **VX,double **VZ,int *nb)
{
	int error;
	int maxsample;

	if (GathDataReady==false)
	{
		sprintf(last_err,"No Position Data ready ");
		return (false);
	}

	error=GatheringCurrentNumberGet(mvsock,&SampleN,&maxsample);
		if (error!=0)
		{
			ErrorStringGet(mvsock,error,last_err);
			return (0);
		}

	ZGath=(double *)malloc((SampleN+1)*sizeof(double));
	if (ZGath==NULL)
		return false;

	XGath=(double *)malloc((SampleN+1)*sizeof(double));
	if (XGath==NULL)
		return false;
	VzGath=(double *)malloc((SampleN+1)*sizeof(double));
	if (ZGath==NULL)
		return false;

	VxGath=(double *)malloc((SampleN+1)*sizeof(double));
	if (XGath==NULL)
		return false;

	int i;
	double *Xpt,*Zpt,*Vxpt,*Vzpt;
	Xpt=XGath;Zpt=ZGath;Vxpt=VxGath;Vzpt=VzGath;
	double XM,ZM,XS,ZS,Vx,Vz;
	char tmp[100];
	for(i=0;i<SampleN;i++)
	{
		error=GatheringDataGet(mvsock,i,&tmp[0]);
		if ( sscanf(&tmp[0],"%lf;%lf;%lf;%lf;%lf;%lf",&XM,&ZM,&XS,&ZS,&Vx,&Vz)!=6)
		{
			sprintf(last_err,"error reading Data Gathering     ");
			strcat (last_err,&tmp[0]);

			*nb=i-1;
			return (false);
		}
		*Xpt=(XM+XS)/2*Direction[0];
		*Zpt=(ZM+ZS)/2*Direction[1];
		*Vzpt=Vz*Direction[0];
		*Vxpt=Vx*Direction[1];
		Xpt++;
		Zpt++;
		Vzpt++;Vxpt++;
		
	}
	*XPos=XGath;
	*ZPos=ZGath;
	*VX=VxGath;
	*VZ=VzGath;
	*nb=SampleN;
	return true;
}

int XPS2d::MasterCircle(double XCenter, double ZCenter, double Teta2, double velocity, double accel, int bg)
{

	char name[20];
	char Content[1000];
	char tmp[100];
	double CX, CZ;
//	double TX, TZ;
	if (!ReadMasterPos(&CX, &CZ))
		return false;
	int error;
	error = SingleAxisSlaveModeDisable(mvsock, Group(XSlave));
	if (error != 0)
	{
		ErrorStringGet(mvsock, error,last_err); return false;
	}
	error = SingleAxisSlaveModeDisable(mvsock, Group(ZSlave));
	if (error != 0)
	{
		ErrorStringGet(mvsock, error, last_err); return false;
	}


	double R;
	TrajectoryRunning = true;
	R = sqrt((CX - XCenter)*(CX - XCenter) + (CZ - ZCenter)*(CZ - ZCenter));
	R = floor(R * 1000 + .5) / 1000;
	double Teta1;
	double Ftang;
	if (fabs(CX - XCenter)<1e-3)
	{
		if (CZ>ZCenter)
			Teta1 = 90;
		else
			Teta1 = -90;
	}
	else
	{
		Teta1 = 180 / PI*atan((CZ - ZCenter) / (CX - XCenter));
	}
	if (CX<XCenter)
		Teta1 += 180;
	Ftang = Teta1 + 90;
	if (Teta2<0)
		Ftang -= 180;

	if (Ftang>360)
		Ftang -= 360;
	if (Ftang<0)
		Ftang += 360;
	//	Ftang*=Direction[0]*Direction[1];
	//	if (Direction[0]==-1)
	//	{
	//		Ftang=180-Ftang;
	//	}
	//	if (Direction[1]==-1)
	//	{
	//		Ftang=360-Ftang;
	//	}
	sprintf(name, "Circle.trj");

	sprintf(Content, "FirstTangent = %f ;Degrees\n", Ftang);
	strcat(Content, "DiscontinuityAngle = .001 ; Degrees\n\n");
	double DTeta;
	//	DTeta=(Teta2)*Direction[0]*Direction[1];
	DTeta = (Teta2);
	sprintf(tmp, "Arc = %lf,%lf\n", R, DTeta);
	strcat(Content, tmp);
	double len = 2 * PI*R*DTeta / 360;
	//	double tparcours=fabs(len/velocity);
	double tparcours = TimeToMove(len, velocity, accel);
	if (!CreateTraj(name, Content))
		return false;
	TCP_SetTimeout(mvsock, tparcours * 3000);
	if (!ExecTraj(name, velocity, accel, 1, bg))
	{
		TCP_SetTimeout(mvsock, timeout);
		return false;
	}
	// RE SET SLAVE MODE
	error = SingleAxisSlaveModeEnable(mvsock, Group(XSlave));
	if (error != 0)
	{
		ErrorStringGet(mvsock, error, last_err);
		return false;
	}
	error = SingleAxisSlaveModeEnable(mvsock, Group(ZSlave));
	if (error != 0)
	{
		ErrorStringGet(mvsock, error, last_err);
		return false;
	}
	TCP_SetTimeout(mvsock, timeout);

	return true;

}

int XPS2d::Circle(double XCenter, double ZCenter, double Teta2,double velocity,double accel,int bg)
{
	
	char name[20];
	char Content[1000];
	char tmp[100];
	double CX,CZ;
	double TX,TZ;
	if(!ReadPos(&CX,&CZ))
		return false;
	if(!ReadTaper(&TX,&TZ))
		return false;


	double R;
	TrajectoryRunning=true;
	R=sqrt((CX-XCenter)*(CX-XCenter)+(CZ-ZCenter)*(CZ-ZCenter));
	R=floor(R*1000+.5)/1000;
	double Teta1;
	double Ftang;
	if(fabs(CX-XCenter)<1e-3)
	{
		if (CZ>ZCenter)
			Teta1=90;
		else
			Teta1=-90;
	}
	else
	{
	Teta1=180/PI*atan((CZ-ZCenter)/(CX-XCenter));
	}
	if (CX<XCenter)
		Teta1+=180;
	Ftang=Teta1+90;
	if(Teta2<0)
		Ftang-=180;

	if (Ftang>360)
		Ftang-=360;
	if (Ftang<0)
		Ftang+=360;
//	Ftang*=Direction[0]*Direction[1];
//	if (Direction[0]==-1)
//	{
//		Ftang=180-Ftang;
//	}
//	if (Direction[1]==-1)
//	{
//		Ftang=360-Ftang;
//	}
	sprintf(name,"Circle.trj");
	
	sprintf(Content,"FirstTangent = %f ;Degrees\n",Ftang);
	strcat (Content,"DiscontinuityAngle = .001 ; Degrees\n\n");
	double DTeta;
//	DTeta=(Teta2)*Direction[0]*Direction[1];
	DTeta=(Teta2);
	sprintf (tmp,"Arc = %lf,%lf\n",R,DTeta);
	strcat(Content,tmp);
	double len=2*PI*R*DTeta/360;
//	double tparcours=fabs(len/velocity);
	double tparcours=TimeToMove(len,velocity,accel);
	if(!CreateTraj(name, Content))
		return false;
	TCP_SetTimeout (mvsock, tparcours*3000);
	if(!ExecTraj(name,velocity,accel,1,bg))
	{
		TCP_SetTimeout (mvsock,timeout );
		return false;
	}
	TCP_SetTimeout (mvsock,timeout );

	return true;

}

static void BackCircleAndTrig(void *p)
{
	XPS2d *pt;
	pt=(XPS2d *)p;
	int error;
	double Pos[2],CPos[2];
	pt->ReadPos(&CPos[0],&CPos[1]);
	Pos[0]=pt->CT_XCenter+(pt->CT_R*cos(PI/180*pt->CT_OBeg));
	Pos[1]=pt->CT_ZCenter+(pt->CT_R*sin(PI/180*pt->CT_OBeg));
	if((CPos[0]!=Pos[0])||(CPos[1]!=Pos[1]))
	{
//		error=GroupMoveAbsolute (pt->mvsock, pt->XZ, 2, &Pos[0]);
		if(!pt->MoveAbs(Pos[0],Pos[1],1))
		{
			pt->TrajectoryRunning=false;
			_endthread();
		}
	}
	//Sleep(1000);
	char EvList[30];
	sprintf(EvList,"");
	error=EventExtendedAllGet(pt->mvsock,EvList);
	if( strlen(EvList)!=0)
		{
			int i;
			for(i=0;i<50;i++)
				EventExtendedRemove(pt->mvsock,i);
		}
	//Gathering donnee 
	error=GatheringReset(pt->mvsock);
	if (error!=0)
	{
		pt->TrajectoryRunning=false;
			ErrorStringGet(pt->mvsock,error,pt->last_err);
			_endthread();
	}
	double FirstTrigLen,EndTrigLen,StepTrigLen;
	FirstTrigLen=fabs(2*PI*pt->CT_R*(pt->CT_OFirstTrig-pt->CT_OBeg)/360);
	StepTrigLen=fabs(2*PI*pt->CT_R*(pt->CT_OTrigLen)/360);
	EndTrigLen=FirstTrigLen+(pt->CT_TrigNb*StepTrigLen)-0.25*StepTrigLen;
	error=XYLineArcPulseOutputSet(pt->mvsock,pt->XZ,FirstTrigLen,EndTrigLen,StepTrigLen/2);
	if (error!=0)
	{
			pt->TrajectoryRunning=false;
		ErrorStringGet(pt->mvsock,error,pt->last_err);
			_endthread();
	}
	char gathstr[300];
	sprintf (gathstr,"%s.CurrentPosition;%s.CurrentPosition;%s.CurrentPosition;%s.CurrentPosition,%s.CurrentVelocity,%s.CurrentVelocity",
		pt->XMaster,pt->ZMaster,pt->XSlave,pt->ZSlave,pt->XMaster,pt->ZMaster);
	error=GatheringConfigurationSet(pt->mvsock,4,gathstr);
	if (error!=0)
	{
			pt->TrajectoryRunning=false;
		ErrorStringGet(pt->mvsock,error,pt->last_err);
			_endthread();
	}
	// Trigger surpulse trajectoire.
	error=EventExtendedConfigurationTriggerSet(pt->mvsock,2,"Always;XZ.XYLineArc.TrajectoryPulse","0;0","0;0","0;0","0;0");
		if (error!=0)
		{
			pt->TrajectoryRunning=false;
		ErrorStringGet(pt->mvsock,error,pt->last_err);
			_endthread();;
		}

	error=EventExtendedConfigurationActionSet(pt->mvsock,2,"GPIO1.DO.DOToggle;GatheringOneData","1;0","0;0","0;0","0;0");
		if (error!=0)
		{
			pt->TrajectoryRunning=false;
		ErrorStringGet(pt->mvsock,error,pt->last_err);
			_endthread();;
		}
		int ev;
	error= EventExtendedStart(pt->mvsock,&ev);
		if (error!=0)
		{
			pt->TrajectoryRunning=false;
		ErrorStringGet(pt->mvsock,error,pt->last_err);
			_endthread();;
		}
	int ret;
	pt->TrajectoryRunning=true;
	ret=pt->Circle(pt->CT_XCenter,pt->CT_ZCenter,pt->CT_OEnd,pt->CT_Velocity,pt->CT_accel,0);
		if (ret!=(int)true)
		{
		pt->TrajectoryRunning=false;
		_endthread();;
		}

	error=	GatheringStopAndSave(pt->mvsock);
	if (error!=0)
		{
			pt->TrajectoryRunning=false;
			ErrorStringGet(pt->mvsock,error,pt->last_err);
			_endthread();;
		}
	error= EventExtendedRemove(pt->mvsock,ev);
		if (error!=0)
		{
			pt->TrajectoryRunning=false;
			ErrorStringGet(pt->mvsock,error,pt->last_err);
			_endthread();;
		}

		pt->TrajectoryRunning=false;
		pt->GathDataReady=true;
		_endthread();
}

int XPS2d::CircleAndTrig(double XCenter, double ZCenter, double R, double OBeg, double OFirstTrig, double OTrigLen, int OTrigNb, double OEnd, double Velocity, double Acceleration)
{
	if (IsMoving())
	{
		sprintf (last_err,"Circle And Trig : XZ Group Not Ready");
		return false;
	}
	GathDataReady=false;
	TrajectoryRunning=true;
	int error=GPIODigitalSet(mvsock,"GPIO1.DO",1,0);
	if (error!=0)
		{
			TrajectoryRunning=false;
			ErrorStringGet(mvsock,error,last_err);
			return (false);
		}
	
	CT_XCenter=XCenter;
	CT_ZCenter=ZCenter;
	CT_R=R;

	CT_OBeg=OBeg;
	CT_OFirstTrig=OFirstTrig;
	CT_OTrigLen=OTrigLen;
	CT_TrigNb=OTrigNb;
	CT_OEnd=OEnd;
	CT_Velocity=Velocity;
	CT_accel=Acceleration;
	double 	T=(OEnd-OBeg)/Velocity;
	TCP_SetTimeout (mvsock,T*1500 );

	if(_beginthread(BackCircleAndTrig,0,(void *)this)==-1)
	{
		TrajectoryRunning=false;
		return (false);
	}	
	return (true);
}

static void BackMasterCircleAndTrig(void *p)
{
	XPS2d *pt;
	pt = (XPS2d *)p;
	int error;
	double Pos[2], CPos[2];
	pt->ReadPos(&CPos[0], &CPos[1]);
// MOVE MASTER AND SLAVE ON AXIS
	Pos[0] = pt->CT_XCenter;
	Pos[1] = pt->CT_ZCenter;
	if ((CPos[0] != Pos[0]) || (CPos[1] != Pos[1]))
	{
		if (!pt->MoveAbs(Pos[0], Pos[1], 1))
		{
			pt->TrajectoryRunning = false;
			_endthread();
		}
	}
	double XMasterCenter, ZMasterCenter;
	pt->ReadMasterPos(&XMasterCenter, &ZMasterCenter);
	// MOVE MASTER ON THE STARTPOINT OF CIRCLE
	Pos[0] = (pt->CT_R*cos(PI / 180 * pt->CT_OBeg)/2);
	Pos[1] = (pt->CT_R*sin(PI / 180 * pt->CT_OBeg)/2);
	if ((CPos[0] != Pos[0]) || (CPos[1] != Pos[1]))
	{
		if (!pt->MasterMoveRel(Pos[0], Pos[1], 1))
		{
			pt->TrajectoryRunning = false;
			_endthread();
		}
	// DISABLE SLAVE MOVE  .


	}
	//Sleep(1000);
	char EvList[30];
	sprintf(EvList, "");
	error = EventExtendedAllGet(pt->mvsock, EvList);
	if (strlen(EvList) != 0)
	{
		int i;
		for (i = 0; i<50; i++)
			EventExtendedRemove(pt->mvsock, i);
	}
	//Gathering donnee 
	error = GatheringReset(pt->mvsock);
	if (error != 0)
	{
		pt->TrajectoryRunning = false;
		ErrorStringGet(pt->mvsock, error, pt->last_err);
		_endthread();
	}
	double FirstTrigLen, EndTrigLen, StepTrigLen;
	FirstTrigLen = fabs(2 * PI*pt->CT_R*(pt->CT_OFirstTrig - pt->CT_OBeg) / 360);
	StepTrigLen = fabs(2 * PI*pt->CT_R*(pt->CT_OTrigLen) / 360);
	EndTrigLen = FirstTrigLen + (pt->CT_TrigNb*StepTrigLen) - 0.25*StepTrigLen;
	error = XYLineArcPulseOutputSet(pt->mvsock, pt->XZ, FirstTrigLen, EndTrigLen, StepTrigLen / 2);
	if (error != 0)
	{
		pt->TrajectoryRunning = false;
		ErrorStringGet(pt->mvsock, error, pt->last_err);
		_endthread();
	}
	char gathstr[300];
	sprintf(gathstr, "%s.CurrentPosition;%s.CurrentPosition;%s.CurrentPosition;%s.CurrentPosition,%s.CurrentVelocity,%s.CurrentVelocity",
		pt->XMaster, pt->ZMaster, pt->XSlave, pt->ZSlave, pt->XMaster, pt->ZMaster);
	error = GatheringConfigurationSet(pt->mvsock, 4, gathstr);
	if (error != 0)
	{
		pt->TrajectoryRunning = false;
		ErrorStringGet(pt->mvsock, error, pt->last_err);
		_endthread();
	}
	// Trigger surpulse trajectoire.
	error = EventExtendedConfigurationTriggerSet(pt->mvsock, 2, "Always;XZ.XYLineArc.TrajectoryPulse", "0;0", "0;0", "0;0", "0;0");
	if (error != 0)
	{
		pt->TrajectoryRunning = false;
		ErrorStringGet(pt->mvsock, error, pt->last_err);
		_endthread();;
	}

	error = EventExtendedConfigurationActionSet(pt->mvsock, 2, "GPIO1.DO.DOToggle;GatheringOneData", "1;0", "0;0", "0;0", "0;0");
	if (error != 0)
	{
		pt->TrajectoryRunning = false;
		ErrorStringGet(pt->mvsock, error, pt->last_err);
		_endthread();;
	}
	int ev;
	error = EventExtendedStart(pt->mvsock, &ev);
	if (error != 0)
	{
		pt->TrajectoryRunning = false;
		ErrorStringGet(pt->mvsock, error, pt->last_err);
		_endthread();;
	}
	int ret;
	pt->TrajectoryRunning = true;
	ret = pt->MasterCircle(XMasterCenter, ZMasterCenter, pt->CT_OEnd, pt->CT_Velocity, pt->CT_accel, 0);
	if (ret != (int)true)
	{
		pt->TrajectoryRunning = false;
		_endthread();;
	}

	error = GatheringStopAndSave(pt->mvsock);
	if (error != 0)
	{
		pt->TrajectoryRunning = false;
		ErrorStringGet(pt->mvsock, error, pt->last_err);
		_endthread();;
	}
	error = EventExtendedRemove(pt->mvsock, ev);
	if (error != 0)
	{
		pt->TrajectoryRunning = false;
		ErrorStringGet(pt->mvsock, error, pt->last_err);
		_endthread();;
	}

	// MOVE MASTER on THE Axis
	Pos[0] = pt->CT_XCenter;
	Pos[1] = pt->CT_ZCenter;

	{
		if (!pt->MasterMoveAbs(Pos[0], Pos[1], 1))
		{
			pt->TrajectoryRunning = false;
			_endthread();
		}
	}

	pt->TrajectoryRunning = false;
	pt->GathDataReady = true;
	_endthread();
}
int XPS2d::MasterCircleAndTrig(double XCenter, double ZCenter, double R, double OBeg, double OFirstTrig, double OTrigLen, int OTrigNb, double OEnd, double Velocity, double Acceleration)
{
	if (IsMoving())
	{
		sprintf(last_err, "Master Circle And Trig : XZ Group Not Ready");
		return false;
	}
	GathDataReady = false;
	TrajectoryRunning = true;
	int error = GPIODigitalSet(mvsock, "GPIO1.DO", 1, 0);
	if (error != 0)
	{
		TrajectoryRunning = false;
		ErrorStringGet(mvsock, error, last_err);
		return (false);
	}

	CT_XCenter = XCenter;
	CT_ZCenter = ZCenter;
	CT_R = R;
	CT_OBeg = OBeg;
	CT_OFirstTrig = OFirstTrig;
	CT_OTrigLen = OTrigLen;
	CT_TrigNb = OTrigNb;
	CT_OEnd = OEnd;
	CT_Velocity = Velocity;
	CT_accel = Acceleration;
	double 	T = (OEnd - OBeg) / Velocity;
	TCP_SetTimeout(mvsock, T * 1500);

	if (_beginthread(BackMasterCircleAndTrig, 0, (void *)this) == -1)
	{
		TrajectoryRunning = false;
		return (false);
	}
	return (true);
}


bool XPS2d::TrajRunning(void)
{
	return TrajectoryRunning;
	
}
static void backMoveRel(void *p)
{
	XPS2d *pt;
	pt=(XPS2d *)p;
	int error;
	double pos[2],nowpos[2],vel[2],accel[2];
	double taper[2];
	if (!pt->ReadTaper(&taper[0],&taper[1]))
	{
		pt->mvthread=0;
		_endthread();
	}
	if (!pt->ReadPos(&(nowpos[0]),&(nowpos[1])))
	{
		pt->mvthread=0;
		_endthread();
	}
	if (!pt->ReadVelocity(&(vel[0]),&(vel[1])))
	{
		pt->mvthread=0;
		_endthread();
	}
	if (!pt->ReadAcceleration(&(accel[0]),&(accel[1])))
	{
		pt->mvthread=0;
		_endthread();
	}
	
	double T0,T1,T;
	T0=fabs((pt->Xset)/vel[0])+vel[0]/accel[0];
	T1=fabs((pt->Zset)/vel[1])+vel[1]/accel[1];
	T=max(T0,T1);
	TCP_SetTimeout (pt->mvsock,T*1200 );
	pos[0]=pt->Xset;pos[1]=pt->Zset;
	pos[0]*=pt->Direction[0];
	pos[1]*=pt->Direction[1];
	if(_isnan(pt->Xset))
	{
		if(_isnan(pt->Zset))
		{//nothing to move
			pt->mvthread=0;
			_endthread();;
		}
		else
		{// move only Z
			
			error=GroupMoveRelative (pt->mvsock, pt->ZMaster, 1, &pos[1]);
			if (error!=0)
			{
				ErrorStringGet(pt->mvsock,error,pt->last_err);
				pt->mvthread=0;
				_endthread();;
			}
			pt->mvthread=0;
			_endthread();;
			
		}
	}
	else
	{
		if(_isnan(pt->Zset))
		{//move only X
			
			error=GroupMoveRelative (pt->mvsock, pt->XMaster, 1, &pos[0]);
			if (error!=0)
			{
				ErrorStringGet(pt->mvsock,error,pt->last_err);
				pt->mvthread=0;
				_endthread();;
			}
			pt->mvthread=0;
			_endthread();;
			
		}
		else
		{// move X and Z
			error=GroupMoveRelative (pt->mvsock, pt->XZ, 2, &pos[0]);
			if (error!=0)
			{
				ErrorStringGet(pt->mvsock,error,pt->last_err);
				pt->mvthread=0;
				_endthread();;
			}
			pt->mvthread=0;
			_endthread();
			
		}
	}
}
int XPS2d::MoveRel(double Xd, double Zd,int bg)
{
	Xset=Xd;Zset=Zd;
	if (IsMoving())
	{
		sprintf (last_err,"MoveRel : XZ Group Not Ready");
		return false;
	}
	mvthread=1;
	if(_beginthread(backMoveRel,0,(void *)this)==-1)
	{
		return (false);
	}
	if( bg==0)
		return (true);
	do
	{
		Sleep(200);
	}while (mvthread==1);
	
	return true;}

static void backMasterMoveAbs(void *p)
{
	XPS2d *pt;
	pt=(XPS2d *)p;
	int error;
	double pos[2],nowpos[2],vel[2],accel[2];
	double taper[2];
	if (!pt->ReadTaper(&taper[0],&taper[1]))
	{
		pt->mvthread=0;
		_endthread();
	}
	if (!pt->ReadPos(&(nowpos[0]),&(nowpos[1])))
	{
		pt->mvthread=0;
		_endthread();
	}
	if (!pt->ReadVelocity(&(vel[0]),&(vel[1])))
	{
		pt->mvthread=0;
		_endthread();
	}
	if (!pt->ReadAcceleration(&(accel[0]),&(accel[1])))
	{
		pt->mvthread=0;
		_endthread();
	}
	
	double T0,T1,T;
	T0=fabs(2*(nowpos[0]-pt->Xset)/vel[0])+vel[0]/accel[0];
	T1=fabs(2*(nowpos[1]-pt->Zset)/vel[1])+vel[1]/accel[1];
	T=max(T0,T1);
	TCP_SetTimeout (pt->mvsock,T*1200 );
//	pos[0]=pt->Xset+taper[0]/2;pos[1]=pt->Zset+taper[1]/2;
	pos[0]=2*(pt->Xset-nowpos[0]);	pos[1]=2*(pt->Zset-nowpos[1]);
	pos[0]*=pt->Direction[0];
	pos[1]*=pt->Direction[1];


	if(_isnan(pt->Xset))
	{
		if(_isnan(pt->Zset))
		{//nothing to move
			pt->mvthread=0;
			_endthread();;
		}
		else
		{// move only Z
			error=SingleAxisSlaveModeDisable(pt->mvsock,pt->Group(pt->ZSlave));
			if (error!=0)
			{
				ErrorStringGet(pt->mvsock,error,pt->last_err);
				_endthread();
			}		
			error=GroupMoveRelative (pt->mvsock, pt->ZMaster, 1, &pos[1]);
			if (error!=0)
			{
				ErrorStringGet(pt->mvsock,error,pt->last_err);
				pt->mvthread=0;
			}
			error=SingleAxisSlaveModeEnable(pt->mvsock,pt->Group(pt->ZSlave));
			if (error!=0)
			{
				ErrorStringGet(pt->mvsock,error,pt->last_err);
				_endthread();
			}
			pt->mvthread=0;
			_endthread();;
			
		}
	}
	else
	{
		if(_isnan(pt->Zset))
		{//move only X
			error=SingleAxisSlaveModeDisable(pt->mvsock,pt->Group(pt->XSlave));
			if (error!=0)
			{
				ErrorStringGet(pt->mvsock,error,pt->last_err);
				_endthread();
			}			
			error=GroupMoveRelative (pt->mvsock, pt->XMaster, 1, &pos[0]);
			if (error!=0)
			{
				ErrorStringGet(pt->mvsock,error,pt->last_err);
				pt->mvthread=0;
			}
			error=SingleAxisSlaveModeEnable(pt->mvsock,pt->Group(pt->XSlave));
			if (error!=0)
			{
				ErrorStringGet(pt->mvsock,error,pt->last_err);
				_endthread();
			}
			pt->mvthread=0;
			_endthread();;
			
		}
		else
		{// move X and Z
			error=SingleAxisSlaveModeDisable(pt->mvsock,pt->Group(pt->XSlave));
			if (error!=0)
			{
				ErrorStringGet(pt->mvsock,error,pt->last_err);
				_endthread();
			}			
			error=SingleAxisSlaveModeDisable(pt->mvsock,pt->Group(pt->ZSlave));
			if (error!=0)
			{
				ErrorStringGet(pt->mvsock,error,pt->last_err);
				_endthread();
			}			

			error=GroupMoveRelative (pt->mvsock, pt->XZ, 2, &pos[0]);
			if (error!=0)
			{
				ErrorStringGet(pt->mvsock,error,pt->last_err);
				pt->mvthread=0;
			}
			error=SingleAxisSlaveModeEnable(pt->mvsock,pt->Group(pt->XSlave));
			if (error!=0)
			{
				ErrorStringGet(pt->mvsock,error,pt->last_err);
				_endthread();
			}
			error=SingleAxisSlaveModeEnable(pt->mvsock,pt->Group(pt->ZSlave));
			if (error!=0)
			{
				ErrorStringGet(pt->mvsock,error,pt->last_err);
				_endthread();
			}
			pt->mvthread=0;
			_endthread();
			
		}
	}
}

int XPS2d::MasterMoveAbs(double X, double Z,int bg)
{
	Xset=X;Zset=Z;
	if (IsMoving())
	{
		sprintf (last_err,"MasterMoveAbs : XZ Group Not Ready");
		return false;
	}
	mvthread=1;
	if(_beginthread(backMasterMoveAbs,0,(void *)this)==-1)
	{
		return (false);
	}
	if( bg==0)
		return (true);
	do
	{
		Sleep(200);
	}while (mvthread==1);
	
	return true;

}
static void backMasterMoveRel(void *p)
{
	XPS2d *pt;
	pt=(XPS2d *)p;
	int error;
	double pos[2],nowpos[2],vel[2],accel[2];
	double taper[2];
	if (!pt->ReadTaper(&taper[0],&taper[1]))
	{
		pt->mvthread=0;
		_endthread();
	}
	if (!pt->ReadPos(&(nowpos[0]),&(nowpos[1])))
	{
		pt->mvthread=0;
		_endthread();
	}
	if (!pt->ReadVelocity(&(vel[0]),&(vel[1])))
	{
		pt->mvthread=0;
		_endthread();
	}
	if (!pt->ReadAcceleration(&(accel[0]),&(accel[1])))
	{
		pt->mvthread=0;
		_endthread();
	}
	
	double T0,T1,T;
	T0=fabs(2*(pt->Xset)/vel[0])+vel[0]/accel[0];
	T1=fabs(2*(pt->Zset)/vel[1])+vel[1]/accel[1];
	T=max(T0,T1);
	TCP_SetTimeout (pt->mvsock,T*1200);
//	pos[0]=pt->Xset+taper[0]/2;pos[1]=pt->Zset+taper[1]/2;
	pos[0]=2*(pt->Xset);	pos[1]=2*(pt->Zset);
	pos[0]*=pt->Direction[0];
	pos[1]*=pt->Direction[1];
	if(_isnan(pt->Xset))
	{
		if(_isnan(pt->Zset))
		{//nothing to move


			pt->mvthread=0;
			_endthread();;
		}
		else
		{// move only Z
			error=SingleAxisSlaveModeDisable(pt->mvsock,pt->Group(pt->ZSlave));
			if (error!=0)
			{
				ErrorStringGet(pt->mvsock,error,pt->last_err);
			}
			error=GroupMoveRelative (pt->mvsock, pt->ZMaster, 1, &pos[1]);
			if (error!=0)
			{
				ErrorStringGet(pt->mvsock,error,pt->last_err);
			}
			error=SingleAxisSlaveModeEnable(pt->mvsock,pt->Group(pt->ZSlave));
			if (error!=0)
			{
				ErrorStringGet(pt->mvsock,error,pt->last_err);
			}
			pt->mvthread=0;
			_endthread();;
			
		}
	}
	else
	{
		if(_isnan(pt->Zset))
		{//move only X
			error=SingleAxisSlaveModeDisable(pt->mvsock,pt->Group(pt->XSlave));
			if (error!=0)
			{
				ErrorStringGet(pt->mvsock,error,pt->last_err);
			}
			
			error=GroupMoveRelative (pt->mvsock, pt->XMaster, 1, &pos[0]);
			if (error!=0)
			{
				ErrorStringGet(pt->mvsock,error,pt->last_err);
			}
			error=SingleAxisSlaveModeEnable(pt->mvsock,pt->Group(pt->XSlave));
			if (error!=0)
			{
				ErrorStringGet(pt->mvsock,error,pt->last_err);
			}

			pt->mvthread=0;
			_endthread();;
			
		}
		else
		{// move X and Z
			error=SingleAxisSlaveModeDisable(pt->mvsock,pt->Group(pt->XSlave));
			if (error!=0)
			{
				ErrorStringGet(pt->mvsock,error,pt->last_err);
			}
			error=SingleAxisSlaveModeDisable(pt->mvsock,pt->Group(pt->ZSlave));
			if (error!=0)
			{
				ErrorStringGet(pt->mvsock,error,pt->last_err);
			}
			error=GroupMoveRelative (pt->mvsock, pt->XZ, 2, &pos[0]);
			if (error!=0)
			{
				ErrorStringGet(pt->mvsock,error,pt->last_err);
			}
			error=SingleAxisSlaveModeEnable(pt->mvsock,pt->Group(pt->XSlave));
			if (error!=0)
			{
				ErrorStringGet(pt->mvsock,error,pt->last_err);
			}
			error=SingleAxisSlaveModeEnable(pt->mvsock,pt->Group(pt->ZSlave));
			if (error!=0)
			{
				ErrorStringGet(pt->mvsock,error,pt->last_err);
			}
			pt->mvthread=0;
			_endthread();
			
		}
	}
}
int XPS2d::MasterMoveRel(double X, double Z,int bg)
{
		Xset=X;Zset=Z;
	if (IsMoving())
	{
		sprintf (last_err,"MasterMoveAbs : XZ Group Not Ready");
		return false;
	}
	mvthread=1;
	if(_beginthread(backMasterMoveRel,0,(void *)this)==-1)
	{
		return (false);
	}
	if( bg==0)
		return (true);
	do
	{
		Sleep(200);
	}while (mvthread==1);
	
	return true;

}
int XPS2d::SlaveMoveAbs(double X, double Z,int bg)
{
		sprintf (last_err,"Not Implemented");
		return false;

}
int XPS2d::SlaveMoveRel(double X, double Z,int bg)
{
		sprintf (last_err,"Not Implemented");
		return false;

}


double XPS2d::TimeToMove(double depl,double V ,double A)
{
	if (fabs(depl) >V*V/A)
		return ((fabs(depl)+V*V/A)/V);
	else
		return(2*sqrt(fabs(depl)/A));
}

static void BackTrajAndTrig(void *p)
{
	XPS2d *pt;
	pt=(XPS2d *)p;
	int error;
	double Pos[2],CPos[2];
	pt->ReadPos(&CPos[0],&CPos[1]);
	Pos[0]=pt->TT_XBeg;
	Pos[1]=pt->TT_ZBeg;
	if((CPos[0]!=Pos[0])||(CPos[1]!=Pos[1]))
	{
//		error=GroupMoveAbsolute (pt->mvsock, pt->XZ, 2, &Pos[0]);
		if(!pt->MoveAbs(Pos[0],Pos[1],1))
		{
			pt->TrajectoryRunning=false;
			_endthread();
		}
	}
	pt->TrajectoryRunning=true;
	//Sleep(1000);
	char EvList[30];
	sprintf(EvList,"");
	error=EventExtendedAllGet(pt->mvsock,EvList);
	if( strlen(EvList)!=0)
		{
			int i;
			for(i=0;i<50;i++)
				EventExtendedRemove(pt->mvsock,i);
		}
	//Gathering donnee 
	error=GatheringReset(pt->mvsock);
	if (error!=0)
	{
		pt->TrajectoryRunning=false;
			ErrorStringGet(pt->mvsock,error,pt->last_err);
			_endthread();
	}
	double FirstTrigLen,EndTrigLen,StepTrigLen;
	FirstTrigLen=pt->TT_FirstTrig;
	StepTrigLen=pt->TT_TrigLen;
	EndTrigLen=FirstTrigLen+(pt->TT_TrigNb*StepTrigLen)-0.25*StepTrigLen;
	error=XYLineArcPulseOutputSet(pt->mvsock,pt->XZ,FirstTrigLen,EndTrigLen,StepTrigLen/2);
	if (error!=0)
	{
			pt->TrajectoryRunning=false;
		ErrorStringGet(pt->mvsock,error,pt->last_err);
			_endthread();
	}
	char gathstr[300];
	sprintf (gathstr,"%s.CurrentPosition;%s.CurrentPosition;%s.CurrentPosition;%s.CurrentPosition,%s.CurrentVelocity,%s.CurrentVelocity",
		pt->XMaster,pt->ZMaster,pt->XSlave,pt->ZSlave,pt->XMaster,pt->ZMaster);
	error=GatheringConfigurationSet(pt->mvsock,4,gathstr);
	if (error!=0)
	{
			pt->TrajectoryRunning=false;
		ErrorStringGet(pt->mvsock,error,pt->last_err);
			_endthread();
	}
	// Trigger surpulse trajectoire.
	error=EventExtendedConfigurationTriggerSet(pt->mvsock,2,"Always;XZ.XYLineArc.TrajectoryPulse","0;0","0;0","0;0","0;0");
		if (error!=0)
		{
			pt->TrajectoryRunning=false;
		ErrorStringGet(pt->mvsock,error,pt->last_err);
			_endthread();;
		}

	error=EventExtendedConfigurationActionSet(pt->mvsock,2,"GPIO1.DO.DOToggle;GatheringOneData","1;0","0;0","0;0","0;0");
		if (error!=0)
		{
			pt->TrajectoryRunning=false;
		ErrorStringGet(pt->mvsock,error,pt->last_err);
			_endthread();;
		}
		int ev;
	error= EventExtendedStart(pt->mvsock,&ev);
		if (error!=0)
		{
			pt->TrajectoryRunning=false;
		ErrorStringGet(pt->mvsock,error,pt->last_err);
			_endthread();;
		}
//	error=pt->Circle(pt->CT_XCenter,pt->CT_ZCenter,pt->CT_OEnd,pt->CT_Velocity,pt->CT_accel,0);
	error=pt->ExecTraj(pt->TT_name,pt->TT_Velocity ,pt->TT_accel,1,0);
		if (error!=(int)true)
		{
			pt->TrajectoryRunning=false;
		_endthread();;
		}
	error=	GatheringStopAndSave(pt->mvsock);
	if (error!=0)
		{
			pt->TrajectoryRunning=false;
			ErrorStringGet(pt->mvsock,error,pt->last_err);
			_endthread();;
		}
	error= EventExtendedRemove(pt->mvsock,ev);
		if (error!=0)
		{
			pt->TrajectoryRunning=false;
			ErrorStringGet(pt->mvsock,error,pt->last_err);
			_endthread();;
		}
		pt->GathDataReady=true;
		pt->TrajectoryRunning=false;
		_endthread();
}

int XPS2d::TrajAndTrig( double xbeg,double zbeg,char *fname,double len,double FirstTrig, double TrigLen, int TrigNb, double Velocity, double Acceleration)
{
	if (IsMoving())
	{
		sprintf (last_err,"Traj And Trig : XZ Group Not Ready");
		return false;
	}
	GathDataReady=false;
	TrajectoryRunning=true;
	int error=GPIODigitalSet(mvsock,"GPIO1.DO",1,0);
	if (error!=0)
		{
			TrajectoryRunning=false;
			ErrorStringGet(mvsock,error,last_err);
			return (false);
		}
	
	TT_XBeg=xbeg;
	TT_ZBeg=zbeg;
	sprintf(TT_name,"%s",fname);
	TT_FirstTrig=FirstTrig;
	TT_TrigLen=TrigLen;
	TT_TrigNb=TrigNb;
	TT_Velocity=Velocity;
	TT_accel=Acceleration;
	TT_len=len;
	double 	T=(len)/Velocity;
	TCP_SetTimeout (mvsock,T*1500 );

	if(_beginthread(BackTrajAndTrig,0,(void *)this)==-1)
	{
		TrajectoryRunning=false;
		return (false);
	}	
	return (true);
}

