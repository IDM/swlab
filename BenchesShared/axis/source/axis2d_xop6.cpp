// This file is part of SWLab Stretched Wire Magnetic Measurement Program 
// 	
// Copyright 2015, ESRF - The European Synchroton, Christophe Penel
//
// SWLab is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// The XOP Toolkit is copyright Wavemetrics, see <http://www.wavemetrics.com>
// The Newport XPS libraries are copyright Newport, see <http://www.newport.com>
// The Eigen library is licensed under the MPL2, see <http://eigen.tuxfamily.org>
// 


#include "XOPStandardHeaders.h"			// Include ANSI headers, Mac headers, IgorXOP.h, XOP.h and XOPSupport.h
#include "axis2d_xop6.h"
#include "XPS2d.h"


/* Global Variables (none) */
char *XPSname[10];
XPS2d *XPSobj[10];
DataFolderHandle XPSDFH[10];


static bool testpath(int i)
{	
	if ( i<0 || i>9)
	{
		XOPNotice("XPS::Path must be betwwen 0 and 9" CR_STR);
		return (false);		
	}
	if(XPSobj[i]==NULL)
	{
		XOPNotice("XPS::Path num is a free one I can't drive it" CR_STR);
		return (false);		
	}
	return (true);
}
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct Axis2DOpenParams  {
	Handle strH;
	double result;
};
typedef struct Axis2DOpenParams Axis2DOpenParams;
#include "XOPStructureAlignmentReset.h"

static int
Axis2DOpen(Axis2DOpenParams* p)
{
	char name[50];
	int len2 = GetHandleSize(p->strH);
	memcpy(&name[0], *p->strH, len2);	
	name[len2]=0;
	// verify that this XPS wasn't already create
	for (int k=0;k<10;k++)
	{if(XPSname[k]!=NULL)
		{if (strcmp(XPSname[k],name)==0)
			{// name already exit return Error
				XOPNotice("XPS::already open" CR_STR);
				SetNaN64(&(p->result));
				return (0);
			}	
		}
	}
	// search a free indice
	int i=0;
	do
		i=i+1;
	while(XPSname[i]!=NULL && i!=9);
	if (i==9)
	{
		XOPNotice("XPS::no free indice to create XPS" CR_STR);
		SetNaN64(&(p->result));
		return (0);		
	}
	DataFolderHandle currentDF;
	int err;
	DataFolderHandle rootDFH;
	if(err=GetCurrentDataFolder(&currentDF))
		return err;
	if (err = GetRootDataFolder(0, &rootDFH))
		return err;
	if (err = GetNamedDataFolder(rootDFH, name, &XPSDFH[i])) 
	{
		// We need to createDataFolder.
		if (err = NewDataFolder(rootDFH, name, &XPSDFH[i]))
		return err;
	}
	// i contain the first free index copy name in table and create the Keithley
//	Kname[i]=new char(strlen (name)+1);
	XPSname[i]=(char *)malloc(strlen(name)+1);
	strcpy(XPSname[i],name);
	XPSobj[i]=new XPS2d(name);
	if(XPSobj[i]->Valid!=true)
	{
		
		XOPNotice(strcat(XPSobj[i]->last_err, CR_STR));
		delete XPSname[i];
		XPSname[i]=NULL;
		delete XPSobj[i];
		XPSobj[i]=NULL;
		SetNaN64(&p->result);
		return 0;

	}
	else
	{
	SetCurrentDataFolder(XPSDFH[i]);
	Variable("path",NT_FP64);
	// Variable for Panel
	Variable ("position",NT_FP64 | NT_CMPLX);
	Variable ("taper",NT_FP64 | NT_CMPLX);
	Variable ("velocity",NT_FP64 | NT_CMPLX);
	Variable ("acceleration",NT_FP64 | NT_CMPLX);

	double j=0;
	double path;
	path=(double)i;
	StoreNumVar("path",&path,&j);
	SetCurrentDataFolder(currentDF);
	p->result=i;
	return(0);	
	}		/* XFunc error code */
}


#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct Axis2DCloseParams  {
	double index;
	double result;
};
typedef struct Axis2DCloseParams Axis2DCloseParams;
#include "XOPStructureAlignmentReset.h"

static int
Axis2DClose(Axis2DCloseParams* p)
{
	int i=(int)p->index;
	if ( !testpath(i))
	{
		p->result=false;
		return (0)	;
	}
	else
	{
		int err;
		DataFolderHandle currentDF;
		if(err=GetCurrentDataFolder(&currentDF))
			return err;
		SetCurrentDataFolder(XPSDFH[i]);
		Variable("path",NT_FP64);
		double path;
		double j;
		SetNaN64(&path);
		SetNaN64(&j);
		StoreNumVar("path",&path,&j);

		delete XPSname[i];
		XPSname[i]=NULL;
		delete XPSobj[i];
		XPSobj[i]=NULL;
		p->result=true;
		SetCurrentDataFolder(currentDF);
		
		return(0);	
	}				/* XFunc error code */
}


struct DPComplexNum {
	double real;
	double imag;
};

static long w2str(waveHndl from ,char **too)
 {
	    waveHndl w;
		w=from;
		if (w==NULL)
		{
			XOPNotice("Wave doesn't exist" CR_STR);
			return false	;

		}
		if (WaveType(w)!=TEXT_WAVE_TYPE)
		{
			XOPNotice("Content of file must be a text wave" CR_STR);
			return false	;
		}
		//long Dim[MAX_DIMENSIONS+1];
		CountInt Dim[MAX_DIMENSIONS + 1];

		//long indice[3];
		IndexInt indice[3];
		int NDim;
		int result;
		
		if (result = MDGetWaveDimensions(w, &NDim, Dim))
			return result;
		
		if (NDim!=1)
		{
			XOPNotice("Content of file must be a 1 D text wave" CR_STR);
			return false	;
		}
		int j;
		indice[1]=0;indice[2]=0;
		Handle strret;
		strret=NewHandle(0L);
		char val[250];
		*too=new char[10000];
		sprintf(*too,"");
		for (j=0;j<Dim[0];j++)
		{
			indice[0]=j;
			MDGetTextWavePointValue(w,indice,strret);
			int len2 = GetHandleSize(strret);
			memcpy(&val[0], *strret, len2);	
			val[len2]=0;
			strcat (*too,val);
			strcat (*too,"\n");

		}
		DisposeHandle(strret);
	return true;
 }
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.
struct Axis2DCreateTrajParams  {
	waveHndl strcontent;
	Handle strfile;
	double index;
	double result;

};
typedef struct Axis2DCreateTrajParams Axis2DCreateTrajParams;
#include "XOPStructureAlignmentReset.h"

static int
Axis2DCreateTraj(Axis2DCreateTrajParams* p)
{
	int i=(int)p->index;

	if ( !testpath(i))
	{
		p->result=false;
		return (0)	;
	}
	else
	{
		char fname[50];
		
		int len2 = GetHandleSize(p->strfile);
		memcpy(&fname[0], *p->strfile, len2);	
		fname[len2]=0;
		char *content=NULL ;
		if (!w2str(p->strcontent,&content))
		{
			p->result=false;
			XOPNotice("Error in w2str function" CR_STR);
			return 0	;	
		}


		if (!XPSobj[i]->CreateTraj(fname, content))
		{
			free(content);
			XOPNotice(XPSobj[i]->last_err);
			XOPNotice(CR_STR);
			p->result=false;
			return 0	;	
		}
		
		free (content);
	}


	p->result=true; 
	return 0;
}



struct Posstruct
{
	double X;
	double Z;
};
 #include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.

struct Axis2DReadPosParams  {

	double index;
	Posstruct result;

};
typedef struct Axis2DReadPosParams Axis2DReadPosParams;
#include "XOPStructureAlignmentReset.h"

static int
Axis2DReadPos(Axis2DReadPosParams* p)
{
	int i=(int)p->index;

	if ( !testpath(i))
	{
		SetNaN64(&(p->result.X));
		SetNaN64(&(p->result.Z));
		return (0)	;
	}
	if (!XPSobj[i]->ReadPos(&(p->result.X),&(p->result.Z)))
		{
			XOPNotice(XPSobj[i]->last_err);
			XOPNotice(CR_STR);
			SetNaN64(&(p->result.X));
			SetNaN64(&(p->result.Z));
			return 0	;	
		}

	return 0;
}
 #include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.

struct Axis2DMoveAbsParams  {
	Posstruct pos;
	double who;
	double index;
	double result;

};
typedef struct Axis2DMoveAbsParams Axis2DMoveAbsParams;
#include "XOPStructureAlignmentReset.h"

static int
Axis2DMoveAbs(Axis2DMoveAbsParams* p)
{
	int i=(int)p->index;

	if ( !testpath(i))
	{
		p->result=false;
		return (0)	;
	}
	if(XPSobj[i]->IsMoving()||XPSobj[i]->TrajRunning())
	{
			XOPNotice("Wire Is moving Can't move now");
			XOPNotice(CR_STR);
			p->result=false;
			return 0	;	
		
	}

	switch ((int)p->who)
	{
	case 'w':case 'W':
		{
		if (!XPSobj[i]->MoveAbs((p->pos.X),(p->pos.Z),0))
			{
			XOPNotice(XPSobj[i]->last_err);
			XOPNotice(CR_STR);
			p->result=false;
			return 0	;	
			}
		p->result=true;
		return 0;
		break;
		}
	case 'M': case 'm':
		{
		if (!XPSobj[i]->MasterMoveAbs((p->pos.X),(p->pos.Z),0))
			{
			XOPNotice(XPSobj[i]->last_err);
			XOPNotice(CR_STR);
			p->result=false;
			return 0	;	
			}
		p->result=true;
		return 0;
		break;
		}
	case 'S': case 's':
		{
		if (!XPSobj[i]->SlaveMoveAbs((p->pos.X),(p->pos.Z),0))
			{
				XOPNotice(XPSobj[i]->last_err);
				XOPNotice(CR_STR);
				p->result=false;
				return 0	;	
			}
		p->result=true;
		return 0;
		break;
		}
	default:
		{
			XOPNotice("MoveAbs only with Master Slave or Wire");
			XOPNotice(CR_STR);
			p->result=false;
			return 0	;
		}

	}

}
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.

struct Axis2DMoveRelParams  {
	Posstruct pos;
	double who;
	double index;
	double result;

};
typedef struct Axis2DMoveRelParams Axis2DMoveRelParams;
#include "XOPStructureAlignmentReset.h"

static int
Axis2DMoveRel(Axis2DMoveRelParams* p)
{
	int i=(int)p->index;

	if ( !testpath(i))
	{
		p->result=false;
		return (0)	;
	}
	if(XPSobj[i]->IsMoving()||XPSobj[i]->TrajRunning())
	{
			XOPNotice("Wire Is moving Can't move now");
			XOPNotice(CR_STR);
			p->result=false;
			return 0	;	
		
	}

	switch ((int)p->who)
	{
	case 'w':case 'W':
		{
		if (!XPSobj[i]->MoveRel((p->pos.X),(p->pos.Z),0))
			{
			XOPNotice(XPSobj[i]->last_err);
			XOPNotice(CR_STR);
			p->result=false;
			return 0	;	
			}
		p->result=true;
		return 0;
		break;
		}
	case 'M': case 'm':
		{
		if (!XPSobj[i]->MasterMoveRel((p->pos.X),(p->pos.Z),0))
			{
			XOPNotice(XPSobj[i]->last_err);
			XOPNotice(CR_STR);
			p->result=false;
			return 0	;	
			}
		p->result=true;
		return 0;
		break;
		}
	case 'S': case 's':
		{
		if (!XPSobj[i]->SlaveMoveRel((p->pos.X),(p->pos.Z),0))
			{
			XOPNotice(XPSobj[i]->last_err);
			XOPNotice(CR_STR);
			p->result=false;
			return 0	;	
			}
		p->result=true;
		return 0;
		break;
		}
	default:
		{
			XOPNotice("MoveAbs only with Master Slave or Wire");
			XOPNotice(CR_STR);
			p->result=false;
			return 0	;
		}
	}
}
 #include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.

struct Axis2DSetAccelParams  {
	Posstruct accel;
	double index;
	double result;

};
typedef struct Axis2DSetAccelParams Axis2DSetAccelParams;
#include "XOPStructureAlignmentReset.h"

static int
Axis2DSetAccel(Axis2DSetAccelParams* p)
{
	int i=(int)p->index;
	
	if ( !testpath(i))
	{
		p->result=false;
		return (0)	;
	}
	if (!XPSobj[i]->SetAcceleration((p->accel.X),(p->accel.Z)))
		{
			XOPNotice(XPSobj[i]->last_err);
			XOPNotice(CR_STR);
			p->result=false;
			return 0	;	
		}
	p->result=true;
	return 0;
}

 #include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.

struct Axis2DReadAccelParams  {

	double index;
	Posstruct result;

};
typedef struct Axis2DReadAccelParams Axis2DReadAccelParams;
#include "XOPStructureAlignmentReset.h"

static int
Axis2DReadAccel(Axis2DReadAccelParams* p)
{
	int i=(int)p->index;

	if ( !testpath(i))
	{
		SetNaN64(&(p->result.X));
		SetNaN64(&(p->result.Z));
		return (0)	;
	}
	if (!XPSobj[i]->ReadAcceleration(&(p->result.X),&(p->result.Z)))
		{
			XOPNotice(XPSobj[i]->last_err);
			XOPNotice(CR_STR);
			return 0	;	
		}
	return 0;
}

 #include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.

struct Axis2DSetVelocityParams  {
	Posstruct Velocity;
	double index;
	double result;

};
typedef struct Axis2DSetVelocityParams Axis2DSetVelocityParams;
#include "XOPStructureAlignmentReset.h"

static int
Axis2DSetVelocity(Axis2DSetVelocityParams* p)
{
	int i=(int)p->index;
	
	if ( !testpath(i))
	{
		p->result=false;
		return (0)	;
	}
	if (!XPSobj[i]->SetVelocity((p->Velocity.X),(p->Velocity.Z)))
		{
			XOPNotice(XPSobj[i]->last_err);
			XOPNotice(CR_STR);
			p->result=false;
			return 0	;	
		}
	p->result=true;
	return 0;
}

 #include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.

struct Axis2DReadVelocityParams  {

	double index;
	Posstruct result;

};
typedef struct Axis2DReadVelocityParams Axis2DReadVelocityParams;
#include "XOPStructureAlignmentReset.h"

static int
Axis2DReadVelocity(Axis2DReadVelocityParams* p)
{
	int i=(int)p->index;

	if ( !testpath(i))
	{
		SetNaN64(&(p->result.X));
		SetNaN64(&(p->result.Z));
		return (0)	;
	}
	if (!XPSobj[i]->ReadVelocity(&(p->result.X),&(p->result.Z)))
		{
			XOPNotice(XPSobj[i]->last_err);
			XOPNotice(CR_STR);
			return 0	;	
		}
	return 0;
}

 #include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.

struct Axis2DDataReadyParams  {

	double index;
	double result;

};
typedef struct Axis2DDataReadyParams Axis2DDataReadyParams;
#include "XOPStructureAlignmentReset.h"

static int
Axis2DDataReady(Axis2DDataReadyParams* p)
{
	int i=(int)p->index;
	p->result=true;
	if ( !testpath(i))
	{
		SetNaN64(&(p->result));
		return (0)	;
	}
	
	p->result= XPSobj[i]->GatheringDataReady();
	return (0);

}

#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.

struct Axis2DIsMovingParams  {

	double index;
	double result;

};
typedef struct Axis2DIsMovingParams Axis2DIsMovingParams;
#include "XOPStructureAlignmentReset.h"

static int
Axis2DIsMoving(Axis2DIsMovingParams* p)
{
	int i=(int)p->index;
	p->result=true;
	if ( !testpath(i))
	{
		SetNaN64(&(p->result));
		return (0)	;
	}
	
	if ((XPSobj[i]->IsMoving())||(XPSobj[i]->TrajRunning()) )
	{
		p->result=true;
	}
	else
	{
		p->result=false;
	}
	return 0;

}
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.

struct Axis2DExecTrajParams  {
	double bg;
	double N;
	double accel;
	double velocity;
	Handle strfile;
	double index;
	double result;

};
typedef struct Axis2DExecTrajParams Axis2DExecTrajParams;
#include "XOPStructureAlignmentReset.h"

static int
Axis2DExecTraj(Axis2DExecTrajParams* p)
{
	int i=(int)p->index;

	if ( !testpath(i))
	{
		SetNaN64(&(p->result));
		return (0)	;
	}
	char fname[50];
	if(XPSobj[i]->IsMoving()||XPSobj[i]->TrajRunning())
	{
			XOPNotice("Wire Is moving Can't move now");
			XOPNotice(CR_STR);
			p->result=false;
			return 0	;	
		
	}
	
	int len2 = GetHandleSize(p->strfile);
	memcpy(&fname[0], *p->strfile, len2);	
	fname[len2]=0;

	if (XPSobj[i]->ExecTraj(fname,p->velocity,p->accel,(int)p->N,(int)p->bg))
		p->result=true;
	else
	{
			XOPNotice(XPSobj[i]->last_err);
			XOPNotice(CR_STR);
			p->result=false;
	}
	return 0;

}
 #include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.

struct Axis2DReferenceParams  {

	double index;
	double result;

};
typedef struct Axis2DReferenceParams Axis2DReferenceParams;
#include "XOPStructureAlignmentReset.h"

static int
Axis2DReference(Axis2DReferenceParams* p)
{
	int i=(int)p->index;

	if ( !testpath(i))
	{
		SetNaN64(&(p->result));
		return (0)	;
	}
	
	if (XPSobj[i]->Reference())
		p->result=true;
	else
		p->result=false;
	return 0;

}

 #include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.

struct Axis2DReadTaperParams  {

	double index;
	Posstruct result;

};
typedef struct Axis2DReadTaperParams Axis2DReadTaperParams;
#include "XOPStructureAlignmentReset.h"

static int
Axis2DReadTaper(Axis2DReadTaperParams* p)
{
	int i=(int)p->index;

	if ( !testpath(i))
	{
		SetNaN64(&(p->result.X));
		SetNaN64(&(p->result.Z));
		return (0)	;
	}
	if (!XPSobj[i]->ReadTaper(&(p->result.X),&(p->result.Z)))
		{
			XOPNotice(XPSobj[i]->last_err);
			XOPNotice(CR_STR);
			SetNaN64(&(p->result.X));
			SetNaN64(&(p->result.Z));
			return 0	;	
		}

	return 0;
}

#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.

struct Axis2DSetTaperParams  {
	Posstruct Taper;
	double index;
	double result;

};
typedef struct Axis2DSetTaperParams Axis2DSetTaperParams;
#include "XOPStructureAlignmentReset.h"

static int
Axis2DSetTaper(Axis2DSetTaperParams* p)
{
	int i=(int)p->index;
	
	if ( !testpath(i))
	{
		p->result=false;
		return (0)	;
	}
	if (!XPSobj[i]->SetTaper((p->Taper.X),(p->Taper.Z)))
		{
			XOPNotice(XPSobj[i]->last_err);
			XOPNotice(CR_STR);
			p->result=false;
			return 0	;	
		}
	p->result=true;
	return 0;
}


 #include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.

struct Axis2DLineToParams  {
	double bg;
	double accel;
	double velocity;
	Posstruct pos;
	double index;
	double result;

};
typedef struct Axis2DLineToParams Axis2DLineToParams;
#include "XOPStructureAlignmentReset.h"

static int
Axis2DLineTo(Axis2DLineToParams* p)
{
	int i=(int)p->index;

	if ( !testpath(i))
	{
		p->result=false;
		return (0)	;
	}
	if(XPSobj[i]->IsMoving()||XPSobj[i]->TrajRunning())
	{
			XOPNotice("Wire Is moving Can't move now");
			XOPNotice(CR_STR);
			p->result=false;
			return 0	;	
		
	}

	if (!XPSobj[i]->LineTo(p->pos.X,p->pos.Z,p->velocity,p->accel,(int)p->bg))
		{
			XOPNotice(XPSobj[i]->last_err);
			XOPNotice(CR_STR);
			p->result=false;
			return 0	;	
		}
	p->result=true;
	return 0;
}

#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.

struct Axis2DAbortParams  {

	double index;
	double result;

};
typedef struct Axis2DAbortParams Axis2DAbortParams;
#include "XOPStructureAlignmentReset.h"

static int
Axis2DAbort(Axis2DAbortParams* p)
{
	int i=(int)p->index;

	if ( !testpath(i))
	{
		SetNaN64(&(p->result));
		return (0)	;
	}
	
	if (XPSobj[i]->Abort())
		p->result=true;
	else
	{
		XOPNotice(XPSobj[i]->last_err);
		XOPNotice(CR_STR);
		p->result=false;
	}
	return 0;

}


#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.

struct Axis2DLineAndTrigParams  {
	double	Acceleration;
	double Velocity;
	Posstruct	EndPos;
	double	TrigNb;
	double TrigStep;
	double FirstTrigLen;
	Posstruct StartPos;
	double index;
	double result;

};
typedef struct Axis2DLineAndTrigParams Axis2DLineAndTrigParams;
#include "XOPStructureAlignmentReset.h"

static int
Axis2DLineAndTrig(Axis2DLineAndTrigParams* p)
{
	int i=(int)p->index;

	if ( !testpath(i))
	{
		SetNaN64(&(p->result));
		return (0)	;
	}
		if(XPSobj[i]->IsMoving()||XPSobj[i]->TrajRunning())
	{
			XOPNotice("Wire Is moving Can't move now");
			XOPNotice(CR_STR);
			p->result=0;
			return 0	;	
		
	}

	if (XPSobj[i]->LineAndTrig(p->StartPos.X,p->StartPos.Z,
								p->FirstTrigLen,p->TrigStep,(int)p->TrigNb,

								p->EndPos.X,p->EndPos.Z,p->Velocity,p->Acceleration))

		p->result=1;
	else
	{
		XOPNotice(XPSobj[i]->last_err);
		XOPNotice(CR_STR);
		p->result=0;
	}
	return 0;

}
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.

struct Axis2DGetGatheringDataParams  {
	double index;
	double result;

};
typedef struct Axis2DGetGatheringDataParams Axis2DGetGatheringDataParams;
#include "XOPStructureAlignmentReset.h"

static int
Axis2DGetGatheringData(Axis2DGetGatheringDataParams* p)
{
	int err;
	int i=(int)p->index;
	if ( !testpath(i))
	{
		SetNaN64(&(p->result));
		return (0)	;
	}
	double *XPos;
	double *ZPos;
	double *VZ,*VX;
	int NB;
	if (!(XPSobj[i]->GetGatheringData(&XPos,&ZPos,&VX,&VZ,&NB)))
	{
		XOPNotice(XPSobj[i]->last_err);
		XOPNotice(CR_STR);
		p->result=false;
	}
	double *XTMP=XPos;
	double *ZTMP=ZPos;
	double *VXTMP=VX;
	double *VZTMP=VZ;

	double XWval[2],ZWval[2];
	Handle currentDF;
	if(err=GetCurrentDataFolder(&currentDF))
		return err;
	waveHndl XW,ZW,VX_H,VZ_H;
	//long Dimw[MAX_DIMENSIONS+1];
	CountInt Dimw[MAX_DIMENSIONS + 1];;
	//long ind[MAX_DIMENSIONS];
	IndexInt ind[MAX_DIMENSIONS];
	Dimw[0]=NB/2;
	Dimw[1]=0;
	ind[1]=0;

	if(err=(MDMakeWave(&ZW,"ZPos",XPSDFH[i],Dimw,NT_FP64,1)!=0))
		return err;
	if(err=(MDMakeWave(&XW,"XPos",XPSDFH[i],Dimw,NT_FP64,1)!=0))
		return err;
	if(err=(MDMakeWave(&VX_H,"VX",XPSDFH[i],Dimw,NT_FP64,1)!=0))
		return err;
	if(err=(MDMakeWave(&VZ_H,"VZ",XPSDFH[i],Dimw,NT_FP64,1)!=0))
		return err;


	//SetNaN64(&XWval[0]);
	//for ((int)i=0;i<NB/2;i++)
	//{
	//	ind[0]=i;
	//	MDSetNumericWavePointValue(XW,ind,XWval);
	//	MDSetNumericWavePointValue(ZW,ind,XWval);
	//}
	for ((int)i=0;i<NB/2;i++)
	{
		XWval[0]=*XTMP;
		ZWval[0]=*ZTMP;
		ind[0]=i;
		if(err=(MDSetNumericWavePointValue(XW,ind,XWval)))
		{
			free(XPos);free(ZPos);free(VX);free(VZ);
			return err;
		}
		if(err=(MDSetNumericWavePointValue(ZW,ind,ZWval)))
		{
			free(XPos);free(ZPos);free(VX);free(VZ);
			return err;
		}
		XTMP+=2;
		ZTMP+=2;
		XWval[0]=*VXTMP;
		ZWval[0]=*VZTMP;
		if(err=(MDSetNumericWavePointValue(VX_H,ind,XWval)))
		{
			free(XPos);free(ZPos);free(VX);free(VZ);
			return err;
		}
		if(err=(MDSetNumericWavePointValue(VZ_H,ind,ZWval)))
		{
			free(XPos);free(ZPos);free(VX);free(VZ);
			return err;
		}
		VXTMP+=2;
		VZTMP+=2;
	}
	free(XPos);
	free(ZPos);
	free(VX);free(VZ);
	p->result=true;
	return 0;

}
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.

struct Axis2DCircleParams  {
	double bg;
	double accel;
	double velocity;
	double Teta2;
	Posstruct center;
	double index;
	double result;

};
typedef struct Axis2DCircleParams Axis2DCircleParams;
#include "XOPStructureAlignmentReset.h"

static int
Axis2DCircle(Axis2DCircleParams* p)
{
	int i=(int)p->index;

	if ( !testpath(i))
	{
		p->result=false;
		return (0)	;
	}
	if(XPSobj[i]->IsMoving()||XPSobj[i]->TrajRunning())
	{
			XOPNotice("Wire Is moving Can't move now");
			XOPNotice(CR_STR);
			p->result=false;
			return 0	;	
		
	}

	//Circle(double XCenter, double ZCenter, double Teta2,double velocity,double accel,int bg)
	if (!XPSobj[i]->Circle(p->center.X,p->center.Z,p->Teta2,p->velocity,p->accel,(int)p->bg))
		{
			XOPNotice(XPSobj[i]->last_err);
			XOPNotice(CR_STR);
			p->result=false;
			return 0	;	
		}
	p->result=true;
	return 0;
}

#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.

struct Axis2DCircleAndTrigParams  {
	double accel;
	double velocity;
	double OEnd;
	double OTrigNb;
	double OTrigLen;
	double OFirstTrig;
	double OBeg;
	double R;
	Posstruct center;
	double index;
	double result;

};
typedef struct Axis2DCircleAndTrigParams Axis2DCircleAndTrigParams;
#include "XOPStructureAlignmentReset.h"

static int
Axis2DCircleAndTrig(Axis2DCircleAndTrigParams* p)
{
	int i=(int)p->index;
	if ( !testpath(i))
	{
		p->result=false;
		return (0)	;
	}
	if(XPSobj[i]->IsMoving()||XPSobj[i]->TrajRunning())
	{
			XOPNotice("Wire Is moving Can't move now");
			XOPNotice(CR_STR);
			p->result=false;
			return 0	;	
		
	}
//CircleAndTrig(double XCenter, double ZCenter, double R, double OBeg, double OFirstTrig, double OTrigLen, int OTrigNb, double OEnd, double Velocity, double Acceleration);
	if (!XPSobj[i]->CircleAndTrig(p->center.X,p->center.Z,p->R,p->OBeg,p->OFirstTrig,p->OTrigLen,(int)p->OTrigNb,p->OEnd, p->velocity, p->accel))
		{
			XOPNotice(XPSobj[i]->last_err);
			XOPNotice(CR_STR);
			p->result=false;
			return 0	;	
		}
	p->result=true;
	return 0;
}

static int
Axis2DMasterCircleAndTrig(Axis2DCircleAndTrigParams* p)
{
	int i = (int)p->index;
	if (!testpath(i))
	{
		p->result = false;
		return (0);
	}
	if (XPSobj[i]->IsMoving() || XPSobj[i]->TrajRunning())
	{
		XOPNotice("Wire Is moving Can't move now");
		XOPNotice(CR_STR);
		p->result = false;
		return 0;

	}
	//CircleAndTrig(double XCenter, double ZCenter, double R, double OBeg, double OFirstTrig, double OTrigLen, int OTrigNb, double OEnd, double Velocity, double Acceleration);
	if (!XPSobj[i]->MasterCircleAndTrig(p->center.X, p->center.Z, p->R, p->OBeg, p->OFirstTrig, p->OTrigLen, (int)p->OTrigNb, p->OEnd, p->velocity, p->accel))
	{
		XOPNotice(XPSobj[i]->last_err);
		XOPNotice(CR_STR);
		p->result = false;
		return 0;
	}
	p->result = true;
	return 0;
}
#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.

struct Axis2DReadMovingTimeParams  {
	double A;
	double V;
	double depl;
	double index;
	double result;

};
typedef struct Axis2DReadMovingTimeParams Axis2DReadMovingTimeParams;
#include "XOPStructureAlignmentReset.h"

static int
Axis2DReadMovingTime(Axis2DReadMovingTimeParams* p)
{	
	int i=(int)p->index;
	if ( !testpath(i))
	{
		p->result=false;
		return (0)	;
	}

	p->result=XPSobj[i]->TimeToMove(p->depl,p->V,p->A);
	return 0	;	
}

#include "XOPStructureAlignmentTwoByte.h"	// All structures passed to Igor are two-byte aligned.

struct Axis2DTrajAndTrigParams  {
	double Accel;
	double Velocity;
	double trig_nb;
	double TLen;
	double First_TLen;
	double len;
	waveHndl strcontent;
	Posstruct beg;
	double index;
	double result;

};
typedef struct Axis2DTrajAndTrigParams Axis2DTrajAndTrigParams;
#include "XOPStructureAlignmentReset.h"

static int
Axis2DTrajAndTrig(Axis2DTrajAndTrigParams* p)
{
	int i=(int)p->index;
	if ( !testpath(i))
	{
		p->result=false;
		return (0)	;
	}
//	char fname[50];
		
//	int len2 = GetHandleSize(p->strfile);
//	memcpy(&fname[0], *p->strfile, len2);	
//	fname[len2]=0;
	char *content=NULL ;
	if (!w2str(p->strcontent,&content))
	{
		p->result=false;
		XOPNotice("Error in w2str function" CR_STR);
		return 0	;	
	}
	if(XPSobj[i]->IsMoving()||XPSobj[i]->TrajRunning())
	{
			XOPNotice("Wire Is moving Can't move now");
			XOPNotice(CR_STR);
			p->result=false;
			return 0	;	
		
	}
	if (!XPSobj[i]->CreateTraj("XOPtraj.trj", content))
		{
			free(content);
			XOPNotice(XPSobj[i]->last_err);
			XOPNotice(CR_STR);
			p->result=false;
			return 0	;	
		}
	if (!XPSobj[i]->TrajAndTrig(p->beg.X,p->beg.Z,"XOPtraj.trj",p->len,p->First_TLen,p->TLen,(int)p->trig_nb,p->Velocity,p->Accel))
	{
			free(content);
			XOPNotice(XPSobj[i]->last_err);
			XOPNotice(CR_STR);
			p->result=false;
			return 0	;
	}
	free (content);

	p->result=true;
	return 0;
}
static long
RegisterFunction()
{
	int funcIndex;

	/*	NOTE:
		Some XOPs should return a result of NIL in response to the FUNCADDRS message.
		See XOP manual "Restrictions on Direct XFUNCs" section.
	*/

	funcIndex = GetXOPItem(0);		/* which function invoked ? */
	switch (funcIndex) {
		case 0:						/* Axis2DAdd(p1, p2) */
			return((long)Axis2DOpen);
			break;
		case 1:						/* Axis2DDiv(p1, p2) */
			return((long)Axis2DClose);
			break;
		case 2:						/* Axis2DComplexConjugate(p1) */
			return((long)Axis2DCreateTraj);
			break;
		case 3:						/* Axis2DComplexConjugate(p1) */
			return((long)Axis2DReadPos);
			break;
		case 4:						/* Axis2DComplexConjugate(p1) */
			return((long)Axis2DMoveAbs);
			break;
		case 5:						/* Axis2DComplexConjugate(p1) */
			return((long)Axis2DSetAccel);
			break;
		case 6:						/* Axis2DComplexConjugate(p1) */
			return((long)Axis2DReadAccel);
			break;
		case 7:						/* Axis2DComplexConjugate(p1) */
			return((long)Axis2DSetVelocity);
			break;
		case 8:						/* Axis2DComplexConjugate(p1) */
			return((long)Axis2DReadVelocity);
			break;
		case 9:						/* Axis2DComplexConjugate(p1) */
			return((long)Axis2DIsMoving);
			break;
		case 10:						/* Axis2DComplexConjugate(p1) */
			return((long)Axis2DExecTraj);
			break;
		case 11:						/* Axis2DComplexConjugate(p1) */
			return((long)Axis2DReference   );
			break;
		case 12:						/* Axis2DComplexConjugate(p1) */
			return((long)Axis2DReadTaper   );
			break;
		case 13:						/* Axis2DComplexConjugate(p1) */
			return((long)Axis2DSetTaper);
			break;
		case 14:						/* Axis2DComplexConjugate(p1) */
			return((long)Axis2DLineTo);
			break;
		case 15:						/* Axis2DComplexConjugate(p1) */
			return((long)Axis2DAbort);
			break;
		case 16:						/* Axis2DComplexConjugate(p1) */
			return((long)Axis2DLineAndTrig);
			break;
		case 17:						/* Axis2DComplexConjugate(p1) */
			return((long)Axis2DGetGatheringData);
			break;
		case 18:						/* Axis2DComplexConjugate(p1) */
			return((long)Axis2DCircle);
			break;
		case 19:						/* Axis2DComplexConjugate(p1) */
			return((long)Axis2DCircleAndTrig);
			break;
		case 20:						/* Axis2DComplexConjugate(p1) */
			return((long)Axis2DMoveRel);
			break;
		case 21:						/* Axis2DComplexConjugate(p1) */
			return((long)Axis2DReadMovingTime);
			break;
		case 22:						/* Axis2DComplexConjugate(p1) */
			return((long)Axis2DTrajAndTrig);
			break;
		case 23:						/* Axis2DComplexConjugate(p1) */
			return((long)Axis2DDataReady);
			break;
		case 24:						/* Axis2DComplexConjugate(p1) */
			return((long)Axis2DMasterCircleAndTrig);
			break;



	}
	return(NIL);
}

/*	DoFunction()

	This will actually never be called because all of the functions use the direct method.
	It would be called if a function used the message method. See the XOP manual for
	a discussion of direct versus message XFUNCs.
*/
static int
DoFunction()
{
	int funcIndex;
	void *p;				/* pointer to structure containing function parameters and result */
	int err;

	funcIndex = GetXOPItem(0);		/* which function invoked ? */
	p = (void *)GetXOPItem(1);		/* get pointer to params/result */
	switch (funcIndex) {
		case 0:						/* Axis2DAdd(p1, p2) */
			err = Axis2DOpen((Axis2DOpenParams*) p);
			break;
		case 1:						/* Axis2DDiv(p1, p2) */
			err = Axis2DClose((Axis2DCloseParams*)p);
			break;
		case 2:						/* Axis2DComplexConjugate(p1) */
			err = Axis2DCreateTraj((Axis2DCreateTrajParams*)p);
			break;
		case 3:						/* Axis2DComplexConjugate(p1) */
			err = Axis2DReadPos((Axis2DReadPosParams*)p);
			break;
		case 4:						/* Axis2DComplexConjugate(p1) */
			err = Axis2DMoveAbs((Axis2DMoveAbsParams*)p);
			break;
		case 5:						/* Axis2DComplexConjugate(p1) */
			err = Axis2DSetAccel((Axis2DSetAccelParams*)p);
			break;
		case 6:						/* Axis2DComplexConjugate(p1) */
			err = Axis2DReadAccel((Axis2DReadAccelParams*)p);
			break;
		case 7:						/* Axis2DComplexConjugate(p1) */
			err = Axis2DSetVelocity((Axis2DSetVelocityParams*)p);
			break;
		case 8:						/* Axis2DComplexConjugate(p1) */
			err = Axis2DReadVelocity((Axis2DReadVelocityParams*)p);
			break;
		case 9:						/* Axis2DComplexConjugate(p1) */
			err = Axis2DIsMoving((Axis2DIsMovingParams*)p);
			break;
		case 10:						/* Axis2DComplexConjugate(p1) */
			err = Axis2DExecTraj((Axis2DExecTrajParams*)p);
			break;
		case 11:						/* Axis2DComplexConjugate(p1) */
			err = Axis2DReference((Axis2DReferenceParams*)p);
			break;
		case 12:						/* Axis2DComplexConjugate(p1) */
			err = Axis2DReadTaper((Axis2DReadTaperParams*)p);
			break;
		case 13:						/* Axis2DComplexConjugate(p1) */
			err = Axis2DSetTaper((Axis2DSetTaperParams*)p);
			break;
		case 14:						/* Axis2DComplexConjugate(p1) */
			err = Axis2DLineTo((Axis2DLineToParams*)p);
			break;
		case 15:						/* Axis2DComplexConjugate(p1) */
			err = Axis2DAbort((Axis2DAbortParams*)p);
			break;
		case 16:						/* Axis2DComplexConjugate(p1) */
			err = Axis2DLineAndTrig((Axis2DLineAndTrigParams*)p);
			break;
		case 17:						/* Axis2DComplexConjugate(p1) */
			err = Axis2DGetGatheringData((Axis2DGetGatheringDataParams*)p);
			break;
		case 18:						/* Axis2DComplexConjugate(p1) */
			err = Axis2DCircle((Axis2DCircleParams*)p);
			break;
		case 19:						/* Axis2DComplexConjugate(p1) */
			err = Axis2DCircleAndTrig((Axis2DCircleAndTrigParams*)p);
			break;
		case 20:						/* Axis2DComplexConjugate(p1) */
			err = Axis2DMoveRel((Axis2DMoveRelParams*)p);
			break;
		case 21:						/* Axis2DComplexConjugate(p1) */
			err = Axis2DReadMovingTime((Axis2DReadMovingTimeParams*)p);
			break;
		case 22:						/* Axis2DComplexConjugate(p1) */
			err = Axis2DTrajAndTrig((Axis2DTrajAndTrigParams*)p);
			break;
		case 23:
			err = Axis2DDataReady((Axis2DDataReadyParams *)p);
			break;
		case 24:						/* Axis2DComplexConjugate(p1) */
			err = Axis2DMasterCircleAndTrig((Axis2DCircleAndTrigParams*)p);
			break;


	}
	return(err); 
}

/*	XOPEntry()

	This is the entry point from the host application to the XOP for all messages after the
	INIT message.
*/

static void
XOPEntry(void)
{	
	long result = 0;

	switch (GetXOPMessage()) {
		case FUNCTION:								/* our external function being invoked ? */
			result = DoFunction();
			break;

		case FUNCADDRS:
			result = RegisterFunction();
			break;
	}
	SetXOPResult(result);
}

/*	main(ioRecHandle)

	This is the initial entry point at which the host application calls XOP.
	The message sent by the host must be INIT.
	main() does any necessary initialization and then sets the XOPEntry field of the
	ioRecHandle to the address to be called for future messages.
*/

HOST_IMPORT void
main(IORecHandle ioRecHandle)
{	
	XOPInit(ioRecHandle);							/* do standard XOP initialization */
	SetXOPEntry(XOPEntry);							/* set entry point for future calls */
	
	if (igorVersion < 200)
		SetXOPResult(OLD_IGOR);
	else
		SetXOPResult(0L);
}
