/*	AXIS3D.c -- illustrates Igor external functions.

*/

#include "XOPStandardHeaders.h"			// Include ANSI headers, Mac headers, IgorXOP.h, XOP.h and XOPSupport.h
#include "axis3d_xop6.h"
#include "XPS3d.h"
#include "math.h"

/* Global Variables (none) */

char *XPS3Dname[10];
XPS3D *XPS3Dobj[10];
DataFolderHandle XPS3DDFH[10];

static bool test3Dpath(int i)
{	
	if ( i<0 || i>9)
	{
		XOPNotice("XPS3D::Path must be betwwen 0 and 9" CR_STR);
		return (false);		
	}
	if(XPS3Dobj[i]==NULL)
	{
		XOPNotice("XPS3D::Path num is a free one I can't drive it" CR_STR);
		return (false);		
	}
	return (true);
}

static long w2str(waveHndl from ,char **too)
 {
	    waveHndl w;
		w=from;
		if (w==NULL)
		{
			XOPNotice("Wave doesn't exist" CR_STR);
			return false	;

		}
		if (WaveType(w)!=TEXT_WAVE_TYPE)
		{
			XOPNotice("Content of file must be a text wave" CR_STR);
			return false	;
		}
		long Dim[MAX_DIMENSIONS+1];
		long indice[3];
		int NDim;
		int result;
		
		if (result = MDGetWaveDimensions(w, &NDim, Dim))
			return result;
		
		if (NDim!=1)
		{
			XOPNotice("Content of file must be a 1 D text wave" CR_STR);
			return false	;
		}
		int j;
		indice[1]=0;indice[2]=0;
		Handle strret;
		strret=NewHandle(0L);
		char val[250];
		*too=new char[100000];
		sprintf(*too,"");
		for (j=0;j<Dim[0];j++)
		{
			indice[0]=j;
			MDGetTextWavePointValue(w,indice,strret);
			int len2 = GetHandleSize(strret);
			memcpy(&val[0], *strret, len2);	
			val[len2]=0;
			strcat (*too,val);
			strcat (*too,"\n");

		}
		DisposeHandle(strret);
	return true;
 }
#pragma pack(2)// All structures passed to Igor are two-byte aligned.
struct AXIS3DOpenParams  {
	Handle strH;
	double result;
};
typedef struct AXIS3DOpenParams AXIS3DAddParams;
# pragma pack()

static int
AXIS3DOpen(AXIS3DOpenParams* p)
{
	char name[50];
	int len2 = GetHandleSize(p->strH);
	memcpy(&name[0], *p->strH, len2);	
	name[len2]=0;
	// verify that this XPS wasn't already create
	for (int k=0;k<10;k++)
	{if(XPS3Dname[k]!=NULL)
		{if (strcmp(XPS3Dname[k],name)==0)
			{// name already exit return Error
				XOPNotice("XPS3D::already open" CR_STR);
				SetNaN64(&(p->result));
				return (0);
			}	
		}
	}
	// search a free indice
	int i=0;
	do
		i=i+1;
	while(XPS3Dname[i]!=NULL && i!=9);
	if (i==9)
	{
		XOPNotice("XPS3D::no free indice to create XPS" CR_STR);
		SetNaN64(&(p->result));
		return (0);		
	}
	DataFolderHandle currentDF;
	int err;
	DataFolderHandle rootDFH;
	if(err=GetCurrentDataFolder(&currentDF))
		return err;
	if (err = GetRootDataFolder(0, &rootDFH))
		return err;
	if (err = GetNamedDataFolder(rootDFH, name, &XPS3DDFH[i])) 
	{
		// We need to createDataFolder.
		if (err = NewDataFolder(rootDFH, name, &XPS3DDFH[i]))
		return err;
	}
	// i contain the first free index copy name in table and create the Keithley
//	Kname[i]=new char(strlen (name)+1);
	XPS3Dname[i]=(char *)malloc(strlen(name)+1);
	strcpy(XPS3Dname[i],name);
	XPS3Dobj[i]=new XPS3D(name);
	if(XPS3Dobj[i]->Valid!=true)
	{
		
		XOPNotice(strcat(XPS3Dobj[i]->last_err,CR_STR));
		delete XPS3Dname[i];
		XPS3Dname[i]=NULL;
		delete XPS3Dobj[i];
		XPS3Dobj[i]=NULL;
		SetNaN64(&p->result);
		return 0;

	}
	else
	{
	SetCurrentDataFolder(XPS3DDFH[i]);
	Variable("path",NT_FP64);
	// Variable for Panel
//	Variable ("PX",NT_FP64 );
//	Variable ("PZ",NT_FP64 );
//	Variable ("PS",NT_FP64 );

//	Variable ("VX",NT_FP64 );
//	Variable ("VZ",NT_FP64 );
//	Variable ("VS",NT_FP64 );

//	Variable ("AX",NT_FP64 );
//	Variable ("AZ",NT_FP64 );
//	Variable ("AS",NT_FP64 );

	double j=0;
	double path;
	path=(double)i;
	StoreNumVar("path",&path,&j);
	SetCurrentDataFolder(currentDF);
	p->result=i;
	return(0);	
	}		/* XFunc error code */
}


#pragma pack(2)// All structures passed to Igor are two-byte aligned.
struct AXIS3DCloseParams  {
	double index;
	double result;
};
typedef struct AXIS3DCloseParams AXIS3DCloseParams;
#pragma pack()

static int
AXIS3DClose(AXIS3DCloseParams* p)
{
	int i=(int)p->index;
	if ( !test3Dpath(i))
	{
		p->result=false;
		return (0)	;
	}
	else
	{
		int err;
		DataFolderHandle currentDF;
		if(err=GetCurrentDataFolder(&currentDF))
			return err;
		SetCurrentDataFolder(XPS3DDFH[i]);
		Variable("path",NT_FP64);
		double path;
		double j;
		SetNaN64(&path);
		SetNaN64(&j);
		StoreNumVar("path",&path,&j);

		delete XPS3Dname[i];
		XPS3Dname[i]=NULL;
		delete XPS3Dobj[i];
		XPS3Dobj[i]=NULL;
		p->result=true;
		SetCurrentDataFolder(currentDF);
		
		return(0);	
	}				/* XFunc error code */
}


struct Posstruct
{
	double X;
	double Z;
	double S;
};
 #pragma pack(2)// All structures passed to Igor are two-byte aligned.


struct Axis3DReadPosParams  {

	double index;
	waveHndl result;

};
typedef struct Axis3DReadPosParams Axis3DReadPosParams;
#pragma pack()

static int
AXIS3DReadPos(Axis3DReadPosParams* p)
{
	int i=(int)p->index;
	int err;
	double pos[3];
	DataFolderHandle currentDF;
	if(err=GetCurrentDataFolder(&currentDF))
		return err;
	for ( int j=0;j<3;j++)
		SetNaN64(&(pos[j]));
	if ( !test3Dpath(i))
	{
		p->result=NULL;
		return (0)	;
	}	
	if (!XPS3Dobj[i]->ReadPos(&(pos[0]),&(pos[1]),&(pos[2])))
		{
			XOPNotice(XPS3Dobj[i]->last_err);
			XOPNotice(CR_STR);
			p->result=NULL;
		}
	long dimensionSizes[MAX_DIMENSIONS+1];
	MemClear(dimensionSizes, sizeof(dimensionSizes));
	dimensionSizes[ROWS] = 3;
	waveHndl waveH;
	double *wp;

	if (err = MDMakeWave(&waveH, "Pos", XPS3DDFH[i], dimensionSizes, NT_FP64, 1))
			return err;
	wp = (double*)WaveData(waveH);
	for (int i=0;i<3;i++)
		*wp++ = pos[i];
	p->result=waveH;
	return 0;
}
#pragma pack(2)// All structures passed to Igor are two-byte aligned.
struct Axis3DSetPosParams  {
	waveHndl pos;
	double index;
	double result;

};
typedef struct Axis3DSetPosParams Axis3DsetPosParams;
#pragma pack()

static int
AXIS3DSetPos(Axis3DSetPosParams* p)
{
	int i=(int)p->index;
	int err;
	double pos[3];
	DataFolderHandle currentDF;
	if(err=GetCurrentDataFolder(&currentDF))
		return err;
	if (p->pos == NULL) {
		SetNaN64(&p->result); // Return NaN if wave is not valid
		return NULL_WAVE_OP; // Return error to Igor
	}
	if ( !test3Dpath(i))
	{
		p->result=false;
		return (0)	;
	}
	double *dPtr;
	float *fPtr;
	switch (WaveType(p->pos)) 
	{
		case NT_FP32:
			fPtr = (float*)WaveData(p->pos);
			pos[0]=(double)fPtr[0];pos[1]=(double)fPtr[1];pos[2]=(double)fPtr[2];
		break;
		case NT_FP64:
			dPtr = (double*)WaveData(p->pos);
			pos[0]=dPtr[0];pos[1]=dPtr[1];pos[2]=dPtr[2];
		break;
		default: // Can�t handle this data type.
			p->result=false; // Return NaN.
			return 0;
	}
	

	if (!XPS3Dobj[i]->SetPos(pos[0],pos[1],pos[2]))
		{
			XOPNotice(XPS3Dobj[i]->last_err);
			XOPNotice(CR_STR);
			p->result=false;
			return (0)	;
		}
	p->result=true;
	return 0;
}

#pragma pack(2)// All structures passed to Igor are two-byte aligned.
struct Axis3DSetVelocityParams  {
	double Velocity;
	double index;
	double result;

};
typedef struct Axis3DSetVelocityParams Axis3DsetVelocityParams;
#pragma pack()

static int
AXIS3DSetVelocity(Axis3DSetVelocityParams* p)
{
	int i=(int)p->index;
	int err;
//	double pos[3];
	DataFolderHandle currentDF;
	if(err=GetCurrentDataFolder(&currentDF))
		return err;
	if ( !test3Dpath(i))
	{
		p->result=false;
		return (0)	;
	}
	if (!XPS3Dobj[i]->SetVelocity(p->Velocity))
		{
			XOPNotice(XPS3Dobj[i]->last_err);
			XOPNotice(CR_STR);
			p->result=false;
			return (0)	;
		}
	
	p->result=true;
	return 0;
}
#pragma pack(2)// All structures passed to Igor are two-byte aligned.
struct Axis3DReadVelocityParams  {
	double index;
	double result;
};
typedef struct Axis3DReadVelocityParams Axis3DReadVelocityParams;
#pragma pack()
static int
AXIS3DReadVelocity(Axis3DReadVelocityParams* p)
{
	int i=(int)p->index;
	int err;
//	double pos[3];
	DataFolderHandle currentDF;
	if(err=GetCurrentDataFolder(&currentDF))
		return err;
	if ( !test3Dpath(i))
	{
		p->result=false;
		return (0)	;
	}
	double V;
	if (!XPS3Dobj[i]->ReadVelocity(&V))
		{
			XOPNotice(XPS3Dobj[i]->last_err);
			XOPNotice(CR_STR);
			SetNaN64(&p->result);
			return (0)	;
		}
	
	p->result=V;
	return 0;
}


#pragma pack(2)// All structures passed to Igor are two-byte aligned.
struct Axis3DSetAccelerationParams  {
	double Acceleration;
	double index;
	double result;

};
typedef struct Axis3DSetAccelerationParams Axis3DsetAccelerationParams;
#pragma pack()

static int
AXIS3DSetAcceleration(Axis3DSetAccelerationParams* p)
{
	int i=(int)p->index;
	int err;
//	double pos[3];
	DataFolderHandle currentDF;
	if(err=GetCurrentDataFolder(&currentDF))
		return err;
	if ( !test3Dpath(i))
	{
		p->result=false;
		return (0)	;
	}
	if (!XPS3Dobj[i]->SetAcceleration(p->Acceleration))
		{
			XOPNotice(XPS3Dobj[i]->last_err);
			XOPNotice(CR_STR);
			p->result=false;
			return (0)	;
		}
	
	p->result=true;
	return 0;
}
#pragma pack(2)// All structures passed to Igor are two-byte aligned.
struct Axis3DReadAccelerationParams  {
	double index;
	double result;
};
typedef struct Axis3DReadAccelerationParams Axis3DReadAccelerationParams;
#pragma pack()
static int
AXIS3DReadAcceleration(Axis3DReadAccelerationParams* p)
{
	int i=(int)p->index;
	int err;
//	double pos[3];
	DataFolderHandle currentDF;
	if(err=GetCurrentDataFolder(&currentDF))
		return err;
	if ( !test3Dpath(i))
	{
		p->result=false;
		return (0)	;
	}
	double V;
	if (!XPS3Dobj[i]->ReadAcceleration(&V))
		{
			XOPNotice(XPS3Dobj[i]->last_err);
			XOPNotice(CR_STR);
			SetNaN64(&p->result);
			return (0)	;
		}
	
	p->result=V;
	return 0;
}

#pragma pack(2)// All structures passed to Igor are two-byte aligned.
struct Axis3DCreateTrajParams  {
	waveHndl S;
	waveHndl Z;
	waveHndl X;
	double index;
	double result;

};
typedef struct Axis3DCreateTrajParams Axis3DCreateTrajParams;
#pragma pack()

static int
Axis3DCreateTraj(Axis3DCreateTrajParams* p)
{
	int i=(int)p->index;
	int err;
	
	DataFolderHandle currentDF;
	if(err=GetCurrentDataFolder(&currentDF))
		return err;
	if ( !test3Dpath(i))
	{
		p->result=false;
		return (0)	;
	}
	
	if ((p->X == NULL) || (p->Z == NULL) || (p->S == NULL))
	{
		p->result = false;
		return NULL_WAVE_OP;
	}

	long XNumPt[MAX_DIMENSIONS+1],ZNumPt[MAX_DIMENSIONS+1],SNumPt[MAX_DIMENSIONS+1];
	int XNumDim,ZNumDim,SNumDim;
	
	MDGetWaveDimensions(p->X, &XNumDim, &XNumPt[0]);
	MDGetWaveDimensions(p->Z, &ZNumDim, &ZNumPt[0]);
	MDGetWaveDimensions(p->S, &SNumDim, &SNumPt[0]);

	if((XNumDim!=1)||(ZNumDim!=1)||(SNumDim!=1))
	{
		XOPNotice("Waves Dimension muste be 1 " CR_STR);
		p->result=false;
		return 0;
	}

	if ( (XNumPt[0]!=ZNumPt[0])||(XNumPt[0]!=SNumPt[0]))
	{
		XOPNotice("Wave lenght must be the same " CR_STR);
		p->result=false;
		return 0;
	}
	float *XPtr_d,*ZPtr_d,*SPtr_d;

	MDChangeWave(p->X,NT_FP32,XNumPt);
	MDChangeWave(p->Z,NT_FP32,ZNumPt);
	MDChangeWave(p->S,NT_FP32,SNumPt);
	XPtr_d = (float*)WaveData(p->X);
	ZPtr_d = (float*)WaveData(p->Z);
	SPtr_d = (float*)WaveData(p->S);

//	char fname[30];
	

	if (!XPS3Dobj[i]->CreateTraj( XPtr_d,ZPtr_d,SPtr_d,XNumPt[0]))
		{
			
			XOPNotice(XPS3Dobj[i]->last_err);
			XOPNotice(CR_STR);
			p->result=false;
			return 0	;	
		}
		
	p->result=true; 
	return 0;
}

#pragma pack(2)// All structures passed to Igor are two-byte aligned.

struct Axis3DIsReadyParams  {

	double index;
	double result;

};
typedef struct Axis3DIsReadyParams Axis3DIsReadyParams;
#pragma pack()

static int
Axis3DIsReady(Axis3DIsReadyParams* p)
{
	int i=(int)p->index;
	p->result=false;
	if ( !test3Dpath(i))
	{
		SetNaN64(&(p->result));
		return (0)	;
	}
	
	if (XPS3Dobj[i]->READY())
	{
		p->result=true;
	}
	else
	{
		p->result=false;
	}
	return 0;

}

#pragma pack(2)// All structures passed to Igor are two-byte aligned.

struct Axis3DRunTrajParams  {
	double Acc;
	double V;
	double index;
	double result;

};
typedef struct Axis3DRunTrajParams Axis3DRunTrajParams;
#pragma pack()

static int
Axis3DRunTraj(Axis3DRunTrajParams* p)
{
	int i=(int)p->index;
	p->result=false;
	if ( !test3Dpath(i))
	{
		SetNaN64(&(p->result));
		return (0)	;
	}
	
	if (XPS3Dobj[i]->RunTraj(p->V,p->Acc))
	{
		p->result=true;
	}
	else
	{
		p->result=false;
	}
	return 0;

}

#pragma pack(2)// All structures passed to Igor are two-byte aligned.

struct Axis3DAbortParams  {

	double index;
	double result;

};
typedef struct Axis3DAbortParams Axis3DAbortParams;
#pragma pack()

static int
Axis3DAbort(Axis3DAbortParams* p)
{
	int i=(int)p->index;
	p->result=false;
	if ( !test3Dpath(i))
	{
		SetNaN64(&(p->result));
		return (0)	;
	}	
	if (XPS3Dobj[i]->Abort())
		p->result=true;
	else
		p->result=false;
	return 0;

}


#pragma pack(2)// All structures passed to Igor are two-byte aligned.

struct Axis3DGetGatheringDataParams  {
	double index;
	double result;

};
typedef struct Axis3DGetGatheringDataParams Axis3DGetGatheringDataParams;
#pragma pack()// 

static int
Axis3DGetGatheringData(Axis3DGetGatheringDataParams* p)
{
	int err;
	int i=(int)p->index;
	if ( !test3Dpath(i))
	{
		SetNaN64(&(p->result));
		return (0)	;
	}
	double *XPos;
	double *ZPos;
	double *YPos;
	int NB;
	if (!(XPS3Dobj[i]->GetGatheringData(&XPos,&ZPos,&YPos,&NB)))
	{
		XOPNotice(XPS3Dobj[i]->last_err);
		XOPNotice(CR_STR);
		p->result=false;
	}
	double *XTMP=XPos;
	double *ZTMP=ZPos;
	double *YTMP=YPos;
	double XWval[2],ZWval[2],YWval[2];
	Handle currentDF;
	if(err=GetCurrentDataFolder(&currentDF))
		return err;
	waveHndl XW,ZW,YW;
	long Dimw[MAX_DIMENSIONS+1];
	long ind[MAX_DIMENSIONS];
	Dimw[0]=NB;
	Dimw[1]=0;
	ind[1]=0;

	if(err=(MDMakeWave(&ZW,"ZPos",XPS3DDFH[i],Dimw,NT_FP64,1)!=0))
		return err;
	if(err=(MDMakeWave(&XW,"XPos",XPS3DDFH[i],Dimw,NT_FP64,1)!=0))
		return err;
	if(err=(MDMakeWave(&YW,"YPos",XPS3DDFH[i],Dimw,NT_FP64,1)!=0))
		return err;

	SetNaN64(&XWval[0]);
	for ((int)i=0;i<NB;i++)
	{
		ind[0]=i;
		MDSetNumericWavePointValue(XW,ind,XWval);
		MDSetNumericWavePointValue(ZW,ind,XWval);
		MDSetNumericWavePointValue(YW,ind,YWval);
	}
	for ((int)i=0;i<NB;i++)
	{
		XWval[0]=*XTMP;
		ZWval[0]=*ZTMP;
		YWval[0]=*YTMP;
		ind[0]=i;
		if(err=(MDSetNumericWavePointValue(XW,ind,XWval)))
		{
			free(XPos);
			free(ZPos);
			free(YPos);
			return err;
		}
		if(err=(MDSetNumericWavePointValue(ZW,ind,ZWval)))
		{
			free(XPos);
			free(ZPos);
			free(YPos);
			return err;
		}
		if(err=(MDSetNumericWavePointValue(YW,ind,YWval)))
		{
			free(XPos);
			free(ZPos);
			free(YPos);
			return err;
		}
		XTMP++; 
		ZTMP++;
		YTMP++;
	}
	free(XPos);
	free(ZPos);
	free(YPos);
	p->result=true;
	return 0;

}

#pragma pack(2)// All structures passed to Igor are two-byte aligned.

struct Axis3DRunTrajAndTrigParams  {
	double TrigLen;
	double Acc;
	double V;
	double index;
	double result;

};
typedef struct Axis3DRunTrajAndTrigParams Axis3DRunTrajAndTrigParams;
#pragma pack()

static int
Axis3DRunTrajAndTrig(Axis3DRunTrajAndTrigParams* p)
{
	int i=(int)p->index;
	p->result=false;
	if ( !test3Dpath(i))
	{
		SetNaN64(&(p->result));
		return (0)	;
	}	
	if (XPS3Dobj[i]->RunTrajAndtrig(p->V,p->Acc,p->TrigLen))
		p->result=true;
	else
		p->result=false;
	return 0;

}



//************************************************************************************************************************************
static long
RegisterFunction()
{
	int funcIndex;

	/*	NOTE:
		Some XOPs should return a result of NIL in response to the FUNCADDRS message.
		See XOP manual "Restrictions on Direct XFUNCs" section.
	*/

	funcIndex = GetXOPItem(0);		/* which function invoked ? */
	switch (funcIndex) {
		case 0:						/* AXIS3DAdd(p1, p2) */
			return((long)AXIS3DOpen);
			break;
		case 1:						/* AXIS3DDiv(p1, p2) */
			return((long)AXIS3DClose);
			break;
		case 2:						/* AXIS3DComplexConjugate(p1) */
			return((long)AXIS3DReadPos);
			break;
		case 3:						/* AXIS3DComplexConjugate(p1) */
			return((long)AXIS3DSetPos);
			break;
		case 4:						/* AXIS3DComplexConjugate(p1) */
			return((long)AXIS3DSetVelocity);
			break;
		case 5:						/* AXIS3DComplexConjugate(p1) */
			return((long)AXIS3DReadVelocity);
			break;
		case 6:						/* AXIS3DComplexConjugate(p1) */
			return((long)AXIS3DSetAcceleration);
			break;
		case 7:						/* AXIS3DComplexConjugate(p1) */
			return((long)AXIS3DReadAcceleration);
			break;	
		case 8:						/* Axis2DComplexConjugate(p1) */
			return((long)Axis3DCreateTraj);
			break;
		case 9:						/* Axis2DComplexConjugate(p1) */
			return((long)Axis3DIsReady);
			break;
		case 10:						/* Axis2DComplexConjugate(p1) */
			return((long)Axis3DRunTraj);
			break;
		case 11:						/* Axis2DComplexConjugate(p1) */
			return((long)Axis3DAbort);
			break;
		case 12:						/* Axis2DComplexConjugate(p1) */
			return((long)Axis3DGetGatheringData);
			break;
		case 13:						/* Axis2DComplexConjugate(p1) */
			return((long)Axis3DRunTrajAndTrig);
			break;

	}
	return(NIL);
}

/*	DoFunction()

	This will actually never be called because all of the functions use the direct method.
	It would be called if a function used the message method. See the XOP manual for
	a discussion of direct versus message XFUNCs.
*/
static int
DoFunction()
{
	int funcIndex;
	void *p;				/* pointer to structure containing function parameters and result */
	int err;

	funcIndex = GetXOPItem(0);		/* which function invoked ? */
	p = (void *)GetXOPItem(1);		/* get pointer to params/result */
	switch (funcIndex) {
		case 0:						/* AXIS3DAdd(p1, p2) */
			err = AXIS3DOpen((AXIS3DOpenParams *)p);
			break;
		case 1:						/* AXIS3DDiv(p1, p2) */
			err = AXIS3DClose((AXIS3DCloseParams *)p);
			break;
		case 2:						/* AXIS3DComplexConjugate(p1) */
			err = AXIS3DReadPos((Axis3DReadPosParams *)p);
			break;
		case 3:						/* AXIS3DComplexConjugate(p1) */
			err = AXIS3DSetPos((Axis3DSetPosParams *)p);
			break;
		case 4:						/* AXIS3DComplexConjugate(p1) */
			err = AXIS3DReadVelocity((Axis3DReadVelocityParams *)p);
			break;
		case 5:						/* AXIS3DComplexConjugate(p1) */
			err = AXIS3DSetVelocity((Axis3DSetVelocityParams *)p);
			break;
		case 6:						/* AXIS3DComplexConjugate(p1) */
			err = AXIS3DReadAcceleration((Axis3DReadAccelerationParams *)p);
			break;
		case 7:						/* AXIS3DComplexConjugate(p1) */
			err = AXIS3DSetAcceleration((Axis3DSetAccelerationParams *)p);
			break;
		case 8:						/* Axis2DComplexConjugate(p1) */
			err = Axis3DCreateTraj((Axis3DCreateTrajParams*)p);
			break;
		case 9:						/* Axis2DComplexConjugate(p1) */
			err = Axis3DIsReady((Axis3DIsReadyParams*)p);
			break;
		case 10:						/* Axis2DComplexConjugate(p1) */
			err = Axis3DRunTraj((Axis3DRunTrajParams*)p);
			break;
		case 11:						/* Axis2DComplexConjugate(p1) */
			err = Axis3DAbort((Axis3DAbortParams*)p);
			break;
		case 12:						/* Axis2DComplexConjugate(p1) */
			err = Axis3DGetGatheringData((Axis3DGetGatheringDataParams*)p);
			break;
		case 13:						/* Axis2DComplexConjugate(p1) */
			err = Axis3DRunTrajAndTrig((Axis3DRunTrajAndTrigParams*)p);
			break;

}
	return(err);
}

/*	XOPEntry()

	This is the entry point from the host application to the XOP for all messages after the
	INIT message.
*/

static void
XOPEntry(void)
{	
	long result = 0;

	switch (GetXOPMessage()) {
		case FUNCTION:								/* our external function being invoked ? */
			result = DoFunction();
			break;

		case FUNCADDRS:
			result = RegisterFunction();
			break;
	}
	SetXOPResult(result);
}

/*	main(ioRecHandle)

	This is the initial entry point at which the host application calls XOP.
	The message sent by the host must be INIT.
	main() does any necessary initialization and then sets the XOPEntry field of the
	ioRecHandle to the address to be called for future messages.
*/

HOST_IMPORT void
main(IORecHandle ioRecHandle)
{	
	XOPInit(ioRecHandle);							/* do standard XOP initialization */
	SetXOPEntry(XOPEntry);							/* set entry point for future calls */
	
	if (igorVersion < 620)
		SetXOPResult(OLD_IGOR);
	else
		SetXOPResult(0L);
}
