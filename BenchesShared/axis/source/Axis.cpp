// Axis.cpp: implementation of the Axis class.
//
//////////////////////////////////////////////////////////////////////
#include <stdio.h>
//#include <iostream.h>


#include "Axis.h"
#include <string.h>
#include <Windows.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Axis::Axis()
{

}
Axis::Axis(char * Name)
{
int Status;

  if((Status = GetConfiguration(Name)) == 0)
	{
		if (strcmp(axis_type,"ANORAD")==0)
		{
//			GenAxis = new Anorad(Name);
//			delete GenAxis;
			//GenAxis = new Anorad(Name);
			//Valid = true;
			Valid = false;
		}
		else if ((strcmp(axis_type,"4006")==0)||(strcmp(axis_type,"4005")==0))
		{
			//GenAxis = new MM4006(Name);
			//Valid = true;
			Valid = false;
		}
		else
		{
			Valid = false;
			GenAxis=0;
		}
		if (strcmp(axis_type,"XPS")==0)
		{
			GenAxis = new XPS(Name);
			Valid = true;
		}

	}
  else
	{
		Valid = false;
		GenAxis=0;
	}
}

Axis::~Axis()
{
if((strcmp(axis_type,"MM4006")==0)||(strcmp(axis_type,"ANORAD")==0)||(strcmp(axis_type,"XPS")==0))
	delete GenAxis;
}

int Axis::Abort()
{
	if  (Valid)
		return GenAxis->Abort();
	else
		return false;
}


int Axis::AbsMove(double RequestedPosition)
{
	if  (Valid)
		return GenAxis->AbsMove(RequestedPosition);
	else
		return false;

}

int Axis::InitOpticalRule()
{
	if  (Valid)
		return GenAxis->InitOpticalRule();
	else
		return false;

}

bool Axis::IsMoving()
{
	if  (Valid)
		return GenAxis->IsMoving();
	else
		return true;

}

bool Axis::IsValid()
{
	if  (Valid)
		return GenAxis->IsValid();
	else
		return false;
}

int Axis::NegativeLimit()
{
	if  (Valid)
		return GenAxis->NegativeLimit();
	else
		return false;	
}

int Axis::PositiveLimit()
{
	if  (Valid)
		return GenAxis->PositiveLimit();
	else
		return false;	

}

int Axis::ReadExternallyLatchedPosition(double * Pos)
{
	if  (Valid)
		return GenAxis->ReadExternallyLatchedPosition(Pos);
	else
		return false;
}

int Axis::ReadPosition(double * Pos)
{
	if  (Valid)
		return GenAxis->ReadPosition(Pos);
	else
		return false;
}

int Axis::RelMove(double Disp)
{
	if  (Valid)
		return GenAxis->RelMove(Disp);
	else
		return false;
}

int Axis::RelMoveOne(int num,double Disp)
{
	if  (Valid)
		return GenAxis->RelMoveOne( num,Disp);
	else
		return false;
}
int Axis::SetExternalLatching()
{
	if  (Valid)
		return GenAxis->SetExternalLatching();
	else
		return false;
}


int Axis::GetConfiguration(char * Name)
{

//**********************************************************
// Define some structures used for the extraction of the 
// resources.
//**********************************************************
typedef struct _AxisRes
{
	char name[30];
	char val[30];
}AxisRes;

enum Devices 
{
  AXE_TYPE
};

AxisRes AxisResTab[] = 
{
  {"AXE_TYPE",       ""}
};
	
	FILE   *Fin;
	char   *Temp;
	char    ConfBuf[1000];
	char    ConfStr[128];
	char    FileName[256];
	int     i, NbRes;
	int     Status;
	


	Status = GetEnvironmentVariable("ConfPath", ConfStr, 100);

  if(Status == 0)  // Environment variable not defined.
    sprintf(FileName, "%s\\%s.conf", "c:\\ConfigurationFolder", Name);
  else
    sprintf(FileName, "%s\\%s.conf", ConfStr, Name);
	
  Fin = fopen(FileName, "r");
  if(Fin == NULL)
	{

		return(-1);
	}

	//--------------------------------------------
	// Get resource names from configuration file.

  fread(ConfBuf, sizeof(ConfBuf), 1, Fin);
	NbRes = sizeof(AxisResTab) / sizeof(AxisRes);
	
  for(i=0; i<NbRes; i++)
  {
		if((Temp = strstr(ConfBuf, AxisResTab[i].name)) == NULL)
		{

			fclose(Fin);
			return(-1);
		}
		else
		{
			sscanf(Temp, "%*s%s", &(AxisResTab[i].val));

		}
  }
	strcpy(axis_type, AxisResTab[AXE_TYPE].val);
	
	fclose(Fin);
	return(0);
}


int Axis::ReadSpeed(double * speed)
{
	if  (Valid)
		return GenAxis->ReadSpeed(speed);
	else
		return false;
}

int Axis::ReadTaper(double * Taper)
{
	if  (Valid)
		return GenAxis->ReadTaper(Taper);
	else
		return false;
}

int Axis::Reference()
{
	if  (Valid)
		return GenAxis->Reference();
	else
		return false;
}

int Axis::SetAcceleration(double accel)
{

	if  (Valid)
		return GenAxis->SetAcceleration(accel);
	else
		return false;
}

int Axis::SetDefaultPosition(double Pos)
{
	if  (Valid)
		return GenAxis->SetDefaultPosition(Pos);
	else
		return false;
}

int Axis::SetSpeed(double Speed)
{
	if  (Valid)
		return GenAxis->SetSpeed(Speed);
	else
		return false;
}

int Axis::SetDefaultTaper(double Taper)
{
	if  (Valid)
		return GenAxis->SetDefaultTaper(Taper);
	else
		return false;
}

int Axis::SetTaper(double Taper)
{
	if  (Valid)
		return GenAxis->SetTaper(Taper);
	else
		return false;
}


int Axis::Move_1Trig(double Pos, double TrigPos)
{
	if  (Valid)
		return GenAxis->Move_1Trig( Pos, TrigPos);
	else
		return false;
}

int Axis::MoveAndTrig(double beg, double fin, double step,double over)
{
	if  (Valid)
		return GenAxis->MoveAndTrig( beg, fin,step,over);
	else
		return false;
}

int Axis::MovingTime(double depl, double *t)
{
	if  (Valid)
		return GenAxis->MovingTime( depl,t);
	else
		return false;
}

int Axis::ReadAcceleration(double *accel)
{
	if  (Valid)
		return GenAxis->ReadAcceleration( accel);
	else
		return false;
}

int Axis::ReadRef(double *refpos)
{
	if  (Valid)
		return GenAxis->ReadRef( refpos);
	else
		return false;

}
