// Anorad.h: interface for the Anorad class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ANORAD_H__A4370D5B_61BD_41BC_B086_9E4F4625666C__INCLUDED_)
#define AFX_ANORAD_H__A4370D5B_61BD_41BC_B086_9E4F4625666C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "Axis.h"
#include "SerialComm.h"
#include "stdio.h"

class Anorad :public Axis
{
public:
	int ReadRef(double*refpos);
	bool ProgRunning();
	bool refmove;
	int MovingTime(double depl,double *t);
	int ReadAcceleration(double *accel);
	int Move_1Trig(double Pos,double TrigPos);
	int SetTaper(double taper);
	int SetExternalLatching();
	int SetDefaultTaper(double taper);
	int InitOpticalRule();
	int ReadExternallyLatchedPosition(double *Pos);
	int RelMove(double move);
	int PositiveLimit();
	int NegativeLimit();
	int SetDefaultPosition(double defpos);
	int SetAcceleration(double accel);
	int SetSpeed(double speed);
	int ReadTaper(double *tape);
	int Reference();
	int MoveAndTrig(double beg,double fin,double step,double over);
	int ReadSpeed(double *speed);
	bool IsValid();
	int ReadPosition(double *Pos);
	bool IsMoving();
	int AbsMove(double Pos);
	int Abort();
	Anorad(char *Name);
	Anorad();
	virtual ~Anorad();
	double homepos;
	int StepByUnit;
	SerialComm *ComuPath;
	char RefType[20];
	bool GetAck();

private:
	double DefAcceleration;
	double DefSpeed;
	bool Valid;
	char SetFile[30];
	int AxisNum;
	char ComuPathName[20];
	int GetConfiguration(char* Name);
	
};

#endif // !defined(AFX_ANORAD_H__A4370D5B_61BD_41BC_B086_9E4F4625666C__INCLUDED_)
