// Anorad.cpp: implementation of the Anorad class.
//
//////////////////////////////////////////////////////////////////////
#include <math.h>
#include "Anorad.h"
#include "SerialComm.h"
#include <process.h>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Anorad::Anorad()
{

}

Anorad::~Anorad()
{
	ComuPath->WriteAscii("SXMO0\r");
	if (!GetAck())
	{
		printf ("bad ack to SXMO0\n");
	}
	
	delete(ComuPath);
}
int Anorad::GetConfiguration(char* Name)
{
//**********************************************************
// Define some structures used for resources extraction.
//**********************************************************
typedef struct _Res
{
	char name[50];
	char val[50];
}Res;

enum Devices 
{
  AXE_TYPE, 
    AXE_NUM, 
    COMU_PATH, 
    STEP_BY_UNIT, 
    SET_FILE,
	HOME_POS,
    DEF_SPEED,
    DEF_ACC, 
	REF_TYPE,

};

Res CtrlResTab[] = 
{
  {"AXE_TYPE",       ""},
  {"AXE_NUM",        ""}, 
  {"COMU_PATH",      ""},
  {"STEP_BY_UNIT", ""},
  {"SET_FILE",      ""},
  {"HOME_POS",""},
  {"DEF_SPEED",      ""},
  {"DEF_ACC",""},
  {"REF_TYPE",""},

};
	
	FILE   *Fin;
	char   *Temp;
	char    Buf[1000];
	char    ConfStr[128];
	char    FileName[256];
	int     i, NbRes;
	int     status;
	

	status = GetEnvironmentVariable("ConfPath", ConfStr, 100);

  if(status == 0)  // Environment variable not defined.
    sprintf(FileName, "%s\\%s.conf", "c:\\ConfigurationFolder", Name);
  else
    sprintf(FileName, "%s\\%s.conf", ConfStr, Name);
	
  Fin = fopen(FileName, "r");
  if(Fin == NULL)
	{
		return(false);
	}

	//--------------------------------------------
	// Get resource names from configuration file.

  fread(Buf, sizeof(Buf), 1, Fin);
	NbRes = sizeof(CtrlResTab) / sizeof(Res);
  for(i=0; i<NbRes; i++)
  {
		if((Temp = strstr(Buf, CtrlResTab[i].name)) == NULL)
		{
			return(false);
		}
		else
		{
			sscanf(Temp, "%*s%s", &(CtrlResTab[i].val));
		}
  }

	strcpy(ComuPathName, CtrlResTab[COMU_PATH].val);
	strcpy(SetFile, CtrlResTab[SET_FILE].val);
	strcpy(RefType, CtrlResTab[REF_TYPE].val);
	StepByUnit       = atoi(CtrlResTab[STEP_BY_UNIT].val);
	AxisNum       = atoi(CtrlResTab[AXE_NUM].val);
	DefSpeed        =  atof(CtrlResTab[DEF_SPEED].val);
	DefAcceleration =  atof(CtrlResTab[DEF_ACC].val);

    homepos=atof(CtrlResTab[HOME_POS].val);
	fclose(Fin);
	return(true);
}

Anorad::Anorad(char *Name)
{
//FILE *setfile;
//char s[30];
char FileName[100],ConfStr[100];
int status;


if(!GetConfiguration(Name))
{
	printf ("can't get configuartion for %s\n",Name);
}
if ((ComuPath= new SerialComm(ComuPathName))==NULL)
{
	printf ("can't create new SERIAL COMM  for %s\n",Name);
}
if (ComuPath->SetParameters(9600,8,1,'N')!=0)
{
	printf ("can't set SERIAL COMM parameter for %s\n",Name);
}

	status = GetEnvironmentVariable("ConfPath", ConfStr, 100);

  if(status == 0)  // Environment variable not defined.
    sprintf(FileName, "%s\\%s.conf", "c:\\ConfigurationFolder", Name);
  else
    sprintf(FileName, "%s\\%s.conf", ConfStr, Name);
	
/*	  setfile = fopen(FileName, "r");
  if(setfile != NULL)
	{
		while (fgets(s,30,	setfile)!=NULL)
		{
			s[strlen(s)-1]='\r';
			ComuPath->WriteAscii(s);
			if (!GetAck())
			{
				printf ("bad ack to %s\n",s);
			}
		}
		 fclose(setfile);
	}
	*/
	while (!GetAck())
	{
		Sleep(500);
 		ComuPath->WriteAscii("SHT2\r");
		
	}
  	ComuPath->WriteAscii("SXMO1\r");
	if (!GetAck())
	{
		printf ("bad ack to SXMO1\n");
	}

  	
	refmove=false;

	SetSpeed(DefSpeed);
	SetAcceleration(DefAcceleration);
Valid=true;
}
bool Anorad::GetAck()
{
	char ack[100];
	if (ComuPath->ReadAscii(ack,100)!=0)
	{
		printf ("bad Read\n");
		return false;
	}

	if (strstr(ack,"0>")==NULL)
	{
		printf ("bad ack %s\n ",ack);
		return false;
	}

	return true;

}

int Anorad::Abort()
{
	char cmd[30];
/*	if (refmove)
	{
		if(ComuPath->WriteAscii("AVSE0 1\r")!=0)
			return false;				
		if (!GetAck())
			return false;
	}
	else
	*/
	{
		sprintf (cmd,"KX\r");
		if(ComuPath->WriteAscii(cmd)!=0)
			return false;
		if (!GetAck())
			return false;
		
	}
	return true;
}

int Anorad::AbsMove(double Pos)
{
	char cmd[30];
	if (refmove)
		return false;
	sprintf (cmd,"SXAP%d\r",(int)(Pos*StepByUnit));
	if(ComuPath->WriteAscii(cmd)!=0)
		return false;
	if (!GetAck())
		return false;
	if(ComuPath->WriteAscii("BX\r")!=0)
		return false;
	return GetAck();

}
bool Anorad::IsMoving()
{
	if (refmove)
		return true;
	char resp[30];
	if(ComuPath->WriteAscii("T0\r")!=0)
		return true;
	if(ComuPath->ReadAscii(resp,29)!=0)
		return true;
	if (strstr(resp,"RUN")!=NULL)
		return true;

	return false;
}
int Anorad::ReadPosition(double *Pos)
{
	char resp[100];
	char tmpstr[20];
	float tmppos;

	if (refmove)
		return false;

	if(ComuPath->WriteAscii("RXCP\r")!=0)
		return false;
	if(ComuPath->ReadAscii(resp,100)!=0)
		return false;
	if (sscanf(resp,"%s%f",&tmpstr,&tmppos)==2)
	{
		*Pos=(double)(tmppos/StepByUnit);
		return true;
	}
	return false;
}

bool Anorad::IsValid()
{

	return Valid;
}
int Anorad::ReadSpeed(double *speed)
{
	char resp[30];
	char tmpstr[20];
	float tmpspeed;

	if (refmove)
		return false;
	if(ComuPath->WriteAscii("RXLV\r")!=0)
		return false;
	if(ComuPath->ReadAscii(resp,29)!=0)
		return false;
	if (sscanf(resp,"%s%f",&tmpstr,&tmpspeed)==2)
	{
		*speed=(double)(tmpspeed/StepByUnit);
		return true;
	}
	return false;
}
int Anorad::MoveAndTrig(double beg,double end,double step,double over)
{
	double l_beg,l_end,l_step,l_back,speed,accel;
	char cmd[30];

	if (refmove)
		return false;

	l_step=fabs(step);
	if (ReadSpeed(&speed)!=(int)true)
		return false;
	if (ReadAcceleration(&accel)!=(int)true)
		return false;
	l_back=over;
	if (end>beg)
	{
		l_beg=beg-l_back;
		l_end=end+l_back;
	}
	else
	{
		l_beg=beg+step;
		l_end=end-step;
	}

	if(AbsMove(l_beg)!=(int)true)
		return false;
	if(ComuPath->WriteAscii("SPM128\r")!=0)
		return false;
	if (!GetAck())
		return false;



	if(ComuPath->WriteAscii("SFE0\r")!=0)
		return false;
	if (!GetAck())
		return false;


	sprintf (cmd,"SXSL%d\r",(int)(beg*StepByUnit));
	if(ComuPath->WriteAscii(cmd)!=0)
		return false;
	if (!GetAck())
		return false;

	sprintf (cmd,"SXEL%d\r",(int)(end*StepByUnit));
	if(ComuPath->WriteAscii(cmd)!=0)
		return false;
	if (!GetAck())
		return false;

	sprintf (cmd,"SXPI%d\r",(int)(step*StepByUnit));
	if(ComuPath->WriteAscii(cmd)!=0)
		return false;
	if (!GetAck())
		return false;

	sprintf (cmd,"SXMT%d\r",(int)(100));
	if(ComuPath->WriteAscii(cmd)!=0)
		return false;
	if (!GetAck())
		return false;

	while (IsMoving())
		Sleep(100);
	if(ComuPath->WriteAscii("SXEG2\r")!=0)
		return false;
	if (!GetAck())
		return false;

	if(ComuPath->WriteAscii("SFE2\r")!=0)
		return false;
	if (!GetAck())
		return false;

	if(AbsMove(l_end)!=(int)true)
		return false;
	
	
	return true;
}

static void BackRef(void *pt)
{
	char cmd[30];
	bool ref_run;
	Anorad *p;
	p=(Anorad *)pt;

	// Set V0 to 0 and V1 with the position at reference in anorad PLC
	sprintf (cmd,"AVSE0 0 %d\r",(int)(p->homepos*p->StepByUnit));
	if(p->ComuPath->WriteAscii(cmd)!=0)
	{
		p->refmove=false;
		_endthread();
	}
	if (!p->GetAck())
	{
		p->refmove=false;
		_endthread();
	}

	sprintf (cmd,"PX%s\r",p->RefType);
	if(p->ComuPath->WriteAscii(cmd)!=0)
	{
		p->refmove=false;
		_endthread();
	}
	if (!p->GetAck())
	{
		p->refmove=false;
		_endthread();
	}
	
	ref_run=true;

/*	while (ref_run)
	{
		p->ComuPath->ReadAscii(ack,19);
		if (strstr(ack,"0P 01")!=NULL)
		{
		ref_run=false;
		}
		Sleep(1000);
	}
	*/
	while (p->ProgRunning())
	{
		Sleep(1000);
	}

	p->refmove=false;
	_endthread();


}
int Anorad::Reference()
{
	if (refmove)
		return false;
	refmove=true;

	if(_beginthread(BackRef,0,(void *)this)==-1)
	{
		return (false);
	}	
	return (true);
}

int Anorad::ReadTaper(double *tape)
{
	tape=0;
	return true;
}

int Anorad::SetSpeed(double speed)
{
	char cmd[30];

	if (refmove)
		return false;
	sprintf (cmd,"SXLV%d\r",(int)(speed*StepByUnit));
	if(ComuPath->WriteAscii(cmd)!=0)
		return false;
	return GetAck();
}

int Anorad::SetAcceleration(double accel)
{
	char cmd[30];

	if (refmove)
		return false;
	sprintf (cmd,"SXLA%d\r",(int)(accel*StepByUnit));
	if(ComuPath->WriteAscii(cmd)!=0)
		return false;
	if (!GetAck())
		return false;
	sprintf (cmd,"SXLD%d\r",(int)(accel*StepByUnit));
	if(ComuPath->WriteAscii(cmd)!=0)
		return false;
	if (!GetAck())
		return false;
	sprintf (cmd,"SXKD%d\r",(int)(accel*StepByUnit));
	if(ComuPath->WriteAscii(cmd)!=0)
		return false;
	return GetAck();

}

int Anorad::SetDefaultPosition(double defpos)
{
	double oldhome,nowpos,newpos;
	char cmd[100];

	if (refmove)
		return false;
	oldhome=homepos;
	homepos=defpos;
	if (ReadPosition(&nowpos)!=(int)true)
		return false;
	newpos=nowpos+(homepos-oldhome);
	sprintf (cmd,"SXZP%d\r",(int)(newpos*StepByUnit));
	if(ComuPath->WriteAscii(cmd)!=0)
		return false;
	return(GetAck());
	
}

int Anorad::NegativeLimit()
{
return true;
}

int Anorad::PositiveLimit()
{
return true;
}

int Anorad::RelMove(double move)
{
return true;
}

int Anorad::ReadExternallyLatchedPosition(double *Pos)
{
	*Pos=0;
	return true;
}

int Anorad::InitOpticalRule()
{
	return true;
}

int Anorad::SetDefaultTaper(double taper)
{
	return true;
}

int Anorad::SetExternalLatching()
{
	return true;
}

int Anorad::SetTaper(double taper)
{
return true;
}

int Anorad::Move_1Trig(double Pos, double TrigPos)
{
	int status;
	status=MoveAndTrig(TrigPos,Pos,fabs(Pos-TrigPos+21),20);
	return status;
}

int Anorad::ReadAcceleration(double *accel)
{
	char resp[30];
	char tmpstr[20];
	float tmpaccel;

	if (refmove)
		return false;

	if(ComuPath->WriteAscii("RXLA\r")!=0)
		return false;
	if(ComuPath->ReadAscii(resp,29)!=0)
		return false;
	if (sscanf(resp,"%s%f",&tmpstr,&tmpaccel)==2)
	{
		*accel=(double)(tmpaccel/StepByUnit);
		return true;
	}
	return false;	
}

int Anorad::MovingTime(double depl, double *t)
{
return true;
}

bool Anorad::ProgRunning()
{
	char resp[200];
	if(ComuPath->WriteAscii("T3\r")!=0)
		return true;
	if(ComuPath->ReadAscii(resp,150)!=0)
		return true;
	
	if((strstr(resp,"NOT")==NULL)&&(strstr(resp,"not")==NULL))
		return true;
	else
		return false;
}

int Anorad::ReadRef(double *refpos)
{
	*refpos=(double) homepos;
	return true;

}
