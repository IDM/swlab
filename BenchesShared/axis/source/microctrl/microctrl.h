// MicroCtrl.h: interface for the MicroCtrl class.
//
//

// ESRF 2000

// C. Penel

// @doc
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MICROCTRL_H__9C4C0AF2_DAEE_11D3_BFC6_00A02459FF70__INCLUDED_)
#define AFX_MICROCTRL_H__9C4C0AF2_DAEE_11D3_BFC6_00A02459FF70__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include "Axis.h"
#include "SerialComm.h"

//@class This is the class for MicroControl controller.

class MicroCtrl :public Axis
{
//@access Public members
public:
	int ReadAccel(double *accel);
	bool aborted;
	//@cmember Serial Line Descriptor
	SerialComm *SerialLine;
	//@cmember number of step by unit ( computed )
	double StepByDegree;
	//@cmember Default Speed used for Displacement
	double DefaultSpeed;

	int ReadSpeed(double *Speed);
	//@cmember Set the default Taper value ( no read )
	int SetDefaultTaper( double Taper);
	
	//@cmember Set the default speed for displacement
	int SetDefaultSpeed(double Speed);
	
	//@cmember Set the offset position 
	int SetDefaultPosition(double Pos);
	
	//@cmember No implemented
	int SetDefaultAcceleration(double accel);

	//@cmember Set the taper value read return it
	int SetTaper(double Taper);

	//@cmember relative move of axis
	int RelMove(double mov);

	//@cmember Read the differnece position betwween two motors of an axis
	int ReadTaper(double *Taper);

	//@cmember function to read axis position.
	int ReadPosition( double *Pos);

	//@cmember function to move axis to negative limit.
	int Reference(void);

	//@cmember function get moving status of axis.
	bool IsMoving(void);

	//@cmember function to move to Pos in mm.
	int AbsMove(double Pos);

	//@cmember function to stop running displacement.
	int Abort(void);

	//@cmember Default constructor.
	MicroCtrl();

	//@cmember  constructor.
	MicroCtrl(char *Name);

	//@cmember  Destructor
	virtual ~MicroCtrl();

//@access Private members
private:
	//@cmember return the state of ramping computing
	bool RampeCalc();

	//@cmember if object MicroCtrl is correctly Set
	bool Valid;
	//@cmember The Set Taper Used now	
	double WantedTaper;
	//@cmember The default Taper Value	
	double DefaultTaper;



	//@cmember serial line name (COM5 for example)
	char SerialName[10];

	//@cmember offset position used 
	double DefaultPosition;

	//@cmember not implemented
	double DefaultAcceleration;


	//@cmember  used to  set StepByDegrees
	int ScrewStep;

	//@cmember used to  set StepByDegrees
	int ReductorRatio;

	//@cmember used to  set StepByDegrees
	int StepByRotate;

	//@cmember Axis number and type 1-->first axis
	//								2-->second axis
	//								3--> both axis
	int AxisNum;

	//@cmember Get the configuration parameters for an axis.
	int GetConfiguration(char *Name);


};
#endif // !defined(AFX_MICROCTRL_H__9C4C0AF2_DAEE_11D3_BFC6_00A02459FF70__INCLUDED_)
