//*********************************************************
//@doc MicroCtrl
//@module MicroCtrl.cpp | Implementation of the MicroCtrl 
//class.
//
// ESRF 2000
// C. PENEL
//@index | MicroCtrl
//=========================================================
#include <process.h>
#include <stdio.h>
#include "MicroCtrl.h"


//*********************************************************
//@mfunc Default constructor, should not be used.
//*********************************************************
MicroCtrl::MicroCtrl()
{
}
//*********************************************************
//@mfunc Destructor.
//*********************************************************
MicroCtrl::~MicroCtrl()
{

	delete SerialLine;
}
//*********************************************************
//@mfunc Usable constructor .
//@parm definition file name
//*********************************************************
MicroCtrl::MicroCtrl(char *Name)
{
	char locstr[256];
	int Status;
	
	sprintf(locstr,"MicroCtrl::MicroCtrl(%s)\n",Name);
	if((Status = GetConfiguration(Name)) == 0)
	{
		SerialLine=new SerialComm(SerialName);
		if(SerialLine->IsValid())
		{
			Status = SerialLine->SetParameters(9600, 8, 1, 'n');
			if (Status==0)
			{
				SetDefaultAcceleration(DefaultAcceleration);
				sprintf (locstr,"OH%i,OL%i,OT%i\r",
						(int) (DefaultSpeed/2*StepByDegree),
						(int) (DefaultSpeed/20*StepByDegree),
						(int) (100));
				SerialLine->WriteAscii(locstr);
//				SerialLine->WriteAscii("OH2500,OL250,OT100\r");		// reference parameter
				SerialLine->WriteAscii("SC1\r");						// encoder counting initialisation
			}
			else
				Valid=false;
		}
		else
			Valid=false;
	}
}

//**********************************************************
// @mfunc Called by the constructor.
// Open the relevant configuration file, extract the 
// settings (ASCII format) and build private parameters. 
// @parm The name of the axis.
//**********************************************************
int MicroCtrl::GetConfiguration(char *Name)
{
//**********************************************************
// Define some structures used for resources extraction.
//**********************************************************
typedef struct _Res
{
	char name[50];
	char val[50];
}Res;

enum Devices 
{
  AXE_TYPE, 
    AXE_NUM, 
    LINE_NAME, 
    STEP_BY_ROTATE, 
    REDUCTOR_RATIO, 
    SCREW_STEP, 
    DEF_SPEED, 
    DEF_ACC, 
    DEF_POS,
	DEF_TAPER
};

Res MicroCtrlResTab[] = 
{
  {"AXE_TYPE",       ""},
  {"AXE_NUM",        ""}, 
  {"LINE_NAME",      ""},
  {"STEP_BY_ROTATE", ""}, 
  {"REDUCTOR_RATIO", ""},
  {"SCREW_STEP",     ""},
  {"DEF_SPEED",      ""},
  {"DEF_ACC",        ""},
  {"DEF_POS",        ""},
  {"DEF_TAPER",		 ""}
};
	
	FILE   *Fin;
	char   *Temp;
	char    Buf[1000];
	char    ConfStr[128];
	char    FileName[256];
	int     i, NbRes;
	int     status;
	

	status = GetEnvironmentVariable("ConfPath", ConfStr, 100);

  if(status == 0)  // Environment variable not defined.
    sprintf(FileName, "%s\\%s.conf", "c:\\ConfigurationFolder", Name);
  else
    sprintf(FileName, "%s\\%s.conf", ConfStr, Name);
	
  Fin = fopen(FileName, "r");
  if(Fin == NULL)
	{
		return(-1);
	}

	//--------------------------------------------
	// Get resource names from configuration file.

  fread(Buf, sizeof(Buf), 1, Fin);
	NbRes = sizeof(MicroCtrlResTab) / sizeof(Res);
  for(i=0; i<NbRes; i++)
  {
		if((Temp = strstr(Buf, MicroCtrlResTab[i].name)) == NULL)
		{
			return(-1);
		}
		else
		{
			sscanf(Temp, "%*s%s", &(MicroCtrlResTab[i].val));
		}
  }

	strcpy(SerialName, MicroCtrlResTab[LINE_NAME].val);

	AxisNum             = atoi(MicroCtrlResTab[AXE_NUM].val);
	StepByDegree        = atoi(MicroCtrlResTab[STEP_BY_ROTATE].val);
	ReductorRatio       = atoi(MicroCtrlResTab[REDUCTOR_RATIO].val);
	ScrewStep           = atoi(MicroCtrlResTab[SCREW_STEP].val);
	DefaultSpeed        =  atof(MicroCtrlResTab[DEF_SPEED].val);
	DefaultAcceleration =  atof(MicroCtrlResTab[DEF_ACC].val);
	DefaultPosition     =  atof(MicroCtrlResTab[DEF_POS].val);
	DefaultTaper     =  atof(MicroCtrlResTab[DEF_TAPER].val);
	WantedTaper=0;
	fclose(Fin);
	aborted=false;
	return(0);
}

//**********************************************************
// @mfunc Function to stop ALL running displacement. 
//**********************************************************
int MicroCtrl::Abort()
{
	int Status;

	return(Status = SerialLine->WriteAscii("AB\r"));
	aborted=true;

}
//**********************************************************
// @mfunc Go to absolute position "Pos" in deg.
// @parm The position Value.
//**********************************************************
int MicroCtrl::AbsMove(double Pos)
{
	double CtrlSetPoint[2], CtrlFrequence[2];
	char cmd[256];

	aborted=false;
	CtrlFrequence[0]=DefaultSpeed*StepByDegree;
	CtrlFrequence[1]=DefaultSpeed*StepByDegree;

	CtrlSetPoint[0]  = - ((-DefaultPosition +(DefaultTaper+WantedTaper)/2 - Pos) * StepByDegree);
	CtrlSetPoint[1]  =  ((-DefaultPosition -(DefaultTaper+WantedTaper)/2 - Pos) * StepByDegree);

	sprintf ( cmd,"01SV%.0f,02SV%.0f,01PA%.0f,02PA%.0f\r",
				CtrlFrequence[0],CtrlFrequence[1],CtrlSetPoint[0],CtrlSetPoint[1]);
	
	return (SerialLine->WriteAscii(cmd));
}

//**********************************************************
// @mfunc return the moving state of microCtrl true or false.
//**********************************************************

bool MicroCtrl::IsMoving()
{
	char Answer[128];
	long st[2];


	SerialLine->SetTimeout(1);
	while (SerialLine->ReadAscii(Answer, 1)==0);
	SerialLine->SetTimeout(50);

	if(SerialLine->WriteAscii("TS\r")!=0)
		return true;
	else
	{
		if(SerialLine->ReadAscii(&Answer[0], 100)!=0) 
			return true;
		else		
			if (sscanf(&Answer[0],"01TS%d\r02TS%d",&st[0],&st[1])!=2)
				return true;			
			else							
				return ((st[0]&1)||(st[1]&1));		
	}

}
static void BackRef(void *p)
{
	MicroCtrl *pl;
	char cmd[128];
	double CtrlFrequence[2];

	pl=(MicroCtrl *)p;
		CtrlFrequence[0]=pl->DefaultSpeed*pl->StepByDegree;
		CtrlFrequence[1]=pl->DefaultSpeed*pl->StepByDegree;

		sprintf ( cmd,"01SV%.0f,02SV%.0f,01PA%.0f,02PA%.0f\r",
			CtrlFrequence[0],CtrlFrequence[1],0,0);
	
		if (! pl->aborted)
			pl->SerialLine->WriteAscii(cmd);

		while (pl->IsMoving()){Sleep(100);};
		
		if (! pl->aborted)
			pl->SerialLine->WriteAscii("1OR\r");
		while (pl->IsMoving()){Sleep(100);};
		if (! pl->aborted)
			pl->SerialLine->WriteAscii("2OR\r");
		while (pl->IsMoving()){Sleep(100);};
		 _endthread();
}
//**********************************************************
// @mfunc Send the axis to its reference  switch.
//**********************************************************
int MicroCtrl::Reference()
{
	aborted=false;
	switch(AxisNum)
	{
	case 1:
		return (SerialLine->WriteAscii("01OR\r"));
		break;
	case 2 :
		return (SerialLine->WriteAscii("02OR\r"));
		break;
	case 3 :
		if(_beginthread(BackRef,0,(void *)this)==-1)
			{
				return (-1);
			}
		return (0);
		break;
	default:
		return (-1);
		break;
	}

}
//**********************************************************
// @mfunc Get the axis position in degree.
// @parm The address of the position variable.
//**********************************************************
int MicroCtrl::ReadPosition(double * Pos)
{
	double  P[2];
	char Answer[128];
	if(SerialLine->WriteAscii("TP\r")!=0)
		return (-1);
	else
	{
		if(SerialLine->ReadAscii(&Answer[0], 100)!=0) 
			return (-1);
		else		
			if (sscanf(&Answer[0],"01TP%lf\r02TP%lf",&P[0],&P[1])!=2)
				return (-1);			
			else
			{
				switch (AxisNum)
				{
					case 3:
						P[0]= P[0]/StepByDegree;
						P[1]=- P[1]/StepByDegree;
						*Pos=(P[0]+P[1])/2 -DefaultPosition;
					break;
					case 2:
						*Pos=(DefaultPosition - P[1]/StepByDegree);
						break;
					case 1 :
						*Pos=-(DefaultPosition - P[0]/StepByDegree);
						break;
					default:
						return (-1);
						break;
				}
				return (0);
			}
	}

	return (0);
}
//**********************************************************
// @mfunc Get the axis position difference in degree.
// @parm The address of the Taper variable.
//**********************************************************

int MicroCtrl::ReadTaper(double *Taper)
{
	double  P[2];
	char Answer[128];
	if(SerialLine->WriteAscii("TP\r")!=0)
		return (-1);
	else
	{
		if(SerialLine->ReadAscii(&Answer[0], 100)!=0) 
			return (-1);
		else		
			if (sscanf(&Answer[0],"01TP%lf\r02TP%lf",&P[0],&P[1])!=2)
				return (-1);			
			else
			{
				switch (AxisNum)
				{
					case 3:
						P[0]=-(DefaultPosition - P[0]/StepByDegree);
						P[1]=DefaultPosition - P[1]/StepByDegree;
						*Taper=P[1]-P[0]-DefaultTaper;
					break;
					case 2:
						return (-1);
						break;
					case 1 :
						return (-1);
						break;
					default:
						return (-1);
						break;
				}
				return (0);
			}
	}

	return (0);
}
//**********************************************************
// @mfunc Move relative  in deg.
// @parm The Displacement Value in degree.
//**********************************************************
int MicroCtrl::RelMove(double mov)
{
	double CtrlSetPoint[2], CtrlFrequence[2];
	char cmd[256];

	aborted=false;
	CtrlFrequence[0]=(double)DefaultSpeed*StepByDegree;
	CtrlFrequence[1]=(double)DefaultSpeed*StepByDegree;

	CtrlSetPoint[0]  = (double) ( mov * StepByDegree);
	CtrlSetPoint[1]  = (double) -( mov * StepByDegree);

	sprintf ( cmd,"01SV%.0f,02SV%.0f,01PR%.0f,02PR%.0f\r",
				CtrlFrequence[0],CtrlFrequence[1],CtrlSetPoint[0],CtrlSetPoint[1]);
	
	return (SerialLine->WriteAscii(cmd));
}
//**********************************************************
// @mfunc Set the wanted  taper for this axis.
// @parm The Wanted Taper value in degree.
//**********************************************************
int MicroCtrl::SetTaper(double Taper)
{
	int  status;
	double SetPoint;

	if (AxisNum != 3)
		return (-1);

	if((status=ReadPosition(&SetPoint))!=0)
		return(status);

	WantedTaper = Taper;


	AbsMove(SetPoint);
	return(status);
}
//**********************************************************
// @mfunc Set the default acceleration for this axis.
// @parm The default acceleration value.
//**********************************************************
int MicroCtrl::SetDefaultAcceleration(double accel)
{
	char locstr[256];
	DefaultAcceleration = accel;

	Valid=true;
		sprintf (locstr,"RV%i,XV%i,RW%i,RX\r",
				(int)(DefaultSpeed/10*StepByDegree),
				(int) (DefaultSpeed*StepByDegree),
				(int) ((DefaultSpeed/accel)*1000));

	SerialLine->WriteAscii(locstr);	
		while(RampeCalc()){Sleep(200);};
	return(0);
}
//**********************************************************
// @mfunc Set the default position for this axis.
// @parm The default position value.
//**********************************************************

int MicroCtrl::SetDefaultPosition(double Pos)
{
	DefaultPosition = Pos;
	return(0);

}
//**********************************************************
// @mfunc Set the default speed for this axis.
// @parm The default speed value.
//**********************************************************

int MicroCtrl::SetDefaultSpeed(double Speed)
{
	DefaultSpeed = Speed;
	return(0);

}
//**********************************************************
// @mfunc Read the default speed for this axis.
// @parm an adress to store the default speed value.
//**********************************************************

int MicroCtrl::ReadSpeed(double * Speed)
{
	*Speed=DefaultSpeed;
	return 0;
}

//**********************************************************
// @mfunc Set the default taper for this axis.
// @parm The default taper value.
//**********************************************************

int MicroCtrl::SetDefaultTaper(double Taper)
{
	DefaultTaper = Taper;
	return(0);

}

//**********************************************************
// @mfunc return true if Ramping Computing is ruuning
// during this time command sends are loose.
//**********************************************************
bool MicroCtrl::RampeCalc()
{
	char Answer[128];
	long st[2];

	SerialLine->SetTimeout(1);
	while (SerialLine->ReadAscii(Answer, 1)==0);
	SerialLine->SetTimeout(50);

	if(SerialLine->WriteAscii("TS\r")!=0)
		return true;
	else
	{
		if(SerialLine->ReadAscii(&Answer[0], 100)!=0) 
			return true;
		else		
			if (sscanf(&Answer[0],"01TS%d\r02TS%d",&st[0],&st[1])!=2)
				return true;			
			else							
				return ((st[0]&4)||st[1]&4);		
	}

}


int MicroCtrl::ReadAccel(double *accel)
{
	*accel=DefaultAcceleration;
	return 0;

}
