// MM4006.h: interface for the MM4006 class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MM4006_H__C32247E4_2D22_11D7_A2BE_00AA36753667__INCLUDED_)
#define AFX_MM4006_H__C32247E4_2D22_11D7_A2BE_00AA36753667__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "Axis.h"

#include "Gpib.h"

class MM4006 :public Axis
{
public:
	int ReadRef(double *refpos);
	int AxisType;
	double direction[8];
	int ReadAcceleration(double * accel);
	int MovingTime(double depl,double *t);
	int SetTaper(double Taper);
	int SetDefaultTaper(double Taper);
	int SetDefaultPosition(double Pos);
	int SetAcceleration(double accel);
	int RelMove(double  mo);
	int ReadTaper(double *Taper);
	int ReadSpeed(double *speed);
	int NegativeLimit();
	bool IsValid();
	int Move_1Trig(double Pos, double TrigPos);
	int Abort();
	int SetSpeed(double Speed);
	int ReadPosition(double *Pos);
	int AbsMove(double Pos);
	bool ReallyMove();
	bool IsMoving();
	int Reference();
	bool refmove;
	char AxisNum[8];
	int nb_axis;
	Gpib *comupath;
	double RefPosition;
	double RefTaper;
	char comu_path_name[20];


	MM4006(char *Name);
	virtual ~MM4006();

private:
	double TrigLength;
	bool Valid;
	int Set_KFactors();
	double P_KD;
	double P_KP;
	double P_KI;

	double WantedTaper;	
	double DefaultAcceleration;
	double DefaultSpeed;
	int ReductorRatio;
	int GetConfiguration(char *Name);
};

#endif // !defined(AFX_MM4006_H__C32247E4_2D22_11D7_A2BE_00AA36753667__INCLUDED_)
