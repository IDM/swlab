// MM4006.cpp: implementation of the MM4006 class.
//
//////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include "MM4006.h"
#include <process.h>
#include <string.h>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

MM4006::MM4006(char *Name)
{

if(!GetConfiguration(Name))
{
	printf ("can't get configuartion for %s\n",Name);
}
if ((comupath= new Gpib(comu_path_name))==NULL)
{
	printf ("can't create new GPIB  for %s\n",Name);
}
Set_KFactors();
	
SetSpeed(DefaultSpeed);
SetAcceleration(DefaultAcceleration);
SetDefaultTaper(RefTaper);
while (IsMoving())
	Sleep(100);

Valid=true;
}

MM4006::~MM4006()
{
	comupath->Write("MF\r");
	delete comupath;
}

int MM4006::GetConfiguration(char *Name)
{
//**********************************************************
// Define some structures used for resources extraction.
//**********************************************************
typedef struct _Res
{
	char name[50];
	char val[50];
}Res;

enum Devices 
{
  AXE_TYPE, 
    AXE_NUM, 
    COMU_PATH, 
    DEF_SPEED,
	DIRECTION,
    DEF_ACC, 
    DEF_POS,
	DEF_TAPER,
	KI,
	KD,
	KP
};

Res CtrlResTab[] = 
{
  {"AXE_TYPE",       ""},
  {"AXE_NUM",        ""}, 
  {"COMU_PATH",      ""},
  {"DEF_SPEED",      ""},
  {"DIRECTION",		 ""},
  {"DEF_ACC",        ""},
  {"DEF_POS",        ""},
  {"DEF_TAPER",		 ""},
  {"KI_FACT",		 ""},
  {"KD_FACT",		 ""},
  {"KP_FACT",		 ""}
};
	
	FILE   *Fin;
	char   *Temp;
	char    Buf[1000];
	char    ConfStr[128];
	char    FileName[256];
	int     i, NbRes;
	int     status;
	

	status = GetEnvironmentVariable("ConfPath", ConfStr, 100);

  if(status == 0)  // Environment variable not defined.
    sprintf(FileName, "%s\\%s.conf", "c:\\ConfigurationFolder", Name);
  else
    sprintf(FileName, "%s\\%s.conf", ConfStr, Name);
	
  Fin = fopen(FileName, "r");
  if(Fin == NULL)
	{
		return(false);
	}

	//--------------------------------------------
	// Get resource names from configuration file.

  fread(Buf, sizeof(Buf), 1, Fin);
	NbRes = sizeof(CtrlResTab) / sizeof(Res);
  for(i=0; i<NbRes; i++)
  {
		if((Temp = strstr(Buf, CtrlResTab[i].name)) == NULL)
		{
			return(false);
		}
		else
		{
			sscanf(Temp, "%*s%s", &(CtrlResTab[i].val));
		}
  }

	strcpy(comu_path_name, CtrlResTab[COMU_PATH].val);
	strcpy(AxisNum, CtrlResTab[AXE_NUM].val);
	nb_axis=strlen(AxisNum);
	DefaultSpeed        =  atof(CtrlResTab[DEF_SPEED].val);
	DefaultAcceleration =  atof(CtrlResTab[DEF_ACC].val);
	RefPosition     =  atof(CtrlResTab[DEF_POS].val);
	RefTaper     =  atof(CtrlResTab[DEF_TAPER].val);
	WantedTaper=0;
	P_KD=atof(CtrlResTab[KD].val);
	P_KI=atof(CtrlResTab[KI].val);
	P_KP=atof(CtrlResTab[KP].val);
	for(i=0;i<nb_axis;i++)
	{
		if(CtrlResTab[DIRECTION].val[i]=='-')
			direction[i]=-1;
		else
			direction[i]=1;
	}
	AxisType=atoi(CtrlResTab[AXE_TYPE].val);
	fclose(Fin);
	return(true);
}

static void backRef(void *p)
{
	int i;
	char msg_axe[100],tmp[20];

	MM4006 *pt;
	pt=(MM4006 *)p;

	pt->refmove=true;
/*	pt->AbsMove(pt->RefPosition+1);
	do
		Sleep(200);
	while (pt->ReallyMove());
	Sleep(200);
	*/
	sprintf(msg_axe,"");

	for(i=0;i<pt->nb_axis;i++)
	{

		sprintf(tmp,"%cOR\r",pt->AxisNum[i]);
		if(pt->comupath->Write(tmp)!=true)
		{
			pt->refmove=false;
			_endthread();
		} 
		do
			Sleep(200);
		while (pt->ReallyMove());
		Sleep(200);

	}
		pt->SetDefaultTaper(pt->RefTaper);
		do
			Sleep(200);
		while (pt->ReallyMove());
		Sleep(200);


	pt->refmove=false;
	_endthread();

}


int MM4006::Reference()
{
	//Reference done in thread because there is some time 
	//where motor are stop during reference

	refmove=true;

	if(_beginthread(backRef,0,(void *)this)==-1)
	{
		return (false);
	}	
	return (true);
}

bool MM4006::IsMoving()
{

	if (refmove==true)
		return true;
	return (ReallyMove());

}


bool MM4006::ReallyMove()
{
	int i,start_pos;
	char status[8],resp[50],*start;
//	bool ret=false;

	if(comupath->Write("MS\r")!=true)
		return (true);
	if(comupath->Read(&resp[0])!=true)
		return (true);
	start=strstr(resp,"MS");
	start_pos=start-resp;

	for (i=0;i<nb_axis;i++)
	{
		status[i]=resp[start_pos+2+5*(AxisNum[i]-49)];
		// Axisnum -49 because AxisNum is store as character and shift by one axis1 is 0ieme
		if ( (status[i]&1)!=0)
			return(true);
	}
	status[i]=0;
	return (false);
}

int MM4006::AbsMove(double Pos)
{
	char cmd[50];
	double LocalPos[2];
	switch (nb_axis)
	{
	case 2 :
		{

			LocalPos[0]=direction[0]*(Pos-RefPosition+RefTaper/2+WantedTaper/2);
			LocalPos[1]=direction[1]*(Pos-RefPosition-RefTaper/2-WantedTaper/2);
			sprintf (cmd ,"%cPA%.3f,%cPA%.3f\r",AxisNum[0],LocalPos[0],AxisNum[1],LocalPos[1]);
			if(comupath->Write(cmd)!=true)
				return (false);
			Sleep(200);
			return (true);
			break;
		}
	case 1 :
		{
			LocalPos[0]=direction[0]*(Pos-RefPosition);
			sprintf (cmd ,"%cPA%.3f\r",AxisNum[0],LocalPos[0]);
			if(comupath->Write(cmd)!=true)
				return (false);
			Sleep(200);
			return (true);
			break;
		}
	default :
		return(false);
		break;
	}
}

int MM4006::ReadPosition(double *Pos)
{
	int i;
	char resp[100],*start;
//	bool ret=false;
	double pos[8];

	if(comupath->Write("TP\r")!=true)
		return (false);
	if(comupath->Read(&resp[0])!=true)
		return (false);
	start=&resp[0];
	i=0;
	while ((start=strstr(start,"TP"))!=NULL)
	{
		sscanf(start,"TP%lf",&(pos[i]));
		i=i+1;start ++;
	}
	*Pos=0;
	for (i=0;i<nb_axis;i++)
	{
		*Pos=*Pos+pos[AxisNum[i]-49]*direction[i];
		// Axisnum -49 because AxisNum is store as character 
		// and shift by one axis1 is 0ieme		
	}	
	*Pos/=nb_axis;
	*Pos+=RefPosition;
	
	return (true);
}

int MM4006::SetSpeed(double Speed)
{
	char msg_axe[20],msg[160];
	int i;
	sprintf(msg,"");
	for(i=0;i<nb_axis;i++)
	{
		sprintf(msg_axe,"%cVA%.3f,",AxisNum[i],Speed);
		strcat(msg,msg_axe);
	}
	msg[strlen(msg)-1]='\r';	
	if(comupath->Write(msg)!=true)
			return (false);
	return (true);
}

int MM4006::Set_KFactors()
{
	char msg_axe[100];
	int i;

	for(i=0;i<nb_axis;i++)
	{
		sprintf(msg_axe,"%cKI%.6f,%cKD%.6f,%cKP%.6f,UF\r",
			AxisNum[i],P_KI,
			AxisNum[i],P_KD,
			AxisNum[i],P_KP);
		if(comupath->Write(msg_axe))
		{
			Sleep(1000);
			
		}
		else
			return (false);
	}

	return (true);
		
}

int MM4006::Abort()
{
	char msg_axe[10],msg[100];
	int i;
	sprintf(msg,"");
	for (i=0;i<nb_axis;i++)
	{
		sprintf(msg_axe,"%cST,",AxisNum[i]);
		strcat(msg,msg_axe);
	}
	msg[strlen(msg)-1]='\r';	
	return(comupath->Write(msg));

}

int MM4006::Move_1Trig(double Pos, double TrigPos)
{
	char cmd[100];
	double LocalPos[2];
	double Trig_Pos[2];
	switch (nb_axis)
	{
	case 2 :
		{
			LocalPos[0]=direction[0]*(Pos-RefPosition-RefTaper/2-WantedTaper/2);
			LocalPos[1]=direction[1]*(Pos-RefPosition+RefTaper/2+WantedTaper/2);
			Trig_Pos[0]=direction[0]*(TrigPos-RefPosition-RefTaper/2-WantedTaper/2);
			Trig_Pos[1]=direction[1]*(TrigPos-RefPosition+RefTaper/2+WantedTaper/2);

			sprintf(cmd ,"%cPA%.3f,%cPA%.3f,%cWP%.3f,%cSB,WT%d,%cCB\r",
				AxisNum[0],LocalPos[0],
				AxisNum[1],LocalPos[1],
				AxisNum[0],Trig_Pos[0],'1',
				1,'1');
			return(comupath->Write(cmd));
			break;
		}
	case 1 :
		{
			LocalPos[0]=direction[0]*(Pos-RefPosition);
			Trig_Pos[0]=direction[0]*(TrigPos-RefPosition);
			sprintf (cmd ,"%cPA%.3f,%cWP%.3f,%cSB,%cWP%.3f,%cCB\r",
				AxisNum[0],LocalPos[0],AxisNum[0],Trig_Pos[0],'0',AxisNum[0],Trig_Pos[0]+1,'0');
			return(comupath->Write(cmd));
			break;
		}
	default :
		return(false);
		break;
	}	
}

bool MM4006::IsValid()
{
	return Valid;
}
static void backNeglimit(void *p)
{
	int i;
//	bool finish=false;
	char cmd[50];
	MM4006 *pt;
	pt=(MM4006 *)p;

	pt->refmove=true;

	for (i=0;i<pt->nb_axis;i++)
	{
		sprintf(cmd,"%cSL-260\r",pt->AxisNum[i]);
		if(pt->comupath->Write(cmd)!=true)
			{
				pt->refmove=false;
				_endthread();
			}
	}
	Sleep(200);
	pt->AbsMove(-250);
	do
		Sleep(200);
	while (pt->ReallyMove());
	Sleep(200);
	pt->refmove=false;
	_endthread();

}
int MM4006::NegativeLimit()
{
	//Reference done in thread because there is some time 
	//where motor are stop during reference


	refmove=true;

	if(_beginthread(backNeglimit,0,(void *)this)==-1)
	{
		return (false);
	}	
	return (true);
}

int MM4006::ReadSpeed(double *speed)
{
	int i;
	char resp[100],cmd[20];
//	bool ret=false;
	double sp[8];
	char n;
	for (i=0;i<nb_axis;i++)
	{
		sprintf(cmd,"%cDV\r",AxisNum[i]);
		if(comupath->Write(cmd)!=true)
			return false;
		if(comupath->Read(&resp[0])!=true)
			return (false);
		if(sscanf(&resp[0],"%cDV%lf",&n,&(sp[i]))!=2)
					if(sscanf(&resp[0],"DV%lf",&(sp[i]))!=1)

			return (false);		
	}
	*speed=sp[0];
	for (i=1;i<nb_axis;i++)
	{
		if (sp[i]!=sp[0])
		{
			*speed=0;
			return false;
		}
	}
	return (true);
}

int MM4006::ReadTaper(double *Taper)
{
	int i;
	char resp[100],*start;
//	bool ret=false;
	double pos[8];
	
	if(nb_axis==1)
	{
		*Taper=0;
		return true;
	}

	if(comupath->Write("TP\r")!=true)
		return (false);
	if(comupath->Read(&resp[0])!=true)
		return (false);
	start=&resp[0];
	i=0;
	while ((start=strstr(start,"TP"))!=NULL)
	{
		sscanf(start,"TP%lf",&(pos[i]));
		i=i+1;start ++;
	}
	*Taper=direction[1]*pos[AxisNum[1]-49]-direction[0]*pos[AxisNum[0]-49]-RefTaper;

	return (true);
}

int MM4006::RelMove(double mo)
{
	char cmd[50];
	switch (nb_axis)
	{
	case 2 :
		{
			sprintf (cmd ,"%cPR%.3f,%cPR%.3f\r",AxisNum[0],direction[0]*mo,AxisNum[1],direction[1]*mo);
			if(comupath->Write(cmd)!=true)
				return (false);
			Sleep(200);
			return (true);
			break;
		}
	case 1 :
		{
			sprintf (cmd ,"%cPR%.3f\r",AxisNum[0],direction[0]*mo);
			if(comupath->Write(cmd)!=true)
				return (false);
			Sleep(200);
			return (true);
			break;
		}
	default :
		return(false);
		break;
	}
}

int MM4006::SetAcceleration(double accel)
{
	char cmd[50];
	switch (nb_axis)
	{
	case 2 :
		{
			sprintf (cmd ,"%cAC%.3f,%cAC%.3f\r",AxisNum[0],accel,AxisNum[1],accel);
			if(comupath->Write(cmd)!=true)
				return (false);
			Sleep(200);
			return (true);
			break;
		}
	case 1 :
		{
			sprintf (cmd ,"%cAC%.3f\r",AxisNum[0],accel);
			if(comupath->Write(cmd)!=true)
				return (false);
			Sleep(200);
			return (true);
			break;
		}
	default :
		return(false);
		break;
	}	
}

int MM4006::SetDefaultPosition(double Pos)
{
	RefPosition=direction[0]*Pos;
	return true;
}

int MM4006::SetDefaultTaper(double Taper)
{
	double pos;
	double OldTaper=RefTaper;

	RefTaper=Taper;

	if (!ReadPosition(&pos))
	{
		RefTaper=OldTaper;
		return false;
	}
	if (!AbsMove(pos))
	{
		RefTaper=OldTaper;
		return false;
	}
	return true;

}

int MM4006::SetTaper(double Taper)
{
	double pos;
	double OldTaper=WantedTaper;

	WantedTaper=Taper;

	if (!ReadPosition(&pos))
	{
		WantedTaper=OldTaper;
		return false;
	}
	if (!AbsMove(pos))
	{
		WantedTaper=OldTaper;
		return false;
	}
	WantedTaper=Taper;
	return true;

}

int MM4006::MovingTime(double depl, double *t)
{
	float f[4];
	char ret[100],srch_str[10],*srch_pt;
	char cmd[25]="";
	int i;
	double v,a;

	if (!Valid)
		return (false);

if (AxisType==4006)
{
	for(i=0;i<nb_axis;i++)
	{
		sprintf (cmd,"%cPT%f\x0a",AxisNum[i],depl);
		if((comupath->Write(cmd))!=true)
			return (false);
		if(comupath->Read(&ret[0])==true)
		{
			sprintf (srch_str,"%cPT",AxisNum[i]);
			if ((srch_pt=strstr(ret,srch_str))!=NULL)
			{
				strcat(srch_str,"%f");
				if (sscanf(srch_pt,srch_str,&f[i])!=1)
					return (false);	
			}
		}
		else
			return false;
	}	
	if (f[0]>f[1])
	{
		*t=f[0];
		return (true);
	}
	*t=f[1];
	return true;
}
else
{
	if(!ReadAcceleration(&a))
	{
		*t=0;
		return false;
	}
	if(!ReadSpeed(&v))
	{
		*t=0;
		return false;
	}

	*t=(depl+v*v/a)/v;
	return true;
}
}

int MM4006::ReadAcceleration(double *accel)
{
	int i;
	char resp[100],cmd[20];
//	bool ret=false;
	double sp[8];
	char n;
	for (i=0;i<nb_axis;i++)
	{
		sprintf(cmd,"%cDA\r",AxisNum[i]);
		if(comupath->Write(cmd)!=true)
			return false;
		if(comupath->Read(&resp[0])!=true)
			return (false);
		if(sscanf(&resp[0],"%cDA%lf",&n,&(sp[i]))!=2)
			return (false);		
	}
	*accel=sp[0];
	for (i=1;i<nb_axis;i++)
	{
		if (sp[i]!=sp[0])
		{
			*accel=0;
			return false;
		}
	}
	return (true);

}

int MM4006::ReadRef(double *refpos)
{
	*refpos=RefPosition;
	return true;
}
