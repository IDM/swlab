# SWLab Stretched Wire Magnetic Measurement program
Copyright ESRF, 2010-2019

SWLab is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details. 

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>. 

The XOP Toolkit is copyright Wavemetrics, see <http://www.wavemetrics.com>

The Newport XPS libraries are copyright Newport, see <http://www.newport.com>

The Eigen library is licensed under the MPL2, see <http://eigen.tuxfamily.org>