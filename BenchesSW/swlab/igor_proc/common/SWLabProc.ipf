// This file is part of SWLab Stretched Wire Magnetic Measurement Program 
// 	
// Copyright 2017, ESRF - The European Synchroton, Gael Le Bec, Christophe Penel, Loic Lefebvre
//
// SWLab is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// Version 1.6.4
// 13/09/2017

#pragma rtGlobals=3		// Use modern global access method and strict wave access.

//-------------------------------------------------------------------------------------
//
// MPMultipoleAnalysisDialog()
// 
// Multipole Analysis Dialog called by the MultipoleProcess menu
//
// GLB, 01/12/2015
//-------------------------------------------------------------------------------------
Function MPMultipoleAnalysisDialog(dialog)
Variable dialog

// Init
Wave/Z AnalysisParameters
Wave/Z/t AnalysisParametersStr
Wave/Z XZAxisDirections
String wDataName
String wAngleName
String xPosName
String zPosName
//

If (WaveExists(AnalysisParametersStr))
	wDataName=AnalysisParametersStr[0]
	wAngleName=AnalysisParametersStr[1]
	xPosName=AnalysisParametersStr[2]
	zPosName=AnalysisParametersStr[3]
Else	
	wDataName="FieldIntegral"
	wAngleName="TrajAngle"
	xPosName="XPosition"
	zPosName="ZPosition"
EndIf

If ( (!dialog)&&((!WaveExists(AnalysisParameters))||!WaveExists(AnalysisParametersStr)) )
	Return -1
EndIf
//
// DIALOG 1 --- Wave names
If (dialog)
	Prompt wDataName, "Data Wave Name"
	Prompt wAngleName, "Angle Wave Name"
	Prompt xPosName, "X Wave Name"
	Prompt zPosName, "Z Wave Name"
	DoPrompt "Multipole Analysis",  wDataName, wAngleName, xPosName, zPosName
	If (V_Flag)
		Return -1							
	EndIf
EndIf
// Check if waves exist
If (( ! WaveExists($wDataName) ) || ( ! WaveExists($wAngleName) ) || ( ! WaveExists($xPosName) ) || ( ! WaveExists($zPosName) ))
	Print "Error: missing wave"
	Return -1
EndIf

// DIALOG 2 --- Analysis Parameters
Wave/Z/t MeasStr 
Variable magType=2
If(!WaveExists(MeasStr))
	magType=2
Else
	String nPoleStr = MeasStr[5]
	If ( StringMatch(nPoleStr, "2-poles")||(StringMatch(nPoleStr, "2-pole") ))
		MagType = 1
	ElseIf  ( StringMatch(nPoleStr, "2-4-poles")||(StringMatch(nPoleStr, "2-4-pole") ))
		MagType = 2
	ElseIf  ( StringMatch(nPoleStr, "4-poles")||(StringMatch(nPoleStr, "4-pole") ))
		MagType = 2
	ElseIf  ( StringMatch(nPoleStr, "6-poles")||(StringMatch(nPoleStr, "6-pole") ))
		MagType = 3
	ElseIf  ( StringMatch(nPoleStr, "8-poles")||(StringMatch(nPoleStr, "8-pole") ))
		MagType = 4	
	EndIf
	
EndIf

Variable r0=10
Variable maxOrder=32
Variable perpError=0e-6
Variable pitchCorr=0
Variable pitchZ0=0
Variable intTime = 0.04
Variable velocity = 5
String pitchPath="C:\ConfigurationFolder\PitchW--TO BE MEASURED IF NEEDED.ibw"
// Use MeasParameters, AnalysisParameters and Radius waves for initialization
Wave/Z MeasParameters
Wave/Z Radius
Variable x0 = 0
Variable z0 = 0
Variable thetaSW = 0
Variable xinv = 1
Variable zinv = 1

If (WaveExists(MeasParameters))
	intTime = MeasParameters[4]
	velocity = MeasParameters[5]
EndIf

If (WaveExists(AnalysisParameters))
	r0 = AnalysisParameters[0]
	magType = AnalysisParameters[1]
	maxOrder = AnalysisParameters[2]
	perpError = AnalysisParameters[3]
	pitchCorr = AnalysisParameters[4]
	pitchZ0 = AnalysisParameters[5]
	thetaSW = AnalysisParameters[6]
EndIf

If(WaveExists(Radius))
	r0=WaveMax(Radius)
EndIf

If (WaveExists(AnalysisParametersStr))
	pitchPath = AnalysisParametersStr[4]
EndIf

If (WaveExists(XZAxisDirections))
	xinv = XZAxisDirections[0]
	zinv = XZAxisDirections[1]
EndIf
//
Prompt r0, "Measurement radius [mm]"
Prompt intTime, "Integration time [s]"
Prompt velocity, "Wire velocity [mm/s]"
Prompt magType, "Magnet type",popup "Dipole;Quadrupole;Sextupole;Octupole"
Prompt maxOrder, "Number of multipole coefficients"
Prompt perpError, "XZ perpendicularity error [rad]"
Prompt pitchCorr, "XZ Pitch Correction (1: Yes / 0: No)"
Prompt pitchZ0, "Z offset for pitch correction"
Prompt pitchPath, "Path to XZ pitch correction wave"
If(dialog)
DoPrompt "Multipole Analysis",  r0, intTime, velocity, magType, maxOrder, perpError, pitchCorr, pitchZ0, pitchPath
	If (V_Flag)
	Return -1							
EndIf
EndIf

Make/o/n=7 AnalysisParameters
AnalysisParameters[0] = r0
AnalysisParameters[1] = magType
AnalysisParameters[2] = maxOrder
AnalysisParameters[3] = perpError
AnalysisParameters[4] = pitchCorr
AnalysisParameters[5] = pitchZ0
AnalysisParameters[6] = thetaSW
Make/o/t/n=5 AnalysisParametersStr
AnalysisParametersStr[0] = wDataName
AnalysisParametersStr[1] = wAngleName
AnalysisParametersStr[2] = xPosName
AnalysisParametersStr[3] = zPosName
AnalysisParametersStr[4] = pitchPath

// ANALYSIS
MPMultipoleAnalysis(wDataName, wAngleName, xPosName, zPosName, r0, intTime, velocity, magType, maxOrder, perpError, pitchCorr, pitchZ0, pitchPath, thetaSW, xinv, zinv)
// Copy results in the Analysis DataFolder
NewDataFolder/o MultipoleData
Duplicate/o $wDataName, $":MultipoleData:"+wDataName
Duplicate/o $wAngleName, $":MultipoleData:"+wAngleName
Duplicate/o $xPosName, $":MultipoleData:"+xPosName
Duplicate/o $zPosName, $":MultipoleData:"+zPosName
Duplicate/o Radius, :MultipoleData:Radius
Duplicate/o MeasParameters, :MultipoleData:MeasParameters
Duplicate/o AnalysisParameters, :MultipoleData:AnalysisParameters
Duplicate/o AnalysisParametersStr, :MultipoleData:AnalysisParametersStr
Wave/Z an, anNorm, bn, bnNorm, Current
Wave/Z/t MeasStr
Wave/Z Noise
Wave/Z QuadAlignResults
Duplicate/o an, :MultipoleData:an
Duplicate/o anNorm, :MultipoleData:anNorm
Duplicate/o bn, :MultipoleData:bn
Duplicate/o bnNorm, :MultipoleData:bnNorm 
Duplicate/o MeasStr, :MultipoleData:MeasStr
Duplicate/o Current, :MultipoleData:Current
If (WaveExists(	QuadAlignResults))
	Duplicate/o QuadAlignResults, :MultipoleData:QuadAlignResults
Endif
Wave/Z DQParameters
If (WaveExists(DQParameters))
	Duplicate/O DQParameters, :MultipoleData:DQParameters
EndIf
If(WaveExists(Noise))
	Duplicate/O Noise, :MultipoleData:Noise
EndIf

// Clear 
KillWaves/z an, anNorm, bn, bnNorm

End

//-------------------------------------------------------------------------------------
// MPMultipoleCompAnalysisDialog()
//-------------------------------------------------------------------------------------
Function MPMultipoleCompAnalysisDialog(dialog)
	Variable dialog
	
// Init
Wave/Z CompAnalysisParameters
Wave/Z CompMeasParameters
Wave/Z XZAxisDirections
Wave/Z/t AnalysisParametersStr
String wDataName
String wAngleName
String xPosName
String zPosName
//

If (WaveExists(AnalysisParametersStr))
	wDataName=AnalysisParametersStr[0]
	wAngleName=AnalysisParametersStr[1]
	xPosName=AnalysisParametersStr[2]
	zPosName=AnalysisParametersStr[3]
Else	
	wDataName="FieldIntegral"
	wAngleName="TrajAngle"
	xPosName="XPosition"
	zPosName="ZPosition"
EndIf

If ( (!dialog)&&((!WaveExists(CompAnalysisParameters))||!WaveExists(AnalysisParametersStr)) )
	Return -1
EndIf
//
// DIALOG 1 --- Wave names
If (dialog)
	Prompt wDataName, "Data Wave Name"
	Prompt wAngleName, "Angle Wave Name"
	Prompt xPosName, "X Wave Name"
	Prompt zPosName, "Z Wave Name"
	DoPrompt "Multipole Analysis",  wDataName, wAngleName, xPosName, zPosName
	If (V_Flag)
		Return -1							
	EndIf
EndIf
// Check if waves exist
If (( ! WaveExists($wDataName) ) || ( ! WaveExists($wAngleName) ) || ( ! WaveExists($xPosName) ) || ( ! WaveExists($zPosName) ))
	Print "Error: missing wave"
	Return -1
EndIf

// DIALOG 2 --- Analysis Parameters
Wave/Z/t MeasStr 
Variable magType=2
If(!WaveExists(MeasStr))
	magType=2
Else
	String nPoleStr = MeasStr[5]
	If ( StringMatch(nPoleStr, "2-poles")||(StringMatch(nPoleStr, "2-pole") ))
		MagType = 1
	ElseIf  ( StringMatch(nPoleStr, "2-4-poles")||(StringMatch(nPoleStr, "2-4-pole") ))
		MagType = 2
	ElseIf  ( StringMatch(nPoleStr, "4-poles")||(StringMatch(nPoleStr, "4-pole") ))
		MagType = 2
	ElseIf  ( StringMatch(nPoleStr, "6-poles")||(StringMatch(nPoleStr, "6-pole") ))
		MagType = 3
	ElseIf  ( StringMatch(nPoleStr, "8-poles")||(StringMatch(nPoleStr, "8-pole") ))
		MagType = 4
	EndIf
	
EndIf

Variable r0=10
Variable maxOrder=32
Variable perpError=0e-6
Variable pitchCorr=0
Variable pitchZ0=0
Variable intTime = 0.04
Variable velocity = 5
Variable thetaSW = 0
String pitchPath="C:\ConfigurationFolder\PitchW--TO BE MEASURED IF NEEDED.ibw"
// Use MeasParameters, AnalysisParameters and Radius waves for initialization
Wave/Z Radius
Variable x0 = 0, z0 = 0
Variable NBPT, NBAvg, Length, Vel
Variable xinv = 1, zinv = 1

If (WaveExists(CompMeasParameters))
	x0 = CompMeasParameters[0]
	x0 = CompMeasParameters[1]
	NBPT = CompMeasParameters[2]
	NBAvg = CompMeasParameters[3]
	Length = CompMeasParameters[4]
	Vel = CompMeasParameters[5]
EndIf
If (WaveExists(Radius))
	r0=WaveMax(Radius)
EndIf

If (WaveExists(CompAnalysisParameters))
	magType = CompAnalysisParameters[1]
	maxOrder = CompAnalysisParameters[2]
	perpError = CompAnalysisParameters[3]
	pitchCorr = CompAnalysisParameters[4]
	pitchZ0 = CompAnalysisParameters[5]
	thetaSW = CompAnalysisParameters[6]
EndIf

If (WaveExists(AnalysisParametersStr))
	pitchPath = AnalysisParametersStr[4]
EndIf

If (WaveExists(XZAxisDirections))
	xinv = XZAxisDirections[0]
	zinv = XZAxisDirections[1]
EndIf
//
Prompt r0, "Measurement radius [mm]"
Prompt Length, "Measurement Length [mm]"
Prompt magType, "Magnet type",popup "Dipole;Quadupole;Sextupole;Octupole"
Prompt maxOrder, "Number of multipole coefficients"
Prompt perpError, "XZ perpendicularity error [rad]"
Prompt pitchCorr, "XZ Pitch Correction (1: Yes / 0: No)"
Prompt pitchZ0, "Z offset for pitch correction"
Prompt pitchPath, "Path to XZ pitch correction wave"
If(dialog)
	DoPrompt "Multipole Analysis",  r0, Length, magType, maxOrder, perpError, pitchCorr, pitchZ0, pitchPath
		If (V_Flag)
		Return -1							
	EndIf
EndIf

Make/o/n=7 CompAnalysisParameters
CompAnalysisParameters[0] = r0
CompAnalysisParameters[1] = magType
CompAnalysisParameters[2] = maxOrder
CompAnalysisParameters[3] = perpError
CompAnalysisParameters[4] = pitchCorr
CompAnalysisParameters[5] = pitchZ0
CompAnalysisParameters[6] = thetaSW
Make/o/t/n=5 AnalysisParametersStr
AnalysisParametersStr[0] = wDataName
AnalysisParametersStr[1] = wAngleName
AnalysisParametersStr[2] = xPosName
AnalysisParametersStr[3] = zPosName
AnalysisParametersStr[4] = pitchPath

//--- Analysis
MPCompMultipoleAnalysis(wDataName, wAngleName, xPosName, zPosName, r0, Length, velocity, magType, maxOrder, perpError, pitchCorr, pitchZ0, pitchPath, thetaSW, xinv, zinv)

//--- Copy results in the Analysis DataFolder
NewDataFolder/o MultipoleData
Duplicate/o $wDataName, $":MultipoleData:"+wDataName
Duplicate/o $wAngleName, $":MultipoleData:"+wAngleName
Duplicate/o $xPosName, $":MultipoleData:"+xPosName
Duplicate/o $zPosName, $":MultipoleData:"+zPosName
Duplicate/o Radius, :MultipoleData:Radius
Duplicate/o CompMeasParameters, :MultipoleData:CompMeasParameters
Duplicate/o CompAnalysisParameters, :MultipoleData:CompAnalysisParameters
Duplicate/o AnalysisParametersStr, :MultipoleData:AnalysisParametersStr
Wave/Z an, bn, Current
Wave/Z/t MeasStr
Duplicate/o an, :MultipoleData:an
Duplicate/o bn, :MultipoleData:bn 
Duplicate/o MeasStr, :MultipoleData:MeasStr
Duplicate/o Current, :MultipoleData:Current
KillWaves/z an, bn

End

//-------------------------------------------------------------------------------------
// MPMultipoleAnalysis()
//-------------------------------------------------------------------------------------
Function MPMultipoleAnalysis(wDataName, wAngleName, xPosName, zPosName, r0, intTime, velocity, magType, maxOrder, perpError, pitchCorr, pitchZ0, pitchPath, thetaSW, xinv, zinv)

String wDataName, wAngleName, xPosName, zPosName, pitchPath
Variable r0, intTime, velocity, magType, maxOrder, perpError, pitchCorr, pitchZ0, thetaSW, xinv, zinv

Wave/Z XPos=$xPosName
Wave/Z ZPos=$zPosName
Wave/Z data=$wDataName

Duplicate/o XPos, XPos1
Duplicate/o ZPos, ZPos1

//---------------------------------------------------------
// Pitch Correction, if needed 
If (pitchCorr)
// Load pitch data
loadWave/o  pitchPath	
Wave/Z pitchW
	If (WaveExists(pitchW))
		// Pitch Correction
		Duplicate/o pitchw pitchW1
		Variable pitchMean = mean(pitchW, waveMin(XPos), waveMax(XPos) )
		pitchW1 = pitchW1-pitchMean
		pitchW1 *= -1 
		MPPitchError(XPos1, ZPos1, pitchW1, pitchZ0)
		Wave/Z xwPitch
		XPos1= xwPitch 
		// Clear
		KillWaves/z xwPitch, pitchW, pitchW1
	Else 
		print "Pitch wave not found"
	EndIf
EndIf
//---------------------------------------------------------
// Build matrix for Least Square Multipole Estimation
Variable x0 = Mean(XPos1)
Variable z0 = Mean(ZPos1)
// Substract position offset
XPos1 -= x0
ZPos1 -= z0
// Normalize by r0
XPos1 /= r0
ZPos1 /=r0
// Invert axis direction if specified
If(xinv<0)
	XPos1 *= -1
EndIf
If(zinv<0)
	ZPos1 *= -1
EndIf
// Build matrix
MPBuildCmplxMatFinLen(XPos1, ZPos1, $wAngleName, maxOrder, velocity*intTime/(4*pi*r0), thetaSW)
Wave/Z Z_Mat
MPBuildRealMat(Z_Mat)
Wave/Z Zr_M
// Compute Inverse Matrix
Variable tol=0.001
MPPseudoInverse(Zr_M, tol)
Wave/Z iZ_M
// Compute Coefficients
 MPCoeff(iZ_M, Data)
 Wave/Z an
 Wave/Z bn
 MPNormCoeff(an, bn, magType)
// Clear waves
KillWaves/z XPos1, ZPos1, Z_mat, Zr_M, iZ_M

End

// --------------------------------------------------------------------------------
//
// MPCompMultipoleAnalysis(wDataName, wAngleName, xPosName, zPosName, r0, Length, velocity, magType, maxOrder, perpError, pitchCorr, pitchZ0, pitchPath, thetaSW)
//
// --------------------------------------------------------------------------------
Function MPCompMultipoleAnalysis(wDataName, wAngleName, xPosName, zPosName, r0, Length, velocity, magType, maxOrder, perpError, pitchCorr, pitchZ0, pitchPath, thetaSW, xinv, zinv)
	String wDataName, wAngleName, xPosName, zPosName, pitchPath
	Variable r0, Length, velocity, magType, maxOrder, perpError, pitchCorr, pitchZ0, thetaSW, xinv, zinv
	
	Wave/Z XPos=$xPosName
	Wave/Z ZPos=$zPosName
	Wave/Z data=$wDataName

	Duplicate/o XPos, XPos1
	Duplicate/o ZPos, ZPos1
	
	//---------------------------------------------------------
	// Pitch Correction, if needed 
	If (pitchCorr)
		// Load pitch data
		loadWave/o  pitchPath	
		Wave/Z pitchW
		If (WaveExists(pitchW))
			// Pitch Correction
			Duplicate/o pitchw pitchW1
			Variable pitchMean = mean(pitchW, waveMin(XPos), waveMax(XPos) )
			pitchW1 = pitchW1-pitchMean
			pitchW1 *= -1 
			MPPitchError(XPos1, ZPos1, pitchW1, pitchZ0)
			Wave/Z xwPitch
			XPos1= xwPitch 
			// Clear
			KillWaves/z xwPitch, pitchW, pitchW1
		Else 
			print "Pitch wave not found"
		EndIf
	EndIf
	
	//---------------------------------------------------------
	// Build matrix for Least Square Multipole Estimation
	// Substract position offset
	Variable x0 = Mean(XPos1)
	Variable z0 = Mean(ZPos1)
	XPos1 -= x0
	ZPos1 -= z0
	// Normalize by r0
	XPos1 /= r0
	ZPos1 /=r0
	// Change axis directions if specified
	If (xinv<0)
		XPos1 *= -1
	EndIf
	If (zinv<0)
		ZPos1 *= -1
	EndIf
	// Build matrix
	MPBuildCompCmplxMatFinLen(XPos1, ZPos1, $wAngleName, maxOrder, length/(2*r0) , magType, thetaSW)
	Wave/Z Z_Mat
	MPBuildRealMat(Z_Mat)
	Wave/Z Zr_M
	// Compute Inverse Matrix
	Variable tol=0.001
	MPPseudoInverse(Zr_M, tol)
	Wave/Z iZ_M
	// Compute Coefficients
	MPCoeff(iZ_M, Data)
	Wave/Z an
	Wave/Z bn
	//MPNormCoeff(an, bn, magType)
	// Clear waves
	KillWaves/z XPos1, ZPos1, Z_mat, Zr_M, iZ_M
	
End


// --------------------------------------------------------------------------------
//
// MPPitchError(xw, zw, pitchw, z0)
// 
// --------------------------------------------------------------------------------
Function MPPitchError(xw, zw, pitchw, z0)
	Wave	xw, zw, pitchw
	Variable z0
	
	// Init waves
	Duplicate/O xw, xwPitch
	Duplicate/O zw, zwPitch
	
	// Grid transformation
	Variable k 
	Variable nPoints = numpnts(xw)
	For (k = 0; k<nPoints; k+=1)
		xwPitch[k] = xw[k] - (zw[k]-z0)* sin( pitchw(xw[k]) )
	EndFor
	
End

// --------------------------------------------------------------------------------
//
// MPBuildCmplxMatFinLen(xw, zw, thetaw, order, dTheta, thetaSW)
//
// --------------------------------------------------------------------------------
Function MPBuildCmplxMatFinLen(xw, zw, thetaw, order, dTheta, thetaSW)
	Wave xw, zw, thetaw
	Variable order, dTheta, thetaSW
	
	Duplicate/o thetaw thetaw1
	thetaw1 = thetaw
	// Build Matrix ------------------------------------------------------
	// --- Init 
	Variable mPoints = numpnts(xw)
	Make/C/O/n=(mPoints, order) Z_Mat
	Variable m, n
	Variable/c cm, zm
	Variable rm
	
	// --- Matrix coefficients
	For (m=0; m<mPoints; m+=1)
		For (n=0; n<order; n+=1)
			rm = cabs( cmplx(xw[m], zw[m])  )
			cm = cmplx( cos( thetaw1[m] - thetaSW), sin(thetaw1[m] - thetaSW) )
			//Z_Mat[m][n] = - rm^(n) * cmplx( cos(thetaSW), sin(thetaSW) )* cmplx(0,-1)^n * cm^(n+1)  * sinc( (n+1)*dtheta ) 	
			Z_Mat[m][n] = - rm^(n) * cmplx(0,-1)^n * cm^(n+1)  * sinc( (n+1)*dtheta ) 	
		EndFor
	EndFor	
	KillWaves/z thetaw1	

End

// --------------------------------------------------------------------------------
//
// MPBuildCompCmplxMatFinLen(xw, zw, thetaw, order, length, magType, thetaSW)
//
// --------------------------------------------------------------------------------
Function MPBuildCompCmplxMatFinLen(xw, zw, thetaw, order, length, magType, thetaSW)
	Wave xw, zw, thetaw
	Variable order, length, magType, thetaSW
	
	Duplicate/o thetaw thetaw1
	thetaw1 = thetaw - pi/2
	// Build Matrix ------------------------------------------------------
	// --- Init 
	Variable mPoints = numpnts(xw)
	Make/C/O/n=(mPoints, order) Z_Mat
	Variable m, n
	Variable/c cm, zm, zmTmp
	Variable L = length
	
	// --- Matrix coefficients
	For (m=0; m<mPoints; m+=1)
		For (n=0; n<order; n+=1)
			zm = cmplx( xw[m], zw[m] )
			cm = cmplx( cos( thetaw1[m] ), sin(thetaw1[m]) )
			If (n>0)
				zmTmp =1 / (2*L*(n+1))  * ( (zm+L*cm)^(n+1) - (zm-L*cm)^(n+1) )
			Else
				zmTmp = cm
			EndIf
			Z_Mat[m][n] = - zmTmp *cmplx( cos(thetaSW), sin(thetaSW))
		EndFor
	EndFor	
	
	KillWaves/z thetaw1	

End


// --------------------------------------------------------------------------------
// 
// MPBuildRealMat(Z_M)
//
// --------------------------------------------------------------------------------
Function MPBuildRealMat(Z_Mat)
	Wave Z_Mat	
	
	// Build Matrix ------------------------------------------------------
	// --- Init 
	Variable mPoints = DimSize(Z_Mat, 0)
	Variable order = DimSize(Z_Mat, 1)
	Make/D/O/n=(mPoints, order*2) Zr_M
	Variable m, n
	Variable ro = cabs(Z_Mat[0][1])

	// --- Matrix coefficients
	// Real part of Z_M
	For (m=0; m<mPoints; m+=1)
		For (n=0; n<order; n+=1)
			Zr_M[m][n] = Real(Z_Mat[m][n])
		EndFor
	EndFor
	// Imag part of Z_M
	For (m=0; m<mPoints; m+=1)
		For (n=0; n<order; n+=1)
			Zr_M[m][n+order] =-Imag(Z_Mat[m][n])
		EndFor
	EndFor			
	
End

// --------------------------------------------------------------------------------
//
// MPPseudoInverse(Zr_M, tol)
//
// --------------------------------------------------------------------------------
Function MPPseudoInverse(Z_M, tol)
	Wave Z_M
	Variable tol

	// Zr_M transpose
	Duplicate/O Z_M Z_Mt
	MatrixTranspose Z_Mt
	MatrixMultiply Z_Mt, Z_M
	Wave/Z M_Product
	duplicate/O M_Product ZtZ
	// SVD
	MatrixSVD ZtZ
	Wave/Z M_U, M_VT, W_W
	Variable n=DimSize(Z_M,1)
	Variable k
	Make/o/n=(n, n) W_M=0
	// Filter small singular value
	For(k=0; k< n; k+=1)
		If(W_W[k]/W_W[0]>tol)
			W_M[k][k]=1/W_W[k]
		EndIf
	EndFor
	// Pseudoinverse ZtZ
	MatrixTranspose M_U
	MatrixTranspose M_VT
	MatrixMultiply M_VT, W_M, M_U
	Duplicate/o M_product, iZtZ
	// PseudoInverse Z
 	MatrixMultiply iZtZ, Z_Mt
 	Duplicate/O M_product iZ_M
 	// Clean Folder
 	KillWaves/z Z_Mt, M_Product, ZtZ, iZtZ, M_U, M_VT, W_W, W_M
		
End

// --------------------------------------------------------------------------------
// 
// MPCoeff(iZ_M, Data )
// 
// --------------------------------------------------------------------------------
Function MPCoeff(iZ_M, Data)
	Wave iZ_M, Data
	
	// Initialize -----------------------------------------------------------
	Variable order = DimSize(iZ_M, 0)/2
	Make/O/n=(order) an, bn
	
	// Compute Coef waves -----------------------------------------
	MatrixMultiply iZ_M, Data
	Wave/Z M_Product
	Duplicate/O M_Product bnan
	Variable k
	For (k = 0; k<order; k+=1)
		bn[k] = bnan[k]
		an[k] = bnan[k+order]
	EndFor
	killwaves/z bnan, M_Product
	
End

// --------------------------------------------------------------------------------
// 
// MPNormCoeff(an, bn, magType)
// 
// --------------------------------------------------------------------------------
Function MPNormCoeff(an, bn, magType)
	Wave an, bn
	Variable magType
	
	// Initialize -----------------------------------------------------------
	Duplicate/o an, anNorm, bnNorm
	
	// Normalize coeff waves -----------------------------------------
	anNorm = 10000*an/bn[magType-1]
	bnNorm = 10000*bn/bn[magType-1]
	
End


// --------------------------------------------------------------------------------
//
// MPChangeRadius(radius, promptRadius)
//
// --------------------------------------------------------------------------------
Function MPChangeRadius(r, pRad)
	Variable r, pRad

	// Init Parameters	
	Wave/Z Radius
	Wave/Z an
	Wave/Z bn
	Wave/Z anNorm
	Wave/Z bnNorm
	Wave/Z AnalysisParameters
	If ((WaveExists(anNorm)==0)||(WaveExists(an)==0)||(WaveExists(bnNorm)==0)||(WaveExists(bn)==0)||(WaveExists(Radius)==0)||(WaveExists(AnalysisParameters)==0))
		Print "Measurement data missing"
		Print "The folder must contain the an, bn, anNorm, bnNorm, AnalysisParameters and Radius waves"
		Return -1
	EndIf

	Variable r1=r
	If (pRad)
		Prompt r1, "New radius [mm]"
		DoPrompt "Radius",  r1
		if (V_Flag)
			return -1							
		endif
	EndIf
	
	Variable magType = AnalysisParameters[1]-1
	Variable k
	Variable mOrder = DimSize(anNorm,0)
	
	String anNormName="anNorm_"+num2str(r1)+"mm"
	String bnNormName="bnNorm_"+num2str(r1)+"mm"	
	String anName="an_"+num2str(r1)+"mm"
	String bnName="bn_"+num2str(r1)+"mm"	
	Duplicate/o anNorm anTmp
	Duplicate/o bnNorm bnTmp
	 
	// Change radius
	For(k=0; k<mOrder; k+=1)
		anTmp[k]=an[k]*(r1/Radius[0])^k
		bnTmp[k]=bn[k]*(r1/Radius[0])^k
	EndFor
	
	// Normalize the multipoles
	duplicate/o bnTmp, bnTmp0
	duplicate/o anTmp, anTmp0
	bnTmp0=bnTmp/bnTmp[magType]*10000
	anTmp0=anTmp/bnTmp[magType]*10000
	
	// Duplicate and Clear
	Duplicate/o anTmp0 $anNormName
	Duplicate/o bnTmp0 $bnNormName	
	Duplicate/o anTmp $anName
	Duplicate/o bnTmp $bnName	
	killwaves/z anTmp, bnTmp, anTmp0, bnTmp0
	
End

// --------------------------------------------------------------------------------
//
// MPDipField()
//
// --------------------------------------------------------------------------------
Function MPDipField(pField)
	Variable pField

	// Init Parameters	
	Wave/Z bn
	
	If ( WaveExists(bn)==0 ) 
		Print "Measurement data missing"
		Print "The folder must contain the bn and Radius waves"
		Return -1
	EndIf
	// Gradient
	Variable b=bn[1]
	Make/o/n=1 IntegratedField
	IntegratedField[0]=b
	If(pField)
		Print "Integrated field: "+num2str(b)+" T mm"
	EndIf
End

// --------------------------------------------------------------------------------
//
// MPDipRoll()
//
// --------------------------------------------------------------------------------
Function MPDipRoll(pRoll)
	Variable pRoll
	// Init Parameters	
	Wave/Z bn
	Wave/Z an
	If ((WaveExists(bn)==0)||(WaveExists(an)==0))
		Print "Measurement data missing"
		Print "The folder must contain the bn and an waves"
		Return -1
	EndIf
	// Tilt
	Variable roll = an[0]/bn[0] 
	Make/o/n=1 RollAngle
	RollAngle[0]=1000*roll
	If(pRoll)
		Print "Roll angle: "+num2str(1000*roll)+" mrad"
	EndIf
End

// --------------------------------------------------------------------------------
//
// MPQuadGradient()
//
// --------------------------------------------------------------------------------
Function MPQuadGradient(pGrad)
	Variable pGrad

	// Init Parameters	
	Wave/Z Radius
	Wave/Z bn
	
	If ((WaveExists(bn)==0)||(WaveExists(Radius)==0))
		Print "Measurement data missing"
		Print "The folder must contain the bn and Radius waves"
		Return -1
	EndIf
	// Gradient
	Variable g=bn[1]/(wavemax(Radius))
	Make/o/n=1 IntegratedGradient
	IntegratedGradient[0]=g
	If(pGrad)
		Print "Integrated gradient: "+num2str(g)+" T"
	EndIf
End

// --------------------------------------------------------------------------------
//
// MPQuadRoll()
//
// --------------------------------------------------------------------------------
Function MPQuadRoll(pRoll)
	Variable pRoll
	// Init Parameters	
	Wave/Z bn
	Wave/Z an
	If ((WaveExists(bn)==0)||(WaveExists(an)==0))
		Print "Measurement data missing"
		Print "The folder must contain the bn and an waves"
		Return -1
	EndIf
	// Tilt
	Variable roll=1/2 * an[1]/bn[1]
	Make/o/n=1 RollAngle
	RollAngle[0]=1000*roll
	If(pRoll)
		Print "Roll angle: "+num2str(1000*roll)+" mrad"
	EndIf
End

// --------------------------------------------------------------------------------
//
// MPQuadGradientRoll()
//
// --------------------------------------------------------------------------------
Function MPQuadGradientRoll()

	Print "--------------------------------------------"
	MPQuadGradient(1)
	MPQuadRoll(1)
	Print "--------------------------------------------"
End

// --------------------------------------------------------------------------------
//
// MPSextuStrength()
//
// --------------------------------------------------------------------------------
Function MPSextuStrength(pStrength)
	Variable pStrength

	// Init Parameters	
	Wave/Z Radius
	Wave/Z bn
	If ((WaveExists(bn)==0)||(WaveExists(Radius)==0))
		Print "Measurement data missing"
		Print "The folder must contain the bn and Radius waves"
		Return -1
	EndIf
	// Gradient
	Variable g=bn[2]/wavemax(Radius)^2 
	Make/o/n=1 IntegratedStrength
	IntegratedStrength[0]=g
	If (pStrength)
		Print "Integrated strength: "+num2str(g)+" T/mm"
	EndIf
End

// --------------------------------------------------------------------------------
//
// MPSextuRoll()
//
// --------------------------------------------------------------------------------
Function MPSextuRoll(pRoll)
	Variable pRoll

	// Init Parameters	
	Wave/Z bn
	Wave/Z an
	If ((WaveExists(bn)==0)||(WaveExists(an)==0))
		Print "Measurement data missing"
		Print "The folder must contain the bn and an waves"
		Return -1
	EndIf
	// Tilt
	Variable roll=1/3 * an[2]/bn[2]
	Make/o/n=1 RollAngle
	RollAngle[0]=1000*roll
	If(pRoll)
		Print "Roll angle: "+num2str(1000*roll)+" mrad"
	EndIf
End

// --------------------------------------------------------------------------------
// MPSextuStrengthRoll()
// --------------------------------------------------------------------------------
Function MPSextuStrengthRoll()

	Print "--------------------------------------------"
	MPSextuStrength(1)
	MPSextuRoll(1)
	Print "--------------------------------------------"
End
// --------------------------------------------------------------------------------
//
// MPOctuStrength()
//
// --------------------------------------------------------------------------------
Function MPOctuStrength(pStrength)
	Variable pStrength

	// Init Parameters	
	Wave/Z Radius
	Wave/Z bn
	If ((WaveExists(bn)==0)||(WaveExists(Radius)==0))
		Print "Measurement data missing"
		Print "The folder must contain the bn and Radius waves"
		Return -1
	EndIf
	// Gradient
	Variable g=bn[3]/wavemax(Radius)^3 
	Make/o/n=1 IntegratedStrength
	IntegratedStrength[0]=g
	If (pStrength)
		Print "Integrated strength: "+num2str(g)+" T/mm^2"
	EndIf
End

// --------------------------------------------------------------------------------
//
// MPOctuRoll()
//
// --------------------------------------------------------------------------------
Function MPOctuRoll(pRoll)
	Variable pRoll

	// Init Parameters	
	Wave/Z bn
	Wave/Z an
	If ((WaveExists(bn)==0)||(WaveExists(an)==0))
		Print "Measurement data missing"
		Print "The folder must contain the bn and an waves"
		Return -1
	EndIf
	// Tilt
	Variable roll=1/4 * an[3]/bn[3]
	Make/o/n=1 RollAngle
	RollAngle[0]=1000*roll
	If(pRoll)
		Print "Roll angle: "+num2str(1000*roll)+" mrad"
	EndIf
End

// --------------------------------------------------------------------------------
// MPOctuStrengthRoll()
// --------------------------------------------------------------------------------
Function MPOctuStrengthRoll()

	Print "--------------------------------------------"
	MPOctuStrength(1)
	MPOctuRoll(1)
	Print "--------------------------------------------"
End

// --------------------------------------------------------------------------------
//
// MPDQStrength()
//
// --------------------------------------------------------------------------------
Function MPDQStrength(pStrength)
	Variable pStrength

End

// --------------------------------------------------------------------------------
//
// MPDQRoll()
//
// --------------------------------------------------------------------------------
Function MPDQRoll(pRoll)
	Variable pRoll

End

// --------------------------------------------------------------------------------
//
//  MPMultiFindCentre()
//
// --------------------------------------------------------------------------------
Function  MPMultiFindCentre(SetCentre)
	Variable SetCentre
	// Data folder
	String DFname = GetDataFolder(1)
	Variable MultipoleDF = 0
	If ( StringMatch(DFname, "root:Multipole:") )
		SetDataFolder "root:Multipole:MultipoleData"
		MultipoleDF = 1
	EndIf
	//--- Init
	Wave/Z MeasParameters
	Wave/Z an
	Wave/Z bn
	Wave/Z Radius
	Wave/Z AnalysisParameters
	If( (!WaveExists(MeasParameters)) || (!WaveExists(an)) || (!WaveExists(bn)) || (!WaveExists(AnalysisParameters)) || (!WaveExists(Radius)) )
		Print "Missing waves. The following waves are needed: "
		Print "MeasParameters, AnalysisParameters, Radius, an, bn."
		Print "These waves are normally created by the measurement and multipole analysis commands."
		Return(-1)
	EndIf
	Variable nIterations = 100
	Variable prec = 1e-12
	Variable xc = MeasParameters[0]
	Variable zc = MeasParameters[1]
	Variable magType = AnalysisParameters[1]
	Variable r0 = Radius[0]
	Variable/C gradB	
	Variable/C B
	Variable/C z = 0
	Variable k
	Make/o/n=2 MagCentreLastMeas

	Duplicate/o an, anTmp
	Duplicate/o bn, bnTmp
	If  (magType>1)
		bnTmp = bn /  bn[magType-1] 
		anTmp = an /  bn[magType-1] 
	EndIf
	If ( (mod(magType,2)) )
		//--- For 6-poles
		// Initialize Waves and Variables
		z = cmplx(gnoise(r0/10), gnoise(r0/10)) 
		//--- Gradient Descent
		For(k=0; k< nIterations; k+=1)
			gradB = MPGrad(z, bnTmp, anTmp, r0)			
			z = z - 100 * gradB
			If ( magsqr(gradB) < prec )
				Break
			EndIf
		EndFor
	ElseIf(magType==2)
		//--- For 4-poles
		// Initialize Waves and Variables
		z = cmplx(gnoise(r0/10), gnoise(r0/10))
		//--- Newton Method
		For(k=0; k< nIterations; k+=1)
			B =  MPField( z, bnTmp, anTmp, r0)
			gradB = MPGrad( z, bnTmp, anTmp, r0)
			z = z - B/gradB
			If ( magsqr(B) < prec )
				Break
			EndIf
		EndFor
	ElseIf(magType==4)
		// For 8-poles
		// Initialize Waves and Variables
		z = cmplx(gnoise(r0/10), gnoise(r0/10))
		//--- Newton Method
		For(k=0; k< nIterations; k+=1)
			B =  MPd2B_dz2( z, bnTmp, anTmp, r0)
			gradB = MPd3B_dz3( z, bnTmp, anTmp, r0)
			z = z - B/gradB
			If ( magsqr(B) < prec )
				Break
			EndIf
		EndFor
	EndIf
	//--- Add initial offset
	MagCentreLastMeas[0]=real(z)+xc
	MagCentreLastMeas[1]=imag(z)+zc
	If (SetCentre)
		Duplicate/o MagCentreLastMeas root:Multipole:MagCentreLastMeas
		Duplicate/o MagCentreLastMeas root:Multipole:MagCentre
	EndIf
	//--- Print position in Igor history
	Print "---------------------------------" 
	Print "Position of the magnet centre"
	Print "x0: "+num2str(MagCentreLastMeas[0])+" mm"
	Print "z0: "+num2str(MagCentreLastMeas[1]) +" mm"
	//--- Set Multipole Centre 
	If (MultipoleDF)
		Duplicate/o MagCentreLastMeas root:Multipole:MagCentreLastMeas
		SetDataFolder "root:Multipole"
	EndIf
	//--- Clean
	KillWaves/z anTmp, bnTmp
End

//-------------------------------------------------------------------------------
// MPGrad()
// Return dB/dz, with z = x+iy, in [T]
// --------------------------------------------------------------------------------
Function/C MPGrad(z, bn, an, r0)
	wave bn, an
	Variable/C z
	Variable r0
	
	Variable k
	Variable nPts = numPnts(bn)
	Variable/C gradB = cmplx( bn[1], an[1] )/r0
	
	
	For (k=2; k<nPts; k+=1)
		gradB = gradB + k*cmplx( bn[k], an[k]) * z^(k-1) / r0^k
	EndFor
	
	Return gradB 
	
End

//-------------------------------------------------------------------------------
// MPd2B_dz2()
// Return d2B/dz2, with z = x+iy, in [T]
// --------------------------------------------------------------------------------
Function/C MPd2B_dz2(z, bn, an, r0)
	wave bn, an
	Variable/C z
	Variable r0
	
	Variable k
	Variable nPts = numPnts(bn)
	Variable/C hessB = 2*cmplx( bn[2], an[2] )/r0^2
	
	
	For (k=3; k<nPts; k+=1)
		hessB = hessB + k*(k-1)*cmplx( bn[k], an[k]) * z^(k-2) / r0^k
	EndFor
	
	Return hessB 
	
End

//-------------------------------------------------------------------------------
// MPd3B_dz3()
// Return d3B/dz3, with z = x+iy, in [T]
// --------------------------------------------------------------------------------
Function/C MPd3B_dz3(z, bn, an, r0)
	wave bn, an
	Variable/C z
	Variable r0
	
	Variable k
	Variable nPts = numPnts(bn)
	Variable/C d3B = 6*cmplx( bn[3], an[3] )/r0^3
	
	
	For (k=4; k<nPts; k+=1)
		d3B = d3B + k*(k-1)*(k-2)*cmplx( bn[k], an[k]) * z^(k-3) / r0^k
	EndFor
	
	Return d3B 
	
End

//-------------------------------------------------------------------------------
// MPdBzdz()
// Returns dBz/dz
// Used for computing the centre of a dipole magnet
// --------------------------------------------------------------------------------
Function MPdBzdz(z, bn, an, r0)
	wave bn, an
	Variable/C z
	Variable r0
	
	Variable k
	Variable nPts = numPnts(bn)
	Variable/C dBzdz = cmplx( bn[1], an[1] )/r0*cmplx(0,1)
	
	For (k=2; k<nPts; k+=1)
		dBzdz = dBzdz + k * cmplx( bn[k], an[k] ) * cmplx(0,1)^k * z^(k-1) / r0^k
	EndFor
	
	Return real(dBzdz)
	
End

//-------------------------------------------------------------------------------
// MPField()
// Return integrated field in [T mm]
// ------------------------------------------------------------------------------
Function/C MPField(z, bn, an, r0)
	wave bn, an
	Variable/C z
	Variable r0
	
	Variable k
	Variable nPts = numPnts(bn)
	Variable/C B = 0
	
	For (k=0; k<nPts; k+=1)
		B = B + cmplx( bn[k], an[k]) * z^(k) / r0^(k) 
	EndFor
	
	Return B 
	
End

//-------------------------------------------------------------------------------
// MPRepeatability()
//-------------------------------------------------------------------------------
Function MPRepeatability()
	// Init
Wave/Z AnalysisParameters
Wave/Z/t AnalysisParametersStr
Wave/Z XZAxisDirections
String wDataName
String wAngleName
String xPosName
String zPosName
//

If (WaveExists(AnalysisParametersStr))
	wDataName=AnalysisParametersStr[0]
	wAngleName=AnalysisParametersStr[1]
	xPosName=AnalysisParametersStr[2]
	zPosName=AnalysisParametersStr[3]
Else	
	wDataName="FieldIntegral"
	wAngleName="TrajAngle"
	xPosName="XPosition"
	zPosName="ZPosition"
EndIf

If ( (!WaveExists(AnalysisParameters))|(!WaveExists(AnalysisParametersStr)) )
	Return -1 
EndIf
//
// DIALOG 1 --- Wave names
Prompt wDataName, "Data Wave Name"
Prompt wAngleName, "Angle Wave Name"
Prompt xPosName, "X Wave Name"
Prompt zPosName, "Z Wave Name"
DoPrompt "Multipole Analysis",  wDataName, wAngleName, xPosName, zPosName
If (V_Flag)
	Return -1							
EndIf
// Check if waves exist
If (( ! WaveExists($wDataName) ) || ( ! WaveExists($wAngleName) ) || ( ! WaveExists($xPosName) ) || ( ! WaveExists($zPosName) ))
	Print "Error: missing wave"
	Return -1
EndIf

// DIALOG 2 --- Analysis Parameters
Wave/Z/t MeasStr 
Variable magType=2
If(!WaveExists(MeasStr))
	magType=2
Else
	String nPoleStr = MeasStr[5]
	If ( StringMatch(nPoleStr, "2-poles")||(StringMatch(nPoleStr, "2-pole") ))
		MagType = 1
	ElseIf  ( StringMatch(nPoleStr, "4-poles")||(StringMatch(nPoleStr, "4-pole") ))
		MagType = 2
	ElseIf  ( StringMatch(nPoleStr, "6-poles")||(StringMatch(nPoleStr, "6-pole") ))
		MagType = 3
	ElseIf  ( StringMatch(nPoleStr, "8-poles")||(StringMatch(nPoleStr, "8-pole") ))
		MagType = 4
	EndIf
	
EndIf

Variable r0=10
Variable maxOrder=32
Variable perpError=0e-6
Variable pitchCorr=0
Variable pitchZ0=0
Variable intTime = 0.04
Variable velocity = 5
String pitchPath="C:\ConfigurationFolder\PitchW--TO BE MEASURED IF NEEDED.ibw"
// Use MeasParameters, AnalysisParameters and Radius waves for initialization
Wave/Z MeasParameters
Wave/Z Radius
Variable x0 = 0
Variable z0 = 0
Variable nPt = 128
Variable nRadius = 2
Variable nRadiusGrouped = 1
Variable nPole = magType
Variable nTurns
Variable thetaSw = 0
Variable xinv = 1
Variable zinv = 1

If (WaveExists(MeasParameters))
	nPt = MeasParameters[2]
	nturns = MeasParameters[3]
	intTime = MeasParameters[4]
	velocity = MeasParameters[5]
EndIf

If (WaveExists(AnalysisParameters))
	r0 = AnalysisParameters[0]
	magType = AnalysisParameters[1]
	maxOrder = AnalysisParameters[2]
	perpError = AnalysisParameters[3]
	pitchCorr = AnalysisParameters[4]
	pitchZ0 = AnalysisParameters[5]
	thetaSW = AnalysisParameters[6]
EndIf

If(WaveExists(Radius))
	r0=WaveMax(Radius)
	nRadius = numPnts(Radius)
EndIf

If (WaveExists(AnalysisParametersStr))
	pitchPath = AnalysisParametersStr[4]
EndIf

If(WaveExists(XZAxisDirections))
	xinv = XZAxisDirections[0]
	zinv = XZAxisDirections[1]
EndIf
//
Prompt r0, "Measurement radius [mm]"
Prompt intTime, "Integration time [s]"
Prompt velocity, "Wire velocity [mm/s]"
Prompt magType, "Magnet type",popup "Dipole;Quadrupole;Sextupole;Octupole"
Prompt maxOrder, "Number of multipole coefficients"
Prompt perpError, "XZ perpendicularity error [rad]"
Prompt pitchCorr, "XZ Pitch Correction (1: Yes / 0: No)"
Prompt pitchZ0, "Z offset for pitch correction"
Prompt pitchPath, "Path to XZ pitch correction wave"
DoPrompt "Multipole Analysis",  r0, intTime, velocity, magType, maxOrder, perpError, pitchCorr, pitchZ0, pitchPath
	If (V_Flag)
	Return -1							
EndIf

Prompt nPt, "Points per turn"
Prompt nRadius, "Number of measurements"
Prompt nRadiusGrouped, "Number of measurements to average"
Prompt nPole, "n for An and Bn computations"
DoPrompt "Repeatability Analysis Parameters",  nRadiusGrouped, nPole, nPt, nRadius
	If (V_Flag)
	Return -1							
EndIf

//---------------------------------------------------------------------
//  Repeatability Analysis
// Init
Wave/Z XPos=$xPosName
Wave/Z ZPos=$zPosName
Wave/Z Data=$wDataName
Wave/Z tAngle = $wAngleName
Variable nTurnsAnalysed = nRadius - mod(nRadius, nRadiusGrouped)
Variable nRadiusGroups = nTurnsAnalysed / nRadiusGrouped
Make/o/n=(nRadiusGroups) RepeatAn, RepeatBn

// For each group of turns
Variable k, l
For (k=0; k<nRadiusGroups; k+=1)
	// Make temp waves
	Make/o/n=(nPt*nRadiusGrouped) XPosTmp, ZPosTmp, DataTmp, AngleTmp
	For(l=0; l<nPt*nRadiusGrouped; l+=1)
		XPosTmp[l] = Xpos[l+k*nPt*nRadiusGrouped] 
		ZPosTmp[l] = Zpos[l+k*nPt*nRadiusGrouped] 
		DataTmp[l] = Data[l+k*nPt*nRadiusGrouped] 
		AngleTmp[l] = tAngle[l+k*nPt*nRadiusGrouped] 
	EndFor
	// Analysis
	MPMultipoleAnalysis("DataTmp", "AngleTmp", "XPosTmp", "ZPosTmp", r0, intTime, velocity, magType, maxOrder, perpError, pitchCorr, pitchZ0, pitchPath, thetaSW, xinv, zinv)
	// Save Results
	Wave/Z an, bn
	RepeatAn[k]=an[nPole-1]
	RepeatBn[k] = bn[nPole-1]
EndFor
// Print Stats
Variable stdAn = sqrt(Variance(RepeatAn))
Variable stdBn = sqrt(Variance(RepeatBn))
Variable avgAn = mean(RepeatAn)
Variable avgBn = mean(RepeatBn)
Print "------------------------------------------------------"
Print "MEASUREMENT STATISTICS"
Print num2str(nPt)+" points per turn"
Print num2str(nturns)+ " turns per measurement"
Print num2str(nRadiusGrouped)+" averaged measurements"
Print "n = "+num2str(nPole)
Print "------------------------------------------------------"
Print "Averate An: "+num2str(avgAn)+" G cm"
Print "Averate Bn: "+num2str(avgBn)+" G cm"
Print "Standard Deviation An: "+num2str(stdAn)+" G cm"
Print "Standard Deviation Bn: "+num2str(stdBn)+" G cm"
Print "------------------------------------------------------"
// Copy results in the Analysis DataFolder
If (!DataFolderExists("RepeatabilityAnalysis"))
	NewDataFolder RepeatabilityAnalysis
EndIf
Duplicate/o $wDataName, $":RepeatabilityAnalysis:"+wDataName
Duplicate/o $wAngleName, $":RepeatabilityAnalysis:"+wAngleName
Duplicate/o $xPosName, $":RepeatabilityAnalysis:"+xPosName
Duplicate/o $zPosName, $":RepeatabilityAnalysis:"+zPosName
Duplicate/o Radius, :RepeatabilityAnalysis:Radius
Duplicate/o MeasParameters, :RepeatabilityAnalysis:MeasParameters
Duplicate/o RepeatAn, $":RepeatabilityAnalysis:RepeatA"+num2str(nPole)
Duplicate/o RepeatBn, $":RepeatabilityAnalysis:RepeatB"+num2str(nPole)

// Clear DataFolder
KillWaves/z XPosTmp, ZPosTmp, DataTmp, AngleTmp, an, bn, anNorm, bnNorm, RepeatAn, RepeatBn

End

// -------------------------------------------------------------------------------
// MPQuadCentre()
// 
// -------------------------------------------------------------------------------
Function MPQuadCentre()
	
	//wave/z cn, sn
	Variable x0, z0, tilt
	wave/Z radius
	wave/Z magCentre 
	Make/o/n=2 MagCentreLastMeas
	wave/Z an 
	wave/Z bn
	Variable r = wavemax(radius)
	
	x0 = -r*Real( cmplx( bn[0], an[0] )/cmplx( bn[1], an[1] )  )+MagCentre[0]
	z0 = -r*Imag( cmplx( bn[0], an[0] )/cmplx( bn[1], an[1] )  )+MagCentre[1]
	Print "Quadrupole centre"
	Print "X = ", x0, " mm"
	Print "Z = ", z0	, " mm"	

	MagCentreLastMeas[0] = x0
	MagCentreLastMeas[1] = z0

End

//---------------------------------------------------------------------
// MPExportData()
//---------------------------------------------------------------------
Function MPExportData()
	
	Wave/Z/T MeasStr
	// Directory
	String pathStr = MeasStr[6]
	String dirStr = MeasStr[4]
	String CompStr = MeasStr[0]
	pathStr = pathstr+dirStr[0,StrSearch(dirStr,"-",0)-1] +":"+CompStr[0,StrSearch(CompStr,"-",0)-1]+"-"+dirStr
	// Location of the Measurement folder
	NewPath/c/o/q path0, pathStr
	pathinfo/show path0 
	// If the present data folder is root:Multipole, the data in root:Multipole:MultipoleData are exported
	// If not, the data in the present data folder are exported
	String DFName = GetDataFolder(1)
	If (StringMatch(DFName,"root:Multipole:"))
		SetDataFolder root:Multipole:MultipoleData
	EndIf
	Wave/Z ExportParameters = root:Multipole:ExportParameters
	Wave/Z Current
	Variable curr = 0
	If(WaveExists(current))
		curr = Current[0]
	EndIf
	variable FileNameVersion=1
	String FileName = MeasStr[5]+"_"+MeasStr[4]+"_"+CompStr[0,StrSearch(CompStr,"-",0)-1]+"_"+num2str(floor(Curr))+"A_"+num2str(FileNameVersion)+".dat"
	Prompt FileName, "File name"
	DoPrompt "Save",  FileName
	If (V_Flag)
		Return -1							
	EndIf
	if (!WaveExists(root:Multipole:FileNamesTable))
	  	make/o/t  root:multipole:FileNamesTable
	  	wave/T FileNamesTable= root:multipole:FileNamesTable
	  	FileNamesTable[0]="Init"
	endif
	wave/t/z FileNamesTable = root:multipole:FileNamesTable
	
	NVAR/Z NexportedFiles=root:Multipole:NExportedFiles
	if (!nvar_Exists(NexportedFiles))
		variable/G root:Multipole:NExportedFiles =0
		NVAR NexportedFiles=root:Multipole:NExportedFiles
	endif
  	
	variable i=0
	Do
		variable j=0
		for (i=0;i<numpnts(FileNamesTable);i+=1)
			if(cmpstr(Filename,FileNamesTable[i])==0)
				j=1
			endif
		endfor
		if(j==1)
			Filename=RemoveEnding(Filename,num2str(FilenameVersion)+".dat")+num2str(FileNameVersion+1)+".dat"
		endif
		FilenameVersion+=1
	while(j==1)
	FileNamesTable[NExportedFiles]=Filename 
	NExportedFiles+=1
	
	//--------------------------------------------
	// Version
	//--------------------------------------------
	Make/O/T/n=3 values, titles
	titles[0] = "Igor Extensions: 1.4"
	titles[1] = "Igor Procedures: 1.6.1"
	titles[2] = "Last update: 05/05/2017"
	Save/A/O/J/P=path0 titles as FileName
	//--------------------------------------------
	// Header
	//--------------------------------------------
	Wave/Z Radius
	Make/o/T/n=10 values, titles
	titles[0] = "Company"
	titles[1] = "Magnet type"
	titles[2] = "Serial number"
	titles[3] = "" 
	titles[4] = "Date"
	titles[5] = "Time"
	titles[6] = ""
	titles[7] = "Current [A]"
	titles[8] = ""
	titles[9] = "Radius [mm]"
	values[0] = MeasStr[0]
	values[1] = MeasStr[5]
	values[2] = MeasStr[4]
	values[3] = ""
	values[4] = MeasStr[2]
	values[5] = MeasStr[3]
	values[6] = ""
	values[7] = num2str(curr)
	values[8] = ""
	values[9] = num2str(WaveMax(Radius))
	// Save Header	
	Save/A/O/J/P=path0 titles, values as FileName
	//--------------------------------------------
	// Strength and roll
	//--------------------------------------------
	// Quadrupoles
	If (StringMatch(MeasStr[5],"4-poles")||(StringMatch(MeasStr[5],"4-pole")))
		Make/O/T/n=3 values, titles
		titles[0] = "Integrated Gradient [T]"
		titles[1] = "Magnetic Length [mm]"
		titles[2] = "Roll Angle [mrad]" 
		values = " "
		Wave/Z IntegratedGradient
		Wave/Z RollAngle
		Wave/Z QuadAlignResults = root:Multipole:QuadAlignResults
		If(WaveExists(IntegratedGradient))
			values[0] = num2str(IntegratedGradient)
		EndIf
		If(WaveExists(QuadAlignResults))
			values[1] = num2Str(QuadAlignResults[5])
		EndIf
		If(WaveExists(RollAngle))
			values[2] = num2str(RollAngle[0])
		EndIf
		Save/A/J/P=path0 titles, values as FileName
	EndIf
	// Octupoles
	If (StringMatch(MeasStr[5],"8-poles")||(StringMatch(MeasStr[5],"8-pole")))
		Make/O/T/n=1 values, titles
		titles[0] = "Roll Angle [mrad]"
		values = " "
		Wave/Z IntegratedGradient
		Wave/Z RollAngle
		If(WaveExists(RollAngle))
			values[0] = num2str(RollAngle[0])
		EndIf
		Save/A/J/P=path0 titles, values as FileName
	EndIf

	// Sextupoles
	If (StringMatch(MeasStr[5],"6-poles")||(StringMatch(MeasStr[5],"6-pole")))
		Make/O/T/n=3 values, titles
		titles[0] = "Integrated Strength [T/m]"
		titles[1] = " "
		titles[2] = "Roll Angle [mrad]" 
		values = " "
		Wave/Z IntegratedStrength
		Wave/Z RollAngle
		If(WaveExists(IntegratedStrength))
			values[0] = num2str(IntegratedStrength)
		EndIf
		If(WaveExists(RollAngle))
			values[2] = num2str(RollAngle[0])
		EndIf	
		Save/A/J/P=path0 titles, values as FileName
	EndIf
	// Dipole-Quadrupoles
	If (StringMatch(MeasStr[5],"2-4-poles")||(StringMatch(MeasStr[5],"2-4-pole")))
		Make/O/T/n=8 values, titles
		titles[0] = "Integrated Field [T m]"
		titles[1] = "Curved Integrated Field [T m]"
		titles[2] = " "
		titles[3] = "Integrated Gradient [T]" 
		titles[4] = "Gradient Magnetic Length (straight) [mm]"
		titles[5] = "Curved Integrated Gradient [T]" 
		titles[6] = " " 
		titles[7] = "Roll Angle [mrad]"
		values = " "
		Wave/Z DQFieldGradRoll
		Wave/Z an=root:Multipole:MultipoleData:an
		Wave/Z bn=root:Multipole:MultipoleData:bn
		Wave/Z QuadAlignResults = root:Multipole:QuadAlignResults
		If(WaveExists(bn))
			values[0] = num2str(bn[0]) // Integrated field
			values[3] = num2str(bn[1] / WaveMax(Radius)) // Integrated gradient
		EndIf
		If(WaveExists(DQFieldGradRoll))
			values[1] = num2str(DQFieldGradRoll[0]) // Curved integrated field
			values[5] = num2str(DQFieldGradRoll[1]) // Curved integrated gradient
			values[7] = num2str(DQFieldGradRoll[3]) // Roll angle
		EndIf
		If(WaveExists(QuadAlignResults))
			values[4] = num2Str(QuadAlignResults[5])
		EndIf
		Save/A/J/P=path0 titles, values as FileName
	EndIf
	//--------------------------------------------
	// Magnetic Centre
	//--------------------------------------------
	Wave/Z MagCentre = root:Multipole:MagCentre
	Wave/Z BenchCentre = root:Multipole:BenchCentre
	Wave/Z MeasParameters = root:Multipole:MeasParameters
	Variable f=MeasParameters[8] // [Hz]
	Variable g = 9.81 // [m/s^2]
	Variable sag=g/(32*f^2)*1000 // [mm]
	Make/O/T/n=4 values, titles
	titles[0] = "Magnetic Centre"
	titles[1] = "X0 [mm]"
	titles[2] = "Z0 [mm]"
	titles[3] = "S0 [mm]"
	values = " "
	If (WaveExists(MagCentre))
		values[1] = num2str(MagCentre[0]-BenchCentre[0])
		values[2] = num2str(MagCentre[1]-BenchCentre[1]-sag)
		// Save/A/J/P=path0 titles, values as FileName - 16/03/2016 LL - 
	endif
	If(WaveExists(QuadAlignResults))
		values[3] = num2Str(QuadAlignResults[4])
	Else 
		values[3] = " "
	endif
	
	Save/A/J/P=path0 titles, values as FileName
	
	//--------------------------------------------
	// Magnetic Axis
	//--------------------------------------------
	Wave/Z MagAxis = root:Multipole:MagAxis
	Wave/Z BenchAxis = root:Multipole:BenchAxis
	Make/O/T/n=3 values, titles
	titles[0] = "Magnetic Axis"
	titles[1] = "DX0 [mm]"
	titles[2] = "DZ0 [mm]"
	values = " "
	If (WaveExists(MagAxis))
		values[1] = num2str(MagAxis[0]-BenchAxis[0])
		values[2] = num2str(MagAxis[1]-BenchAxis[1])
		// Save/A/J/P=path0 titles, values as FileName - 16/03/2016 LL - 
		// MagAxis[]-BenchAxis[], before it was just MagAxis[] - 04/04/2016 LL - 
	endif
	Save/A/J/P=path0 titles, values as FileName
	
	// ------------------------------------------
	// Test results
	// ------------------------------------------
	Wave/Z TestResult = root:Multipole:TestResult
	If(!WaveExists(TestResult))
		Make/o/n=4 TestResult = 0
	EndIf
	// Gradient test (for DQ)
	If (StringMatch(MeasStr[5],"2-4-poles")||(StringMatch(MeasStr[5],"2-4-pole")))
		String GradResult = "YES"	
		If (TestResult[3] ==0 )
			GradResult = "NO"
		EndIf
	EndIf
	// Centering test
	String CentreResult = "YES"
	If ( TestResult[0] == 0 )
		CentreResult = "NO"
	EndIf
	// Roll test
	String RollResult = "YES"
	If ( TestResult[1] == 0 )
		RollResult = "NO"	
	EndIf
	// Higher Order Multipoles
	String MultipoleResult = "YES"
	If ( TestResult[2] == 0)
		MultipoleResult = "NO"
	EndIf
	// Save
	Make/O/T/n=1 values, titles
	If (StringMatch(MeasStr[5],"2-4-poles")||(StringMatch(MeasStr[5],"2-4-pole")))
		titles[0] =  "Gradient within specifications:"
		values[0] = GradResult
	EndIf
	If ( ExportParameters[0] == 1 )
		titles[0] =  "Magnetic centre within specifications:"
		values[0] = CentreResult
		Save/A/J/P=path0 titles, values as FileName
	EndIf
	If ( ExportParameters[1] == 1 )
		titles[0] =  "Roll angle within specifications:"
		values[0] = RollResult
		Save/A/J/P=path0 titles, values as FileName
	EndIf	
	If ( ExportParameters[2] == 1 )
		titles[0] =  "Multipoles within specifications:"
		values[0] = MultipoleResult
		Save/A/J/P=path0 titles, values as FileName
	EndIf			
	// ------------------------------------------
	// Shims
	// ------------------------------------------
	// Sextupoles
	If ( ExportParameters[3]  )
		Variable MagWidth = ExportParameters[6]
		Wave/Z BenchCentre = root:Multipole:BenchCentre
		Wave/Z roll = root:Multipole:MultipoleData:RollAngle
		Variable dz1 = BenchCentre[1]-MagCentre[1] - roll[0]/1000 * MagWidth/2
		Variable dz2 = BenchCentre[1]-MagCentre[1] + roll[0]/1000 * MagWidth/2
		Make/O/T/n=2 values, titles
		titles[0] = "Vertical shim on -X side [mm]: "
		titles[1] = "Vertical shim on +X side [mm]: "
		values[0] = num2str(dz1)
		values[1] = num2str(dz2)
		Save/A/J/P=path0 titles, values as FileName
	EndIf
	
	//--------------------------------------------
	// Multipoles [T mm] and [a.u.]
	//--------------------------------------------
	Wave/Z anNorm=root:Multipole:MultipoleData:anNorm
	Wave/Z bnNorm=root:Multipole:MultipoleData:bnNorm
	Wave/Z an=root:Multipole:MultipoleData:an
	Wave/Z bn=root:Multipole:MultipoleData:bn
	Make/O/T/n=1 titles
	titles[0] = "Multipole coefficients "
	Make/O/T/n=1 anU, bnU, anNormU, bnNormU
	anU = "[T mm]"
	bnU = anU
	anNormU = "[1]"
	bnNormU = anNormU
	If (WaveExists(an)&&(WaveExists(anNorm)))
		Save/A/J/P=path0 titles as FileName
		Save/A/J/P=path0 anNormU, bnNormU, anU, bnU as FileName
		Save/A/J/W/P=path0 anNorm, bnNorm, an, bn as FileName
	EndIf
	//--------------------------------------------
	// Multipoles [T mm] and [a.u.]
	//--------------------------------------------
	String anName = "root:Multipole:MultipoleData:an_"+num2str(ExportParameters[4])+"mm"
	String bnName = "root:Multipole:MultipoleData:bn_"+num2str(ExportParameters[4])+"mm"
	String anNormName = "root:Multipole:MultipoleData:anNorm_"+num2str(ExportParameters[4])+"mm"
	String bnNormName = "root:Multipole:MultipoleData:bnNorm_"+num2str(ExportParameters[4])+"mm"	
	Wave/Z an=$anName
	Wave/Z bn=$bnName
	Wave/Z anNorm=$anNormName
	Wave/Z bnNorm=$bnNormName
	Make/O/T/n=1 titles
	titles[0] = "Multipole coefficients at"+num2str(ExportParameters[4])+" mm"
	Make/O/T/n=1 anU, bnU, anNormU, bnNormU
	anU = "[T mm]"
	bnU = anU
	anNormU = "[1]"
	bnNormU = anNormU
	If (WaveExists(an)&&(WaveExists(anNorm)))
		Save/A/J/P=path0 titles as FileName
		Save/A/J/P=path0 anNormU, bnNormU, anU, bnU as FileName
		Save/A/J/W/P=path0 anNorm, bnNorm, an, bn as FileName
	EndIf

	//-----------------------------------------
	// Raw Data
	//-----------------------------------------
	
	Wave/Z FieldIntegral=root:Multipole:FieldIntegral
	Wave/Z XPosition=root:Multipole:XPosition
	Wave/Z ZPosition=root:Multipole:ZPosition
	Wave/Z TrajAngle=root:Multipole:TrajAngle
	Make/O/T/N=1 titles
	titles[0] = "Raw data"
	Make/O/T/N=1 FieldIntegralU, PositionU, AngleU
	FieldIntegralU = "[T mm]"
	PositionU = "[mm]"
	AngleU = "[rad]"
	If((ExportParameters[5])&&WaveExists(FieldIntegral)&&WaveExists(XPosition)&&WaveExists(ZPosition)&&WaveExists(TrajAngle))
		Save/A/J/P=path0 titles as FileName
		Save/A/J/P=path0 FieldIntegralU, PositionU, PositionU, AngleU  as FileName
		Save/A/J/P=path0 FieldIntegral, XPosition, ZPosition, TrajAngle  as FileName
	EndIf
	
	// Clear
	KillWaves/z titles, AngleU, anNormU, anU, bnNormU, bnU, FieldIntegralU, PositionU
	//KillPath/z path1
	
End

// ------------------------------------------------------------------
// MPInputVoltage()
// 
// Prints the maximum input voltage
// Can be used to check the range of the voltmeter
// ------------------------------------------------------------------
Function MPInputVoltage()

Wave/Z MeasParameters
Wave/Z FieldIntegral
Wave/Z Noise
If(!WaveExists(MeasParameters))
	Print "MeasParameters wave is missing."
	Return -1
EndIf
If(!WaveExists(FieldIntegral))
	Print "FieldIntegral wave is missing."
	Return -1
EndIf

WaveStats/Q FieldIntegral

Variable maxV = MeasParameters[5]*Max( WaveMax(FieldIntegral), -WaveMin(FieldIntegral))*1e-6
Print "-------------------------------"
Print "MAXIMUM INPUT VOLTAGE: "+num2str(maxV)+" V"
Print "RMS INPUT VOLTAGE: "+ num2str(MeasParameters[5]*1e-6*(V_rms))+" V"
If(WaveExists(Noise))
	If(numpnts(Noise)>0)
		WaveStats/Q Noise
		Print "MAXIMUM NOISE VOLTAGE: "+num2str(MeasParameters[5]*1e-6*max(V_max,-V_min))+" V"
		Print "RMS NOISE VOLTAGE: " +num2str(MeasParameters[5]*1e-6*(V_rms))+" V"
	EndIf
EndIf
Print "-------------------------------"

KillVariables/A/Z
End

// ------------------------------------------------------------------
//
// MPMultipoleStraightToCurved()
//
// Convert straight multipoles to "curved" multipoles
// GLB / 20/02/2017
// ------------------------------------------------------------------
Function MPMultipoleStraightToCurved(CurvRadius, GFRRadius, magLength, bendSign)
	Variable CurvRadius, GFRRadius, magLength, bendSign
	
	// an and bn waves in the current datafolder?
	Wave/Z an
	Wave/Z bn
	If ( (!WaveExists(an)) || (!WaveExists(bn)) )
		Print "an or bn wave is missing"
		Return -1
	EndIf
	
	// Conversion matrix
	Variable npts = numpnts(an)
	make/o/n=(npts, npts) a=0
	Variable i, j
	For(	i=1; i<=npts; i+=1)
		For( j=0; j<npts; j+=1)
			If(j<=i-1 )
				a[i-1][j]  =  - factorial(i-1)/( factorial(j) * factorial(i-1-j) *(1+2*(j-i)) * 8^(i-1-j) )* (bendSign*magLength^2 / (CurvRadius*GFRRadius) )^(i-1-j) 
			EndIf
		EndFor	
	EndFor
	MatrixTranspose a 
	
	// Compute "curved multipoles"
	Make/o/c/n=(npts) cn_tmp = cmplx(bn, an)
	MatrixMultiply a, cn_tmp
	Wave/Z M_Product
	Make/o/n=(npts) anCurved = Imag(M_Product)
	Make/o/n=(npts) bnCurved = Real(M_Product)	
	
	// Clean
	KillWaves/z M_Product, cn_tmp, a
End

// --------------------------------------------------------------------------------
//
//  MPDQAnalysis()
//
// Analysis of DQ measurements
// - Computes the field and gradient integral along a parabola
// - Computes the magnet centre
// - Computes the roll angle for the dipole and quad component
// - Prints the main results in the Igor history if needed
// - Save the results in the 	DQFieldGradRoll wave
//	DQFieldGradRoll[1] = DQg
//	DQFieldGradRoll[2] = DQrollB
//	DQFieldGradRoll[3] = DQrollG
// --------------------------------------------------------------------------------
Function  MPDQAnalysis(SetCentre, pResult)
	Variable SetCentre, pResult
	// Data folder
	String DFname = GetDataFolder(1)
	Variable MultipoleDF = 0
	If ( StringMatch(DFname, "root:Multipole:") )
		SetDataFolder "root:Multipole:MultipoleData"
		MultipoleDF = 1
	EndIf
	//--- Init
	Wave/Z MeasParameters
	Wave/Z an
	Wave/Z bn
	Wave/Z Radius
	Wave/Z AnalysisParameters
	Wave/Z DQParameters
	If( (!WaveExists(MeasParameters)) || (!WaveExists(an)) || (!WaveExists(bn)) || (!WaveExists(AnalysisParameters)) || (!WaveExists(Radius)) || (!WaveExists(DQParameters)) )
		Print "Missing waves. The following waves are needed: "
		Print "MeasParameters, AnalysisParameters, Radius, an, bn."
		Print "These waves are normally created by the measurement and multipole analysis commands."
		Return(-1)
	EndIf
	Variable xc = MeasParameters[0]
	Variable zc = MeasParameters[1]
	Variable magType = AnalysisParameters[1]
	Variable r0 = WaveMax(Radius)
	Variable/C z = 0
	Variable k
	Variable bl = DQParameters[0]
	Variable gl = DQParameters[1]
	Variable magLength = DQParameters[2]
	Variable curvRadius = DQParameters[3]
	Variable theta = DQParameters[4]
	Variable bendSign = DQParameters[5]

	// Curved field and gradient
	MPMultipoleStraightToCurved(CurvRadius, r0, magLength, bendSign)
	Wave/Z anCurved, bnCurved
	Variable DQb = bnCurved[0]
	Variable DQg = bnCurved[1] / r0
	Variable DQrollB = 1000*anCurved[0] / bnCurved[0]
	Variable DQrollG = 1000*anCurved[1] / bnCurved[1] / 2
	Make/O/N=4 DQFieldGradRoll
	Make/O/N=1 RollAngle
	DQFieldGradRoll[0] = DQb
	DQFieldGradRoll[1] = DQg
	DQFieldGradRoll[2] = DQrollB
	DQFieldGradRoll[3] = DQrollG
	RollAngle[0]=DQrollG
	//--- Print result in Igor history
	If(pResult)
		Print "---------------------------------" 
		Print "DIPOLE-QUADRUPOLE "
		Print "INTEGRATED FIELD:         "+num2str(DQb)+" T mm"
		Print "INTEGRATED GRADIENT:  "+num2str(DQg)+" T"
		Print "ROLL ANGLE:                   "+num2str(DQrollG)+" mrad (QUADRUPOLE)" 
		Print " "
		Print "PARABOLIC TRAJECTORY. RADIUS OF CURVATURE: "+num2str(curvRadius)+" mm. "+"DEFLECTION ANGLE: "+num2str(theta)+" rad. "+"MAG LENGTH: " +num2str(magLength)+ " mm." 
	EndIf
	// Magnet centre
	Make/o/n=2 MagCentreLastMeas
	z = Cmplx(bnCurved[0]-bl, anCurved[0]) / Cmplx(bnCurved[1], anCurved[1])
	//z = Cmplx(bn[0]-bl, an[0]) / Cmplx(bn[1], an[1]) // Centre assuming straight magnet, for tests
	MagCentreLastMeas[0] = - r0 * real(z) + xc
	MagCentreLastMeas[1] = - r0 *Imag(z) + zc
	If (SetCentre)
		Duplicate/o MagCentreLastMeas root:Multipole:MagCentreLastMeas
		Duplicate/o MagCentreLastMeas root:Multipole:MagCentre
	EndIf
	//--- Print position in Igor history
	If(pResult)
		Print "---------------------------------" 
		Print "POSITION OF THE MAGNET CENTRE"
		Print "X0: "+num2str(MagCentreLastMeas[0])+" mm"
		Print "Z0: "+num2str(MagCentreLastMeas[1]) +" mm"
	EndIf
	//--- Set Multipole Centre 
	If (MultipoleDF)
		Duplicate/o MagCentreLastMeas root:Multipole:MagCentreLastMeas
		SetDataFolder "root:Multipole"
	EndIf

End


// --------------------------------------------------------------------------------
// MPDQAnalysisDialog()
// --------------------------------------------------------------------------------
Function MPDQAnalysisDialog(dialog)
	Variable dialog
	
	// Initialise the DQ parameter  wave 
	Wave/Z DQParameters
	If ( !WaveExists(DQParameters) )
		Make/O/N=6 DQParameters
		DQParameters[0] = 584.2  // Field      [T mm]
		DQParameters[1] = -38.437 // Gradient [T]
		DQParameters[2] = 1044 	// Magnetic length [mm]
		DQParameters[3] = 35206 // Radius of curvature [mm]
		DQParameters[4] = 0.0292 // deflection angle [rad]
		DQParameters[5] = 1 // +1 or -1 depending if the trajectory is bent towards + x or -x
	EndIf
	Variable bl = DQParameters[0]
	Variable gl = DQParameters[1]
	Variable ml = DQParameters[2]
	Variable cr = DQParameters[3]
	Variable th = DQParameters[4]
	Variable sb = DQParameters[5]
	
	// Prompt values
	If (dialog)
		Prompt bl, "Nominal Field Integral (Curved) [T mm]"
		Prompt gl, "Nominal Gradient Integral (Curved) [T]"
		Prompt ml, "Magnetic Length [mm]"
		Prompt cr, "Radius of curvature [mm]"
		Prompt th, "Deflection Angle [rad]"
		Prompt sb, "Sign of the deflection (+/-1)"
		DoPrompt "Parabolic Trajectory Parameters",  ml, cr, th, bl, gl, sb
		If (V_Flag)
			Return -1							
		EndIf
		DQParameters[0] = bl
		DQParameters[1] = gl
		DQParameters[2] = ml
		DQParameters[3] = cr
		DQParameters[4] = th
		DQParameters[5] = sb
		Print "Radius of curvature: "+num2str(ml/sin(th))+" mm"
	EndIf
	
	// Analysis
	MPDQAnalysis(1, 1)

End

// -------------------------------------
// MPDefRefField()
// Create a reference field folder
// -------------------------------------
Function MPDefRefField()

	If (!DataFolderExists("root:Multipole:MultipoleData"))
		return -1	
	EndIf
	DuplicateDataFolder/O=1 root:Multipole:MultipoleData root:Multipole:MultipoleDataRef
End

// -------------------------------------
// MPDefAmbField()
// Create an ambient field folder
// -------------------------------------
Function MPDefAmbField()

	If (!DataFolderExists("root:Multipole:MultipoleData"))
		return -1	
	EndIf
	DuplicateDataFolder/O=1 root:Multipole:MultipoleData root:Multipole:MultipoleDataAmb
End

// -------------------------------------
// Subtract reference field
// -------------------------------------
Function MPSubtractRefField()
	
	If (!DataFolderExists("root:Multipole:MultipoleDataRef"))
		print "Error: Reference measurement not found"
	EndIf
	
	If (stringmatch(GetDataFolder(1), "root:Multipole:"))
		SetDataFolder root:Multipole:MultipoleData
	EndIf
	
	NVAR/Z FieldRefSubtracted
	
	If (NVAR_Exists(FieldRefSubtracted))
		If (FieldRefSubtracted)
			return -1
		EndIf
	EndIf
	
	Wave/Z XPosition
	Wave/Z ZPosition
	Wave/Z TrajAngle
	Wave/Z FieldIntegral
	Wave/Z XPositionRef = root:Multipole:MultipoleDataRef:XPosition
	Wave/Z ZPositionRef = root:Multipole:MultipoleDataRef:ZPosition
	Wave/Z TrajAngleRef = root:Multipole:MultipoleDataRef:TrajAngle
	Wave/Z FieldIntegralRef = root:Multipole:MultipoleDataRef:FieldIntegral
	If (!WaveExists(XPosition) || !WaveExists(ZPosition) || !WaveExists(TrajAngle) || !WaveExists(FieldIntegral))
		return -1
	EndIf
		
	// Tests if the reference and the present measurements are compatible
	If (numpnts(XPosition) != numpnts(XPositionRef))
		print "Error: This measurement and the reference are not compatible"
	EndIf
		
	Variable eps = 0.002
	Variable n = numpnts(XPosition)
	Duplicate/O XPosition dPositionTestTmp
	dPositionTestTmp = sqrt((XPosition - XPositionRef)^2)
	If (sum(dPositionTestTmp) / n > eps)
		print "Error: This measurement and the reference are not compatible" 		
	EndIf
		
	dPositionTestTmp = sqrt((ZPosition - ZPositionRef)^2)
	If (sum(dPositionTestTmp) / n > eps)
		print "Error: This measurement and the reference are not compatible" 		
	EndIf
		
	dPositionTestTmp = sqrt((TrajAngle - TrajAngleRef)^2)
	If (sum(dPositionTestTmp) / n > eps)
		print "Error: This measurement and the reference are not compatible" 		
	EndIf
		
	// Subtract
	FieldIntegral -= FieldIntegralRef
	If (!NVAR_Exists(FieldRefSubtracted))
		Variable/G FieldRefSubtracted
	EndIf
	
	FieldRefSubtracted = 1
	
	// Clean
	KillWaves/Z dPositionTestTmp
	
End

// -------------------------------------
// Subtract ambient field
// -------------------------------------
Function MPSubtractAmbField()
	
	If (!DataFolderExists("root:Multipole:MultipoleDataAmb"))
		print "Error: Ambient measurement not found"
	EndIf
	
	If (stringmatch(GetDataFolder(1), "root:Multipole:"))
		SetDataFolder root:Multipole:MultipoleData
	EndIf
	
	NVAR/Z FieldAmbSubtracted
	
	If (NVAR_Exists(FieldAmbSubtracted))
		If (FieldAmbSubtracted)
			return -1
		EndIf
	EndIf
	
	Wave/Z XPosition
	Wave/Z ZPosition
	Wave/Z TrajAngle
	Wave/Z FieldIntegral
	Wave/Z XPositionAmb = root:Multipole:MultipoleDataAmb:XPosition
	Wave/Z ZPositionAmb = root:Multipole:MultipoleDataAmb:ZPosition
	Wave/Z TrajAngleAmb = root:Multipole:MultipoleDataAmb:TrajAngle
	Wave/Z FieldIntegralAmb = root:Multipole:MultipoleDataAmb:FieldIntegral
	If (!WaveExists(XPosition) || !WaveExists(ZPosition) || !WaveExists(TrajAngle) || !WaveExists(FieldIntegral))
		return -1
	EndIf
	
	// Tests if the ambient and the present measurements are compatible
	If (numpnts(XPosition) != numpnts(XPositionAmb))
		print "Error: This measurement and the ambient are not compatible"
	EndIf
	
	Variable eps = 0.002
	Variable n = numpnts(XPosition)
	Duplicate/O XPosition dPositionTestTmp
	dPositionTestTmp = sqrt((XPosition - XPositionAmb)^2)
	If (sum(dPositionTestTmp) / n > eps)
		print "Error: This measurement and the ambient are not compatible" 		
	EndIf
		
	dPositionTestTmp = sqrt((ZPosition - ZPositionAmb)^2)
	If (sum(dPositionTestTmp) / n > eps)
		print "Error: This measurement and the ambient are not compatible" 		
	EndIf
		
	dPositionTestTmp = sqrt((TrajAngle - TrajAngleAmb)^2)
	If (sum(dPositionTestTmp) / n > eps)
		print "Error: This measurement and the ambient are not compatible" 		
	EndIf
		
	// Subtract
	FieldIntegral -= FieldIntegralAmb
	If (!NVAR_Exists(FieldAmbSubtracted))
		Variable/G FieldAmbSubtracted
	EndIf
	FieldAmbSubtracted = 1
		
	// Clean
	KillWaves/Z dPositionTestTmp
		
End

// 

// -------------------------------------
// MPLinearScanStore()
// -------------------------------------
Function MPLinearScanStore(twoComponents)
	Variable twoComponents // 1 if perp component measured, else 0

	KillDataFolder/Z root:Multipole:MultipoleData
	NewDataFolder/O root:Multipole:MultipoleData
	Wave/Z FieldIntegral = root:Multipole:FieldIntegral
	Wave/Z FieldIntegralPerp = root:Multipole:FieldIntegralPerp
	Wave/Z Noise = root:Multipole:Noise
	Wave/Z XPosition = root:Multipole:XPosition
	Wave/Z ZPosition = root:Multipole:ZPosition
	Wave/Z TrajAngle = root:Multipole:TrajAngle
	Wave/Z params = root:Multipole:LinearMeasParameters
	Wave/Z MeasStr = root:Multipole:MeasStr
	Wave/Z MagCentre = root:Multipole:MagCentre
	Wave/Z MagAxis = root:Multipole:MagAxis
	Wave/Z Current = root:MultipoleCurrent
	
	if (WaveExists(FieldIntegral))
		Duplicate/O FieldIntegral root:Multipole:MultipoleData:FieldIntegral
	EndIf
	if (WaveExists(FieldIntegralPerp) && twoComponents)
		Duplicate/O FieldIntegralPerp root:Multipole:MultipoleData:FieldIntegralPerp
	endif
	if (WaveExists(Noise))
		Duplicate/O Noise root:Multipole:MultipoleData:Noise
	EndIf
	if (WaveExists(XPosition))
		Duplicate/O XPosition root:Multipole:MultipoleData:XPosition
	EndIf
	if (WaveExists(ZPosition))
		Duplicate/O ZPosition root:Multipole:MultipoleData:ZPosition
	EndIf
	if (WaveExists(TrajAngle))
		Duplicate/O TrajAngle root:Multipole:MultipoleData:TrajAngle
	EndIf
	if (WaveExists(params))
		Duplicate/O params root:Multipole:MultipoleData:LinearMeasParameters
	EndIf
	if (WaveExists(MeasStr))
		Duplicate/O MeasStr root:Multipole:MultipoleData:MeasStr
	EndIf
	if (WaveExists(MagCentre))
		Duplicate/O MagCentre root:Multipole:MultipoleData:MagCentre
	EndIf
	if (WaveExists(MagAxis))
		Duplicate/O MagAxis root:Multipole:MultipoleData:MagAxis
	EndIf
	if (WaveExists(Current))
		Duplicate/O Current root:Multipole:MultipoleData:Current
	EndIf

End

// -------------------------------
// Process and store magnet module data
Function MPStoreModule(strModuleName, strMagStoreName, extraLen, flipH, flipV, refMeas)
	String strModuleName // Magnet module name
	String strMagStoreName // Magnet store name
	Variable extraLen   // Extrapolate if > 0
								// by setting Iz to 0 at Xmin - extra_len and Xmzx + extra_len
	Variable flipH		// 1 if flipped arround z axis, else 0
	Variable flipV    // 1 if flipped arround s axis, else 0
	Variable refMeas // Subtract reference if 1
	 
	// Keep track of the present data folder
	DFREF saveDFR = GetDataFolderDFR()		
	
	// Create new data folder if not existing
	If(!DataFolderExists("root:"+strMagStoreName))
		NewDataFolder $"root:"+strMagStoreName
	Endif
	
	// Module data folder, containing the raw data
	String folderName = "root:"+strMagStoreName+":"+strModuleName
	If (!DataFolderExists(folderName))
		NewDataFolder $folderName
	EndIf
	If (!flipV)
		folderName += ":Up"
	Else
		folderName += ":Down"
	EndIf
	NewDataFolder/O/S $folderName

	//Wave/Z multipoledata = root:Multipole:MultipoleData
	If (!DataFolderExists("root:Multipole:MultipoleData"))
		print "MultipoleData not found"
		return -1
	EndIf
	DuplicateDataFolder/O=1 root:Multipole:MultipoleData $folderName+":MultipoleData"
	
	// Subtract the ambient field if not yet done
	SetDataFolder $folderName+":MultipoleData"
	//MPSubtractAmbField()
	if (refMeas)
		MPSubtractRefField()
	endif
	SetDataFolder $folderName
	
	// Iz measurements
	wave/Z FieldIntegral = :MultipoleData:FieldIntegral
	Wave/Z XPosition = :MultipoleData:XPosition
	Duplicate/O FieldIntegral Iz
	variable dx = XPosition[1] - XPosition[0]
	SetScale/P x, XPosition[0], dx, Iz
	
	// Magnet flipped tags
	Make/O/N=2 FlipTag
	FlipTag[0] = flipH
	FlipTag[1] = flipV
	
	// Extrapolate if needed...
	If (extraLen > 0)
		Variable n = numpnts(Iz), k
		Make/N=(n+2)/O IzTmp, XPositionTmp
		IzTmp[0] = 0
		IzTmp[n - 1] = 0
		XPositionTmp[0] = XPosition[0] - extraLen
		XPositionTmp[n + 1] = XPosition[n - 1] + extraLen
		For (k = 0; k < n; k++)
			IzTmp[k+1] = FieldIntegral[k]
			XPositionTmp[k+1] = XPosition[k]
		EndFor
		
		variable n_ext = round((XPosition[n - 1] - XPosition[0] + 2 * extraLen) / dx)
		interpolate2/N=(n_ext) XPositionTmp, IzTmp
		Wave/Z IzTmp_CS
		Duplicate/O IzTmp_CS Iz
	EndIf	
	
	// Kill Tmp waves
	KillWaves/Z IzTmp, IzTmp_CS, XPositionTmp
	
	// Compute Ix from Iz
	// Needs Calc_Ix()
#if Exists("calc_ix")
	
	// Needs the Simulation folder
	If (!DataFolderExists("root:Simulations"))
		Print "Warning: the root:Simulation folder must be initialized for Horizontal field computation"
		Print "Please use the InitSim() command or the Undulators/Simulations/Initialization menu"
	Else
		KillWaves/Z IX
		calc_ix(Iz)
		Wave/Z ixc_loc
		Duplicate/O ixc_loc Ix
		KillWaves/Z ixc_loc
	EndIf

#else
	Print "Warning: calc_ix() is needed for horizontal field computation. "
	Print "This function should be in the magnet.ipf file"
	Print "Please check BenchesSW/racetrack/igor_proc/undulators"

#endif

	// Back to initial folder
	SetDataFolder saveDFR
	
	
End
