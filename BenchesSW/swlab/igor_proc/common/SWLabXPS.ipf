// This file is part of SWLab Stretched Wire Magnetic Measurement Program 
// 	
// Copyright 2016, ESRF - The European Synchroton, Gael Le Bec, Christophe Penel, Loic Lefebvre
//
// SWLab is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// Version 1.5.7
// 23/02/2017

#pragma rtGlobals=1		// Use modern global access method.


//------------------------------------------------------------------------------------
//  SWMoveToCentre(correctSag)
//------------------------------------------------------------------------------------
Function SWMoveToCentre(correctSag, promptSag)
	Variable correctSag, promptSag
	
	// Initialize
	Variable xa = 0
	Variable za = 0
	Wave/z MagAxisLastMeas = root:Multipole:MagAxisLastMeas
	Wave/Z MagAxis = root:Multipole:MagAxis
	Wave/Z MeasParameters = root:Multipole:MeasParameters
	Variable f
	
	If (WaveExists(MeasParameters))
		f=MeasParameters[8] // [Hz]
	Else	
		f = 1e4
	EndIf
	Variable g = 9.81 // [m/s^2]
	Variable xc = 0
	Variable zc = 0
	
	Wave/Z MagCentreLastMeas = root:Multipole:MagCentreLastMeas
	Wave/Z MagCentre = root:Multipole:MagCentre	
	NVAR/z xpsPath = root:xps2d:path
	
	Variable sag
	
	//------------------------------------------------------------------------------------
	//  SWMoveToAxis

	If( !WaveExists(MagAxisLastMeas) )
		//Print "The MagAxisLastMeas wave is missing"
		If (!WaveExists(MagAxis))
		//	Print "The MagAxis wave is missing"
			xa=0
			za=0
		//	Print "Wire moved to (0, 0)"
		Else
	//		Print "Wire moved to default values (MagAxis[0], MagAxis[1])"		
			xa = MagAxis[0]
			za = MagAxis[1]
		EndIf
	ElseIf ( abs(MagAxisLastMeas[0]) < 45 && abs(MagAxisLastMeas[1]) < 45 )
		xa = MagAxisLastMeas[0]
		za = MagAxisLastMeas[1]
	Else 
		xa = MagAxis[0]
		za = MagAxis[1]	
	EndIf

	// Move to axis
	Axis2dSetTaper(xpsPath,cmplx(xa,za))
	Do
		Sleep/S 0.5 
	While ( Axis2dIsMoving(xpsPath) )
	//------------------------------------------------------------------------------------
	
	If( !WaveExists(MagCentreLastMeas) )
		//Print "The MagCentreLastMeas wave is missing"
		If (!WaveExists(MagCentre))
		//	Print "The MagCentre wave is missing"
		//	Print "Wire moved to (0, 0)"
		Else
		//	Print "Wire moved to default values (MagCentre[0], MagCentre[1])"		
			xc = MagCentre[0]
			zc = MagCentre[1]
		EndIf
	ElseIf (  ( abs(MagCentreLastMeas[0]) < 45 )  &&  ( abs(  MagCentreLastMeas[1] < 45 )  ) )   
		xc = MagCentreLastMeas[0]
		zc = MagCentreLastMeas[1]
	Else 
		xc = MagCentre[0]
		zc = MagCentre[1]
	EndIf

	If (!correctSag)
		//--- Sag not corrected
		Axis2dMoveAbs(xpsPath,char2num("w") ,cmplx(xc, zc) )
	Else
		//--- Sag corrected
		If (promptSag)
			Prompt f, "f0 [Hz]: "
			DoPrompt "Wire 1st mode",  f
			If (V_Flag)
				Return -1							
			EndIf
			MeasParameters[8] = f
		EndIf
		// Compute Sag [mm]
		sag = g/(32*f^2)*1000
		//Print "Corrected sag: "+num2str(sag)+" mm"
		// Move
		zc -= sag
		Axis2dMoveAbs(xpsPath,char2num("w") ,cmplx(xc, zc) )
	EndIf
	
End

// -------------------------------------------
// SWMoveToPark()
// -------------------------------------------
Function SWMoveToPark()

	Wave/Z pos = root:Multipole:ParkPosition
	NVAR/z xpsPath = root:xps2d:path
	
	If (WaveExists(pos))
		Axis2dMoveAbs(xpsPath,char2num("w") ,cmplx(pos[0], pos[1]) )
	EndIf
End