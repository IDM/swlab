// This file is part of SWLab Stretched Wire Magnetic Measurement Program 
// 	
// Copyright 2016, 2017, 2018 ESRF - The European Synchroton, Christophe Penel, Loic Lefebvre, Gael Le Bec
//
// SWLab is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// Version 1.6.4
// 11/01/2018

#pragma rtGlobals=3		// Use modern global access method and strict wave access.

//---------------------------------------------------------
// Build the TaskPanel
//---------------------------------------------------------
Window DQTaskPanel() : Panel
	// Check initialization	
	// MultipoleCheckOpen()
	If (!DataFolderExists("root:Multipole")||(numType(root:K2182:path)==2)||(numType(root:XPS2d:path)==2))
		Print "Please initialize the bench with the following command:"
		Print "SWLab Measurement > Initialization > Open"
		Return -1
	EndIf
	// --- Date and time
	Print  Date()+", "+Time()
	//--- Set Multipole XOP to quiet
	MultiQuiet(1)
	// Load Measurement Parameters
	MultipoleLoadParam()
	
	if (strlen(WinList("DQTaskPanel",";","WIN: 64" ))>0)
		DoWindow /F DQTaskPanel
		return
	endif
	PauseUpdate; Silent 1		
	// building window...
	if (!datafolderExists("root:Multipole:DQTaskPanel"))
		Newdatafolder root:Multipole:DQTaskPanel
		make/T /N=5 root:Multipole:DQTaskPanel:Tasklabel
		make /N=5 root:Multipole:DQTaskPanel:TaskFollow
		root:Multipole:DQTaskPanel:TaskFollow=0
		variable /G root:Multipole:DQTaskPanel:Mytoggle
		variable /G root:Multipole:DQTaskPanel:CurrentIter
		variable /G root:Multipole:DQTaskPanel:CurrentTask
		 root:Multipole:DQTaskPanel:Tasklabel[0]="Set Power Supply"
		 root:Multipole:DQTaskPanel:Tasklabel[1]="Axis measurement"
		 root:Multipole:DQTaskPanel:Tasklabel[2]="Circular measurement"
		 root:Multipole:DQTaskPanel:Tasklabel[3]="Check results"
		 root:Multipole:DQTaskPanel:Tasklabel[4]="Save"
		 variable /G  root:Multipole:DQTaskPanel:Toggle0
		 variable /G  root:Multipole:DQTaskPanel:Toggle1
		 variable /G  root:Multipole:DQTaskPanel:Toggle2
		 variable /G  root:Multipole:DQTaskPanel:Toggle3
		 variable /G  root:Multipole:DQTaskPanel:Toggle4
		 variable /G root:Multipole:DQTaskPanel:firstIter
		 
	endif	
	variable Ntask=numpnts(root:Multipole:DQTaskPanel:TaskLabel)
	root:Multipole:DQTaskPanel:Mytoggle=0
	
	NewPanel /K=1 /W=(400,148,750,168+25*NTask)
	//	ShowTools/A	
	string cmd
	variable ind=0
	//--- Build check boxes
	do
		cmd= "CheckBox  taskcheck"+num2str(ind)+",pos={30,15+"+num2str(25*ind)+"},size={ 40,14},disable=2,title=\""+root:Multipole:DQTaskPanel:TaskLabel[ind]+"\",variable=root:Multipole:DQTaskPanel:toggle"+num2str(ind)+",mode=1, fStyle=1"
		Execute/Q cmd
		ind+=1
	while( ind<ntask)
	Variable /G  root:Multipole:DQTaskPanel:SaveDQChoice=0
	//--- Start and Stop buttons 
	Button StartTasks,pos={257,11},size={65,31},proc=StartDQTaskButton,title="Start"
	Button StopTasks,pos={257,50},size={65,31},proc=StopDQTaskButton,title="Stop"

	DQTaskTo( root:Multipole:DQTaskPanel:CurrentTask)

	
EndMacro

//------------------------------------------------------------------------------------------
// DQCenterAxisTask(s)
//------------------------------------------------------------------------------------------
Function DQCenterAxisTask(s)
STRUCT WMBackgroundStruct &s

//--- Initialization 
NVAR StepTask=root:Multipole:DQTaskPanel:CurrentTask
Wave/Z Param=root:Multipole:CenterMeasParameters
Wave/Z mParam=root:Multipole:MeasParameters
Wave/Z QC=root:Multipole:MagCentre
Wave/Z QA=root:Multipole:MagAxis
NVAR CurrentIter=root:Multipole:DQTaskPanel:CurrentIter
NVAR SaveDQChoice =root:Multipole:DQTaskPanel:SaveDQChoice
NVAR mytoggle=root:Multipole:DQTaskPanel:Mytoggle
NVAR firstIteration=root:Multipole:DQTaskPanel:firstIter
NVAR/Z FieldIntDisplay = root:Multipole:DQTaskPanel:FieldIntDisplay
If ( !NVAR_Exists(FieldIntDisplay) )
	Variable/G root:Multipole:DQTaskPanel:FieldIntDisplay = 0
EndIf
NVAR/Z TimerVar=root:Multipole:DQTaskPanel:TimerVar
If ( !NVAR_Exists(TimerVar) )
	Variable/G root:Multipole:DQTaskPanel:TimerVar
EndIf
Variable NBITER=param[7]
wave/Z TXC= root:Multipole:IterativeAlignment:MagCentreX
wave/Z TZC= root:Multipole:IterativeAlignment:MagCentreZ
wave/Z TXA= root:Multipole:IterativeAlignment:MagAxisX
wave/Z TZA= root:Multipole:IterativeAlignment:MagAxisZ
Wave/Z current = root:Multipole:Current
Variable curr=current[0]
Variable cycle=1
NVAR/Z curr0 =  root:Multipole:DQTaskPanel:curr0
If  ( !NVAR_Exists(curr0) )
	Variable/G root:Multipole:DQTaskPanel:curr0 = current[0]
EndIf
Variable CyclingWaitTime
Wave/Z ExportParameters=root:Multipole:ExportParameters
Variable rSpec = ExportParameters[4]

//--- Change the color of the Start button at each call
if (mytoggle==0)
	mytoggle=1
	ModifyControl/Z StartTasks,fColor=(0,5000,0)	
else
	mytoggle=0
	ModifyControl/Z StartTasks,fColor=(52224,52224,52224)
endif

// --- Finite State Machine
//
// 0		INITIALIZATION
// 0 		Initialization
// 0.1	PS ready
//
// 1		AXIS MEASUREMENT
// 1 		Launch quadrupole alignment 
// 1.1	Wait for alignment result
//
// 2		CIRCULAR MEASUREMENTS
// 2 		Launch circular measurements
// 2.1	Wait  for circular measurement results
// 2.2	Multipole analysis and tests vs specifications
//
// 3		VALIDATION
// 3		Open measurement validation dialog
// 3.1 	User validation
//
// 4		SAVE
// 4		Export Measurement results
strswitch(num2str(steptask))			// numeric switch
		// -------------------------------
		// State 0
		// Initialization
		// -------------------------------
		// 0 - Init 
		case "0":
			Print time()+"     Initialize..."
			Duplicate/o root:Multipole:BenchCentre root:Multipole:MagCentre
			Duplicate/o root:Multipole:BenchAxis root:Multipole:MagAxis
			ModifyControl StartTasks,fColor=(0,5000,0)		
			firstIteration = 1		
			// Ask metadata
			MultipoleMetaData()
			// Next step
			DQTaskTo(0.1)
			return 0
		break 
		// 0.1 - Set Current
		case "0.1":
			// Prepare Power Supply
			curr=curr0
			Prompt curr, "Current [A]:" 
			Prompt cycle, "Set current (0: no, 1: yes)"
			DoPrompt "Power Supply", curr, cycle
			If (V_Flag)
				DQTaskTo(0)
				curr=current[0]
				Return -1							
			EndIf
			If (cycle == 1)
				curr0 = curr
				current[0] = curr
				DQTaskTo(0.2)
			ElseIf (cycle == 0)
				curr=current[0]
				DQTaskTo(1)
			Else
				print "Answer must be '0' or '1' "
				DQTaskTo(0.1)
			EndIf
			return 0
		break
		// 0.2 - PS cycling
		case "0.2":  
			Print time()+"     Set cycling current..."
			curr0 = curr
#if Exists("SetCyclingCurrent")			
			SetCyclingCurrent()
#endif
			current[0] = curr
			TimerVar = DateTime
			DQTaskTo(0.3)
			return 0
		break
		// 0.3 - Wait for PS cycling
		case "0.3":
			CyclingWaitTime = 15 // s
			If( DateTime > TimerVar + CyclingWaitTime)
				DQTaskTo(0.4)
			EndIf
			return 0
		break
		// 0.4 - PS set current
		case "0.4":
			Print time()+"     Set current..."
			curr = curr0
#if Exists("PSSetCurrent")				
			PSSetCurrent(curr)
#endif
			current[0] = curr
			TimerVar = DateTime
			DQTaskTo(0.5)
			return 0
		break
		// 0.5 - Wait for current stability
		case "0.5":
			CyclingWaitTime = 60 // s
			If( DateTime > TimerVar + CyclingWaitTime)
				DQTaskTo(1)
			EndIf
			return 0
		
		// -------------------------------
		// State 1 
		// Quadrupole alignment
		// Determination of the DQ axis
		// -------------------------------
		// 1 - Launch quadrupole alignment
		case "1":  
			Print time()+"     DQ axis and length measurements..."
			Make/o/n=4 root:Multipole:DQCentreAxisMeasParameters
			Wave/Z  DQCentreAxisMeasParameters = root:Multipole:DQCentreAxisMeasParameters
			DQCentreAxisMeasParameters[0] = QC[0]
			DQCentreAxisMeasParameters[1] = QC[1]
			DQCentreAxisMeasParameters[2] = QA[0]
			DQCentreAxisMeasParameters[3] = QA[1]
			
			MultiQuadAlignment( QC[0], QC[1], QA[0], QA[1], param[4], param[5], param[8], mParam[5], mParam[6], param[6])
			DQTaskto(1.1)
			return 0
		// 1.1 - Wait for quadrupole alignment result
		case "1.1" :
			If( !MultiIsMeasuring() )
				MultiGetLastMeas()
				Wave/Z QC=root:Multipole:MagCentre
				Wave/Z MagCentreLastMeas = root:Multipole:MagCentreLastMeas
				Wave/Z MagAxisLastMeas=root:Multipole:MagAxisLastMeas
				Wave/Z QuadAlignResults=root:Multipole:QuadAlignResults
				// GLB 2018-07-19 
				//MagCentreLastMeas[0] = QC[0]
				//QC[1] = MagCentreLastMeas[1]
				QC[1] = QuadAlignResults[1];
				MagCentreLastMeas = QC
				// GLB 2018-07-19 END
				MagAxisLastMeas[0]=QuadAlignResults[2]
				MagAxisLastMeas[1]=QuadAlignResults[3]		
				QA = MagAxisLastMeas
				Wave/Z/t MeasStr=root:Multipole:MeasStr
				DQTaskTo(2) 
			endif
			return 0
		break
		// -------------------------------
		// State 2
		// Circular Measurements
		// -------------------------------
		// 2 - Launch circular measurements
		case "2":  
			Print time()+"     Circular measurements..."
			MultipoleSetCentreAxis()	
			//Print  Date()+", "+Time()
			Wave/Z/t MeasStr=root:Multipole:MeasStr
			MeasStr[2] = Date()
			MeasStr[3] = Time()
			CI(0,0)
			DQTaskto(2.1)
			return 0
		// 2.1 - Wait  for circular measurement results
		case "2.1":
			If( !MultiIsMeasuring() )
				If (!cmpStr(MultiGetLastError(),"No Error\n"))
					MultiGetLastMeas()
					SetDataFolder root:Multipole
					DQTaskto(2.2)
					return 0
				else
					print "  MultiRepeatCircularScan " ,param[0],param[1],"Radius",param[2],param[3],param[4],param[5],param[6],param[7],")"
					MultipoleLastErr()
					 ModifyControl StartTasks,fColor=(52224,52224,52224)
					return 2				
				endif
			endif
			return 0

		break;
		// 2.2 - Multipole analysis and tests vs specifications
		case"2.2":
				Print time()+"     Analysis..."
				SetDatafolder root:Multipole
				// Multipole analysis
				MPMultipoleAnalysisDialog(0)
				// Compute multipoles at specified radius
				SetDataFolder root:Multipole:MultipoleData
				MPChangeRadius(rSpec,0)
				// Plot the integrated field
				If ( ! FieldIntDisplay )
					Wave/Z FieldIntegral
					Display FieldIntegral
					FieldIntDisplay = 1
				EndIf
				// DQ analysis: field, gradient, roll and centre
				MPDQAnalysis(1, 0)
				// Check results 
				MultipoleCheckDQResult()
				// Move the wire to the magnet centre 
				Print "WIRE MOVED TO THE MAGNET CENTRE"
				SWMoveToCentre(1,0)
				DQTaskTo(3)
				return 0
		break;
		// -------------------------------
		// State 3
		// Validation by the user
		// -------------------------------
		// 3 - Open measurement validation dialog
		case "3":
			SaveDQChoice=0
			Execute/Q "VALIDATEDQMEAS()"
			DQTaskTo(3.1)
			return 0		
		break
		// 3.1 - User validation
		case "3.1":
			switch (SaveDQChoice)
			case 0 :
				return 0
				break
			case 1 :
				MultipoleSetCentreAxis()	
				DQTaskTo(4)
				return 0
				break
			case 2 :
				MultipoleSetCentreAxis()	
				DQTaskTo(0.1)
				//Wave/Z DQParameters = root:Multipole:DQParameters
				//Wave/Z DQFieldGradRoll = root:Multipole:MultipoleData:DQFieldGradRoll
				//Variable gl = DQFieldGradRoll[1]
				//current[0] = curr* DQParameters[1]/gl
				return 0
				break
			case 3:
				print "MEASUREMENT CANCELLED"
				ModifyControl StartTasks,fColor=(52224,52224,52224)
				DQTaskTo(4)
				return 2
				break
			endswitch			
		// -------------------------------
		// State 4
		// Export measurement results
		// -------------------------------		
		case "4":
				SetDataFolder root:Multipole:MultipoleData
				MPExportData()	
				Print time()+"     Export"
				Variable turnOff = 1
				Prompt turnOff, "Turn off the power supply? (0: no, 1: yes)"
				DoPrompt "Power Supply", turnOff
				If (V_Flag)
					turnOff = 0	
				EndIf
				If ( turnOff )
#if Exists("PSOff")
					PSOff()
#endif
				EndIf
				ModifyControl StartTasks,fColor=(52224,52224,52224)
				Print "-----------------------------"
				Print " "
				Print "MEASUREMENT COMPLETED"
				If ( turnOff )
				Print "THE POWER SUPPLY AS BEEN TURNED OFF"
				EndIf
				Print " "
				Print "-----------------------------" 
				
				return 1
		break;	

	endswitch

return 0

end

//

//-----------------------------------------------------------------------------------------------------------------------------------
function DQTaskTo(num)
variable num
variable val

variable i
DoWindow /F DQTaskPanel
variable nbt=numpnts(root:Multipole:DQTaskPanel:TaskLabel)

if (num >nbt-1)
	DoAlert 0 , "BAD TASK NUMBER "
	 CtrlNamedBackground _all_, kill
	return 0
endif
NVAR CurrentTask=root:Multipole:DQTaskPanel:CurrentTask
string cmd
CurrentTask=Num
for (i=0;i<nbt;i+=1)
	cmd="root:Multipole:DQTaskPanel:toggle"+num2str(i)+"=0"
	execute cmd	
endfor
if (num>=0 )
	cmd="root:Multipole:DQTaskPanel:toggle"+num2str(floor(num))+"=1"
	execute cmd
endif
end
//-----------------------------------------------------------------------------------------------------------------------------------
Function StartDQTaskButton(ba) : ButtonControl
	STRUCT WMButtonAction &ba
	Variable numTicks = 60 
	Wave/Z Param=root:Multipole:CenterMeasParameters
	switch( ba.eventCode )
		case 2: // mouse up
			// click code here
			//NewDataFolder/O root:Multipole:IterativeAlignment
			//Make/O/N=(param[7]) root:Multipole:IterativeAlignment:MagCentreX
			//Make/O/N=(param[7]) root:Multipole:IterativeAlignment:MagCentreZ
			//Make/O/N=(param[7]) root:Multipole:IterativeAlignment:MagAxisX
			//Make/O/N=(param[7])root:Multipole:IterativeAlignment:MagAxisZ
			CtrlNamedBackground DQFindCentreAxis, period=numTicks, proc=DQCenterAxisTask
			CtrlNamedBackground DQFindCentreAxis, start
			DQTaskTo(0)
			break
		case -1: // control being killed
			break
	endswitch

	return 0
End
//-----------------------------------------------------------------------------------------------------------------------------------
Function StopDQTaskButton(ba) : ButtonControl
	STRUCT WMButtonAction &ba
	switch( ba.eventCode )
		case 2: // mouse up
			// click code here
			 ModifyControl StartTasks,fColor=(52224,52224,52224)
			 CtrlNamedBackground _all_, kill
			 MultiAbort()
			break
		case -1: // control being killed
			break
	endswitch

	return 0
End

Window VALIDATEDQMEAS() : Panel
	PauseUpdate; Silent 1		// building window...
	if (strlen(WinList("VALIDATEDQMEAS",";","WIN: 64" ))>0)
		DoWindow /F VALIDATEDQMEAS
		return
	endif

	NewPanel /K=1/W=(600,200,950,360) As "Check results"
	DrawText 32, 20, "Please check the results printed in the Igor history"
	Button SAVEMEAS,pos={50,40},size={150,25},proc=DQCHOICE,title="Continue"
	Button REDOMEAS,pos={50,80},size={150,25},proc=DQCHOICE,title="Redo"
	Button STOPMEAS,pos={50,120},size={150,25},proc=DQCHOICE,title="Cancel"
EndMacro

Function DQCHOICE(ba) : ButtonControl
	STRUCT WMButtonAction &ba
	NVAR SAVEDQCHOICE=  root:Multipole:DQTaskPanel:SAVEDQCHOICE
	switch( ba.eventCode )
		case 2: // mouse up
			// click code here
			strswitch(ba.CtrlName)
				case "SAVEMEAS":
					SAVEDQCHOICE=1
					DoWindow/K VALIDATEDQMEAS
					break
			
				case "REDOMEAS":
					SAVEDQCHOICE=2
					DoWindow/K VALIDATEDQMEAS
					break
					
				case  "STOPMEAS":
					SAVEDQCHOICE=3
					DoWindow/K VALIDATEDQMEAS
					break
			endswitch	
						
		case -1: // control being killed
			break
	endswitch

	return 0
End





// ------------------------------------------------------------------------------
// MultipoleCheckDQResult()
// ------------------------------------------------------------------------------
Function MultipoleCheckDQResult()
	
	Wave/Z/T MeasStr = root:Multipole:MeasStr
	Wave/Z current = root:Multipole:Current
	Wave/Z MagCentre = root:Multipole:MagCentre
	Wave/Z MagAxis = root:Multipole:MagAxis
	Wave/Z strength = root:Multipole:MultipoleData:IntegratedStrength
	Wave/Z an = root:Multipole:MultipoleData:anNorm_7mm
	Wave/Z bn = root:Multipole:MultipoleData:bnNorm_7mm	
	Wave/Z BenchCentre = root:Multipole:BenchCentre
	Wave/Z BenchAxis = root:Multipole:BenchAxis
	Wave/Z ExportParameters = root:Multipole:ExportParameters
	Wave/Z MeasParameters = root:Multipole:MeasParameters
	Wave/Z DQFieldGradRoll = root:Multipole:MultipoleData:DQFieldGradRoll
	Wave/Z DQParameters = root:Multipole:DQParameters
	Wave/Z QuadAlignResults = root:Multipole:QuadAlignResults
	Variable g = 9.81 // [m/s^2]
	Variable f = MeasParameters[8]
	Variable sag=g/(32*f^2)*1000 // [mm]
	Variable bl = DQFieldGradRoll[0]
	Variable gl = DQFieldGradRoll[1]
	Variable rollAngle = DQFieldGradRoll[3]
	Variable glTol = 0.001 // Integrated gradient tolerance
	NVAR/Z curr0 =  root:Multipole:DQTaskPanel:curr0

	
	Make/o/n=4 root:Multipole:TestResult
	Wave/Z TestResult = root:Multipole:TestResult
	TestResult = 1
	
	// Magnet width [mm]
	Variable MagWidth = ExportParameters[6]
	
	// Gradient test
	String GradientResult = "YES"
	If ( (gl > DQParameters[1] * (1 + glTol)) || ( gl < DQParameters[1] * (1-glTol)) )
		GradientResult = "NO"
		TestResult[3] = 0
	EndIf
	
	// Multipole test
	String MultipoleResult = "YES"
	//If (stringMatch(MeasStr[4], "*DQ1*"))
	//	If ( 	( an[2] > 42 ) || ( an[2] < - 42)  || ( bn[2] > 7 + 42 ) || ( bn[2] < 7 - 42 ) ||  ( an[3] > 22 ) 	|| ( an[3] < - 22 ) ||  ( bn[3] > 1 + 22 ) || ( bn[3] < 1 - 22 ) || ( an[4] > 11 ) || ( an[4] < -11 ) || ( bn[4] > -9 + 11 )|| ( bn[4] < -9- 11) ) 	
	//		MultipoleResult = "NO"
	//		TestResult[2] = 0
	//	EndIf 
	//ElseIf (stringMatch(MeasStr[4], "*DQ2*"))
	//	If ( ( an[2] > 33 ) || ( an[2] < - 33)  || ( bn[2] > -9 + 33 ) ) 	
	//		MultipoleResult = "NO"
	//		TestResult[2] = 0
	//	EndIf
	//Else
	//	Print "UNIDENTIFIED DQ TYPE. THE SERIAL NUMBER SHALL CONTAIN DQ1 OR DQ2."
	//EndIf
	String CentreResult = "YES"
	String RollResult = "YES"
	If ( abs(rollAngle) > 0.12 )
		TestResult[1] = 0
		RollResult = "NO"
	Endif
	
	
	Print "-----------------------------"
	Print "DQ MEASUREMENTS"
	Print Secs2Date(DateTime,1)
	Print Secs2Time(DateTime,1)
	Print "SERIAL NUMBER: "+MeasStr[4]
	Print "CURRENT:            "+num2Str(Current[0])+" A"
	Print "CENTRE:              "+ "x0: "+ num2Str(MagCentre[0]-BenchCentre[0])+" mm;    "+"z0: "+ num2Str(MagCentre[1]-sag-BenchCentre[1])+" mm;    "+"s0: "+num2str(QuadAlignResults[4])
	Print "AXIS:                    "+"dx0: "+num2Str(MagAxis[0]-BenchAxis[0])+" mm;   "+"dz0: "+num2Str(MagAxis[1]-BenchAxis[1])+" mm"
	Print "MAGNETIC LENGTH: "+num2str(QuadAlignResults[5])+" mm"
	Print "ROLL ANGLE:       "+num2Str(rollAngle)+" mrad"
	Print "PITCH ANGLE:           "+num2Str(1000 * (MagAxis[1]-BenchAxis[1]) / QuadAlignResults[7] ) +" mrad"	
	Print "----------"
	Print "INTEGRALS ALONG A PARABOLIC TRAJECTORY" 
	Print "RADIUS OF CURVATURE: "+num2str(DQParameters[3])+" mm. "+"DEFLECTION ANGLE: "+num2str(DQParameters[4])+" rad. "+"MAG LENGTH: " +num2str(DQParameters[2])+ " mm." 	
	Print "FIELD INTEGRAL: "+num2Str(bl)+" T m"  
	Print "GRAD INTEGRAL: "+num2Str(gl)+" T" 
	Print "----------"	
	Print "INTEGRALS ALONG A STRAIGHT LINE"
	Print "MULTIPOLES AT 7 mm"
	Print "b1: "+num2Str(bn[0])+" units;   a1: "+num2Str(an[0])+" units"
	Print "b2: "+num2Str(bn[1])+" units;   a2: "+num2Str(an[1])+" units"
	Print "b3:  "+num2Str(bn[2])+" units;   a3: "+num2Str(an[2])+" units"
	Print "b4: "+num2Str(bn[3])+" units;   a4: "+num2Str(an[3])+" units"
	Print "b5: "+num2Str(bn[4])+" units;   a5: "+num2Str(an[4])+" units"	
	Print "b6: "+num2Str(bn[5])+" units;   a6: "+num2Str(an[5])+" units"	
	//Print "MULTIPOLES WITHIN SPECIFICATIONS: "+MultipoleResult+"."
	If (TestResult[3] == 0 )
	curr0 = current[0]* DQParameters[1]/gl
	Print "-----------------------------"
	Print "WRONG GRADIENT"
	Print "SET THE CURRENT TO  "+num2str(curr0)+" A AT THE NEXT ITERATION" 
	Else
	Print "-----------------------------"
	Print "GRADIENT IS CORRECT"
	EndIf
	Print "-----------------------------"
	If (TestResult[1] == 0 )
	Print "THE ROLL ANGLE IS OUT OF SPECIFICATIONS"
	Print "PLEASE CORRECT THE ROLL ANGLE"
	Else
	Print "THE ROLL ANGLE IS CORRECT"
	Print "-----------------------------"
	EndIf
	// Clear
	KillStrings/z MultipoleResult, CentreResult, RollResult
End

