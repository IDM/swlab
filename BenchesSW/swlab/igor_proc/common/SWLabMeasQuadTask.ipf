// This file is part of SWLab Stretched Wire Magnetic Measurement Program 
// 	
// Copyright 2017, 2018,  ESRF - The European Synchroton, Gael Le Bec, Christophe Penel
//
// SWLab is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// Version 1.6.4
// 11/01/2018

#pragma rtGlobals=3		// Use modern global access method and strict wave access.

//---------------------------------------------------------
// Build the TaskPanel
//---------------------------------------------------------
Window QuadTaskPanel() : Panel
	
	//--- Check initialization	
	// MultipoleCheckOpen()
	If (!DataFolderExists("root:Multipole")||(numType(root:K2182:path)==2)||(numType(root:XPS2d:path)==2))
		Print "Please initialize the bench with the following command:"
		Print "SWLab Measurement > Initialization > Open"
		Return -1
	EndIf
	//--- Set Multipole XOP to quiet
	MultiQuiet(1)
	// --- Date and time
	Print  Date()+", "+Time()
	//--- Load Measurement Parameters
	MultipoleLoadParam()
	if (strlen(WinList("QuadTaskPanel",";","WIN: 64" ))>0)
		DoWindow /F QuadTaskPanel
		

		return
	endif
	PauseUpdate; Silent 1		
	//--- building window...
	if (!datafolderExists("root:Multipole:QuadTaskPanel"))
		Newdatafolder root:Multipole:QuadTaskPanel
		make/o/T /N=5 root:Multipole:QuadTaskPanel:Tasklabel
		make/o/N=5 root:Multipole:QuadTaskPanel:TaskFollow
		root:Multipole:QuadTaskPanel:TaskFollow=0
		variable /G root:Multipole:QuadTaskPanel:Mytoggle
		variable /G root:Multipole:QuadTaskPanel:CurrentTask
		root:Multipole:QuadTaskPanel:Tasklabel[0]="Set Power Supply"
		root:Multipole:QuadTaskPanel:Tasklabel[1]="Alignment"
		root:Multipole:QuadTaskPanel:Tasklabel[2]="Harmonic measurements"
		root:Multipole:QuadTaskPanel:Tasklabel[3]="Check results"
		root:Multipole:QuadTaskPanel:Tasklabel[4]="Save"
		variable /G  root:Multipole:QuadTaskPanel:Toggle0
		variable /G  root:Multipole:QuadTaskPanel:Toggle1
		variable /G  root:Multipole:QuadTaskPanel:Toggle2
		variable /G  root:Multipole:QuadTaskPanel:Toggle3
		variable /G  root:Multipole:QuadTaskPanel:Toggle4
		 
	endif	
	variable Ntask=numpnts(root:Multipole:QuadTaskPanel:TaskLabel)
	root:Multipole:QuadTaskPanel:Mytoggle=0
	
	NewPanel /K=1 /W=(400,148,750,168+25*NTask)
	// ShowTools/A	
	string cmd
	variable ind=0
	//--- Build Check box
	do
		 cmd= "CheckBox  taskcheck"+num2str(ind)+",pos={30,15+"+num2str(25*ind)+"},size={ 40,14},disable=2,title=\""+root:Multipole:QuadTaskPanel:TaskLabel[ind]+"\",variable=root:Multipole:QuadTaskPanel:toggle"+num2str(ind)+",mode=1, fStyle=1"
//			print cmd
		Execute/Q cmd
		ind+=1
	while( ind<ntask)
	Variable /G  root:Multipole:QuadTaskPanel:SAVEChoice=0
	//--- Start and Stop buttons
	Button StartTasks,pos={257,11},size={65,31},proc=StartQuadTaskButton,title="Start"
	Button StopTasks,pos={257,50},size={65,31},proc=StopQuadTaskButton,title="Stop"

	TaskTo( root:Multipole:QuadTaskPanel:CurrentTask)
	
EndMacro

//------------------------------------------------------------------------------------------
// QuadCenterAxisTask(s)
//------------------------------------------------------------------------------------------
Function QuadCenterAxisTask(s)
STRUCT WMBackgroundStruct &s

//--- Initialization
NVAR StepTask=root:Multipole:QuadTaskPanel:CurrentTask
NVAR SaveChoice =root:Multipole:quadTaskPanel:SaveChoice
NVAR mytoggle=root:Multipole:QuadTaskPanel:Mytoggle
Wave/Z param=root:Multipole:CenterMeasParameters
Wave/Z mParam=root:Multipole:MeasParameters
Wave/Z QC=root:Multipole:MagCentre
Wave/Z QA=root:Multipole:MagAxis
Wave/Z/t MeasStr=root:Multipole:MeasStr
Wave/Z current = root:Multipole:Current
Variable curr=current[0]
Variable cycle=0
Wave/Z ExportParameters=root:Multipole:ExportParameters
Variable rSpec = ExportParameters[4]
NVAR/Z FieldIntDisplay = root:Multipole:QuadTaskPanel:FieldIntDisplay
If ( !NVAR_Exists(FieldIntDisplay) )
	Variable/G root:Multipole:QuadTaskPanel:FieldIntDisplay = 0
EndIf
NVAR/Z TimerVar=root:Multipole:QuadTaskPanel:TimerVar
If ( !NVAR_Exists(TimerVar) )
	Variable/G root:Multipole:QuadTaskPanel:TimerVar
EndIf
NVAR/Z nIterAlign = root:Multipole:QuadTaskPanel:nIterAlign
If ( !NVAR_Exists(nIterAlign) )
	Variable/G root:Multipole:QuadTaskPanel:nIterAlign = 0
EndIf
//Make/o/n=2 MagCentreTmp
curr = current[0]
NVAR/Z curr0 =  root:Multipole:QuadTaskPanel:curr0
If  ( !NVAR_Exists(curr0) )
	Variable/G root:Multipole:QuadTaskPanel:curr0 = curr
EndIf
Variable CyclingWaitTime
//--- Change the color of the Start button at each call
if (mytoggle==0)
	mytoggle=1
	ModifyControl/Z StartTasks,fColor=(0,5000,0)	
else
	mytoggle=0
	ModifyControl/Z StartTasks,fColor=(52224,52224,52224)
endif

// --- Finite State Machine
//
// 0 		INITIALIZATION
// 0 		Initialization
// 0.1	PS ready
//
// 1		ALIGNMENT
// 1 		Launch quadrupole alignment
// 1.1 	Wait for quadrupole alignment result		
//
// 2		CIRCULAR MEASUREMENTS
// 2 		Launch circular measurements
// 2.1	Wait  for circular measurement results
// 2.2	Multipole analysis and tests vs specifications
//
// 3		VALIDATION
// 3		Open measurement validation dialog
// 3.1 	User validation
//
// 4		SAVE
// 4		Export Measurement results

strswitch(num2str(steptask))			
		// -------------------------------
		// State 0
		// Initialization
		// -------------------------------
		// 0 - Init and set power suppy
		case "0":
			Print time()+"     Initialize..."
			ModifyControl StartTasks,fColor=(0,5000,0)				
			// Ask metadata
			if (MultipoleMetaData()==-1)
				ModifyControl StartTasks,fColor=(52224,52224,52224)
				Return -1							
			EndIf
			Duplicate/o root:Multipole:BenchCentre root:Multipole:MagCentre
			Duplicate/o root:Multipole:BenchAxis root:Multipole:MagAxis
			nIterAlign = 0
			// Prepare Power Supply
			// Prompt cycle, "Cycle (0/1)"
			// DoPrompt "Power Supply", cycle
			// If (V_Flag)
				// ModifyControl StartTasks,fColor=(52224,52224,52224)
				// Return -1							
			// EndIf
			current[0] = curr
			// Set power supply
			// 
			//------------------------------------------------------------------
			// CALL POWER SUPPLY FUNCTIONS HERE
			//-----------------------------------------------------------------
			//
			TaskTo(0.1)
			return 0
		break 
		// 0.1 - Set Current
		case "0.1":
			// Prepare Power Supply
			Prompt cycle, "Set current (0: no, 1: yes)"
			DoPrompt "Power Supply", cycle
			If (V_Flag)
				TaskTo(0)
				Return -1							
			EndIf
			curr0 = curr
			If (cycle == 1)
				TaskTo(0.2)
			ElseIf (cycle == 0)
				TaskTo(1)
			Else
				print "Answer must be '0' or '1' "
				TaskTo(0.1)			
			EndIf
			return 0
		break
		// 0.2 - PS cycling
		case "0.2":  
			Print time()+"     Set cycling current..."
			curr0 = curr
#if exists("SetCyclingCurrent")
			SetCyclingCurrent()
#endif
			current[0] = curr
			TimerVar = DateTime
			TaskTo(0.3)
			return 0
		break
		// 0.3 - Wait for PS cycling
		case "0.3":
			CyclingWaitTime = 15 // s
			If( DateTime > TimerVar + CyclingWaitTime)
				TaskTo(0.4)
			EndIf
			return 0
		break
		// 0.4 - PS set current
		case "0.4":
			Print time()+"     Set current..."
			String MagnetType = MeasStr[4]
			MagnetType = MagnetType[0,StrSearch(MagnetType,"-",0)-1]
#if Exists("SetCurrentQF1")
			strswitch(MagnetType) 
				case "QF1":
				SetCurrentQF1()
				break
				case "QD2":
				SetCurrentQD2()
				break
				case "QD3":
				SetCurrentQD3()
				break
				case "QF4E":
				SetCurrentQD2()
				break
				case "QF6":
				SetCurrentQF6()
				break
				case "QF8":
				SetCurrentQF8()
				break
			endswitch 
#endif
			//--------------
			TimerVar = DateTime
			TaskTo(0.5)
			return 0
		break
		// 0.5 - Wait for current stability
		case "0.5":
			CyclingWaitTime = 60 // s
			If( DateTime > TimerVar + CyclingWaitTime)
				TaskTo(1)
			EndIf
			return 0
		
		// -------------------------------
		// State 1
		// ALIGNMENT
		// -------------------------------
		// 1 - Launch quadrupole alignment
		case "1":
			Print time()+"     Quad axis and length measurements, iteration "+num2str(nIterAlign+1)+"..."		
	  	//	MultiQuadAlignment( param[0], param[1], param[2], param[3], param[4], param[4], param[8], mParam[5], mParam[6], param[6]) //	LL - 30/03/2017 // 
			MultiQuadAlignment( QC[0], QC[1], QA[0], QA[1], param[4], param[5], param[8], mParam[5], mParam[6], param[6])		
			TaskTo(1.1)
			return 0;	
		break				
		// 1.1 - Wait for quadrupole alignment result, 
		case "1.1" :
			If( !MultiIsMeasuring() )
				MultiGetLastMeas()
				Wave/Z MagCentreLastMeas=root:Multipole:MagCentreLastMeas
				Wave/Z MagAxisLastMeas=root:Multipole:MagAxisLastMeas
				Wave/Z QuadAlignResults=root:Multipole:QuadAlignResults
				MagCentreLastMeas[0]=QuadAlignResults[0]
				MagCentreLastMeas[1]=QuadAlignResults[1]
				MagAxisLastMeas[0]=QuadAlignResults[2]
				MagAxisLastMeas[1]=QuadAlignResults[3]
				QC = MagCentreLastMeas
				QA = MagAxisLastMeas
				Wave/Z/t MeasStr=root:Multipole:MeasStr
				MeasStr[2] = Date()
				MeasStr[3] = Time()
				nIterAlign +=1 
				If (nIterAlign < param[7] )
					TaskTo(1)
				Else
					TaskTo(2)
				EndIf
			endif
			return 0
		break
		// -------------------------------
		// State 2	
		// Circular measurements
		// -------------------------------
		// 2 - Launch circular measurements
		case "2": 
			Print time()+"     Circular measurements..."
			// Set centre and axis to the last values measured
			//Print  Date()+", "+Time()
			Wave/Z/t MeasStr=root:Multipole:MeasStr
			MeasStr[2] = Date()
			MeasStr[3] = Time()
			//print " Center : X = " ,QC[0] , " Z= "  ,QC[1]
			//print " Taper  :TX = " ,QA[0] , " TZ=" ,QA[1]
			//print "-------------------------------" 
			// Circular measurements
			CI(0,1)
			TaskTo(2.1)
			return 0
		// 2.1 - Wait  for circular measurement results
		case "2.1":
			If( !MultiIsMeasuring() )
				If (!cmpStr(MultiGetLastError(),"No Error\n"))
					MultiGetLastMeas()
					SetDataFolder root:Multipole
					// Convert field integral to [T mm]
					//Wave FieldIntegral
					//FieldIntegral /= 1000
					Taskto(2.2)
					return 0
				else
					print "Error during circular measurement."
					MultipoleLastErr()
					ModifyControl StartTasks,fColor=(52224,52224,52224)
					return 2				
				endif
			endif
			return 0

		break;
		// 2.2 - Multipole analysis and tests vs specifications
		case"2.2":
			Print time()+"     Analysis..."
			SetDatafolder root:Multipole
			// MultipoleAnalysis
			MPMultipoleAnalysisDialog(0)
			SetDataFolder root:Multipole:MultipoleData
			// Compute multipoles at specified radius
			MPChangeRadius(rSpec,0)
			// Plot the integrated field
			If ( ! FieldIntDisplay )
				Wave/Z FieldIntegral
				Display FieldIntegral
				FieldIntDisplay = 1
			EndIf
			// Gradient and Roll angle
			MPQuadRoll(0)
			MPQuadGradient(0)
			// Display the field integral
			//Wave FieldIntegral
			//Display/N=dispFieldInt FieldIntegral
			// Check results
			SetDataFolder root:Multipole
			MultipoleCheckQuadResult()
			// Move the wire to the magnet centre 
			Print "WIRE MOVED TO MAGNET CENTRE"
			//////////////////////////////////////////////////////////
			//MPMultiFindCentre(1) // LL - 30/03/2017 
			//////////////////////////////////////////////////////////
			SWMoveToCentre(1,0)
			taskto(3)
			return 0
		break;
		// -------------------------------
		// State 3
		// Validation by the user
		// -------------------------------
		// 3 - Open measurement validation dialog
		case "3":
			nIterAlign = 0
			KillWaves/z MagCentreTmp
			SaveChoice=0
			Execute/Q "ValidateMeas()"
			taskto(3.1)
			return 0		
		break
		// 3.1 - User validation
		case "3.1":
			switch (SaveChoice)
			case 0 :
				return 0
				break
			case 1 :
				Taskto(4)
				return 0
				break
			case 2 :
				Taskto(1)
				return 0
				break
			case 3:
				print "Measurement cancelled."
				 ModifyControl StartTasks,fColor=(52224,52224,52224)
				return 2
				break
		endswitch					
		// -------------------------------
		// State 4
		// Export measurement results
		// -------------------------------			
		case "4":
				MPExportData()
				Print time()+"     Export"	
				Variable turnOff = 1
				Prompt turnOff, "Turn off the power supply? (0: no, 1: yes)"
				DoPrompt "Power Supply", turnOff
				If (V_Flag)
					turnOff = 0	
				EndIf
				If ( turnOff )
#if Exists("PSOff")
					PSOff()
#endif
					cycle = 0
				EndIf
				ModifyControl StartTasks,fColor=(52224,52224,52224)
				Print "-----------------------------"
				Print " "
				Print "MEASUREMENT COMPLETED"
				If ( turnOff )
				Print "THE POWER SUPPLY AS BEEN TURNED OFF"
				EndIf
				Print " "
				Print "-----------------------------" 
				//DoAlert  0, "Measurement done"
				Print "-----------------------------"
				Print "MEASUREMENT DONE"
				Taskto(0)
				return 1
		break;	
		 // when no case matches
		 
	endswitch

Return 0
End

//

//-----------------------------------------------------------------------------------------------------------------------------------
function TaskTo(num)
variable num
variable val

variable i
DoWindow /F QuadTaskPanel
variable nbt=numpnts(root:Multipole:QuadTaskPanel:TaskLabel)

if (num >nbt-1)
	DoAlert 0 , "BAD TASK NUM "
	return 0
endif
NVAR CurrentTask=root:Multipole:QuadTaskPanel:CurrentTask
string cmd
CurrentTask=Num
for (i=0;i<nbt;i+=1)
	cmd="root:Multipole:QuadTaskPanel:toggle"+num2str(i)+"=0"
	execute/Q cmd	
endfor
if (num>=0 )
	cmd="root:Multipole:QuadTaskPanel:toggle"+num2str(floor(num))+"=1"
	execute/Q cmd
endif
end
//-----------------------------------------------------------------------------------------------------------------------------------
Function StartQuadTaskButton(ba) : ButtonControl
	STRUCT WMButtonAction &ba
	Variable numTicks = 60 
	Wave/Z Param=root:Multipole:CenterMeasParameters
	switch( ba.eventCode )
		case 2: // mouse up
			// click code here
			CtrlNamedBackground quadFindCentreAxis, period=numTicks, proc=QuadCenterAxisTask
			CtrlNamedBackground quadFindCentreAxis, start
			Taskto(0)
			break
		case -1: // control being killed
			break
	endswitch

	return 0
End
//-----------------------------------------------------------------------------------------------------------------------------------
Function StopQuadTaskButton(ba) : ButtonControl
	STRUCT WMButtonAction &ba
	switch( ba.eventCode )
		case 2: // mouse up
			variable nbt=numpnts(root:Multipole:QuadTaskPanel:TaskLabel)
			string cmd = "root:Multipole:QuadTaskPanel:toggle0=0"
			execute/Q cmd				
			variable i
			for (i=0;i<nbt;i+=1)
				cmd="root:Multipole:QuadTaskPanel:toggle"+num2str(i)+"=0"
				execute/Q cmd	
			endfor
			// click code here
			 ModifyControl StartTasks,fColor=(52224,52224,52224)
			 //CtrlNamedBackground QuadCenterAxisTask, kill
			 MultiAbort()
			 CtrlNamedBackground _all_, kill	
			break
		case -1: // control being killed
			break
	endswitch

	return 0
End
//-----------------------------------------------------------------------------------------------------------------------------------
Window ValidateMeas() : Panel
	PauseUpdate; Silent 1		// building window...
	if (strlen(WinList("VALIDATEMEAS",";","WIN: 64" ))>0)
		DoWindow /F VALIDATEMEAS
		return
	endif

	NewPanel /K=1/W=(600,200,950,360) As "Check results"
	DrawText 32, 20, "Please check the results printed in the Igor history"
	Button SAVEMEAS,pos={50,40},size={150,25},proc=CHOICE,title="Continue"
	Button REDOMEAS,pos={50,80},size={150,25},proc=CHOICE,title="Redo"
	Button STOPMEAS,pos={50,120},size={150,25},proc=CHOICE,title="Cancel"
EndMacro
//-----------------------------------------------------------------------------------------------------------------------------------
Function CHOICE(ba) : ButtonControl
	STRUCT WMButtonAction &ba
	NVAR SAVECHOICE=  root:Multipole:QuadTaskPanel:SAVECHOICE
	switch( ba.eventCode )
		case 2: // mouse up
			// click code here
			strswitch(ba.CtrlName)
				case "SAVEMEAS":
					SAVECHOICE=1
					DoWindow/K ValidateMeas
					break
			
				case "REDOMEAS":
					SAVECHOICE=2
					DoWindow/K ValidateMeas
					break
					
				case  "STOPMEAS":
					SAVECHOICE=3
					DoWindow/K ValidateMeas
					break
			endswitch	
						
		case -1: // control being killed
			break
	endswitch

	return 0
End



// ------------------------------------------------------------------------------
// MultipoleCheckQuadResult()
// ------------------------------------------------------------------------------
Function MultipoleCheckQuadResult()
	
	// CHECK QUADRUPOLE MEASUREMENTS 
	// ACCORDING TO SPECIFICATIONS
	Wave/Z ExportParameters = root:Multipole:ExportParameters
	String anNormName = "root:Multipole:MultipoleData:anNorm_"+num2str(ExportParameters[4])+"mm"
	String bnNormName = "root:Multipole:MultipoleData:bnNorm_"+num2str(ExportParameters[4])+"mm"	
	Wave/Z an=$anNormName
	Wave/Z bn=$bnNormName	
	Wave/Z/T MeasStr = root:Multipole:MeasStr
	Wave/Z current = root:Multipole:Current
	Wave/Z MagCentre = root:Multipole:MagCentre
	Wave/Z MagAxis = root:Multipole:MagAxis
	Wave/Z roll = root:Multipole:MultipoleData:RollAngle
	Wave/Z grad = root:Multipole:MultipoleData:IntegratedGradient
	Wave/Z BenchCentre = root:Multipole:BenchCentre
	Wave/Z BenchAxis = root:Multipole:BenchAxis
	Wave/Z QuadAlignResults = root:Multipole:QuadAlignResults
	Wave/Z TolParam = root:Multipole:ToleranceParameters
	Wave/Z MeasParameters = root:Multipole:MeasParameters
	Variable f=MeasParameters[8] // [Hz]
	Variable g = 9.81 // [m/s^2]
	Variable sag=g/(32*f^2)*1000 // [mm]
	
	// Magnet width
	Variable MagWidth = ExportParameters[6]
	Make/o/n=4 root:Multipole:TestResult
	Wave/Z TestResult = root:Multipole:TestResult
	TestResult = 1
	// Multipole test
	String MultipoleResult = "YES"
	TestResult[2] = 1
	Variable i
	Variable n=(numpnts(TolParam)-2)/2
	For (i=0; i<n; i+=1)
		If ( abs(an[ TolParam[i+2]  ] ) > TolParam[i+4]  || abs(bn[ TolParam[i+2]  ] ) > TolParam[i+4]  )
			MultipoleResult = "NO"
			TestResult[2] = 0
		EndIf
	EndFor
	// Centrering test
	String CentreResult = "YES"
	If (abs(MagCentre[1]-sag-BenchCentre[1]) > TolParam[0] )
		CentreResult = "NO"
		TestResult[0] = 0
	EndIf
	// Roll test
	String RollResult = "YES"
	If( abs(roll[0]) > TolParam[1] )
		RollResult = "NO"
		TestResult[1] = 0
	EndIf
	// Pitch test
	String PitchResult = "YES"
	if (abs( 1000 * (MagAxis[1]-BenchAxis[1]) / QuadAlignResults[7] ) > 3 * TolParam[1] )
		PitchResult = "NO"
		TestResult[3] = 0
	EndIf
	
	Print "-----------------------------"
	Print "QUADRUPOLE MEASUREMENTS"
	Print Secs2Date(DateTime,1)
	Print Secs2Time(DateTime,1)
	Print "SERIAL NUMBER:     "+MeasStr[4]
	Print "CURRENT:                "+num2Str(Current[0])+" A"
	Print "CENTRE:                  "+ "x0: "+ num2Str(MagCentre[0]-BenchCentre[0])+" mm;    "+"z0: "+ num2Str(MagCentre[1]-sag-BenchCentre[1])+" mm;    "+"s0: "+ num2Str(QuadAlignResults[4])+" mm"
	Print "AXIS:                        "+"dx0: "+num2Str(MagAxis[0]-BenchAxis[0])+" mm;   "+"dz0: "+num2Str(MagAxis[1]-BenchAxis[1])+" mm"
	Print "ROLL ANGLE:           "+num2Str(roll[0])+" mrad"
	Print "PITCH ANGLE:           "+num2Str(1000 * (MagAxis[1]-BenchAxis[1]) / QuadAlignResults[7] ) +" mrad"
	Print "GRADIENT:               "+num2Str(grad[0])+" T" 
	Print "MAGNETIC LENGTH: "+num2str(QuadAlignResults[5])+" mm"
	Print "MULTIPOLES AT "+num2str(ExportParameters[4])+" mm"
	Print "b1: "+num2Str(bn[0])+" units;   a1: "+num2Str(an[0])+" units"
	Print "b2: "+num2Str(bn[1])+" units;   a2: "+num2Str(an[1])+" units"
	Print "b3: "+num2Str(bn[2])+" units;   a3: "+num2Str(an[2])+" units"
	Print "b4: "+num2Str(bn[3])+" units;   a4: "+num2Str(an[3])+" units"
	Print "b5: "+num2Str(bn[4])+" units;   a5: "+num2Str(an[4])+" units"	
	Print "MULTIPOLES WITHIN SPECIFICATIONS: "+MultipoleResult+"."
	Print "MAGNETIC CENTRE WITHIN SPECIFICATIONS: "+CentreResult+"."
	Print "ROLL ANGLE WITHIN SPECIFICATIONS: "+RollResult+"."	
	Print "PITCH ANGLE WITHIN SPECIFICATIONS: "+PitchResult+"."	
	Print "-----------------------------"
	If(StringMatch(CentreResult, "NO")||StringMatch(RollResult, "NO"))
		Print "PLEASE INSERT SHIMS: dZ(-x0) = "+num2str( BenchCentre[1]-MagCentre[1]+sag - roll[0]/1000 * MagWidth/2)+" mm and dZ(x0) = "+num2str( BenchCentre[1]-MagCentre[1]+sag + roll[0]/1000 * MagWidth/2)+"mm"
	EndIf

	// Clear 
	KillStrings/z MultipoleResult
End