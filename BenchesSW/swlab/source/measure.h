#if !defined(AFX_MEASURE_H__59F3556B_46AF_4EDC_901A_AC04181019E4__INCLUDED_)
#define AFX_MEASURE_H__59F3556B_46AF_4EDC_901A_AC04181019E4__INCLUDED_

#include <process.h>
#include <XPS2d.h>
#include <Keithley.h>
#include <math.h>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/SVD>
#include <unsupported/Eigen/KroneckerProduct>
using Eigen::MatrixXd;
using Eigen::SparseMatrix;
using Eigen::VectorXd;
using Eigen::JacobiSVD;

#pragma once


class measure
{
public:
	XPS2d * XPS;
	Keithley *MULTI;
	Keithley *NOISE_MULTI;
	char last_err[500];
	bool Valid;
	char MultiName[30];
	char NoiseMulti[30];
	char XPSName[20];
	double WireLength;
	double JMesLen;
	double vmax;
	double rap_step_len;

	bool Measuring;
	bool Level2Measuring;
	bool DataAvailable;
	bool AbortRequest;
	bool Quiet;
	double MultiData[1025];
	double NoiseData[1025];
	double *result;
	double *noise;
	double *X_result;
	double *Z_result;
	double *T_result;
	double *QC_result;
	double *QT_result;
	double *VX_result;
	double *VZ_result;
	double *A_result;
	int MesLen;
	int MAXNUMPT;
	int LastMesType;
	double LastQuadcenter[2];
	double LastQuadAxis[2];

	measure(char *name);
	~measure(void);
	int GetConfiguration(char *Name);
	bool IsMeasuring();
	int suppr_chaine(char *suj ,char*obj);
	bool Abort();
	bool NoiseMeas();
	bool IntegralMeas(double X,double Z,double XLen,double ZLen,double V , double Acc, double nb, double tend);
	struct IntegralMeasstruct
	{	double X;
		double Z;
		double Len;
		double Angle;
		double V ;
		double Acc;
		int nb;
		double tend;
	} PimStruct;

	struct LinearIntegralScanstruct
	{	double XB;
		double ZB;
		double XE;
		double ZE;
		double NBpt;
		double TI;
		double Acc;
		double NBScan;

	} LISStruct;

	struct CircularIntegralScanstruct
	{	double XC;
		double ZC;
		double R;
		double NBpt;
		double NBturn;
		double TI;
		double Velocity;
		double Acc;
		double driftOrder;
	} CISStruct;
	struct MultiCircularIntegralScanstruct
	{	double XC;
		double ZC;
		double *R;
		double NBpt;
		double NBturn;
		double TI;
		double Velocity;
		double Acc;
		double driftOrder;
	} MCISStruct;
	struct CompCircularIntegralScanstruct
	{	double XC;
		double ZC;
		double *R;
		double MeasLen;
		double NBPT;
		double NBAvg;
		double V;
		double Acc;
		double CompOrder;
		double Phi0;
		double tend;
	} CCISStruct;
	struct QuadCenterStruct
	{
		double x0;
		double z0;
		double TX0;
		double TZ0;
		double r0; 
		double  nPoints; 
		double nAve;
		double XCenter; 
		double ZCenter;
		double MesLen;
	}QCStruct;
	struct QuadAlignStruct
	{
		double X0;
		double Z0;
		double TX0;
		double TZ0;
		double DX;
		double DT;
		double MEASLEN;
		double VEL;
		double ACC;
		double NAVE;
	}QAStruct;


	bool DoubleIntegralMeas(double X,double Z,double XLen,double ZLen,double V , double Acc,double nb);
	bool LinearIntegralScan(double XB,double ZB,double XE,double ZE,double NB,double TI,double Acc,double NBScan);
	bool CircularScan(double XC,double ZC,double R,double NBPT,double NBTURN,double TI,double Velocity,double Acc, double driftOrder);
	bool MultiCircularScan(double XC, double ZC, double *R, double NBPT, double NBTURN, double TI, double Velocity, double Acc, double driftOrder);
	int GetLastMes(double **res,double**noi, double **Xres, double ** Zres, double **Tres, double **QC, double **QT, double **VXres, double **VZres, double **Ares);
	bool SetMesType(int type);
	int GetMesType();
	bool CompCircularScan(double XC,double ZC,double *R, double MeasLen,double NBPT,double NBAvg,double V,double Acc,double CompOrder,double Phi0, double tend);
	bool QuadCenter(double x0, double z0, double TX0, double TZ0, double r0, double  nPoints, double nAve);
	bool QuadAxis(double x0, double z0, double TX0, double TZ0, double r0, double MesLen, double  nPoints);
	bool QuadAlign(double x0, double z0, double tx0, double tz0, double dx, double dt, double MeasLen, double vel, double acc, double  nave);
	bool QuadCenterAxis();
	int LinFit(double *X, double*Y, int len, double *a, double *b,  double *X0);
	// Drift compensation functions
	double Legendre(int n, double x);
	SparseMatrix<double> JkMatrix(int p, int k);
	SparseMatrix<double> JMatrix(int p);
	int DriftCorr(double wData[], int np, int nt, int pOrder, double tol);
	bool MasterCircularScan(double XC, double ZC, double R, double NBPT, double NBTURN, double TI, double Velocity, double Acc);
	bool SetQuiet(bool QuietSwitch);
	bool GetQuiet();
};

#endif // !defined(AFX_MEAS_H__59F3556B_46AF_4EDC_901A_AC04181019E4__INCLUDED_)