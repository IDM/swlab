/*
	Multipole.h -- equates for Multipole XOP
*/

#include <measure.h>

/* Multipole custom error codes */
#define OLD_IGOR 1 + FIRST_XOP_ERR
#define NO_MEASCREATED 2 + FIRST_XOP_ERR
#define MEAS_ALREADYCREATED 3 + FIRST_XOP_ERR
#define MEASALREADYRUNNING 4  + FIRST_XOP_ERR
#define ERR_GETLASTMES 5  + FIRST_XOP_ERR
#define ERR_WAVECREATE 6  + FIRST_XOP_ERR
#define ERR_UNKNOWMES 7  + FIRST_XOP_ERR
#define ERR_NOTEXECUTE 8  + FIRST_XOP_ERR
#define ERR_NOCLOSEINMEAS 9  + FIRST_XOP_ERR
#define ERR_NOTSAMELEN 10 + FIRST_XOP_ERR


// Measure number define
#define UNKNOWMES 0
#define INTEGRAL 1
#define SECOND_INTEGRAL 2
#define LINEAR_SCAN 3
#define CIRCLE_SCAN 4
#define MULTI_CIRCLE_SCAN 5
#define MULTI_COMP_CIRCLE_SCAN 6
#define MULTI_QUAD_CENTER 7
#define MULTI_QUAD_AXIS 8
#define MULTI_QUAD_CENTER_AXIS 9
#define MULTI_LINEFIT 10
#define MASTER_CIRCLE_SCAN 11
#define MULTI_QUAD_ALIGN 12
#define MULTI_QUIET 13




/* Prototypes */
HOST_IMPORT int XOPMain(IORecHandle ioRecHandle);
