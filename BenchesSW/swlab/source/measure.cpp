﻿// This file is part of SWLab Stretched Wire Magnetic Measurement Program 
// 	
// Copyright 2016, ESRF - The European Synchroton, Christophe Penel, Gael Le Bec
//
// SWLab is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// The XOP Toolkit is copyright Wavemetrics, see <http://www.wavemetrics.com>
// The Newport XPS libraries are copyright Newport, see <http://www.newport.com>
// The Eigen library is licensed under the MPL2, see <http://eigen.tuxfamily.org>
// 

#include <measure.h>
#include <Keithley.h>
#include <complex>
#include <vector>	
#include <numeric>

int measure::suppr_chaine(char *suj ,char*obj)
{
char *p;
	  p=strstr(suj,obj);
		if(p)
			memmove(p,p+strlen(obj),sizeof obj);
	return true;
}

int measure::GetConfiguration(char *Name)
{
	//**********************************************************
	// Define some structures used for resources extraction.
	//**********************************************************
	typedef struct _Res
	{
		char name[50];
		char val[50];
	}Res;

	enum Devices 
	{
		XPS2D, 
		MES_MULTI,
		WIRE_LENGTH,
		VMAX,
		MESRATIO,
		JMESLEN,
	};

	Res CtrlResTab[] = 
	{
		{"XPS2D",       ""},
		{"MES_MULTI",        ""}, 
		{"WIRE_LENGTH",      ""},
		{"V_MAX",	""},
		{"MESRATIOTIME",	""},
		{ "JMESLEN", "" },

	};

	FILE   *Fin;
	char   *Temp;
	char    Buf[1000];
	char    ConfStr[128];
	char    FileName[256];
	int     i, NbRes;
	int     status;


	status = GetEnvironmentVariable("ConfPath", ConfStr, 100);

	if(status == 0)  // Environment variable not defined.
		sprintf(FileName, "%s\\%s.conf", "c:\\ConfigurationFolder", Name);
	else
		sprintf(FileName, "%s\\%s.conf", ConfStr, Name);

	Fin = fopen(FileName, "r");
	if(Fin == NULL)
	{
		sprintf (last_err,"GetConfiguration: can't open file: %s\n",FileName);
		Valid=false;
		return(false);
	}

	//--------------------------------------------
	// Get resource names from configuration file.

	fread(Buf, sizeof(Buf), 1, Fin);
	NbRes = sizeof(CtrlResTab) / sizeof(Res);
	for(i=0; i<NbRes; i++)
	{
		if((Temp = strstr(Buf, CtrlResTab[i].name)) == NULL)
		{
			sprintf(last_err,"GetConfiguration: can't find %s in conf file\n",CtrlResTab[i].name);
			fclose (Fin);
			Valid=false;
			return(false);
		}
		else
		{
			sscanf(Temp, "%*s%s", &(CtrlResTab[i].val));
		}
	}
	if ((Temp = strstr(Buf, "NOISE_MULTI")) != NULL)
	{
		sscanf(Temp, "%*s%s", &(NoiseMulti));
	}
	else
	{
		sprintf(NoiseMulti, "");
	}
	fclose(Fin);
	strcpy(MultiName, CtrlResTab[MES_MULTI].val);
	strcpy(XPSName, CtrlResTab[XPS2D].val);
	WireLength=atof(CtrlResTab[WIRE_LENGTH].val);
	JMesLen = atof(CtrlResTab[JMESLEN].val);
	vmax=atof(CtrlResTab[VMAX].val);
	rap_step_len=atof(CtrlResTab[MESRATIO].val);
	Measuring=false;
	Level2Measuring=false;
	DataAvailable=false;
	Valid=true;
	AbortRequest=false;
	MAXNUMPT=1025;
	result=(double *)malloc(sizeof(double));
	noise = (double *)malloc(sizeof(double));
	X_result=(double *)malloc(sizeof(double));
	Z_result=(double *)malloc(sizeof(double));
	T_result=(double *)malloc(sizeof(double));
	VX_result=(double *)malloc(sizeof(double));
	VZ_result=(double *)malloc(sizeof(double));
	QC_result = (double *)malloc(sizeof(double)*2);
	QT_result = (double *)malloc(sizeof(double)*2);
	A_result = (double *)malloc(sizeof(double) * 8);
	sprintf (last_err,"No error\n");
	return(true);
}

measure::measure(char *name)
{
	if (GetConfiguration(name))
	{
		XPS=new XPS2d(XPSName);
		Quiet = 0;
		if (!XPS->Valid)
		{
			sprintf(last_err, XPS->last_err);
			delete XPS;
			XPS = NULL;
			Valid = false;		
		}
		else
		{ 
			//XPS->Reference();
			MULTI = new Keithley(MultiName);
			if (MULTI->comu_path == NULL)
			{
				Valid = false;
				delete XPS;
				XPS = NULL;		
				delete MULTI;
				MULTI = NULL;
				NOISE_MULTI = NULL;
				sprintf(last_err, "Multimeter could not be created\n");
			}
			else
			{
				Valid = true;
				if (strlen(NoiseMulti) != 0)
				{
					NOISE_MULTI = new Keithley(NoiseMulti);
					if (NOISE_MULTI->comu_path == NULL)
					{
						//Valid = false;
						//delete XPS;
						//XPS = NULL;
						//delete MULTI;
						//MULTI = NULL;
						delete NOISE_MULTI;
						NOISE_MULTI = NULL;
						sprintf(last_err, "NOISE multimeter could not be created\n");
					}
				}
				else
				{
					Valid = true;
					NOISE_MULTI = NULL;
				}
			}
				
		}
		
	}
	else
	{
		XPS=NULL;
		MULTI=NULL;
		NOISE_MULTI = NULL;
		Valid=false;
	}
}


measure::~measure(void)
{
	if (XPS!=NULL)
		delete XPS;
	if (MULTI!=NULL)
		delete MULTI;
	if (NOISE_MULTI != NULL)
		delete NOISE_MULTI;
	free (result);
	free(noise);
	free (X_result);
	free (Z_result);
	free(T_result);
	free(VX_result);
	free(VZ_result);
	free(QC_result);
	free(QT_result);

}

bool measure::IsMeasuring()
{
	return (Measuring||Level2Measuring) ;
}

bool measure::Abort()
{
	XPS->Abort();
	AbortRequest=true;
	if (!MULTI->reset())
		return false;
	if (NOISE_MULTI != NULL)
		if (!NOISE_MULTI->reset())
			return false;
	return true;
}

static void BackIntegralMeas(void* p)
{
	measure *pt;
	pt=(measure *)p;
//	Follow Imeas Procedur describe in MeasProc File
	if (pt->AbortRequest)
	{
		delete pt->MULTI;
		pt->MULTI=new Keithley(pt->MultiName);
		if (pt->NOISE_MULTI != NULL)
		{
			delete pt->NOISE_MULTI;
			pt->MULTI = new Keithley(pt->NoiseMulti);
		}
	}
	pt->AbortRequest=false;


	double ti=pt->XPS->TimeToMove(pt->PimStruct.Len,pt->PimStruct.V,pt->PimStruct.Acc) + 0.2;
	double XLen=pt->PimStruct.Len*sin(pt->PimStruct.Angle);
	double ZLen=pt->PimStruct.Len*cos(pt->PimStruct.Angle);
	double V1;
	if (fabs(XLen)>fabs(ZLen))
		V1=fabs(XLen)/pt->PimStruct.Len*pt->PimStruct.V;
	else
		V1=fabs(ZLen)/pt->PimStruct.Len*pt->PimStruct.V;
		
	if(!(pt->XPS->SetVelocity(V1,V1)))
		{sprintf (pt->last_err,"Error in Set Velocity to  %lf mm/s\n",V1 );pt->Measuring=false;_endthread();}
	if(!(pt->XPS->SetAcceleration(pt->PimStruct.Acc,pt->PimStruct.Acc)))
		{sprintf (pt->last_err,"Error in Set Acceleration to  %lf mm/s2\n",pt->PimStruct.Acc );pt->Measuring=false;_endthread();}
	double t2 = ceil(ti / 0.02)*.02 + 0.16;

	if(!pt->MULTI->EnterIddle())
		{sprintf (pt->last_err,"Error in Multi EnterIddle \n" );pt->Measuring=false;_endthread();}
	if (!pt->MULTI->SetTrigSource("BUS"))
		{sprintf (pt->last_err,"Cant Set BUS Trig Source on MULTI\n" );pt->Measuring=false;_endthread();}
	if (!pt->MULTI->SetMode("VOLT:DC"))
		{sprintf (pt->last_err,"Cant Set mode to VOLT:DC on MULTI\n");pt->Measuring=false;_endthread();}
	
	if (!pt->MULTI->SetIntegrationTime(t2))
		{sprintf (pt->last_err,"Cant Set Integration time to %lf on MULTI\n",t2 );pt->Measuring=false;_endthread();}
	if (!pt->MULTI->SetIntegration(1))
		{sprintf (pt->last_err,"Cant Set Integration  to ON on MULTI\n" );pt->Measuring=false;_endthread();}
	if (!pt->MULTI->SetBuffer((int)pt->PimStruct.nb*2))
		{sprintf (pt->last_err,"Cant Set buffer size  to %d on MULTI\n",pt->PimStruct.nb*2 );pt->Measuring=false;_endthread();}
	if (!pt->MULTI->SetTriggerCount((int)pt->PimStruct.nb*2))
		{sprintf (pt->last_err,"Cant Set trigger Count  to %d on MULTI\n",pt->PimStruct.nb*2 );pt->Measuring=false;_endthread();}
	if (!pt->MULTI->SetDisplay(0))
		{sprintf (pt->last_err,"Cant Set Display  to %d on MULTI\n",0 );pt->Measuring=false;_endthread();}
	if (!pt->MULTI->LeaveIddle())
		{sprintf(pt->last_err, "Cant Leave Iddle Mode on MULTI\n"); pt->Measuring = false; _endthread();}
	if (pt->NOISE_MULTI != NULL)
	{
		if (!pt->NOISE_MULTI->EnterIddle())
			{sprintf(pt->last_err, "Error in NOISE_MULTI EnterIddle \n"); pt->Measuring = false; _endthread();}
		if (!pt->NOISE_MULTI->SetTrigSource("BUS"))
			{sprintf(pt->last_err, "Cant Set BUS Trig Source on NOISE_MULTI\n"); pt->Measuring = false; _endthread();}
		if (!pt->NOISE_MULTI->SetMode("VOLT:DC"))
			{sprintf(pt->last_err, "Cant Set mode to VOLT:DC on NOISE_MULTI\n"); pt->Measuring = false; _endthread();}
		if (!pt->NOISE_MULTI->SetIntegrationTime(t2))
			{sprintf(pt->last_err, "Cant Set Integration time to %lf on NOISE_MULTI\n", t2); pt->Measuring = false; _endthread();}
		if (!pt->NOISE_MULTI->SetIntegration(1))
			{sprintf(pt->last_err, "Cant Set Integration  to ON on NOISE_MULTI\n"); pt->Measuring = false; _endthread();}
		if (!pt->NOISE_MULTI->SetBuffer((int)pt->PimStruct.nb * 2))
			{sprintf(pt->last_err, "Cant Set buffer size  to %d on NOISE_MULTI\n", pt->PimStruct.nb * 2); pt->Measuring = false; _endthread();}
		if (!pt->NOISE_MULTI->SetTriggerCount((int)pt->PimStruct.nb * 2))
			{sprintf(pt->last_err, "Cant Set trigger Count  to %d on NOISE_MULTI\n", pt->PimStruct.nb * 2); pt->Measuring = false; _endthread();}
		if (!pt->NOISE_MULTI->SetDisplay(0))
			{sprintf(pt->last_err, "Cant Set Display  to %d on NOISE_MULTI\n", 0); pt->Measuring = false; _endthread();}
		if (!pt->NOISE_MULTI->LeaveIddle())
			{sprintf(pt->last_err, "Cant Leave Iddle Mode on MULTI\n"); pt->Measuring = false; _endthread();}

	}
	if(pt->AbortRequest)
		{sprintf (pt->last_err,"Abort requested \n" );pt->Measuring=false;_endthread();}
	if(!(pt->XPS->MoveAbs(pt->PimStruct.X-XLen/2,pt->PimStruct.Z+ZLen/2,1)))
		{sprintf (pt->last_err,"Cant Move XPS to Start Position \n" );pt->Measuring=false;_endthread();}

	if(pt->AbortRequest)
		{sprintf (pt->last_err,"Abort requested \n" );pt->Measuring=false;_endthread();}
	
	for (int i=0;i<pt->PimStruct.nb;i++)
	{
		if (pt->NOISE_MULTI != NULL)
			if (!pt->NOISE_MULTI->Trig())
			{sprintf(pt->last_err, "Cant Trig on NOISE_MULTI\n"); pt->Measuring = false; _endthread();}
		if (!pt->MULTI->Trig())
			{sprintf (pt->last_err,"Cant Trig on MULTI\n" );pt->Measuring=false;_endthread();}
		if(!(pt->XPS->MoveAbs(pt->PimStruct.X+XLen/2,pt->PimStruct.Z-ZLen/2,1)))
			{sprintf (pt->last_err,"Cant Move XPS to Beg Position \n" );pt->Measuring=false;_endthread();}
		do
		{
			Sleep(50);
			if(pt->AbortRequest)
				{sprintf (pt->last_err,"Abort requested \n" );pt->Measuring=false;_endthread();}

		}
		while(pt->MULTI->GetReadBufferPoint()<i*2+1);
		if (pt->NOISE_MULTI!=NULL)
			if (!pt->NOISE_MULTI->Trig())
				{sprintf(pt->last_err, "Cant Trig on MULTI\n"); pt->Measuring = false; _endthread();}
		if (!pt->MULTI->Trig())
			{sprintf (pt->last_err,"Cant Trig on MULTI\n" );pt->Measuring=false;_endthread();}
		if(!(pt->XPS->MoveAbs(pt->PimStruct.X-XLen/2,pt->PimStruct.Z+ZLen/2,1)))
			{sprintf (pt->last_err,"Cant Move XPS to end Position \n" );pt->Measuring=false;_endthread();}
		do
		{
			if(pt->AbortRequest)
				{sprintf (pt->last_err,"Abort requested \n" );pt->Measuring=false;_endthread();}
			Sleep(50);
		}
		while(pt->MULTI->GetReadBufferPoint()<i*2+2);
	}
	
	if (!pt->MULTI->GetBufferData(pt->MultiData,(int)pt->PimStruct.nb*2))
		{sprintf (pt->last_err,"Cant %d GetBufferData on MULTI\n",pt->PimStruct.nb*2 );pt->Measuring=false;_endthread();}
	if (pt->NOISE_MULTI != NULL)
	{
		if (!pt->NOISE_MULTI->GetBufferData(pt->NoiseData, (int)pt->PimStruct.nb * 2))
		{
			sprintf(pt->last_err, "Cant %d GetBufferData on MULTI\n", pt->PimStruct.nb * 2); pt->Measuring = false; _endthread();
		}
	}
	if(pt->AbortRequest)
		{sprintf (pt->last_err,"Abort requested \n" );pt->Measuring=false;_endthread();}
	double avg;
	int imax,imin;
	double maxVal,minVal;
	imax=0;imin=0;
	avg=0;minVal=1e99;maxVal=-1e99;
	pt->result=(double *)realloc(pt->result,pt->PimStruct.nb*sizeof(double));	
	if (pt->NOISE_MULTI != NULL) 
		pt->noise= (double *)realloc(pt->noise, pt->PimStruct.nb * sizeof(double));
	pt->X_result=(double *)realloc(pt->X_result,pt->PimStruct.nb*sizeof(double));
	pt->Z_result=(double *)realloc(pt->Z_result,pt->PimStruct.nb*sizeof(double));
	pt->T_result=(double *)realloc(pt->T_result,pt->PimStruct.nb*sizeof(double));
	pt->VX_result=(double *)realloc(pt->VX_result,pt->PimStruct.nb*sizeof(double));
	pt->VZ_result=(double *)realloc(pt->VZ_result,pt->PimStruct.nb*sizeof(double));

	for(int i=0 ;i<pt->PimStruct.nb;i++)
	{

		pt->result[i]=(pt->MultiData[2*i]-pt->MultiData[2*i+1])/2;
		pt->result[i]*=1e8/(pt->PimStruct.Len/10)/1000;
		avg+=pt->result[i];
		if (pt->result[i]>maxVal)
		{
			maxVal=pt->result[i];
			imax=i;
		}
		if(pt->result[i]<minVal)
		{
			minVal=pt->result[i];
			imin=i;
		}
		pt->X_result[i]=pt->PimStruct.X;
		pt->Z_result[i]=pt->PimStruct.Z;
		pt->T_result[i]=pt->PimStruct.Angle;
		pt->VX_result[i]=0;
		pt->VZ_result[i]=0;
	}
	avg/=pt->PimStruct.nb;
	pt->MesLen=pt->PimStruct.nb;
	pt->result[imax]=avg;
	pt->result[imin]=avg;
	if(pt->AbortRequest)
		{sprintf (pt->last_err,"Abort requested \n" );pt->Measuring=false;_endthread();}

	pt->MULTI->SetDisplay(1);
	pt->MULTI->SetTrigSource("IMM");
	pt->MULTI->FreeRun();
	if (pt->NOISE_MULTI != NULL)
	{
		for (int i = 0; i<pt->PimStruct.nb; i++)
		{

			pt->noise[i] = (pt->NoiseData[2 * i] - pt->NoiseData[2 * i + 1]) / 2;
			pt->noise[i] *= 1e8 / (pt->PimStruct.Len / 10) / 1000;
			avg += pt->noise[i];
			if (pt->noise[i]>maxVal)
			{
				maxVal = pt->noise[i];
				imax = i;
			}
			if (pt->noise[i]<minVal)
			{
				minVal = pt->noise[i];
				imin = i;
			}

		}
		avg /= pt->PimStruct.nb;
		pt->MesLen = pt->PimStruct.nb;
		pt->noise[imax] = avg;
		pt->noise[imin] = avg;
		if (pt->AbortRequest)
		{
			sprintf(pt->last_err, "Abort requested \n"); pt->Measuring = false; _endthread();
		}

		pt->NOISE_MULTI->SetDisplay(1);
		pt->NOISE_MULTI->SetTrigSource("IMM");
		pt->NOISE_MULTI->FreeRun();
	}


	pt->DataAvailable=true;
	pt->Measuring=false;

	_endthread();
}

bool measure::IntegralMeas(double X,double Z,double Len,double Angle,double V , double Acc,double nb, double tend)
{
	if ( IsMeasuring())
	{
		sprintf(last_err,"Can't run IntegralMeas. Another measurement is running\n");
		return false;
	}
	sprintf(last_err, "No Error\n");	
	DataAvailable=false;
	Measuring=true;
	PimStruct.X=X;PimStruct.Z=Z;
	PimStruct.Len=Len;PimStruct.Angle=Angle;
	PimStruct.V=V;PimStruct.Acc=Acc;
	PimStruct.nb=(int)nb;
	PimStruct.tend = tend;
	if(_beginthread(BackIntegralMeas,0,(void *)this)==-1)
	{
		return (false);
	}	
	return (true);

}

static void BackDoubleIntegralMeas(void* p)
{
	measure *pt;
	pt=(measure *)p;
//	Follow Jmeas Procedur describe in MeasProc File
	if (pt->AbortRequest)
	{
		if (pt->NOISE_MULTI != NULL)
		{
			delete pt->NOISE_MULTI;
			pt->NOISE_MULTI = new Keithley(pt->NoiseMulti);
		}
		delete pt->MULTI;
		pt->MULTI=new Keithley(pt->MultiName);
	}
	pt->AbortRequest=false;
	double XLen=pt->PimStruct.Len*sin(pt->PimStruct.Angle);
	double ZLen=pt->PimStruct.Len*cos(pt->PimStruct.Angle);

	double ti=pt->XPS->TimeToMove(pt->PimStruct.Len,pt->PimStruct.V,pt->PimStruct.Acc);
	double tend = pt->PimStruct.tend;
	double V1;
	if (fabs(XLen)>fabs(ZLen))
		V1=fabs(XLen)/pt->PimStruct.Len*pt->PimStruct.V;
	else
		V1=fabs(ZLen)/pt->PimStruct.Len*pt->PimStruct.V;

	if(!(pt->XPS->SetVelocity(V1,V1)))
		{sprintf (pt->last_err,"Error in Set Velocity to  %lf mm/s\n",V1 );pt->Measuring=false;_endthread();}
	if(!(pt->XPS->SetAcceleration(pt->PimStruct.Acc,pt->PimStruct.Acc)))
		{sprintf (pt->last_err,"Error in Set Acceleration to  %lf mm/s2\n",pt->PimStruct.Acc );pt->Measuring=false;_endthread();}

	double t2 = ceil(ti / 0.02)*.02 + tend;

	if(!pt->MULTI->EnterIddle())
		{sprintf (pt->last_err,"Error in Multi EnterIddle\n" );pt->Measuring=false;_endthread();}
	if (!pt->MULTI->SetTrigSource("BUS"))
		{sprintf (pt->last_err,"Cant Set BUS Trig Source on MULTI\n" );pt->Measuring=false;_endthread();}
	if (!pt->MULTI->SetMode("VOLT:DC"))
		{sprintf (pt->last_err,"Cant Set mode to VOLT:DC on MULTI\n");pt->Measuring=false;_endthread();}
	if (!pt->MULTI->SetIntegrationTime(t2))
		{sprintf (pt->last_err,"Cant Set Integration time to %lf on MULTI\n",t2 );pt->Measuring=false;_endthread();}
	if (!pt->MULTI->SetIntegration(1))
		{sprintf (pt->last_err,"Cant Set Integration  to ON on MULTI\n" );pt->Measuring=false;_endthread();}
	if (!pt->MULTI->SetBuffer((int)pt->PimStruct.nb*2))
		{sprintf (pt->last_err,"Cant Set buffer size  to %d on MULTI\n",pt->PimStruct.nb*2 );pt->Measuring=false;_endthread();}
	if (!pt->MULTI->SetTriggerCount((int)pt->PimStruct.nb*2))
		{sprintf (pt->last_err,"Cant Set trigger Count  to %d on MULTI\n",pt->PimStruct.nb*2 );pt->Measuring=false;_endthread();}
	if (!pt->MULTI->SetDisplay(0))
		{sprintf (pt->last_err,"Cant Set Display  to %d on MULTI\n",0 );pt->Measuring=false;_endthread();}
	if(!(pt->XPS->MoveAbs(pt->PimStruct.X,pt->PimStruct.Z,1)))
		{sprintf (pt->last_err,"Cant Move XPS to Start Position \n" );pt->Measuring=false;_endthread();}
	if(pt->AbortRequest)
		{sprintf (pt->last_err,"Abort requested \n" );pt->Measuring=false;_endthread();}
	if (!pt->MULTI->LeaveIddle())
		{sprintf (pt->last_err,"Cant Leave Iddle Mode on MULTI\n" );pt->Measuring=false;_endthread();}
	if (pt->NOISE_MULTI != NULL)
	{
		if (!pt->NOISE_MULTI->EnterIddle())
			{sprintf(pt->last_err, "Error in NOISE_MULTI EnterIddle\n"); pt->Measuring = false; _endthread();}
		if (!pt->NOISE_MULTI->SetTrigSource("BUS"))
			{sprintf(pt->last_err, "Cant Set BUS Trig Source on NOISE_MULTI\n"); pt->Measuring = false; _endthread();}
		if (!pt->NOISE_MULTI->SetMode("VOLT:DC"))
			{sprintf(pt->last_err, "Cant Set mode to VOLT:DC on NOISE_MULTI\n"); pt->Measuring = false; _endthread();}
		if (!pt->NOISE_MULTI->SetIntegrationTime(t2))
			{sprintf(pt->last_err, "Cant Set Integration time to %lf on NOISE_MULTI\n", t2); pt->Measuring = false; _endthread();}
		if (!pt->NOISE_MULTI->SetIntegration(1))
			{sprintf(pt->last_err, "Cant Set Integration  to ON on NOISE_MULTI\n"); pt->Measuring = false; _endthread();}
		if (!pt->NOISE_MULTI->SetBuffer((int)pt->PimStruct.nb * 2))
			{sprintf(pt->last_err, "Cant Set buffer size  to %d on NOISE_MULTI\n", pt->PimStruct.nb * 2); pt->Measuring = false; _endthread();}
		if (!pt->NOISE_MULTI->SetTriggerCount((int)pt->PimStruct.nb * 2))
			{sprintf(pt->last_err, "Cant Set trigger Count  to %d on NOISE_MULTI\n", pt->PimStruct.nb * 2); pt->Measuring = false; _endthread();}
		if (!pt->NOISE_MULTI->SetDisplay(0))
			{sprintf(pt->last_err, "Cant Set Display  to %d on NOISE_MULTI\n", 0); pt->Measuring = false; _endthread();}		
		if (!pt->NOISE_MULTI->LeaveIddle())
			{sprintf(pt->last_err, "Cant Leave Iddle Mode on NOISE_MULTI\n"); pt->Measuring = false; _endthread();}
	}
	if (!(pt->XPS->MoveAbs(pt->PimStruct.X, pt->PimStruct.Z, 1)))
	{
		sprintf(pt->last_err, "Cant Move XPS to Start Position \n"); pt->Measuring = false; _endthread();
	}
	if (pt->AbortRequest)
	{
		sprintf(pt->last_err, "Abort requested \n"); pt->Measuring = false; _endthread();
	}
	if(!(pt->XPS->MasterMoveRel(-XLen/4,+ZLen/4,1)))
		{sprintf (pt->last_err,"Cant Move Master XPS to Start Position \n" );pt->Measuring=false;_endthread();}
	if(pt->AbortRequest)
		{sprintf (pt->last_err,"Abort requested \n" );pt->Measuring=false;_endthread();}

	for (int i=0;i<pt->PimStruct.nb;i++)
	{
		if (pt->NOISE_MULTI!=NULL)
			if (!pt->NOISE_MULTI->Trig())
			{sprintf(pt->last_err, "Cant Trig on MULTI\n"); pt->Measuring = false; _endthread();}
		if (!pt->MULTI->Trig())
			{sprintf (pt->last_err,"Cant Trig on MULTI\n" );pt->Measuring=false;_endthread();}
		if(!(pt->XPS->MasterMoveRel(XLen/2,-ZLen/2,1)))
			{sprintf (pt->last_err,"Cant Move Master XPS to Loop First  Position \n" );pt->Measuring=false;_endthread();}
		do
		{
			if(pt->AbortRequest)
				{sprintf (pt->last_err,"Abort requested \n" );pt->Measuring=false;_endthread();}
			Sleep(50);
		}
		while(pt->MULTI->GetReadBufferPoint()<i*2+1);
		if (pt->NOISE_MULTI!=NULL)
			if (!pt->NOISE_MULTI->Trig())
				{sprintf(pt->last_err, "Cant Trig on NOISE_MULTI\n"); pt->Measuring = false; _endthread();}
		if (!pt->MULTI->Trig())
			{sprintf (pt->last_err,"Cant Trig on MULTI\n" );pt->Measuring=false;_endthread();}
		if(!(pt->XPS->MasterMoveRel(-XLen/2,ZLen/2,1)))
			{sprintf (pt->last_err,"Cant Move Master XPS to  Loop Second Position \n" );pt->Measuring=false;_endthread();}
		do
		{
			if(pt->AbortRequest)
				{sprintf (pt->last_err,"Abort requested \n" );pt->Measuring=false;_endthread();}
			Sleep(50);
		}
		while(pt->MULTI->GetReadBufferPoint()<i*2+2);

	}
	if(!(pt->XPS->MasterMoveRel(XLen/4,-ZLen/4,1)))
		{sprintf (pt->last_err,"Cant Move Master XPS to Initial Position \n" );pt->Measuring=false;_endthread();}
	if(pt->AbortRequest)
		{sprintf (pt->last_err,"Abort requested \n" );pt->Measuring=false;_endthread();}

	if (!pt->MULTI->GetBufferData(pt->MultiData,(int)pt->PimStruct.nb*2))
		{sprintf (pt->last_err,"Cant %d GetBufferData on MULTI\n",pt->PimStruct.nb*2 );pt->Measuring=false;_endthread();}
	if (pt->NOISE_MULTI != NULL)
	{
		if (!pt->NOISE_MULTI->GetBufferData(pt->NoiseData, (int)pt->PimStruct.nb * 2))
			{sprintf(pt->last_err, "Cant %d GetBufferData on NOISE_MULTI\n", pt->PimStruct.nb * 2); pt->Measuring = false; _endthread();}

	}

	if(pt->AbortRequest)
		{sprintf (pt->last_err,"Abort requested \n" );pt->Measuring=false;_endthread();}

		double avg;
	int imax,imin;
	double maxVal,minVal;
	imax=0;imin=0;
	avg=0;minVal=1e99;maxVal=-1e99;
	pt->result=(double *)realloc(pt->result,pt->PimStruct.nb*sizeof(double));
	if (pt->NOISE_MULTI != NULL) 
	{
		pt->noise = (double *)realloc(pt->noise, pt->PimStruct.nb * sizeof(double));
		pt->noise[0] = (double)nanf("nan");
	}
	pt->X_result=(double *)realloc(pt->X_result,pt->PimStruct.nb*sizeof(double));
	pt->Z_result=(double *)realloc(pt->Z_result,pt->PimStruct.nb*sizeof(double));
	pt->T_result=(double *)realloc(pt->T_result,pt->PimStruct.nb*sizeof(double));
	pt->VX_result=(double *)realloc(pt->VX_result,pt->PimStruct.nb*sizeof(double));
	pt->VZ_result=(double *)realloc(pt->VZ_result,pt->PimStruct.nb*sizeof(double));

	for(int i=0 ;i<pt->PimStruct.nb;i++)
	{
		pt->result[i]=(pt->MultiData[2*i]-pt->MultiData[2*i+1])/2;
		pt->result[i]*=1e8/(pt->PimStruct.Len/10) / 1000 * pt->WireLength;
		avg+=pt->result[i];
		if (pt->result[i]>maxVal)
		{
			maxVal=pt->result[i];
			imax=i;
		}
		if(pt->result[i]<minVal)
		{
			minVal=pt->result[i];
			imin=i;
		}
		pt->X_result[i]=pt->PimStruct.X;
		pt->Z_result[i]=pt->PimStruct.Z;
		pt->T_result[i]=pt->PimStruct.Angle;
		pt->VX_result[i]=0;
		pt->VZ_result[i]=0;

	}
	avg/=pt->PimStruct.nb;
	pt->MesLen=pt->PimStruct.nb;
	pt->result[imax]=avg;
	pt->result[imin]=avg;
	if(pt->AbortRequest)
		{sprintf (pt->last_err,"Abort requested \n" );pt->Measuring=false;_endthread();}

	pt->MULTI->SetDisplay(1);
	pt->MULTI->SetTrigSource("IMM");
	pt->MULTI->FreeRun();
	if (pt->NOISE_MULTI != NULL)
	{
		for (int i = 0; i<pt->PimStruct.nb; i++)
		{
			pt->noise[i] = (pt->NoiseData[2 * i] - pt->NoiseData[2 * i + 1]) / 2;
			pt->noise[i] *= 1e8 / (pt->PimStruct.Len / 10) / 1000 * pt->WireLength;
			avg += pt->noise[i];
			if (pt->noise[i]>maxVal)
			{
				maxVal = pt->noise[i];
				imax = i;
			}
			if (pt->noise[i]<minVal)
			{
				minVal = pt->noise[i];
				imin = i;
			}

		}
		avg /= pt->PimStruct.nb;
		pt->MesLen = pt->PimStruct.nb;
		pt->noise[imax] = avg;
		pt->noise[imin] = avg;
		if (pt->AbortRequest)
			{sprintf(pt->last_err, "Abort requested \n"); pt->Measuring = false; _endthread();}

		pt->NOISE_MULTI->SetDisplay(1);
		pt->NOISE_MULTI->SetTrigSource("IMM");
		pt->NOISE_MULTI->FreeRun();
	}

	pt->DataAvailable=true;
	pt->Measuring=false;

	_endthread();
}
bool measure::DoubleIntegralMeas(double X,double Z,double Len,double Angle,double V , double Acc,double nb)
{
	if ( IsMeasuring())
	{
		sprintf(last_err,"Can't Run DoubleIntegralMeas Another Measure is running\n");
		return false;
	}
	sprintf (last_err,"No Error\n");
	DataAvailable=false;
	Measuring=true;
	PimStruct.X=X;PimStruct.Z=Z;
	PimStruct.Len=Len;PimStruct.Angle=Angle;
	PimStruct.V=V;PimStruct.Acc=Acc;
	PimStruct.nb=(int)nb;
	if(_beginthread(BackDoubleIntegralMeas,0,(void *)this)==-1)
	{
		return (false);
	}	
	return (true);

}

int measure::GetLastMes(double **res,double **noi,double **Xres,double **Zres,double **Tres,double **QCres,double **QTres, double **VXres, double **VZres, double **Ares)
{
	if (IsMeasuring())
	{
		if (strstr(&last_err[0],"Wait End of Current Measure Please\n")==NULL)
			strcat (&last_err[0],"Wait End of Current Measure Please\n");
	}
	if (DataAvailable)
	{
		*res=result;
		*noi = noise;
		*Xres=X_result;
		*Zres=Z_result;
		*Tres=T_result;
		*QCres = QC_result;
		*QTres = QT_result;
		*VXres = VX_result;
		*VZres = VZ_result;
		*Ares = A_result;
//	sprintf (last_err,"No Error\n");	
		suppr_chaine(&last_err[0],"No Data Available\n");
		suppr_chaine(&last_err[0],"Wait End of Current Measure Please\n");
		return MesLen;
	}
	else
	{
		
		if (strstr(&last_err[0],"No Data Available\n")==NULL)
			strcat (&last_err[0],"No Data Available\n");
		return 0;
	}
}

bool measure::SetMesType(int type)
{
	LastMesType=type;
	return true;
}

int measure::GetMesType()
{
	return LastMesType;
}
static void BackLinearIntegralScan(void* p)
{
	measure *pt;
	pt=(measure *)p;
	if (pt->AbortRequest)
	{
		if (pt->NOISE_MULTI != NULL)
		{
			delete pt->NOISE_MULTI;
			pt->NOISE_MULTI = new Keithley(pt->NoiseMulti);
		}
		delete pt->MULTI;
		pt->MULTI=new Keithley(pt->MultiName);
	}
	pt->AbortRequest=false;

	double measlen=sqrt(pow(pt->LISStruct.XE-pt->LISStruct.XB,2)+pow(pt->LISStruct.ZE-pt->LISStruct.ZB,2));
	double step=measlen/(pt->LISStruct.NBpt-1);
	double XStep=(pt->LISStruct.XE-pt->LISStruct.XB)/(pt->LISStruct.NBpt-1);
	double ZStep=(pt->LISStruct.ZE-pt->LISStruct.ZB)/(pt->LISStruct.NBpt-1);
	double  V=pt->rap_step_len*step/pt->LISStruct.TI;
	double  tacc=V/pt->LISStruct.Acc;
	//double dacc=1.5*V*tacc;
	double dacc = 0.6*V*tacc;
	double TI1=pt->LISStruct.TI;
	if (V>pt->vmax)
	{
		sprintf (pt->last_err,"Velocity : %lf too HIGH ( Max Velocity= %lf) \n ",V,pt->vmax);
		TI1=ceil((pt->rap_step_len*step/pt->vmax )/0.02)*0.02;
		V=pt->rap_step_len*step/TI1;
		char suite[100];
		sprintf(suite,"Integration time set to %lf     Velocity Set to %lf \n", TI1 ,V);
		strcat (pt->last_err,suite);
	}
	double ZD=pt->LISStruct.ZB-((pt->LISStruct.ZE-pt->LISStruct.ZB)*(dacc+pt->rap_step_len/2*step)/measlen);
	double XD=pt->LISStruct.XB-((pt->LISStruct.XE-pt->LISStruct.XB)*(dacc+pt->rap_step_len/2*step)/measlen);
	double ZF=pt->LISStruct.ZE+((pt->LISStruct.ZE-pt->LISStruct.ZB)*(dacc+pt->rap_step_len/2*step)/measlen);
	double XF=pt->LISStruct.XE+((pt->LISStruct.XE-pt->LISStruct.XB)*(dacc+pt->rap_step_len/2*step)/measlen);

	if(!(pt->XPS->MoveAbs(XD,ZD,1)))
		{sprintf (pt->last_err,"Cant Move XPS to Start Position \n" );pt->Measuring=false;_endthread();}
	if(pt->AbortRequest)
		{sprintf (pt->last_err,"Abort requested \n");pt->Measuring=false;_endthread();}

	if(!pt->MULTI->EnterIddle())
		{sprintf (pt->last_err,"Error in Multi EnterIddle \n" );pt->Measuring=false;_endthread();}
	if (!pt->MULTI->SetTrigSource("EXT"))
		{sprintf (pt->last_err,"Cant Set EXT Trig Source on MULTI\n" );pt->Measuring=false;_endthread();}
	if (!pt->MULTI->SetMode("VOLT:DC"))
		{sprintf (pt->last_err,"Cant Set mode to VOLT:DC on MULTI\n");pt->Measuring=false;_endthread();}
	if (!pt->MULTI->SetIntegrationTime(TI1))
		{sprintf (pt->last_err,"Cant Set Integration time to %lf on MULTI\n",TI1 );pt->Measuring=false;_endthread();}
	Sleep(100);
	if (!pt->MULTI->SetDisplay(0))
		{sprintf (pt->last_err,"Cant Set Display  to %d on MULTI\n",0 );pt->Measuring=false;_endthread();}
	if (!pt->MULTI->SetLowPass(1))
		{sprintf (pt->last_err,"Cant Set LowPass  to %d on MULTI\n",1 );pt->Measuring=false;_endthread();}
	if (!pt->MULTI->SetIntegration(1))
		{sprintf (pt->last_err,"Cant Set Integration  to ON on MULTI\n" );pt->Measuring=false;_endthread();}

	if (!pt->MULTI->SetBuffer((int)pt->LISStruct.NBpt*2))
		{sprintf (pt->last_err,"Cant Set buffer size  to %lf on MULTI\n",pt->LISStruct.NBpt*2 );pt->Measuring=false;_endthread();}
	if (!pt->MULTI->SetTriggerCount((int)pt->LISStruct.NBpt*2))
		{sprintf (pt->last_err,"Cant Set trigger Count  to %lf on MULTI\n",pt->LISStruct.NBpt*2 );pt->Measuring=false;_endthread();}
	if (!pt->MULTI->LeaveIddle())
		{sprintf (pt->last_err,"Cant Leave Iddle Mode on MULTI\n" );pt->Measuring=false;_endthread();}
	if (pt->NOISE_MULTI != NULL)
	{
		if (!pt->NOISE_MULTI->EnterIddle())
			{sprintf(pt->last_err, "Error in NOISE_MULTI EnterIddle \n"); pt->Measuring = false; _endthread();}
		if (!pt->NOISE_MULTI->SetTrigSource("EXT"))
			{sprintf(pt->last_err, "Cant Set EXT Trig Source on NOISE_MULTI\n"); pt->Measuring = false; _endthread();}
		if (!pt->NOISE_MULTI->SetMode("VOLT:DC"))
			{sprintf(pt->last_err, "Cant Set mode to VOLT:DC on NOISE_MULTI\n"); pt->Measuring = false; _endthread();}
		if (!pt->NOISE_MULTI->SetIntegrationTime(TI1))
			{sprintf(pt->last_err, "Cant Set Integration time to %lf on NOISE_MULTI\n", TI1); pt->Measuring = false; _endthread();}
		Sleep(100);
		if (!pt->NOISE_MULTI->SetDisplay(0))
			{sprintf(pt->last_err, "Cant Set Display  to %d on NOISE_MULTI\n", 0); pt->Measuring = false; _endthread();}
		if (!pt->NOISE_MULTI->SetLowPass(1))
			{sprintf(pt->last_err, "Cant Set LowPass  to %d on NOISE_MULTI\n", 1); pt->Measuring = false; _endthread();}
		if (!pt->NOISE_MULTI->SetIntegration(1))
			{sprintf(pt->last_err, "Cant Set Integration  to ON on NOISE_MULTI\n"); pt->Measuring = false; _endthread();}
		if (!pt->NOISE_MULTI->SetBuffer((int)pt->LISStruct.NBpt * 2))
			{sprintf(pt->last_err, "Cant Set buffer size  to %lf on NOISE_MULTI\n", pt->LISStruct.NBpt * 2); pt->Measuring = false; _endthread();}
		if (!pt->NOISE_MULTI->SetTriggerCount((int)pt->LISStruct.NBpt * 2))
			{sprintf(pt->last_err, "Cant Set trigger Count  to %lf on NOISE_MULTI\n", pt->LISStruct.NBpt * 2); pt->Measuring = false; _endthread();}
		if (!pt->NOISE_MULTI->LeaveIddle())
			{sprintf(pt->last_err, "Cant Leave Iddle Mode on NOISE_MULTI\n"); pt->Measuring = false; _endthread();}
	}

	pt->result=(double *)realloc(pt->result,(size_t)pt->LISStruct.NBpt*sizeof(double));
	if (pt->NOISE_MULTI != NULL)
		pt->noise = (double *)realloc(pt->noise, (size_t)pt->LISStruct.NBpt * sizeof(double));
	pt->X_result=(double *)realloc(pt->X_result,(size_t)pt->LISStruct.NBpt*sizeof(double));
	pt->Z_result=(double *)realloc(pt->Z_result,(size_t)pt->LISStruct.NBpt*sizeof(double));
	pt->T_result=(double *)realloc(pt->T_result,(size_t)pt->LISStruct.NBpt*sizeof(double));
	pt->VX_result=(double *)realloc(pt->VX_result,(size_t)pt->LISStruct.NBpt*sizeof(double));
	pt->VZ_result=(double *)realloc(pt->VZ_result,(size_t)pt->LISStruct.NBpt*sizeof(double));
	int ScanNum;
	for ( int i=0;i<pt->LISStruct.NBpt;i++)
	{
		pt->result[i]=0;
		pt->X_result[i]=0;
		pt->Z_result[i]=0;
		pt->T_result[i]=0;
		pt->VX_result[i]=0;
		pt->VZ_result[i]=0;

	}
	if (pt->NOISE_MULTI != NULL)
	{
		for (int i = 0; i<pt->LISStruct.NBpt; i++)
			pt->noise[i] = 0;
	}

	for (ScanNum=0; ScanNum<pt->LISStruct.NBScan;ScanNum++)
	{
			if(!pt->MULTI->EnterIddle())
				{sprintf (pt->last_err,"Error in Multi EnterIddle \n" );pt->Measuring=false;_endthread();}
			if (!pt->MULTI->SetBuffer((int)pt->LISStruct.NBpt*2))
				{sprintf (pt->last_err,"Cant Set buffer size  to %lf on MULTI\n",pt->LISStruct.NBpt*2 );pt->Measuring=false;_endthread();}
			if (!pt->MULTI->SetTriggerCount((int)pt->LISStruct.NBpt*2))
				{sprintf (pt->last_err,"Cant Set trigger Count  to %lf on MULTI\n",pt->LISStruct.NBpt*2 );pt->Measuring=false;_endthread();}
			if (!pt->MULTI->LeaveIddle())
				{sprintf (pt->last_err,"Cant Leave Iddle Mode on MULTI\n" );pt->Measuring=false;_endthread();}
			if (pt->NOISE_MULTI != NULL)
			{
				if (!pt->NOISE_MULTI->EnterIddle())
					{sprintf(pt->last_err, "Error in NOISE_MULTI EnterIddle \n"); pt->Measuring = false; _endthread();}
				if (!pt->NOISE_MULTI->SetBuffer((int)pt->LISStruct.NBpt * 2))
					{sprintf(pt->last_err, "Cant Set buffer size  to %lf on NOISE_MULTI\n", pt->LISStruct.NBpt * 2); pt->Measuring = false; _endthread();}
				if (!pt->NOISE_MULTI->SetTriggerCount((int)pt->LISStruct.NBpt * 2))
					{sprintf(pt->last_err, "Cant Set trigger Count  to %lf on NOISE_MULTI\n", pt->LISStruct.NBpt * 2); pt->Measuring = false; _endthread();}
				if (!pt->NOISE_MULTI->LeaveIddle())
					{sprintf(pt->last_err, "Cant Leave Iddle Mode on NOISE_MULTI\n"); pt->Measuring = false; _endthread();}

			}

			if(!(pt->XPS->LineAndTrig(XD,ZD,dacc,step,(int)pt->LISStruct.NBpt,XF,ZF,V,pt->LISStruct.Acc)))
				{sprintf (pt->last_err,"Cant Line and trig on XPS \n" );pt->Measuring=false;_endthread();}
			do
			{
				if(pt->AbortRequest)
					{sprintf (pt->last_err,"Abort requested \n" );pt->Measuring=false;_endthread();}
				Sleep(100);
			}
			while(!pt->XPS->GatheringDataReady());
			double *XPOS0,*ZPOS0,*VX0,*VZ0;
			int nb_pt0;
			if(!pt->XPS->GetGatheringData(&XPOS0,&ZPOS0,&VX0,&VZ0,&nb_pt0))
				{sprintf (pt->last_err,"Cant GetGathering Data on XPS \n" );pt->Measuring=false;_endthread();}

			if(!(pt->XPS->LineAndTrig(XF,ZF,dacc,step,(int)pt->LISStruct.NBpt,XD,ZD,V,pt->LISStruct.Acc)))
				{sprintf (pt->last_err,"Cant Line and trig on  XPS \n" );pt->Measuring=false;_endthread();}
			do
			{
				if(pt->AbortRequest)
					{sprintf (pt->last_err,"Abort requested \n");pt->Measuring=false;_endthread();}  
				Sleep(100);
			}
			while(!pt->XPS->GatheringDataReady());
			double *XPOS1,*ZPOS1,*VX1,*VZ1;
			int nb_pt1;
			if(!pt->XPS->GetGatheringData(&XPOS1,&ZPOS1,&VX1,&VZ1,&nb_pt1))
				{sprintf (pt->last_err,"Cant GetGathering Data on XPS \n" );pt->Measuring=false;_endthread();}

			if (!pt->MULTI->GetBufferData(pt->MultiData,(int)pt->LISStruct.NBpt*2))
				{sprintf (pt->last_err,"Cant %d GetBufferData on MULTI\n",pt->PimStruct.nb*2 );pt->Measuring=false;_endthread();}
			if (pt->NOISE_MULTI != NULL)
			{
				if (!pt->NOISE_MULTI->GetBufferData(pt->NoiseData, (int)pt->LISStruct.NBpt * 2))
				{
					sprintf(pt->last_err, "Cant %d GetBufferData on NOISE_MULTI\n", pt->PimStruct.nb * 2); pt->Measuring = false; _endthread();
				}
			}
			if(pt->AbortRequest)
				{sprintf (pt->last_err,"Abort requested \n" );pt->Measuring=false;_endthread();}
			int i,j;
			double XP0,ZP0,XP1,ZP1;
			for (i=0;i<nb_pt1/2;i++)
			{
				// treat return of Position for Scan in Each direction
				XP0=XPOS0[i*2]+XStep*pt->rap_step_len/2 ;
				ZP0=ZPOS0[i*2]+ZStep*pt->rap_step_len/2 ;
				XP1=XPOS1[nb_pt1-i*2-2]-XStep*pt->rap_step_len/2 ;
				ZP1=ZPOS1[nb_pt1-i*2-2]-ZStep*pt->rap_step_len/2 ;

				pt->X_result[i] += (XP0 + XP1) / 2;
				pt->Z_result[i] += (ZP0 + ZP1) / 2;
				pt->result[i] += (pt->MultiData[i] - pt->MultiData[(int)pt->LISStruct.NBpt * 2 - i - 1]) / (2 * pt->rap_step_len*step*1e-9) / 1000;

				pt->T_result[i] = atan2(pt->LISStruct.XE - pt->LISStruct.XB, pt->LISStruct.ZB - pt->LISStruct.ZE);

			//	pt->X_result[i]+=(XP0+XP1)/2;
			//	pt->Z_result[i]+=(ZP0+ZP1)/2;
				//	pt->result[i] += (pt->MultiData[i] - pt->MultiData[(int)pt->LISStruct.NBpt * 2 - i - 1]) / (2 * pt->rap_step_len*step*1e-9)/i/1000;
				pt->T_result[i]=atan2(pt->LISStruct.XE-pt->LISStruct.XB,pt->LISStruct.ZB-pt->LISStruct.ZE);// reverse Z and X to have orthogonal direction
				
			}
			if (pt->NOISE_MULTI != NULL)
			{
				for (j = 0; j<nb_pt1 / 2; j++)
					pt->noise[j] += (pt->NoiseData[j] - pt->NoiseData[(int)pt->LISStruct.NBpt * 2 - j - 1]) / (2 * pt->rap_step_len*step*1e-9) / 1000;
			}
	}
	for ( int i=0;i<pt->LISStruct.NBpt;i++)
	{
		pt->result[i]/=pt->LISStruct.NBScan;
		pt->X_result[i]/=pt->LISStruct.NBScan;
		pt->Z_result[i]/=pt->LISStruct.NBScan;
	}
	if (pt->NOISE_MULTI != NULL)
	{
		for (int i = 0; i<pt->LISStruct.NBpt; i++)
		pt->noise[i] /= pt->LISStruct.NBScan;
	}
	if(pt->AbortRequest)
		{sprintf (pt->last_err,"Abort requested \n" );pt->Measuring=false;_endthread();}
	pt->MesLen=(int)pt->LISStruct.NBpt;

	pt->MULTI->SetDisplay(1);
	pt->MULTI->SetTrigSource("IMM");
	pt->MULTI->FreeRun();
	if (pt->NOISE_MULTI != NULL)
	{
		pt->NOISE_MULTI->SetDisplay(1);
		pt->NOISE_MULTI->SetTrigSource("IMM");
		pt->NOISE_MULTI->FreeRun();

	}
	pt->DataAvailable=true;
	pt->Measuring=false;
	_endthread();
}

bool measure::LinearIntegralScan(double XB,double ZB,double XE,double ZE,double NBpt,double TI,double Acc,double NBScan)
{
	if ( IsMeasuring())
	{
		sprintf(last_err,"Can't Run LinearIntegraleScan Another Measure is running\n");
		return false;
	}
	sprintf (last_err,"No Error\n");
	DataAvailable=false;
	Measuring=true;
	// Fill Lineare Intergrale Scan struct to pass as parameter through this
	LISStruct.XB=XB;LISStruct.ZB=ZB;
	LISStruct.XE=XE;LISStruct.ZE=ZE;
	LISStruct.TI=TI;LISStruct.Acc=Acc;
	LISStruct.NBpt=(int)NBpt;
	LISStruct.NBScan=(int)NBScan;


	if(_beginthread(BackLinearIntegralScan,0,(void *)this)==-1)
	{
		return (false);
	}	
	return (true);

}

static void BackCircularIntegralScan(void* p)
{
	measure *pt;
	pt=(measure *)p;
	if (pt->AbortRequest)
	{
		if (pt->NOISE_MULTI != NULL)
		{
			delete pt->NOISE_MULTI;
			pt->NOISE_MULTI = new Keithley(pt->NoiseMulti);
		}
		delete pt->MULTI;
		pt->MULTI=new Keithley(pt->MultiName);
	}
	pt->AbortRequest=false;
	double measlen=2*M_PI*pt->CISStruct.R;
	double step=measlen/pt->CISStruct.NBpt;
	double  stepdeg=360/pt->CISStruct.NBpt;
//	double V=pt->rap_step_len*step/pt->CISStruct.TI;
	double V = pt->CISStruct.Velocity;
	double  tacc=V/pt->CISStruct.Acc;
	double  dacc=1.5*V*tacc/(2);
	double OBEG=-45;
	double OEND=360*pt->CISStruct.NBturn+90;
	double TI1=pt->CISStruct.TI;
	int TotalTrigs=(int)(pt->CISStruct.NBpt*pt->CISStruct.NBturn);
	int driftOrder = (int)(pt->CISStruct.driftOrder);

	double V_rap = V *pt->CISStruct.TI / step;
	if (V_rap > 0.8)
	{
		sprintf(pt->last_err, "Error: Velocity * Integration Time * number of points / (2 * pi * R ) > 0.8  \n ");
		//V = 0.8 * step / TI1;
		//char suite[100];
		//sprintf(suite, "Velocity set to %lf\n", V);
		//strcat(pt->last_err, suite);
		//sprintf(suite, "Measurement Stopped %f %f \n", pt->CISStruct.TI, V);
		//strcat(pt->last_err, suite);
		pt->DataAvailable = false;
		pt->Measuring = false;
		_endthread();
	}
	if (V>pt->vmax)
	{
		sprintf (pt->last_err,"Velocity : %lf too HIGH ( Max Velocity= %lf ) \n ",V,pt->vmax);
		TI1=ceil((pt->rap_step_len*step/pt->vmax )/0.02)*0.02;
		V=pt->rap_step_len*step/TI1;
		char suite[100];
		sprintf(suite,"Integration time set to %lf.  Velocity Set to %lf\n", TI1 ,V);
		strcat (pt->last_err,suite);
	}

	if(!pt->MULTI->EnterIddle())
		{sprintf (pt->last_err,"Error in Multi EnterIddle \n" );pt->Measuring=false;_endthread();}
	if (!pt->MULTI->SetTrigSource("EXT"))
		{sprintf (pt->last_err,"Cant Set EXT Trig Source on MULTI\n" );pt->Measuring=false;_endthread();}
	if (!pt->MULTI->SetMode("VOLT:DC"))
		{sprintf (pt->last_err,"Cant Set mode to VOLT:DC on MULTI\n");pt->Measuring=false;_endthread();}
	if (!pt->MULTI->SetIntegrationTime(TI1))
		{sprintf (pt->last_err,"Cant Set Integration time to %lf on MULTI\n",TI1 );pt->Measuring=false;_endthread();}
	Sleep(100);
	if (!pt->MULTI->SetDisplay(0))
		{sprintf (pt->last_err,"Cant Set Display  to %d on MULTI\n",0 );pt->Measuring=false;_endthread();}
	if (!pt->MULTI->SetLowPass(1))
		{sprintf (pt->last_err,"Cant Set LowPass  to %d on MULTI\n",1 );pt->Measuring=false;_endthread();}
	if (!pt->MULTI->SetIntegration(1))
		{sprintf (pt->last_err,"Cant Set Integration  to ON on MULTI\n" );pt->Measuring=false;_endthread();}
	if (!pt->MULTI->SetBuffer((int)(TotalTrigs )))
		{sprintf (pt->last_err,"Cant Set buffer size  to %d on MULTI\n",TotalTrigs );pt->Measuring=false;_endthread();}
	if (!pt->MULTI->SetTriggerCount((int)(TotalTrigs) ))
		{sprintf (pt->last_err,"Cant Set trigger Count  to %d on MULTI\n",TotalTrigs );pt->Measuring=false;_endthread();}
	if (!pt->MULTI->LeaveIddle())
		{sprintf (pt->last_err,"Cant Leave Iddle Mode on MULTI\n" );pt->Measuring=false;_endthread();}
	if (pt->NOISE_MULTI != NULL)
	{
		if (!pt->NOISE_MULTI->EnterIddle())
		{
			sprintf(pt->last_err, "Error in NOISE_MULTI EnterIddle \n"); pt->Measuring = false; _endthread();
		}
		if (!pt->NOISE_MULTI->SetTrigSource("EXT"))
		{
			sprintf(pt->last_err, "Cant Set EXT Trig Source on NOISE_MULTI\n"); pt->Measuring = false; _endthread();
		}
		if (!pt->NOISE_MULTI->SetMode("VOLT:DC"))
		{
			sprintf(pt->last_err, "Cant Set mode to VOLT:DC on NOISE_MULTI\n"); pt->Measuring = false; _endthread();
		}
		if (!pt->NOISE_MULTI->SetIntegrationTime(TI1))
		{
			sprintf(pt->last_err, "Cant Set Integration time to %lf on NOISE_MULTI\n", TI1); pt->Measuring = false; _endthread();
		}
		Sleep(100);
		if (!pt->NOISE_MULTI->SetDisplay(0))
		{
			sprintf(pt->last_err, "Cant Set Display  to %d on NOISE_MULTI\n", 0); pt->Measuring = false; _endthread();
		}
		if (!pt->NOISE_MULTI->SetLowPass(1))
		{
			sprintf(pt->last_err, "Cant Set LowPass  to %d on NOISE_MULTI\n", 1); pt->Measuring = false; _endthread();
		}
		if (!pt->NOISE_MULTI->SetIntegration(1))
		{
			sprintf(pt->last_err, "Cant Set Integration  to ON on NOISE_MULTI\n"); pt->Measuring = false; _endthread();
		}
		if (!pt->NOISE_MULTI->SetBuffer((int)(TotalTrigs)))
		{
			sprintf(pt->last_err, "Cant Set buffer size  to %d on NOISE_MULTI\n", TotalTrigs); pt->Measuring = false; _endthread();
		}
		if (!pt->NOISE_MULTI->SetTriggerCount((int)(TotalTrigs)))
		{
			sprintf(pt->last_err, "Cant Set trigger Count  to %d on NOISE_MULTI\n", TotalTrigs); pt->Measuring = false; _endthread();
		}
		if (!pt->NOISE_MULTI->LeaveIddle())
		{
			sprintf(pt->last_err, "Cant Leave Iddle Mode on NOISE_MULTI\n"); pt->Measuring = false; _endthread();
		}
	}

	if(!pt->XPS->CircleAndTrig(pt->CISStruct.XC,pt->CISStruct.ZC,
								pt->CISStruct.R,OBEG,-stepdeg/2*pt->rap_step_len,stepdeg
								,(int)TotalTrigs,OEND,V,pt->CISStruct.Acc))
		{sprintf (pt->last_err,"Cant CircleAndTrig \n" );pt->Measuring=false;_endthread();}
	do
	{
		if(pt->AbortRequest)
			{sprintf (pt->last_err,"Abort requested \n" );pt->Measuring=false;_endthread();}
		Sleep(100);
	}
	while(!pt->XPS->GatheringDataReady());
	double *XPOS0,*ZPOS0,*XPOS1,*ZPOS1,*VX0,*VX1,*VZ0,*VZ1;
	double BDATA0[1025];double BDATA1[1025];
	double NOISE0[1025], NOISE1[1025];
	int nb_pt0;
	if(!pt->XPS->GetGatheringData(&XPOS0,&ZPOS0,&VX0,&VZ0,&nb_pt0))
		{sprintf (pt->last_err,"Cant GetGathering Data on XPS \n" );pt->Measuring=false;_endthread();}
	if(pt->AbortRequest)
		{sprintf (pt->last_err,"Abort requested \n" );pt->Measuring=false;_endthread();}

	if (!pt->MULTI->GetBufferData(BDATA0,(int)(TotalTrigs)))
		{sprintf (pt->last_err,"Cant %d GetBufferData on MULTI\n",TotalTrigs);pt->Measuring=false;_endthread();}
	if (pt->NOISE_MULTI != NULL)
	{
		if (!pt->NOISE_MULTI->GetBufferData(NOISE0, (int)(TotalTrigs)))
		{
			sprintf(pt->last_err, "Cant %d GetBufferData on NOISE_MULTI\n", TotalTrigs); pt->Measuring = false; _endthread();
		}
	}
	if(pt->AbortRequest)
		{sprintf (pt->last_err,"Abort requested \n" );pt->Measuring=false;_endthread();}

	// Scan dans l'autre sens -------------------------------------------
	if (!pt->MULTI->SetBuffer((int)(TotalTrigs )))
		{sprintf (pt->last_err,"Cant Set buffer size  to %d on MULTI\n",TotalTrigs );pt->Measuring=false;_endthread();}
	if (!pt->MULTI->SetTriggerCount((int)(TotalTrigs) ))
		{sprintf (pt->last_err,"Cant Set trigger Count  to %d on MULTI\n",TotalTrigs );pt->Measuring=false;_endthread();}
	if (!pt->MULTI->LeaveIddle())
		{sprintf (pt->last_err,"Cant Leave Iddle Mode on MULTI\n" );pt->Measuring=false;_endthread();}
	if (pt->NOISE_MULTI != NULL)
	{
		if (!pt->NOISE_MULTI->SetBuffer((int)(TotalTrigs)))
		{
			sprintf(pt->last_err, "Cant Set buffer size  to %d on NOISE_MULTI\n", TotalTrigs); pt->Measuring = false; _endthread();
		}
		if (!pt->NOISE_MULTI->SetTriggerCount((int)(TotalTrigs)))
		{
			sprintf(pt->last_err, "Cant Set trigger Count  to %d on NOISE_MULTI\n", TotalTrigs); pt->Measuring = false; _endthread();
		}
		if (!pt->NOISE_MULTI->LeaveIddle())
		{
			sprintf(pt->last_err, "Cant Leave Iddle Mode on NOISE_MULTI\n"); pt->Measuring = false; _endthread();
		}
	}
	if(!pt->XPS->CircleAndTrig(pt->CISStruct.XC,pt->CISStruct.ZC,
								pt->CISStruct.R,-OBEG,(-360/pt->CISStruct.NBpt)+stepdeg/2*pt->rap_step_len,stepdeg
								,(int)TotalTrigs,-OEND,V,pt->CISStruct.Acc))
		{sprintf (pt->last_err,"Cant CircleAndTrig \n" );pt->Measuring=false;_endthread();}
	do
	{
		if(pt->AbortRequest)
			{sprintf (pt->last_err,"Abort requested \n" );pt->Measuring=false;_endthread();}
		Sleep(100);
	}
	while(!pt->XPS->GatheringDataReady());
	int nb_pt1;
	if(!pt->XPS->GetGatheringData(&XPOS1,&ZPOS1,&VX1,&VZ1,&nb_pt1))
		{sprintf (pt->last_err,"Cant GetGathering Data on XPS \n" );pt->Measuring=false;_endthread();}
	if(pt->AbortRequest)
		{sprintf (pt->last_err,"Abort requested \n" );pt->Measuring=false;_endthread();}
	if (!pt->MULTI->GetBufferData(BDATA1,(int)(TotalTrigs)))
		{sprintf (pt->last_err,"Cant %d GetBufferData on MULTI\n",TotalTrigs);pt->Measuring=false;_endthread();}
	if (pt->NOISE_MULTI != NULL)
	{
		if (!pt->NOISE_MULTI->GetBufferData(NOISE1, (int)(TotalTrigs)))
		{
			sprintf(pt->last_err, "Cant %d GetBufferData on NOISE_MULTI\n", TotalTrigs); pt->Measuring = false; _endthread();
		}
	}
	if(pt->AbortRequest)
		{sprintf (pt->last_err,"Abort requested \n" );pt->Measuring=false;_endthread();}
//ZERO On all Return Value
	pt->result=(double *)realloc(pt->result,TotalTrigs*sizeof(double));
	if (pt->NOISE_MULTI != NULL)
		pt->noise = (double *)realloc(pt->noise, TotalTrigs * sizeof(double));
	pt->X_result=(double *)realloc(pt->X_result,TotalTrigs*sizeof(double));
	pt->Z_result=(double *)realloc(pt->Z_result,TotalTrigs*sizeof(double));
	pt->T_result=(double *)realloc(pt->T_result,TotalTrigs*sizeof(double));
	pt->VX_result=(double *)realloc(pt->VX_result,TotalTrigs*sizeof(double));
	pt->VZ_result=(double *)realloc(pt->VZ_result,TotalTrigs*sizeof(double));

	for (int i=0;i<TotalTrigs;i++)
	{
		pt->result[i]=0; pt->X_result[i]=0;pt->Z_result[i]=0; pt->T_result[i]=0;
	}
	if (pt->NOISE_MULTI != NULL)
	{
		for (int i = 0; i<TotalTrigs; i++)
			pt->noise[i] = 0;
	}
//  DATA TREAT
// Drift correction
	if (driftOrder>0){
		if (pt->CISStruct.NBturn>1)
		{
			pt->DriftCorr(BDATA0, (int)pt->CISStruct.NBpt, (int)pt->CISStruct.NBturn, driftOrder, 0.001);
			pt->DriftCorr(BDATA1, (int)pt->CISStruct.NBpt, (int)pt->CISStruct.NBturn, driftOrder, 0.001);
			
			if (pt->NOISE_MULTI != NULL)
			{
				pt->DriftCorr(NOISE0, (int)pt->CISStruct.NBpt, (int)pt->CISStruct.NBturn, driftOrder, 0.001);
				pt->DriftCorr(NOISE1, (int)pt->CISStruct.NBpt, (int)pt->CISStruct.NBturn, driftOrder, 0.001);
			}
		}
	}

// Accumulation des mesures retournees -------------------
	//double relVelocity0;
	//double relVelocity1;
	for (int i = 0; i < TotalTrigs; i++)
	{
		//relVelocity0 = V / sqrt(pow(VX0[i], 2) + pow(VZ0[i], 2));
		//relVelocity1 = V / sqrt(pow(VX1[TotalTrigs - 1 - i], 2) + pow(VZ1[TotalTrigs - 1 - i], 2));
		//pt->result[i] = -(relVelocity0*BDATA0[i] - relVelocity1*BDATA1[(TotalTrigs - 1 - i)]) / (2 * V * pt->CISStruct.TI * 1e-9)/1000;
		pt->result[i] = -(BDATA0[i] - BDATA1[(TotalTrigs - 1 - i)]) / (2 * V * pt->CISStruct.TI * 1e-9) / 1000;
		pt->VX_result[i] = (VX0[2*i] - VX1[2*(TotalTrigs - 1 - i)])/2;
		pt->VZ_result[i] = (VZ0[2*i] - VZ1[2*(TotalTrigs - 1 - i)])/2;
	}
	if (pt->NOISE_MULTI != NULL)
	{
		for (int i = 0; i < TotalTrigs; i++)
			pt->noise[i] = -(NOISE0[i] - NOISE1[(TotalTrigs - 1 - i)]) / (2 * V * pt->CISStruct.TI * 1e-9) / 1000;
	}
// Correction positions ---------------------------------------------
	double ThetaCorr = pt->rap_step_len*M_PI /pt->CISStruct.NBpt;
	
	std::complex<double> XZPOS[2050];
	std::complex<double> CENTER(pt->CISStruct.XC,pt->CISStruct.ZC);
	std::complex<double> CORR(0,ThetaCorr);
//POS0
	for (int i=0;i<2*TotalTrigs;i++)//Gathering return Negative and positive trig
	{
		std::complex<double> PTCurr(XPOS0[i]-pt->CISStruct.XC,ZPOS0[i]-pt->CISStruct.ZC);
		 XZPOS[i]=PTCurr;
		 XZPOS[i]*=exp(CORR);
		 XZPOS[i]+=CENTER;
		 XPOS0[i]=real(XZPOS[i]);
		 ZPOS0[i]=imag(XZPOS[i]);
		
	}
//POS1
	for (int i=0;i<2*TotalTrigs;i++)//Gathering return Postive and Negative trig
	{
		std::complex<double> PTCurr(XPOS1[i]-pt->CISStruct.XC,ZPOS1[i]-pt->CISStruct.ZC);
		 XZPOS[i]=PTCurr;
		 XZPOS[i]*=(exp(-CORR));
		 XZPOS[i]+=CENTER;
		 XPOS1[i]=real(XZPOS[i]);
		 ZPOS1[i]=imag(XZPOS[i]);
	}
// Accumulation des positions retournees ------------------
	for (int i=0;i<TotalTrigs;i++)
	{
		pt->X_result[i]=(XPOS0[2*i]+XPOS1[2*(TotalTrigs-i-1)])/2;
		pt->Z_result[i]=(ZPOS0[2*i]+ZPOS1[2*(TotalTrigs-i-1)])/2;
	}

// Moyenne sur plusieurs tours ---------------------------------
	for (int j=1;j<pt->CISStruct.NBturn;j++)	// On fait la somme des valeur sur le premier tour --> J=1
	{
		for(int i=0;i<pt->CISStruct.NBpt;i++)
		{
			pt->result[i]+=pt->result[(int)(i+j*pt->CISStruct.NBpt)];
			pt->X_result[i]+=pt->X_result[(int)(i+j*pt->CISStruct.NBpt)];
			pt->Z_result[i]+=pt->Z_result[(int)(i+j*pt->CISStruct.NBpt)];
			pt->VX_result[i] += pt->VX_result[(int)(i + j*pt->CISStruct.NBpt)];
			pt->VZ_result[i] += pt->VZ_result[(int)(i + j*pt->CISStruct.NBpt)];
		}
	}
	if (pt->NOISE_MULTI != NULL)
	{
		for (int j = 1; j<pt->CISStruct.NBturn; j++)	// On fait la somme des valeur sur le premier tour --> J=1
		{
			for (int i = 0; i<pt->CISStruct.NBpt; i++)
			{
				pt->noise[i] += pt->noise[(int)(i + j*pt->CISStruct.NBpt)];
			}
		}
	}
	double meanval=0;
		for(int i=0;i<pt->CISStruct.NBpt;i++)
		{
			pt->result[i]/=pt->CISStruct.NBturn;
			meanval+=pt->result[i];
			pt->X_result[i]/=pt->CISStruct.NBturn;
			pt->Z_result[i]/=pt->CISStruct.NBturn;
			pt->VX_result[i] /= pt->CISStruct.NBturn;
			pt->VZ_result[i] /= pt->CISStruct.NBturn;
			pt->T_result[i]=atan2(pt->Z_result[i]-pt->CISStruct.ZC,pt->X_result[i]-pt->CISStruct.XC)+M_PI/2;//Angle calculated on mean val 
		}
		for(int i=0;i<pt->CISStruct.NBpt;i++)
		{
			pt->result[i]-=meanval/pt->CISStruct.NBpt;
		}
		if (pt->NOISE_MULTI != NULL)
		{
			double meanval = 0;
			for (int i = 0; i<pt->CISStruct.NBpt; i++)
			{
				pt->noise[i] /= pt->CISStruct.NBturn;
			}
			for (int i = 0; i<pt->CISStruct.NBpt; i++)
			{
				pt->noise[i] -= meanval / pt->CISStruct.NBpt;
			}
		}
// 
// PFOUUUUUUUUU


	pt->MesLen=(int)pt->CISStruct.NBpt;


	if(pt->AbortRequest)
		{sprintf (pt->last_err,"Abort requested \n" );pt->Measuring=false;_endthread();}

	pt->MULTI->SetDisplay(1);
	pt->MULTI->SetTrigSource("IMM");
	pt->MULTI->FreeRun();
	if (pt->NOISE_MULTI != NULL)
	{
		pt->NOISE_MULTI->SetDisplay(1);
		pt->NOISE_MULTI->SetTrigSource("IMM");
		pt->NOISE_MULTI->FreeRun();

	}
	pt->DataAvailable=true;
	pt->Measuring=false;

	_endthread();
}

bool measure::CircularScan(double XC,double ZC,double R,double NBPT,double NBTURN,double TI,double Velocity,double Acc, double driftOrder)
{	if ( IsMeasuring())
	{
		sprintf(last_err,"Can't Run CircularScan Another Measure is running\n");
		return false;
	}
	sprintf (last_err,"No Error\n");
	DataAvailable=false;
	Measuring=true;
	CISStruct.XC= XC;CISStruct.ZC= ZC;
	CISStruct.R= R;
	CISStruct.NBpt =NBPT;CISStruct.NBturn= NBTURN;
	CISStruct.Velocity = Velocity;
	CISStruct.TI= TI;CISStruct.Acc= Acc;
	CISStruct.driftOrder = driftOrder;

	// Check stage range
	//if (std::abs(XC) + R >= 50 ||std::abs(ZC) + R >= 50)
	//{
	//	return (false);
	//}

	if(_beginthread(BackCircularIntegralScan,0,(void *)this)==-1)
	{
		return (false);
	}	
	return (true);

	return true;
}

 static void BackMultiCircularIntegralScan(void* p)
{
	measure *pt;
	pt=(measure *)p;
	if (pt->AbortRequest)
	{
		if (pt->NOISE_MULTI != NULL)
		{
			delete pt->NOISE_MULTI;
			pt->NOISE_MULTI = new Keithley(pt->NoiseMulti);
		}

		delete pt->MULTI;
		pt->MULTI=new Keithley(pt->MultiName);
	}
	pt->AbortRequest=false;
	//int TotalPoints=(int)(pt->MCISStruct.NBpt*pt->MCISStruct.R[0]);// nb of circle are store in R[0]  // GLB 22/06/2016
	int OneMesPoints;	
	int MeasPoint=0;

	// Max radius
	double Rmax = *std::max_element(pt->MCISStruct.R + 1, pt->MCISStruct.R + 1 + (int)pt->MCISStruct.R[0]);
	// Max number of points per tuns
	int Nmax = (int) pt->MCISStruct.NBpt;
	int NBTurnMin = (int)pt->MCISStruct.NBturn;								// GLB 22/06/2016

	int TotalPoints = 0;												// GLB 22/06/2016
	for (int MesNum = 0; MesNum < pt->MCISStruct.R[0]; MesNum++)		// GLB 22/06/2016
	{
		TotalPoints = TotalPoints + (int)(std::floor( Nmax * pt->MCISStruct.R[MesNum + 1] / Rmax));
	}

	double * F_R,*N_R, *F_X, *F_Z, *F_T, *F_VX, *F_VZ;
	F_R = (double *)malloc((size_t)TotalPoints*sizeof(double));
	if (pt->NOISE_MULTI != NULL)
		N_R = (double *)malloc((size_t)TotalPoints * sizeof(double));
	else
		N_R = (double *)malloc(sizeof(double));
	F_X = (double *)malloc((size_t)TotalPoints*sizeof(double));
	F_Z = (double *)malloc((size_t)TotalPoints*sizeof(double));
	F_T = (double *)malloc((size_t)TotalPoints*sizeof(double));
	F_VX = (double *)malloc((size_t)TotalPoints * sizeof(double));
	F_VZ = (double *)malloc((size_t)TotalPoints * sizeof(double));

	for ( int MesNum=0; MesNum<pt->MCISStruct.R[0]; MesNum++)
	{
			pt->CISStruct.R = pt->MCISStruct.R[MesNum+1];
			//pt->CISStruct.Velocity = pt->MCISStruct.Velocity * pt->CISStruct.R / Rmax;  // GLB 21/06/2016
			pt->CISStruct.Velocity = pt->MCISStruct.Velocity;							  //
			//pt->CISStruct.Acc = pt->MCISStruct.Acc;										//
			pt->CISStruct.NBpt = (int)std::floor(Nmax * pt->CISStruct.R / Rmax);		  // 	
			pt->CISStruct.NBturn = (int)std::floor(NBTurnMin * Rmax / pt->CISStruct.R);	  // \
			//pt->CISStruct.NBturn = NBTurnMin;
			if (pt->CISStruct.NBturn < 1){ pt->CISStruct.NBturn = 1; }
			pt->Measuring=true;pt->DataAvailable=false;
			if(_beginthread(BackCircularIntegralScan,0,(void*)p)==-1)
			{sprintf (pt->last_err,"single circulare thread can't start \n" );pt->Level2Measuring=false;_endthread();}
			while (pt->Measuring)
				Sleep(200);
			if(pt->DataAvailable)
			{
				OneMesPoints = (int)(std::floor(Nmax * pt->CISStruct.R / Rmax));		// GLB 21/06/2016
				for(int i=0;i<OneMesPoints;i++)
				{
					//int StorPos=MesNum*OneMesPoints+i;								// GLB 21/06/2016
					int StorPos = MeasPoint+i;
					F_R[StorPos]=pt->result[i];
					F_X[StorPos]=pt->X_result[i];
					F_Z[StorPos]=pt->Z_result[i];
					F_VX[StorPos] = pt->VX_result[i];
					F_VZ[StorPos] = pt->VZ_result[i];
					F_T[StorPos]=pt->T_result[i];					
				}	
				if (pt->NOISE_MULTI != NULL)
				{
					for (int i = 0; i < OneMesPoints; i++)
					{
						int StorPos = MeasPoint + i;
						N_R[StorPos] = pt->noise[i];
					}
				}
				MeasPoint = MeasPoint + OneMesPoints;									// GLB 21/06/2016
			}
			if(pt->AbortRequest)
				{sprintf (pt->last_err,"Abort requested  \n" );pt->Level2Measuring=false;_endthread();}				
	}
	delete[] pt->result;

	
	if (pt->NOISE_MULTI != NULL)
	{
		delete[] pt->noise;
		pt->noise = N_R;
	}
	delete[] pt->X_result;
	delete[] pt->Z_result;
	delete[] pt->T_result;
	delete[] pt->VX_result;
	delete[] pt->VZ_result;
	pt->result=F_R;
	pt->X_result=F_X;
	pt->Z_result=F_Z;
	pt->T_result=F_T;
	pt->VX_result = F_VX;
	pt->VZ_result = F_VZ;
	pt->MesLen=(int)TotalPoints;

	pt->DataAvailable=true;
	pt->Level2Measuring=false;
	pt->Measuring=false;
	
	_endthread();
}

 bool measure::MultiCircularScan(double XC, double ZC, double *R, double NBPT, double NBTURN, double TI, double Velocity, double Acc, double driftOrder)
 {
	 if (IsMeasuring())
	 {
		 sprintf(last_err, "Can't Run Multi CircularScan Another Measure is running\n");
		 return false;
	 }
	 sprintf(last_err, "No Error\n");
	 DataAvailable = false;
	 Level2Measuring = true;
	 Measuring = true;
	 MCISStruct.XC = XC; CISStruct.XC = XC;
	 MCISStruct.ZC = ZC; CISStruct.ZC = ZC;
	 MCISStruct.R = R;
	 MCISStruct.NBpt = NBPT; CISStruct.NBpt = NBPT;
	 MCISStruct.NBturn = NBTURN; CISStruct.NBturn = NBTURN;
	 MCISStruct.TI = TI; CISStruct.TI = TI;
	 MCISStruct.Velocity = Velocity;
	 MCISStruct.Acc = Acc; CISStruct.Acc = Acc;
	 MCISStruct.driftOrder = driftOrder; CISStruct.driftOrder = driftOrder;

	 // Check stage range
	//if (std::abs(XC) + R[1] >= 50 || std::abs(ZC) + R[1] >= 50)
	//{
	//	return (false);
	//}

	if(_beginthread(BackMultiCircularIntegralScan,0,(void *)this)==-1)
	{
		return (false);
	}	

	return true;
}

static void BackCompCircularIntegralScan(void* p)
{
	measure *pt;
	pt=(measure *)p;
	if (pt->AbortRequest)
	{
		if (pt->NOISE_MULTI != NULL)
		{
			delete pt->NOISE_MULTI;
			pt->NOISE_MULTI = new Keithley(pt->NoiseMulti);
		}
		delete pt->MULTI;
		pt->MULTI=new Keithley(pt->MultiName);
	}
	pt->AbortRequest=false;	
	int OneMesPoints=(int)(pt->CCISStruct.NBAvg );
	int nbturn=(int)pt->CCISStruct.R[0];
	int TotalPoints=(int)(pt->CCISStruct.NBPT*nbturn);

	double * F_R,*F_N,*F_X,*F_Z,*F_T;
	F_R=(double *)malloc((size_t)TotalPoints*sizeof(double));
	F_N= (double *)malloc((size_t)TotalPoints * sizeof(double));
	F_X=(double *)malloc((size_t)TotalPoints*sizeof(double));
	F_Z=(double *)malloc((size_t)TotalPoints*sizeof(double));
	F_T=(double *)malloc((size_t)TotalPoints*sizeof(double));
	for (int MeasNum=0;MeasNum<nbturn;MeasNum++)
	{
		for (int k=0;k<pt->CCISStruct.NBPT;k++)
		{
			double theta=-k*2*M_PI/pt->CCISStruct.NBPT;
			//		double alpha=(pt->CCISStruct.CompOrder-1)*theta-pt->CCISStruct.Phi0-M_PI/2;
			double alpha=-(pt->CCISStruct.CompOrder-1)*theta+pt->CCISStruct.Phi0;
			double X0=pt->CCISStruct.XC+pt->CCISStruct.R[MeasNum+1]*cos(theta);
			double Z0=pt->CCISStruct.ZC+pt->CCISStruct.R[MeasNum+1]*sin(theta);

			pt->Measuring=true;pt->DataAvailable=false;
			pt->PimStruct.X=X0;
			pt->PimStruct.Z=Z0;
			pt->PimStruct.Len=pt->CCISStruct.MeasLen;
			pt->PimStruct.Angle=alpha;
			pt->PimStruct.V=pt->CCISStruct.V;
			pt->PimStruct.Acc=pt->CCISStruct.Acc;
			pt->PimStruct.nb=(int)pt->CCISStruct.NBAvg;
			pt->PimStruct.tend = pt->CCISStruct.tend;
			pt->Measuring=true;pt->DataAvailable=false;
			if(_beginthread(BackIntegralMeas,0,(void *)p)==-1)
			{sprintf (pt->last_err,"Measurement thread can't start in compensated mode\n" );pt->Level2Measuring=false;_endthread();}

			while (pt->Measuring)
				Sleep(200);
			if(pt->DataAvailable)
			{
				F_R[k+MeasNum*(int)pt->CCISStruct.NBPT]=0;
				if (pt->NOISE_MULTI != NULL)
					F_N[k + MeasNum*(int)pt->CCISStruct.NBPT] = 0;
				F_X[k+MeasNum*(int)pt->CCISStruct.NBPT]=0;
				F_Z[k+MeasNum*(int)pt->CCISStruct.NBPT]=0;
				F_T[k+MeasNum*(int)pt->CCISStruct.NBPT]=0;	
				for(int i=0;i<OneMesPoints;i++)
				{

					F_R[k+MeasNum*(int)pt->CCISStruct.NBPT]+=pt->result[i];
					F_X[k+MeasNum*(int)pt->CCISStruct.NBPT]+=pt->X_result[i];
					F_Z[k+MeasNum*(int)pt->CCISStruct.NBPT]+=pt->Z_result[i];
					F_T[k+MeasNum*(int)pt->CCISStruct.NBPT]+=pt->T_result[i];					
				}
				if (pt->NOISE_MULTI != NULL)
					for (int i = 0; i<OneMesPoints; i++)
						F_N[k + MeasNum*(int)pt->CCISStruct.NBPT] += pt->noise[i];
				F_R[k+MeasNum*(int)pt->CCISStruct.NBPT]/=OneMesPoints;
				F_X[k+MeasNum*(int)pt->CCISStruct.NBPT]/=OneMesPoints;
				F_Z[k+MeasNum*(int)pt->CCISStruct.NBPT]/=OneMesPoints;
				F_T[k+MeasNum*(int)pt->CCISStruct.NBPT]/=OneMesPoints;	
			}
			if(pt->AbortRequest)
			{sprintf (pt->last_err,"Abort requested \n" );pt->Level2Measuring=false;_endthread();}				
		}
	}
	delete[] pt->result;delete[] pt->X_result;delete[] pt->Z_result;delete[] pt->T_result;
	if (pt->NOISE_MULTI != NULL)
	{
		delete[] pt->noise;
		pt->noise = F_N;
	}
	pt->result=F_R;
	pt->X_result=F_X;
	pt->Z_result=F_Z;
	pt->T_result=F_T;
	pt->MesLen=(int)TotalPoints;

	pt->DataAvailable=true;
	pt->Level2Measuring=false;
	pt->Measuring=false;

	_endthread();
}
bool measure::CompCircularScan(double XC,double ZC,double *R, double MeasLen,double NBPT,double NBAvg,double V,double Acc,double CompOrder,double Phi0, double tend)
{
	if ( IsMeasuring())
	{
		sprintf(last_err,"Can't Run Multi CirculareScan Another Measure is running\n");
		return false;
	}
	sprintf (last_err,"No Error\n");
	DataAvailable=false;
	Level2Measuring=true;
	Measuring=true;
	CCISStruct.XC=XC;
	CCISStruct.ZC=ZC;
	CCISStruct.R=R;
	CCISStruct.MeasLen=MeasLen;
	CCISStruct.NBPT=NBPT;
	CCISStruct.NBAvg=NBAvg;
	CCISStruct.V=V;
	CCISStruct.Acc=Acc;
	CCISStruct.CompOrder=CompOrder;
	CCISStruct.Phi0=Phi0;
	CCISStruct.tend = tend;

	// Check stage range
	//if (std::abs(XC) + R[1] >= 50 || std::abs(ZC) + R[1] >= 50)
	//{
	//	return (false);
	//}

	if(_beginthread(BackCompCircularIntegralScan,0,(void *)this)==-1)
	if( false)
	{
		return (false);
	}	

	return true;
}
static void BackQuadCenter(void* p)
{
	measure *pt;
	pt = (measure *)p;
	double x0; double z0;
	double Tx0; double Tz0;
	double r0;
	double  nPoints;
	double nAve;
	

	x0 = pt->QCStruct.x0;
	z0 = pt->QCStruct.z0;
	Tx0 = pt->QCStruct.TX0;
	Tz0 = pt->QCStruct.TZ0;
	r0 = pt->QCStruct.r0;
	nPoints=pt->QCStruct.nPoints  ;
	nAve=pt->QCStruct.nAve ;
// prepare Parameter for linear scan
	pt->LISStruct.XB = x0-r0;pt->LISStruct.ZB = z0;
	pt->LISStruct.XE = x0+r0; pt->LISStruct.ZE = z0;
	double acc1, acc2;
	pt->XPS->ReadAcceleration(&acc1, &acc2);

	pt->LISStruct.TI = 0.08; pt->LISStruct.Acc = min(acc1,acc2);
	pt->LISStruct.NBpt = (int)nPoints;
	pt->LISStruct.NBScan = (int)nAve;

	if(pt->XPS->SetTaper(Tx0, Tz0)!=(int)true)
	{
		sprintf(pt->last_err, "Can't Set taper in BackQuadCenter \n");

		pt->Level2Measuring = false;
		pt->Measuring = false;
		_endthread();
	}
	do
		Sleep(200);
	while (pt->XPS->IsMoving());

//start Linearscan
	if (_beginthread(BackLinearIntegralScan, 0, p) == -1)
	{
		sprintf(pt->last_err, "Can't run first linear integral scan in quad Center Find\n");
		
		pt->Level2Measuring = false;
		pt->Measuring = false;
		_endthread();
	}
// wait the end of first scan
	do
		Sleep(500);
	while (pt->Measuring);


	double a, b;

	pt->LinFit(pt->X_result, pt->result,(int) nPoints, &a, &b, &(pt->QCStruct.XCenter));

//rearm Measuring Flag for second scan after getting the measure
	pt->Measuring = true;
		

	// prepare Parameter for linear scan



	pt->LISStruct.XB = x0 ; pt->LISStruct.ZB = z0-r0;
	pt->LISStruct.XE = x0 ; pt->LISStruct.ZE = z0+r0;
	pt->LISStruct.TI = 0.08; pt->LISStruct.Acc = min(acc1,acc2);
	pt->LISStruct.NBpt = (int)nPoints;
	pt->LISStruct.NBScan = (int)nAve;
	
	//start Linearscan
	if (_beginthread(BackLinearIntegralScan, 0, p) == -1)
	{
		sprintf(pt->last_err, "Can't run second linear integral scan in quad Center Find\n");

		pt->Level2Measuring = false;
		pt->Measuring = false;
		_endthread();
	}
	// wait the end of first scan
	do
		Sleep(500);
	while (pt->Measuring);
	// get the result of the first measurement

	pt->LinFit(pt->Z_result, pt->result, (int)nPoints, &a, &b, & (pt->QCStruct.ZCenter));
	
	delete[] pt->QC_result;
	pt->QC_result = (double *)malloc(sizeof(double)*2);
	pt->QC_result[0] = pt->QCStruct.XCenter;
	pt->QC_result[1] = pt->QCStruct.ZCenter;
	pt->LastQuadcenter[0] = pt->QC_result[0];
	pt->LastQuadcenter[1] = pt->QC_result[1];

	delete[] pt->QT_result;
	pt->QT_result = (double *)malloc(sizeof(double) * 2);
	pt->QT_result[0] =Tx0;
	pt->QT_result[1] = Tz0;
	pt->LastQuadAxis[0] = Tx0;
	pt->LastQuadAxis[1] = Tz0;

	pt->DataAvailable = true;
	pt->Level2Measuring = false;
	pt->Measuring = false;

	_endthread();
}
bool measure::QuadCenter(double x0, double z0, double TX0, double TZ0, double r0, double  nPoints, double nAve)
{
	if (IsMeasuring())
	{
		sprintf(last_err, "Can't Run Multi QuadCenter Another Measure is running\n");
		return false;
	}
	sprintf(last_err, "No Error\n");
	DataAvailable = false;
	Level2Measuring = true;
	Measuring = true;
	QCStruct.x0 = x0;
	QCStruct.z0 = z0;
	QCStruct.TX0 = TX0;
	QCStruct.TZ0 = TZ0;
	QCStruct.r0 = r0;
	QCStruct.nPoints = nPoints;
	QCStruct.nAve = nAve;

	if (_beginthread(BackQuadCenter, 0, (void *)this) == -1)
		if (false)
		{
			return (false);
		}

	return true;
}

static void BackQuadAxis(void* p)
{

	measure *pt;
	pt = (measure *)p;
	double x0; double z0;
	double Tx0; double Tz0;
	double r0;
	int  nPoints;
	double nAve;
	double MesLen;
	double CurrTap;
	x0 = pt->QCStruct.x0;
	z0 = pt->QCStruct.z0;
	Tx0 = pt->QCStruct.TX0;
	Tz0 = pt->QCStruct.TZ0;
	r0 = fabs(pt->QCStruct.r0);
	nPoints =(int) pt->QCStruct.nPoints;
	nAve = pt->QCStruct.nAve;
	MesLen = pt->JMesLen;

	double minXTap = Tx0 - r0;
	double minZTap = Tz0 - r0;
//	double currentTaper;
	double trash;
	double a, b, NewTZ0,NewTX0;
	NewTZ0 = 0; NewTX0 = 0;
	pt->PimStruct.X =x0; 
	pt->PimStruct.Z = z0;
	pt->PimStruct.Len = MesLen;			
	pt->PimStruct.Angle = 0;			// Measure of JX First
	//pt->PimStruct.V = 20;				// could be read from XPS2d
	pt->XPS->ReadVelocity(&(pt->PimStruct.V), &trash);
	double acc1, acc2;
	pt->XPS->ReadAcceleration(&acc1, &acc2);
	pt->PimStruct.Acc=min(acc1,acc2);
	pt->PimStruct.nb = (int)nAve;
	pt->XPS->MoveAbs(x0, z0,1);

	double *IX2 = (double*)malloc(sizeof(double)*nPoints);
	double *ZTap = (double*)malloc(sizeof(double)*nPoints);
	double TStep = (2 * r0 )/ nPoints;
	{// Start to find Z Taper 
		for (int i = 0; i < nPoints; i++)
		{
			
			CurrTap = minZTap + i*TStep;
			pt->XPS->SetTaper(Tx0,CurrTap);							//Set Taper 
			do
				Sleep(200);
			while (pt->XPS->IsMoving());										//wait end of move
			Sleep(200);
			pt->XPS->MoveAbs(x0, z0, 1);
			if (_beginthread(BackDoubleIntegralMeas, 0, p) == -1)		//run double integrale meas
			{
				sprintf(pt->last_err, "Can't run BackDoubleIntegralMeas\n");
				pt->Level2Measuring = false; pt->Measuring = false; _endthread();
			}
			do
				Sleep(500);
			while (pt->Measuring);												//wait end of measure

			if (pt->DataAvailable)												//If Measure OK
			{
				IX2[i] = 0;
				ZTap[i] = CurrTap;									//store Z Taper  in ZTap
				for (int j = 0; j < pt->PimStruct.nb; j++)
					IX2[i] += *pt->result;										// sum Jx in IX2
				IX2[i] /= pt->PimStruct.nb;										// and divide by nb(avg) to obtain mean val

			}
			else
			{
				sprintf(pt->last_err, "No Data Available after %d ieme Double integral meas\n", i);
				pt->Level2Measuring = false; pt->Measuring = false; _endthread();
			}
			pt->Measuring = true;												// Rearm Mesuring and datavalaible flags
			pt->DataAvailable = false;
		}
		pt->LinFit(ZTap, IX2, nPoints, &a, &b, &NewTZ0);			// NewTZ0 is the researched Taper
	//	pt->XPS->SetTaper(Tx0, NewTZ0);								//Set Taper 
		do
			Sleep(200);
		while (pt->XPS->IsMoving());
	}
	Sleep(200);
	pt->XPS->MoveAbs(x0, z0, 1);
	double *IZ2 = (double*)malloc(sizeof(double)*nPoints);
	double *XTap = (double*)malloc(sizeof(double)*nPoints);

		{// Start to find X Taper 
			pt->PimStruct.Angle = M_PI/2;			// Measure of JX second
			
			for (int i = 0; i < nPoints; i++)
			{

				CurrTap = minXTap + i*TStep;
				pt->XPS->SetTaper(CurrTap, NewTZ0);							//Set Taper 
				do
					Sleep(200);
				while (pt->XPS->IsMoving());										//wait end of move
				Sleep(200);
				pt->XPS->MoveAbs(x0, z0, 1);
				if (_beginthread(BackDoubleIntegralMeas, 0, p) == -1)		//run double integrale mes
				{
					sprintf(pt->last_err, "Can't run BackDoubleIntegralMeas scan in quad Axis Find\n");
					pt->Level2Measuring = false; pt->Measuring = false; _endthread();
				}
				do
					Sleep(500);
				while (pt->Measuring);												//wait end of measure

				if (pt->DataAvailable)												//If Measure OK
				{
					IZ2[i] = 0;
					XTap[i] = CurrTap;									//store Z Taper  in ZTap
					for (int j = 0; j < pt->PimStruct.nb; j++)
						IZ2[i] += *pt->result;										//store mean Jx in IX2
					IZ2[i] /= pt->PimStruct.nb;
				}
				else
				{
					sprintf(pt->last_err, "No Data Available after %d ieme Double integral meas\n", i);
					pt->Level2Measuring = false; pt->Measuring = false; _endthread();
				}
				pt->Measuring = true;												// Rearm Mesuring and datavalaible flags
				pt->DataAvailable = false;
			}
			pt->LinFit(XTap, IZ2, nPoints, &a, &b, &NewTX0);			// NewTZ0 is the researched Taper
//			pt->XPS->SetTaper( NewTX0,NewTZ0);							//Set Taper 
			do
				Sleep(200);
			while (pt->XPS->IsMoving());
		}
		pt->XPS->SetTaper(NewTX0, NewTZ0);							//Set Taper 
		do
			Sleep(200);
		while (pt->XPS->IsMoving());
		delete[] IZ2;
		delete[] XTap;
		delete[] IX2;
		delete[] ZTap;

	// PREPARE RESULTS FOR GETLAST MEAS
	delete[] pt->QC_result;
	pt->QC_result = (double *)malloc(sizeof(double) * 2);
	pt->LastQuadcenter[0] = pt->QC_result[0];
	pt->LastQuadcenter[1] = pt->QC_result[1];
	pt->QC_result[0] = x0;
	pt->QC_result[1] = z0;

	delete[] pt->QT_result;
	pt->QT_result = (double *)malloc(sizeof(double) * 2);
	pt->LastQuadAxis[0] = pt->QT_result[0];
	pt->LastQuadAxis[1] = pt->QT_result[1];
	pt->QT_result[0] = NewTX0;
	pt->QT_result[1] = NewTZ0;

	pt->DataAvailable = true;
	pt->Level2Measuring = false;
	pt->Measuring = false;

	_endthread();
}
bool measure::QuadAxis(double x0, double z0, double TX0, double TZ0, double r0, double  nPoints, double nAve)
{
	if (IsMeasuring())
	{
		sprintf(last_err, "Can't Run Multi QuadAxis Another Measure is running\n");
		return false;
	}
	//Prompt x0, "X Centre [mm]"
	//Prompt z0, "Z Centre [mm]"
	//Prompt taperX0p, "X Taper [mm]"
	//Prompt TaperZ0p, "Z Taper [mm]"
	//Prompt taperMaxp, "Max Taper [mm]"
	//Prompt measLengthp, "Meas Length [mm]"
	//Prompt nPointsp, "Number of Points"

	sprintf(last_err, "No Error\n");
	DataAvailable = false;
	Level2Measuring = true;
	Measuring = true;
	QCStruct.x0 = x0;
	QCStruct.z0 = z0;
	QCStruct.TX0 = TX0;
	QCStruct.TZ0 = TZ0;
	QCStruct.r0 = r0;
	QCStruct.nAve = nAve;
	QCStruct.nPoints = nPoints;

	QCStruct.MesLen = 2 * r0 / (nPoints - 1);

	if (_beginthread(BackQuadAxis, 0, (void *)this) == -1)
		if (false)
		{
			return (false);
		}

	return true;
}

static void BackQuadCenterAxis(void* p)
{
	measure *pt;
	pt = (measure *)p;
	Sleep(5000);
	pt->DataAvailable = true;
	pt->Level2Measuring = false;
	pt->Measuring = false;

	_endthread();
}
bool measure::QuadCenterAxis()
{
	if (IsMeasuring())
	{
		sprintf(last_err, "Can't Run Multi QuadCenterAxis Another Measure is running\n");
		return false;
	}
	sprintf(last_err, "No Error\n");
	DataAvailable = false;
	Level2Measuring = true;
	Measuring = true;
	if (_beginthread(BackQuadCenterAxis, 0, (void *)this) == -1)
		if (false)
		{
			return (false);
		}

	return true;
}

static void BackQuadAlign(void* p)
{
	// Initialise and get measurement parameters
	measure *pt;
	pt = (measure *)p;
	double x0; 
	double z0;
	double tx0; 
	double tz0;
	double dx;
	double dt;
	double len;
	double vel;
	double acc;
	double iz10; 
	double iz20;
	double iz1;
	double iz2;
	double jz1;
	double jz2;
	double ix1;
	double ix2;
	double jx1;
	double jx2;
	int nave;
	int i;
	//------------------------------------------
	//
	// FIELD MEASUREMENTS
	//
	//------------------------------------------
	// Fill Quadrupole alignment structure
	x0 = pt->QAStruct.X0;
	z0 = pt->QAStruct.Z0;
	tx0 = pt->QAStruct.TX0;
	tz0 = pt->QAStruct.TZ0;
	dx = pt->QAStruct.DX;
	dt = pt->QAStruct.DT;
	len = pt->QAStruct.MEASLEN;
	vel = pt->QAStruct.VEL;
	acc = pt->QAStruct.ACC;	
	nave = (int)pt->QAStruct.NAVE;
	// Fill Integral Measurement structure
	pt->PimStruct.X = x0;
	pt->PimStruct.Z = z0;
	pt->PimStruct.Len = len;
	pt->PimStruct.Angle = M_PI / 2;
	pt->PimStruct.V = vel;
	pt->PimStruct.Acc = acc;
	pt->PimStruct.nb = nave;
	// -----------------------------------------
	// Vertical field integral measurement at x0 - dx
	// Stored in iz10
	// -----------------------------------------
	// Set taper to initial value
	pt->XPS->SetTaper(tx0, tz0);
	do
		Sleep(200);
	while (pt->XPS->IsMoving());								
	Sleep(200);
	// Move to initial position
	pt->XPS->MoveAbs(x0-dx, z0, 1);
	do
		Sleep(200);
	while (pt->XPS->IsMoving());
	Sleep(200);
	pt->PimStruct.X = x0-dx;
	// Field integral measurement
	pt->Measuring = true;
	pt->DataAvailable = false;
	if (_beginthread(BackIntegralMeas, 0, p) == -1)		
	{
		sprintf(pt->last_err, "Can't run BackIntegralMeas\n");
		pt->Level2Measuring = false; pt->Measuring = false; _endthread();
	}
	// Wait for measurement result
	do
		Sleep(200);
	while (pt->Measuring);
	// Read field integral
	if (pt->DataAvailable)
	{
		iz10 = 0;
		for (i = 0; i < pt->PimStruct.nb; i++)
			iz10 += *pt->result;										
		iz10 /= pt->PimStruct.nb;
	}
	else
	{
		sprintf(pt->last_err, "No Data Available\n");
		pt->Level2Measuring = false; pt->Measuring = false; _endthread();
	}
	// Rearm Measuring and dataAvailable flags
	pt->Measuring = true;												
	pt->DataAvailable = false;
	// -----------------------------------------
	// Vertical field integral measurement at x0 + dx
	// Stored in iz20
	// -----------------------------------------
	// Move to second position
	pt->XPS->MoveAbs(x0 + dx, z0, 1);
	do
		Sleep(200);
	while (pt->XPS->IsMoving());
	Sleep(200);
	pt->PimStruct.X = x0 + dx;
	// Field integral measurement
	if (_beginthread(BackIntegralMeas, 0, p) == -1)
	{
		sprintf(pt->last_err, "Can't run BackIntegralMeas\n");
		pt->Level2Measuring = false; pt->Measuring = false; _endthread();
	}
	// Wait for measurement result
	do
		Sleep(200);
	while (pt->Measuring);
	// Read field integral 
	if (pt->DataAvailable)
	{
		iz20 = 0;
		for (i = 0; i < pt->PimStruct.nb; i++)
			iz20 += *pt->result;
		iz20 /= pt->PimStruct.nb;
	}
	else
	{
		sprintf(pt->last_err, "No Data Available\n");
		pt->Level2Measuring = false; pt->Measuring = false; _endthread();
	}
	// Rearm Measuring and dataAvailable flags
	pt->Measuring = true;
	pt->DataAvailable = false;
	// -----------------------------------------
	// Vertical field integral measurement at x0  and Tx0 + DT
	// Stored in iz1
	// -----------------------------------------
	// Move to initial position
	pt->XPS->MoveAbs(x0, z0, 1);
	do
		Sleep(200);
	while (pt->XPS->IsMoving());
	Sleep(200);
	// Set taper to TX + DT
	pt->XPS->SetTaper(tx0+dt, tz0);
	do
		Sleep(200);
	while (pt->XPS->IsMoving());
	Sleep(200);
	pt->PimStruct.X = x0;
	// Field integral measurement
	if (_beginthread(BackIntegralMeas, 0, p) == -1)
	{
		sprintf(pt->last_err, "Can't run BackIntegralMeas\n");
		pt->Level2Measuring = false; pt->Measuring = false; _endthread();
	}
	// Wait for measurement result
	do
		Sleep(200);
	while (pt->Measuring);
	// Read field integral 
	if (pt->DataAvailable)
	{
		iz1 = 0;
		for (i = 0; i < pt->PimStruct.nb; i++)
			iz1 += *pt->result;
		iz1 /= pt->PimStruct.nb;
	}
	else
	{
		sprintf(pt->last_err, "No Data Available \n");
		pt->Level2Measuring = false; pt->Measuring = false; _endthread();
	}
	// Rearm Messuring and dataAvailable flags
	pt->Measuring = true;
	pt->DataAvailable = false;
	// -----------------------------------------
	// Vertical double field integral measurement at x0  and Tx0 + DT
	// Stored in jz1
	// -----------------------------------------
	// Field integral measurement
	if (_beginthread(BackDoubleIntegralMeas, 0, p) == -1)
	{
		sprintf(pt->last_err, "Can't run BackDoubleIntegralMeas\n");
		pt->Level2Measuring = false; pt->Measuring = false; _endthread();
	}
	// Wait for measurement result
	do
		Sleep(200);
	while (pt->Measuring);
	// Read field integral
	if (pt->DataAvailable)
	{
		jz1 = 0;
		for (i = 0; i < pt->PimStruct.nb; i++)
			jz1 += *pt->result;
		jz1 /= pt->PimStruct.nb;
	}
	else
	{
		sprintf(pt->last_err, "No Data Available after Double integral meas\n");
		pt->Level2Measuring = false; pt->Measuring = false; _endthread();
	}
	// Rearm Measuring and dataAvailable flags
	pt->Measuring = true;
	pt->DataAvailable = false;
	// -----------------------------------------
	// Vertical field integral measurement at x0  and Tx0 - DT
	// Stored in iz2
	// -----------------------------------------
	// Move to initial position
	pt->XPS->MoveAbs(x0, z0, 1);
	do
		Sleep(200);
	while (pt->XPS->IsMoving());
	Sleep(200);
	// Set taper to TX - DT
	pt->XPS->SetTaper(tx0 - dt, tz0 ); 
	do
		Sleep(200);
	while (pt->XPS->IsMoving());
	Sleep(200);
	pt->PimStruct.X = x0;
	// Field integral measurement
	if (_beginthread(BackIntegralMeas, 0, p) == -1)
	{
		sprintf(pt->last_err, "Can't run BackIntegralMeas\n");
		pt->Level2Measuring = false; pt->Measuring = false; _endthread();
	}
	// Wait for measurement result
	do
		Sleep(200);
	while (pt->Measuring);
	// Read field integral
	if (pt->DataAvailable)
	{
		iz2 = 0;
		for (i = 0; i < pt->PimStruct.nb; i++)
			iz2 += *pt->result;
		iz2 /= pt->PimStruct.nb;
	}
	else
	{
		sprintf(pt->last_err, "No Data Available \n");
		pt->Level2Measuring = false; pt->Measuring = false; _endthread();
	}
	// Rearm Mesuring and datavalaible flags
	pt->Measuring = true;
	pt->DataAvailable = false;
	// -----------------------------------------
	// Vertical double field integral measurement at x0  and Tx0 - DT
	// Stored in jz2
	// -----------------------------------------
	// Field integral measurement
	if (_beginthread(BackDoubleIntegralMeas, 0, p) == -1)
	{
		sprintf(pt->last_err, "Can't run BackDoubleIntegralMeas\n");
		pt->Level2Measuring = false; pt->Measuring = false; _endthread();
	}
	// Wait for measurement result
	do
		Sleep(200);
	while (pt->Measuring);
	// Read field integral
	if (pt->DataAvailable)
	{
		jz2 = 0;
		for (i = 0; i < pt->PimStruct.nb; i++)
			jz2 += *pt->result;
		jz2 /= pt->PimStruct.nb;
	}
	else
	{
		sprintf(pt->last_err, "No Data Available after  Double integral meas\n");
		pt->Level2Measuring = false; pt->Measuring = false; _endthread();
	}
	// Rearm Measuring and dataAvailable flags
	pt->Measuring = true;
	pt->DataAvailable = false;
	// -----------------------------------------
	// Horizontal field integral measurement at x0  and Tz0 + DT
	// Stored in ix1
	// -----------------------------------------
	pt->PimStruct.Angle = 0;
	// Move to initial position
	pt->XPS->MoveAbs(x0, z0, 1);
	do
		Sleep(200);
	while (pt->XPS->IsMoving());
	Sleep(200);
	// Set taper to TX + DT
	pt->XPS->SetTaper(tx0, tz0 + dt);
	do
		Sleep(200);
	while (pt->XPS->IsMoving());
	Sleep(200);
	pt->PimStruct.X = x0;
	// Field integral measurement
	if (_beginthread(BackIntegralMeas, 0, p) == -1)
	{
		sprintf(pt->last_err, "Can't run BackIntegralMeas\n");
		pt->Level2Measuring = false; pt->Measuring = false; _endthread();
	}
	// Wait for measurement result
	do
		Sleep(200);
	while (pt->Measuring);
	// Read field integral 
	if (pt->DataAvailable)
	{
		ix1 = 0;
		for (i = 0; i < pt->PimStruct.nb; i++)
			ix1 += *pt->result;
		ix1 /= pt->PimStruct.nb;
	}
	else
	{
		sprintf(pt->last_err, "No Data Available \n");
		pt->Level2Measuring = false; pt->Measuring = false; _endthread();
	}
	// Rearm Measuring and dataAvailable flags
	pt->Measuring = true;
	pt->DataAvailable = false;
	// -----------------------------------------
	// Horizontal double field integral measurement at x0  and Tz0 + DT
	// Stored in jx1
	// -----------------------------------------
	// Field integral measurement
	if (_beginthread(BackDoubleIntegralMeas, 0, p) == -1)
	{
		sprintf(pt->last_err, "Can't run BackdoubleIntegralMeas\n");
		pt->Level2Measuring = false; pt->Measuring = false; _endthread();
	}
	// Wait for measurement result
	do
		Sleep(200);
	while (pt->Measuring);
	// Read field integral
	if (pt->DataAvailable)
	{
		jx1 = 0;
		for (i = 0; i < pt->PimStruct.nb; i++)
			jx1 += *pt->result;
		jx1 /= pt->PimStruct.nb;
	}
	else
	{
		sprintf(pt->last_err, "No Data Available after Double integral meas\n");
		pt->Level2Measuring = false; pt->Measuring = false; _endthread();
	}
	// Rearm Measuring and dataAvailable flags
	pt->Measuring = true;
	pt->DataAvailable = false;
	// -----------------------------------------
	// Horizontal field integral measurement at x0  and Tz0 - DT
	// Stored in ix2
	// -----------------------------------------
	// Move to initial position
	pt->XPS->MoveAbs(x0, z0, 1);
	do
		Sleep(200);
	while (pt->XPS->IsMoving());
	Sleep(200);
	// Set taper to TX - DT
	pt->XPS->SetTaper(tx0, tz0 - dt);
	do
		Sleep(200);
	while (pt->XPS->IsMoving());
	Sleep(200);
	pt->PimStruct.X = x0;
	// Field integral measurement
	if (_beginthread(BackIntegralMeas, 0, p) == -1)
	{
		sprintf(pt->last_err, "Can't run BackIntegralMeas\n");
		pt->Level2Measuring = false; pt->Measuring = false; _endthread();
	}
	// Wait for measurement result
	do
		Sleep(200);
	while (pt->Measuring);
	// Read field integral
	if (pt->DataAvailable)
	{
		ix2 = 0;
		for (i = 0; i < pt->PimStruct.nb; i++)
			ix2 += *pt->result;
		ix2 /= pt->PimStruct.nb;
	}
	else
	{
		sprintf(pt->last_err, "No Data Available \n");
		pt->Level2Measuring = false; pt->Measuring = false; _endthread();
	}
	// Rearm Measuring and dataAvailable flags
	pt->Measuring = true;
	pt->DataAvailable = false;
	// -----------------------------------------
	// Horizontal double field integral measurement at x0  and Tz0 - DT
	// Stored in jx2
	// -----------------------------------------
	// Field integral measurement
	if (_beginthread(BackDoubleIntegralMeas, 0, p) == -1)
	{
		sprintf(pt->last_err, "Can't run BackDoubleIntegralMeas\n");
		pt->Level2Measuring = false; pt->Measuring = false; _endthread();
	}
	// Wait for measurement result
	do
		Sleep(200);
	while (pt->Measuring);
	// Read field integral
	if (pt->DataAvailable)
	{
		jx2 = 0;
		for (i = 0; i < pt->PimStruct.nb; i++)
			jx2 += *pt->result;
		jx2 /= pt->PimStruct.nb;
	}
	else
	{
		sprintf(pt->last_err, "No Data Available after Double integral meas\n");
		pt->Level2Measuring = false; pt->Measuring = false; _endthread();
	}
	// -----------------------------------------
	// Reset centre and taper
	// -----------------------------------------
	// Move to initial position
	pt->XPS->MoveAbs(x0, z0, 1);
	do
		Sleep(200);
	while (pt->XPS->IsMoving());
	Sleep(200);
	// Set taper to TX - DT
	pt->XPS->SetTaper(tx0, tz0);
	do
		Sleep(200);
	while (pt->XPS->IsMoving());
	Sleep(200);

	//------------------------------------------
	//
	// DATA ANALYSIS
	//
	//------------------------------------------
	// Field gradient
	double g = (iz20 - iz10) / (2 * dx);
	// Wire length
	double wlen=pt->WireLength;
	// Longitudinal position
	double dsZ= wlen/(2*g*dt)*(iz1-iz2);
	double dsX = wlen / (2 * g*dt)*(ix1 - ix2);
	double ds = (dsX+dsZ) / 2;
	// Magnetic length
	double lmZ = sqrt(6 * (  wlen/(g*dt)*(jz2-jz1) + ds*(wlen-2*ds)   ) );
	double lmX = sqrt(6 * (wlen / (g*dt)*(jx2 - jx1) + ds*(wlen - 2 * ds)));
	double lm = (lmX + lmZ) / 2;
	// Centre and taper -- X
	double tx0meas = 6 * wlen / (g * lm * lm) * ( (iz1 + iz2) * (wlen / 2 - ds) - (jz1 + jz2) );
	double x0meas = (iz1 + iz2) / (2 * g) - tx0meas * ds / wlen;
	tx0meas = tx0 - tx0meas;
	x0meas = x0 - x0meas;
	// Centre and taper -- Z
	double tz0meas = 6 * wlen / (g * lm * lm) * ((ix1 + ix2) * (wlen / 2 - ds) - (jx1 + jx2));
	double z0meas = (ix1 + ix2) / (2 * g) - tx0meas * ds / wlen;
	tz0meas = tz0 - tz0meas;
	z0meas = z0 - z0meas;
	// Centre and taper -- X (OLD METHOD -- For debug)
	//double x0meas_old = x0 - 1 / (g*lm*lm)*((lm*lm / 2 - 3 * wlen*ds + 6 * ds*ds)*(iz1 + iz2) + 6 * ds*(jz1 + jz2));
	//double tx0meas_old = tx0 - 1 / (g*lm*lm)*(3 * wlen*(wlen - 2 * ds)*(iz1 + iz2) - 6 * wlen*(jz1 + jz2));
	// Centre and taper -- Z (OLD METHOD -- For debug)
	//double z0meas_old = z0 - 1 / (g*lm*lm)*((lm*lm / 2 - 3 * wlen*ds + 6 * ds*ds)*(ix1 + ix2) + 6 * ds*(jx1 + jx2));
	//double tz0meas_old = tz0 - 1 / (g*lm*lm)*(3 * wlen*(wlen - 2 * ds)*(ix1 + ix2) - 6 * wlen*(jx1 + jx2));

	// Prepare data for GetLastMeas
	delete[] pt->A_result;
	pt->A_result = (double *)malloc(sizeof(double) * 18);
	pt->A_result[0] = x0meas;
	pt->A_result[1] = z0meas;
	pt->A_result[2] = tx0meas;
	pt->A_result[3] = tz0meas;
	pt->A_result[4] = ds;
	pt->A_result[5] = lm;
	pt->A_result[6] = g;
	pt->A_result[7] = wlen;
	pt->A_result[8] = iz10; // X0-DX, Z0, TX0, TZ0
	pt->A_result[9] = iz20; // X0+DX, Z0, TX0, TZ0
	pt->A_result[10] = iz2; // X0, Z0, TX0-DT, TZ0
	pt->A_result[11] = iz1; // X0, Z0, TX0+DT, TZ0
	pt->A_result[12] = jz2; // X0, Z0, TX0-DT, TZ0
	pt->A_result[13] = jz1; // X0, Z0, TX0+DT, TZ0
	pt->A_result[14] = ix2; // X0, Z0, TX0, TZ0-DT
	pt->A_result[15] = ix1; // X0, Z0, TX0, TZ0+DT
	pt->A_result[16] = jx2; // X0, Z0, TX0, TZ0-DT
	pt->A_result[17] = jx1; // X0, Z0, TX0, TZ0+DT

	pt->MesLen = 18;

	pt->DataAvailable = true;
	pt->Level2Measuring = false;
	pt->Measuring = false;
	_endthread();
}

bool measure::QuadAlign(double x0, double z0, double tx0, double tz0, double dx, double dt, double MeasLen, double vel, double acc, double  nave)
{
	if (IsMeasuring())
	{
		sprintf(last_err, "Can't Run Multi QuadCenterAxis Another Measure is running\n");
		return false;
	}
	sprintf(last_err, "No Error\n");
	DataAvailable = false;
	Level2Measuring = true;
	QAStruct.X0 = x0; QAStruct.Z0 = z0;
	QAStruct.TX0 = tx0; QAStruct.TZ0 = tz0;
	QAStruct.DX = dx; QAStruct.DT = dt;
	QAStruct.MEASLEN = MeasLen;
	QAStruct.VEL = vel;
	QAStruct.ACC = acc;
	QAStruct.NAVE = nave;

	if (_beginthread(BackQuadAlign, 0, (void *)this) == -1)
		if (false)
		{
			return (false);
		}

	return true;
}


// --------------------------------------------------------------------------
// Legendre(int n, double x)
//
// Legendre Polynomials for n=0...8
// --------------------------------------------------------------------------
double measure::Legendre(int n, double x){
	double y = 0;
	switch (n){
	case 0:
		y = 1;
		break;
	case 1:
		y = x;
		break;
	case 2:
		y = 1 / 2 * (3 * pow(x, 2) - 1);
		break;
	case 3:
		y = 1 / 2 * (5 * pow(x, 3) - 3 * x);
		break;
	case 4:
		y = 1 / 8 * (35 * pow(x, 4) - 30 * pow(x, 2) + 3);
		break;
	case 5:
		y = 1 / 8 * (63 * pow(x, 5) - 70 * pow(x, 3) + 15);
		break;
	case 6:
		y = 1 / 16 * (231 * pow(x, 6) - 315 * pow(x, 4) + 105 * pow(x, 2) - 5);
		break;
	case 7:
		y = 1 / 6 * (429 * pow(x, 7) - 693 * pow(x, 5) + 315 * pow(x, 3) - 35);
		break;
	case 8:
		y = 1 / 128 * (6435 * pow(x, 8) - 12012 * pow(x, 6) + 6930 * pow(x, 4) - 1260 * pow(x, 2) + 35);
	}
	return y;
}

// --------------------------------------------------------------------------
// SparseMatrix<double> JkMatrix(int p, int k)
//
// (-1 0 0 ... 0 1 0  ...  0)
// (0 -1 0 ... 0 1 0  ...  0)
// (           ...          )
// (0 0 ...    0 1 0  ... -1)
//
//               K
//
// GLB, 04/12/2015
// --------------------------------------------------------------------------
SparseMatrix<double> measure::JkMatrix(int p, int k){
	SparseMatrix<double> jpk(p - 1, p);
	int m;
	for (m = 0; m < k; m++)	{
		jpk.coeffRef(m, m) = -1;
	}
	for (m = k; m < p - 1; m++)	{
		jpk.coeffRef(m, m + 1) = -1;
	}
	for (m = 0; m < p - 1; m++)	{
		jpk.coeffRef(m, k) = 1;
	}
	return jpk;
}

// --------------------------------------------------------------------------
// SparseMatrix<double> JMatrix(int p)
//
// ( Jp1 )
// ( Jp2 )
// (  ...  )
// ( Jpp )
//
//
// GLB, 04/12/2015
// --------------------------------------------------------------------------
SparseMatrix<double> measure::JMatrix(int p){
	// Initialize
	SparseMatrix<double> mask(p - 1, 1);
	int k;
	mask.coeffRef(0, 0) = 1;
	SparseMatrix<double> jpk = JkMatrix(p, 0);
	SparseMatrix<double> jp = kroneckerProduct(mask, jpk);
	SparseMatrix<double> kron;
	// Fill the matrix
	for (k = 1; k < p - 1; k++){
		mask.setZero();
		mask.coeffRef(k, 0) = 1;
		jpk = JkMatrix(p, k);
		kron = kroneckerProduct(mask, jpk);
		jp = jp + kron;
	}
	// Return 
	return jp;
}

// --------------------------------------------------------------------------
// DriftCorr(wData, np, nt, pOrder, tol)
//
// Drift correction for wData
// nt turns
// pOrder polynomial fit
//
// GLB, 04/12/2015
// --------------------------------------------------------------------------
int measure::DriftCorr(double wData[], int np, int nt, int pOrder, double tol){
	// Initialize
	VectorXd data(np*nt);
	int k, l;
	double x;
	// Convert wData to VectorXd
	for (k = 0; k < np*nt; k++){
		data(k) = wData[k];
	}
	if (pOrder > 8){ pOrder = 8; }
	// Build D matrix
	SparseMatrix<double> jp = JMatrix(nt);
	SparseMatrix<double> in(np, np);
	in.setIdentity();
	SparseMatrix<double> d = kroneckerProduct(jp, in);
	// Build D^T.D
	SparseMatrix<double> dtd = d.transpose()*d;
	// Build F matrix
	MatrixXd f(np*nt, pOrder);
	for (k = 0; k < pOrder; k++){
		for (l = 0; l < np*nt; l++){
			x = 2 / (np*nt - 1)*l - 1;
			f(l, k) = Legendre(k + 1, x);
		}
	}
	MatrixXd ft = f.transpose();
	SparseMatrix<double> fs = f.sparseView();
	// Build F^T.D^T.D.F
	SparseMatrix<double> fddfs = fs.transpose()*d.transpose()*d*fs;
	MatrixXd fddf(fddfs);
	// Build (F^T.D^T.D.F)^-1
	// SVD
	JacobiSVD<Eigen::MatrixXd> svd(fddf, Eigen::ComputeFullU | Eigen::ComputeFullV);
	VectorXd s = svd.singularValues();
	MatrixXd u = svd.matrixU();
	MatrixXd v = svd.matrixV();
	// Filter out small singular values
	MatrixXd sm = MatrixXd::Zero(pOrder, pOrder);
	for (k = 0; k < pOrder; k++){
		if (s(k) / s(0) >= tol){
			sm(k, k) = s(k);
		}
	}
	// Pseudo-inverse
	MatrixXd ifddf = v*sm*u.transpose();
	// Estimate drift coefficients
	MatrixXd driftCoef = ifddf*ft*dtd*data;
	// Compute drift
	MatrixXd drift = driftCoef.transpose()*ft;
	drift = drift + data.mean()*MatrixXd::Ones(drift.rows(), drift.cols());
	// Substract drift to the original data
	for (k = 0; k < np*nt; k++){
		wData[k] = wData[k] - drift(k);
	}

	// Return
	return 0;
}
//////////////////////////////////
// compute linear fit from measurement 
//
//a =(Σ(xi - xmoy)(yi - ymoy))/Σ(xi - xmoy)^2
//b=a = ymoy – a xmoy
//X0=-b/a      valeur X pour Y=0
//
//////////////////////////////////////
int measure::LinFit(double *XVal, double*YVal,int len ,double *a,double *b, double *X0)
{
	// get the result of the first measurement
	std::vector <double> VX, VY, COVVec,VarXVec;
	
	// Calculate Linear regression 
	for (int i = 0; i < len; i++)
	{
		VX.push_back(XVal[i]);
		VY.push_back(YVal[i]);
	}
	double SY = std::accumulate(VY.begin(), VY.end(), 0.0);
	double My = SY / VY.size();
	double SX = std::accumulate(VX.begin(), VX.end(), 0.0);
	double Mx = SX / VX.size();

	for (int i = 0; i < len; i++)
	{
		COVVec.push_back((VX[i] - Mx)*(VY[i] - My));
		VarXVec.push_back((VX[i] - Mx)*(VX[i] - Mx));
	}

	double Cov = std::accumulate(COVVec.begin(), COVVec.end(), 0.0) / (len);
	double VarX = std::accumulate(VarXVec.begin(), VarXVec.end(), 0.0) / (len);
	*a = Cov /VarX;
	*b = My - (*a)*Mx;
	*X0 = -(*b) /(*a);
	VX.clear();
	VY.clear();
	COVVec.clear();
	VarXVec.clear();
	return 0;
}


static void BackMasterCircularIntegralScan(void* p)
{
	measure *pt;
	pt = (measure *)p;
	if (pt->AbortRequest)
	{
		if (pt->NOISE_MULTI != NULL)
		{
			delete pt->NOISE_MULTI;
			pt->NOISE_MULTI = new Keithley(pt->NoiseMulti);
		}
		delete pt->MULTI;
		pt->MULTI = new Keithley(pt->MultiName);
	}
	pt->AbortRequest = false;
	double measlen = 2 * M_PI*pt->CISStruct.R;
	double step = measlen / pt->CISStruct.NBpt;
	double  stepdeg = 360 / pt->CISStruct.NBpt;
	//	double V=pt->rap_step_len*step/pt->CISStruct.TI;
	double V = pt->CISStruct.Velocity;
	double  tacc = V / pt->CISStruct.Acc;
	double  dacc = 1.5*V*tacc / (2);
	double OBEG = -45;
	double OEND = 360 * pt->CISStruct.NBturn + 90;
	double TI1 = pt->CISStruct.TI;
	int TotalTrigs = (int)(pt->CISStruct.NBpt*pt->CISStruct.NBturn);

	double V_rap = V *pt->CISStruct.TI / step;
	if (V_rap > 0.8)
	{
		sprintf(pt->last_err, "Velocity Ratio >0.8  \n ");

		char suite[100];
		sprintf(suite, "Measure Stopped %f %f \n", pt->CISStruct.TI, V);
		strcat(pt->last_err, suite);
		pt->DataAvailable = false;
		pt->Measuring = false;
		_endthread();
	}
	if (V>pt->vmax)
	{
		sprintf(pt->last_err, "Velocity : %lf too HIGH ( Max Velocity= %lf ) \n ", V, pt->vmax);
		TI1 = ceil((pt->rap_step_len*step / pt->vmax) / 0.02)*0.02;
		V = pt->rap_step_len*step / TI1;
		char suite[100];
		sprintf(suite, "Integration time set to %lf    Velocity Set to %lf\n", TI1, V);
		strcat(pt->last_err, suite);
	}
	if (!pt->MULTI->EnterIddle())
	{
		sprintf(pt->last_err, "Error in Multi EnterIddle \n"); pt->Measuring = false; _endthread();
	}
	if (!pt->MULTI->SetTrigSource("EXT"))
	{
		sprintf(pt->last_err, "Cant Set EXT Trig Source on MULTI\n"); pt->Measuring = false; _endthread();
	}
	if (!pt->MULTI->SetMode("VOLT:DC"))
	{
		sprintf(pt->last_err, "Cant Set mode to VOLT:DC on MULTI\n"); pt->Measuring = false; _endthread();
	}
	if (!pt->MULTI->SetIntegrationTime(TI1))
	{
		sprintf(pt->last_err, "Cant Set Integration time to %lf on MULTI\n", TI1); pt->Measuring = false; _endthread();
	}
	Sleep(100);
	if (!pt->MULTI->SetDisplay(0))
	{
		sprintf(pt->last_err, "Cant Set Display  to %d on MULTI\n", 0); pt->Measuring = false; _endthread();
	}
	if (!pt->MULTI->SetLowPass(1))
	{
		sprintf(pt->last_err, "Cant Set LowPass  to %d on MULTI\n", 1); pt->Measuring = false; _endthread();
	}
	if (!pt->MULTI->SetIntegration(1))
	{
		sprintf(pt->last_err, "Cant Set Integration  to ON on MULTI\n"); pt->Measuring = false; _endthread();
	}
	if (!pt->MULTI->SetBuffer((int)(TotalTrigs)))
	{
		sprintf(pt->last_err, "Cant Set buffer size  to %d on MULTI\n", TotalTrigs); pt->Measuring = false; _endthread();
	}
	if (!pt->MULTI->SetTriggerCount((int)(TotalTrigs)))
	{
		sprintf(pt->last_err, "Cant Set trigger Count  to %d on MULTI\n", TotalTrigs); pt->Measuring = false; _endthread();
	}
	if (!pt->MULTI->LeaveIddle())
	{
		sprintf(pt->last_err, "Cant Leave Iddle Mode on MULTI\n"); pt->Measuring = false; _endthread();
	}
	if (pt->NOISE_MULTI != NULL)
	{
		if (!pt->NOISE_MULTI->EnterIddle())
		{
			sprintf(pt->last_err, "Error in NOISE_MULTI EnterIddle \n"); pt->Measuring = false; _endthread();
		}
		if (!pt->NOISE_MULTI->SetTrigSource("EXT"))
		{
			sprintf(pt->last_err, "Cant Set EXT Trig Source on NOISE_MULTI\n"); pt->Measuring = false; _endthread();
		}
		if (!pt->NOISE_MULTI->SetMode("VOLT:DC"))
		{
			sprintf(pt->last_err, "Cant Set mode to VOLT:DC on NOISE_MULTI\n"); pt->Measuring = false; _endthread();
		}
		if (!pt->NOISE_MULTI->SetIntegrationTime(TI1))
		{
			sprintf(pt->last_err, "Cant Set Integration time to %lf on NOISE_MULTI\n", TI1); pt->Measuring = false; _endthread();
		}
		Sleep(100);
		if (!pt->NOISE_MULTI->SetDisplay(0))
		{
			sprintf(pt->last_err, "Cant Set Display  to %d on NOISE_MULTI\n", 0); pt->Measuring = false; _endthread();
		}
		if (!pt->NOISE_MULTI->SetLowPass(1))
		{
			sprintf(pt->last_err, "Cant Set LowPass  to %d on NOISE_MULTI\n", 1); pt->Measuring = false; _endthread();
		}
		if (!pt->NOISE_MULTI->SetIntegration(1))
		{
			sprintf(pt->last_err, "Cant Set Integration  to ON on NOISE_MULTI\n"); pt->Measuring = false; _endthread();
		}
		if (!pt->NOISE_MULTI->SetBuffer((int)(TotalTrigs)))
		{
			sprintf(pt->last_err, "Cant Set buffer size  to %d on NOISE_MULTI\n", TotalTrigs); pt->Measuring = false; _endthread();
		}
		if (!pt->NOISE_MULTI->SetTriggerCount((int)(TotalTrigs)))
		{
			sprintf(pt->last_err, "Cant Set trigger Count  to %d on NOISE_MULTI\n", TotalTrigs); pt->Measuring = false; _endthread();
		}
		if (!pt->NOISE_MULTI->LeaveIddle())
		{
			sprintf(pt->last_err, "Cant Leave Iddle Mode on NOISE_MULTI\n"); pt->Measuring = false; _endthread();
		}
	}
	if (!pt->XPS->MasterCircleAndTrig(pt->CISStruct.XC, pt->CISStruct.ZC,
		pt->CISStruct.R, OBEG, -stepdeg / 2 * pt->rap_step_len, stepdeg
		, (int)TotalTrigs, OEND, V, pt->CISStruct.Acc))
	{
		sprintf(pt->last_err, "Cant CircleAndTrig \n"); pt->Measuring = false; _endthread();
	}
	do
	{
		if (pt->AbortRequest)
		{
			sprintf(pt->last_err, "Abort requested \n"); pt->Measuring = false; _endthread();
		}
		Sleep(100);
	} while (!pt->XPS->GatheringDataReady());
	double *XPOS0, *ZPOS0, *XPOS1, *ZPOS1, *VX0, *VX1, *VZ0, *VZ1;
	double BDATA0[1025]; double BDATA1[1025];
	double NOISE0[1025], NOISE1[1025];
	int nb_pt0;
	if (!pt->XPS->GetGatheringData(&XPOS0, &ZPOS0, &VX0, &VZ0, &nb_pt0))
	{
		sprintf(pt->last_err, "Cant GetGathering Data on XPS \n"); pt->Measuring = false; _endthread();
	}
	if (pt->AbortRequest)
	{
		sprintf(pt->last_err, "Abort requested \n"); pt->Measuring = false; _endthread();
	}
	if (!pt->MULTI->GetBufferData(BDATA0, (int)(TotalTrigs)))
	{
		sprintf(pt->last_err, "Cant %d GetBufferData on MULTI\n", TotalTrigs); pt->Measuring = false; _endthread();
	}
	if (pt->NOISE_MULTI != NULL)
	{
		if (!pt->NOISE_MULTI->GetBufferData(NOISE0, (int)(TotalTrigs)))
		{
			sprintf(pt->last_err, "Cant %d GetBufferData on NOISE_MULTI\n", TotalTrigs); pt->Measuring = false; _endthread();
		}
	}

	if (pt->AbortRequest)
	{
		sprintf(pt->last_err, "Abort requested \n"); pt->Measuring = false; _endthread();
	}

	// Scan dans l'autre sens -------------------------------------------
	if (!pt->MULTI->SetBuffer((int)(TotalTrigs)))
	{
		sprintf(pt->last_err, "Cant Set buffer size  to %d on MULTI\n", TotalTrigs); pt->Measuring = false; _endthread();
	}
	if (!pt->MULTI->SetTriggerCount((int)(TotalTrigs)))
	{
		sprintf(pt->last_err, "Cant Set trigger Count  to %d on MULTI\n", TotalTrigs); pt->Measuring = false; _endthread();
	}
	if (!pt->MULTI->LeaveIddle())
	{
		sprintf(pt->last_err, "Cant Leave Iddle Mode on MULTI\n"); pt->Measuring = false; _endthread();
	}

	if (pt->NOISE_MULTI != NULL)
	{
		if (!pt->NOISE_MULTI->SetBuffer((int)(TotalTrigs)))
		{
			sprintf(pt->last_err, "Cant Set buffer size  to %d on NOISE_MULTI\n", TotalTrigs); pt->Measuring = false; _endthread();
		}
		if (!pt->NOISE_MULTI->SetTriggerCount((int)(TotalTrigs)))
		{
			sprintf(pt->last_err, "Cant Set trigger Count  to %d on NOISE_MULTI\n", TotalTrigs); pt->Measuring = false; _endthread();
		}
		if (!pt->NOISE_MULTI->LeaveIddle())
		{
			sprintf(pt->last_err, "Cant Leave Iddle Mode on NOISE_MULTI\n"); pt->Measuring = false; _endthread();
		}
	}

	if (!pt->XPS->MasterCircleAndTrig(pt->CISStruct.XC, pt->CISStruct.ZC,
		pt->CISStruct.R, -OBEG, (-360 / pt->CISStruct.NBpt) + stepdeg / 2 * pt->rap_step_len, stepdeg
		, (int)TotalTrigs, -OEND, V, pt->CISStruct.Acc))
	{
		sprintf(pt->last_err, "Cant CircleAndTrig \n"); pt->Measuring = false; _endthread();
	}
	do
	{
		if (pt->AbortRequest)
		{
			sprintf(pt->last_err, "Abort requested \n"); pt->Measuring = false; _endthread();
		}
		Sleep(100);
	} while (!pt->XPS->GatheringDataReady());
	int nb_pt1;
	if (!pt->XPS->GetGatheringData(&XPOS1, &ZPOS1, &VX1, &VZ1, &nb_pt1))
	{
		sprintf(pt->last_err, "Cant GetGathering Data on XPS \n"); pt->Measuring = false; _endthread();
	}
	if (pt->AbortRequest)
	{
		sprintf(pt->last_err, "Abort requested \n"); pt->Measuring = false; _endthread();
	}
	if (!pt->MULTI->GetBufferData(BDATA1, (int)(TotalTrigs)))
	{
		sprintf(pt->last_err, "Cant %d GetBufferData on MULTI\n", TotalTrigs); pt->Measuring = false; _endthread();
	}
	if (pt->NOISE_MULTI != NULL)
	{
		if (!pt->NOISE_MULTI->GetBufferData(NOISE1, (int)(TotalTrigs)))
		{
			sprintf(pt->last_err, "Cant %d GetBufferData on NOISE_MULTI\n", TotalTrigs); pt->Measuring = false; _endthread();
		}
	}
	if (pt->AbortRequest)
	{
		sprintf(pt->last_err, "Abort requested \n"); pt->Measuring = false; _endthread();
	}
	//ZERO On all Return Value
	pt->result = (double *)realloc(pt->result, TotalTrigs*sizeof(double));
	if (pt->NOISE_MULTI != NULL)
		pt->noise = (double *)realloc(pt->noise, TotalTrigs * sizeof(double));
	pt->X_result = (double *)realloc(pt->X_result, TotalTrigs*sizeof(double));
	pt->Z_result = (double *)realloc(pt->Z_result, TotalTrigs*sizeof(double));
	pt->T_result = (double *)realloc(pt->T_result, TotalTrigs*sizeof(double));
	pt->VX_result = (double *)realloc(pt->VX_result, TotalTrigs*sizeof(double));
	pt->VZ_result = (double *)realloc(pt->VZ_result, TotalTrigs*sizeof(double));

	for (int i = 0; i<TotalTrigs; i++)
	{
		pt->result[i] = 0; pt->X_result[i] = 0; pt->Z_result[i] = 0; pt->T_result[i] = 0;
	}
	if (pt->NOISE_MULTI != NULL)
		for (int i = 0; i<TotalTrigs; i++)
			pt->noise[i] = 0;
	//  DATA TREAT
	//Drift correction
	if (pt->CISStruct.NBturn>1)
	{
		int driftOrder = 4;
		pt->DriftCorr(BDATA0, (int)pt->CISStruct.NBpt, (int)pt->CISStruct.NBturn, driftOrder, 0.001);
		pt->DriftCorr(BDATA1, (int)pt->CISStruct.NBpt, (int)pt->CISStruct.NBturn, driftOrder, 0.001);
	}
	// Accumulation des mesures retournees -------------------
	for (int i = 0; i<TotalTrigs; i++)
		//pt->result[i]=-(BDATA0[i]-BDATA1[(TotalTrigs-1-i)])/(2*pt->rap_step_len*step*1e-9);
		pt->result[i] = -(BDATA0[i] - BDATA1[(TotalTrigs - 1 - i)]) / (2 * V * pt->CISStruct.TI * 1e-9)/1000;
	// Correction positions ---------------------------------------------
	double ThetaCorr = pt->rap_step_len*M_PI / pt->CISStruct.NBpt;
	if (pt->NOISE_MULTI != NULL)
		for (int i = 0; i<TotalTrigs; i++)
			pt->noise[i] = -(NOISE0[i] - NOISE1[(TotalTrigs - 1 - i)]) / (2 * V * pt->CISStruct.TI * 1e-9) / 1000;
	std::complex<double> XZPOS[2050];
	std::complex<double> CENTER(pt->CISStruct.XC, pt->CISStruct.ZC);
	std::complex<double> CORR(0, ThetaCorr);
	//POS0
	for (int i = 0; i<2 * TotalTrigs; i++)//Gathering return Negative and positive trig
	{
		std::complex<double> PTCurr(XPOS0[i] - pt->CISStruct.XC, ZPOS0[i] - pt->CISStruct.ZC);
		XZPOS[i] = PTCurr;
		XZPOS[i] *= exp(CORR);
		XZPOS[i] += CENTER;
		XPOS0[i] = real(XZPOS[i]);
		ZPOS0[i] = imag(XZPOS[i]);

	}
	//POS1
	for (int i = 0; i<2 * TotalTrigs; i++)//Gathering return Postive and Negative trig
	{
		std::complex<double> PTCurr(XPOS1[i] - pt->CISStruct.XC, ZPOS1[i] - pt->CISStruct.ZC);
		XZPOS[i] = PTCurr;
		XZPOS[i] *= (exp(-CORR));
		XZPOS[i] += CENTER;
		XPOS1[i] = real(XZPOS[i]);
		ZPOS1[i] = imag(XZPOS[i]);
	}
	// Accumulation des positions retournees ------------------
	for (int i = 0; i<TotalTrigs; i++)
	{
		pt->X_result[i] = (XPOS0[2 * i] + XPOS1[2 * (TotalTrigs - i - 1)]) / 2;
		pt->Z_result[i] = (ZPOS0[2 * i] + ZPOS1[2 * (TotalTrigs - i - 1)]) / 2;
	}

	// Moyenne sur plusieurs tours ---------------------------------
	for (int j = 1; j<pt->CISStruct.NBturn; j++)	// On fait la somme des valeur sur le premier tour --> J=1
	{
		for (int i = 0; i<pt->CISStruct.NBpt; i++)
		{
			pt->result[i] += pt->result[(int)(i + j*pt->CISStruct.NBpt)];
			pt->X_result[i] += pt->X_result[(int)(i + j*pt->CISStruct.NBpt)];
			pt->Z_result[i] += pt->Z_result[(int)(i + j*pt->CISStruct.NBpt)];

		}
		if (pt->NOISE_MULTI != NULL)
		{
			for (int i = 0; i<pt->CISStruct.NBpt; i++)
			{
				pt->noise[i] += pt->noise[(int)(i + j*pt->CISStruct.NBpt)];
			}
		}
	}
	double meanval = 0;
	for (int i = 0; i<pt->CISStruct.NBpt; i++)
	{
		pt->result[i] /= pt->CISStruct.NBturn;
		meanval += pt->result[i];
		pt->X_result[i] /= pt->CISStruct.NBturn;
		pt->Z_result[i] /= pt->CISStruct.NBturn;
		pt->T_result[i] = atan2(pt->Z_result[i] - pt->CISStruct.ZC, pt->X_result[i] - pt->CISStruct.XC) + M_PI / 2;//Angle calculated on mean val 

	}
	for (int i = 0; i<pt->CISStruct.NBpt; i++)
	{
		pt->result[i] -= meanval / pt->CISStruct.NBpt;
	}
	if (pt->NOISE_MULTI != NULL)
	{
		for (int i = 0; i<pt->CISStruct.NBpt; i++)
		{
			pt->noise[i] /= pt->CISStruct.NBturn;
			meanval += pt->noise[i];

		}
		for (int i = 0; i<pt->CISStruct.NBpt; i++)
		{
			pt->noise[i] -= meanval / pt->CISStruct.NBpt;
		}
	}
	// 
	// PFOUUUUUUUUU


	pt->MesLen = (int)pt->CISStruct.NBpt;


	if (pt->AbortRequest)
	{
		sprintf(pt->last_err, "Abort requested \n"); pt->Measuring = false; _endthread();
	}

	pt->MULTI->SetDisplay(1);
	pt->MULTI->SetTrigSource("IMM");
	pt->MULTI->FreeRun();
	if (pt->NOISE_MULTI != NULL)
	{
		pt->NOISE_MULTI->SetDisplay(1);
		pt->NOISE_MULTI->SetTrigSource("IMM");
		pt->NOISE_MULTI->FreeRun();
	}
	pt->DataAvailable = true;
	pt->Measuring = false;

	_endthread();
}

bool measure::MasterCircularScan(double XC, double ZC, double R, double NBPT, double NBTURN, double TI, double Velocity, double Acc)
{
	if (IsMeasuring())
	{
		sprintf(last_err, "Can't Run CircularScan Another Measure is running\n");
		return false;
	}
	sprintf(last_err, "No Error\n");
	DataAvailable = false;
	Measuring = true;
	CISStruct.XC = XC; CISStruct.ZC = ZC;
	CISStruct.R = R;
	CISStruct.NBpt = NBPT; CISStruct.NBturn = NBTURN;
	CISStruct.Velocity = Velocity;
	CISStruct.TI = TI; CISStruct.Acc = Acc;


	if (_beginthread(BackMasterCircularIntegralScan, 0, (void *)this) == -1)
	{
		return (false);
	}
	return (true);

	return true;
}

bool measure::NoiseMeas()
{
	return (NOISE_MULTI != NULL);

}

bool measure::SetQuiet(bool QuietSwitch)
{
	Quiet = QuietSwitch;
	return true;
}

bool measure::GetQuiet()
{
	return Quiet;
}

