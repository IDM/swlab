// This file is part of SWLab Stretched Wire Magnetic Measurement Program 
// 	
// Copyright 2016, ESRF - The European Synchroton, Christophe Penel, Gael Le Bec
//
// SWLab is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// The XOP Toolkit is copyright Wavemetrics, see <http://www.wavemetrics.com>
// The Newport XPS libraries are copyright Newport, see <http://www.newport.com>
// The Eigen library is licensed under the MPL2, see <http://eigen.tuxfamily.org>
// 

/*

HR, 091021
Updated for 64-bit compatibility.

HR, 2013-02-08
Updated for Xcode 4 compatibility. Changed to use XOPMain instead of main.
As a result the XOP now requires Igor Pro 6.20 or later.
*/
//#define _CRTDBG_MAP_ALLOC
//#include <stdlib.h>
//#include <crtdbg.h>

#include "XOPStandardHeaders.h"			// Include ANSI headers, Mac headers, IgorXOP.h, XOP.h and XOPSupport.h
#include "multipole_xop7.h"

/* Global Variables (none) */
measure *Mes;
static DataFolderHandle RootDF,MultipoleDF;
char Igor_err[500];// error, warning message for Igor
double Rtable[1025];// Radius table for multiple circle measure
Handle LastCmdstrH;
char *chng(char *buff, char old, char niu)
{
char *ptr;
for(;;)
	{
  ptr = strchr(buff, old);
  if(ptr==NULL) break;
     buff[(int)(ptr-buff)]=niu;
	}
return buff;
}

void Print_err(char * err)
{
	sprintf(Igor_err,err);
	XOPNotice(chng(Igor_err,'\n',CR_CHAR));	
}

#pragma pack(2)		// All structures passed to Igor are two-byte aligned.
struct MultipoleOpenParams  {
	Handle strH;
	double result;
};
typedef struct MultipoleOpenParams MultipoleOpenParams;
#pragma pack()		// Reset structure alignment to default.

extern "C" int
	MultipoleOpen(MultipoleOpenParams* p)
{
	char name[50];
	int len2 = GetHandleSize(p->strH);
	memcpy(&name[0], *p->strH, len2);	
	name[len2]=0;
	if (Mes!=NULL)
	{
		p->result=false;
		XOPNotice("A Measurement object has already been created." CR_STR);
		return MEAS_ALREADYCREATED;
	}
	
	int err;
	if (err = GetRootDataFolder(0, &RootDF))
		return err;
	Mes=new measure(name);
	if (Mes==NULL)
	{
		p->result=false;
		XOPNotice("Unable to create a measurement object." CR_STR);
	}
	else
	{
		if (Mes->Valid)
		{
			p->result=true;
			NewDataFolder(RootDF,name,&MultipoleDF);
			//long Dimw[MAX_DIMENSIONS+1];
			CountInt Dimw[MAX_DIMENSIONS + 1];
			Dimw[0]=1;
			Dimw[1]=0;
			double R=10;
			waveHndl radius;
			if(MDMakeWave(&radius,"Radius",MultipoleDF,Dimw,NT_FP64,1)==0)
			{
				Dimw[0]=0;
				MDSetNumericWavePointValue(radius,Dimw,&R);
			}
			else
				Print_err("WARNING: radius wave not created \n");
			if (LastCmdstrH == NULL)
				LastCmdstrH = NewHandle(0L);
			SetCurrentDataFolder(MultipoleDF);
			SetIgorStringVar("LastCmd", "NOT INITIALISED", 1);
			SetCurrentDataFolder(RootDF);
		}
		else
		{
			Mes->Valid = false;
			p->result=false;
			Print_err(Mes->last_err);
			delete Mes;
			Mes = NULL;
		}
	}

	return(0);					/* XFunc error code */
}


#pragma pack(2)		// All structures passed to Igor are two-byte aligned.
struct MultipoleCloseParams  {

	double result;
};
typedef struct MultipoleCloseParams MultipoleCloseParams;
#pragma pack()		// Reset structure alignment to default.

extern "C" int
	MultipoleClose(MultipoleCloseParams* p)
{

	if (Mes!=NULL)
	{
		if (Mes->IsMeasuring())
		{
			XOPNotice("Can't close multipole object during measurement sequence." CR_STR);
			p->result=false;
			return ERR_NOCLOSEINMEAS;          /* XFunc error code */
		}
		delete Mes;
		Mes=NULL;
		p->result=true;
		return 0;
	}
	else
	{
		XOPNotice("No measurement to close." CR_STR);
		p->result=false;
		return NO_MEASCREATED;          /* XFunc error code */
	}					
}


#pragma pack(2)		// All structures passed to Igor are two-byte aligned.
struct DPComplexNum {
	double real;
	double imag;
};
#pragma pack()		// Reset structure alignment to default.

#pragma pack(2)		// All structures passed to Igor are two-byte aligned.
struct MultipoleIsMesuringParams  {
	double result;
};
typedef struct MultipoleIsMesuringParams MultipoleIsMesuringParams;
#pragma pack()		// Reset structure alignment to default.

extern "C" int
	MultipoleIsMesuring(MultipoleIsMesuringParams* p)
{
	if( Mes!=NULL)
	{
		p->result=Mes->IsMeasuring();
		return 0;
	}
	else
	{
		p->result=false;
		XOPNotice("Measurement not created." CR_STR);
		return NO_MEASCREATED;
	}

}

#pragma pack(2)		// All structures passed to Igor are two-byte aligned.
struct MultipoleAbortParams  {
	double result;
};
typedef struct MultipoleAbortParams MultipoleAbortParams;
#pragma pack()		// Reset structure alignment to default.

extern "C" int
	MultipoleAbort(MultipoleAbortParams* p)
{
	if( Mes!=NULL)
	{
		p->result=Mes->Abort();
		return 0;
	}
	else
	{
		p->result=false;
		XOPNotice("Measurement not created." CR_STR);
		return NO_MEASCREATED;
	}

}

#pragma pack(2)		// All structures passed to Igor are two-byte aligned.
struct MultipoleIntegralMeasParams  {
	double tend;
	double nb;
	double acc;
	double V;
	double Angle;
	double Len;
	double Z;
	double X;
	double result;
};
typedef struct MultipoleIntegralMeasParams MultipoleIntegralMeasParams;
#pragma pack()		// Reset structure alignment to default.

extern "C" int
	MultipoleIntegralMeas(MultipoleIntegralMeasParams* p)
{
	if( Mes!=NULL)
	{
		if(Mes->IsMeasuring())
		{
			p->result=false;
			XOPNotice("Measurement already running." CR_STR);
			return MEASALREADYRUNNING;
		}
		else
		{
			Mes->SetMesType(INTEGRAL);
			if(Mes->IntegralMeas(p->X,p->Z,p->Len,p->Angle,p->V,p->acc,p->nb,p->tend))
			{
				p->result=true;
				return 0;
			}
			else
			{
				p->result=false;
				Print_err(Mes->last_err);
				return NO_MEASCREATED;
			}
		}
	}
	else
	{
		p->result=false;
		XOPNotice("Measurement not created." CR_STR);
		return NO_MEASCREATED;
	}

}

#pragma pack(2)		// All structures passed to Igor are two-byte aligned.
struct GetLastMesParams  {
	double result;
};
typedef struct GetLastMesParams GetLastMesParams;
#pragma pack()		// Reset structure alignment to default.

extern "C" int
	GetLastMes(GetLastMesParams* p)
{
	if( Mes!=NULL)
	{
		double *res,*noise,*Xres,*Zres,*Tres,*QCres,*QTres,*VXres, *VZres, *Ares;
		int len;
		len=Mes->GetLastMes(&res,&noise,&Xres,&Zres,&Tres,&QCres,&QTres,&VXres,&VZres,&Ares);
		if (len > 0)
		{
			//long Dimw[MAX_DIMENSIONS + 1];
			CountInt Dimw[MAX_DIMENSIONS + 1];
			//long ind[MAX_DIMENSIONS];
			IndexInt ind[MAX_DIMENSIONS];
			Dimw[0] = len;
			Dimw[1] = 0;
			ind[1] = 0;

			//long Dimw2[MAX_DIMENSIONS + 1];
			CountInt Dimw2[MAX_DIMENSIONS + 1];
			Dimw2[0] = 2;
			Dimw2[1] = 0;

			//long DimNoise[MAX_DIMENSIONS + 1];
			CountInt DimNoise[MAX_DIMENSIONS + 1];
			if (Mes->NoiseMeas())
				DimNoise[0] = len;
			else
				DimNoise[0] = 0;
			DimNoise[1] = 0;

			waveHndl res_H,noise_H, Xres_H, Zres_H, Tres_H, QCres_H, QTres_H, VXres_H, VZres_H, Ares_H;
			DataObjectValue v;

			char cmdstr[256];
			// -----------------------
			// Modif. GLB 30/11/2016
			// -----------------------
			switch (Mes->GetMesType())
			{
			case INTEGRAL:
				if ((MDMakeWave(&res_H, "FieldIntegral", MultipoleDF, Dimw, NT_FP64, 1) == 0) &&
					(MDMakeWave(&noise_H, "Noise", MultipoleDF, DimNoise, NT_FP64, 1) == 0) &&
					(MDMakeWave(&Xres_H, "XPosition", MultipoleDF, Dimw, NT_FP64, 1) == 0) &&
					(MDMakeWave(&Zres_H, "ZPosition", MultipoleDF, Dimw, NT_FP64, 1) == 0) &&
					(MDMakeWave(&Tres_H, "TrajAngle", MultipoleDF, Dimw, NT_FP64, 1) == 0))
				{
					for (ind[0] = 0; ind[0] < len; ind[0]++)
					{
						MDSetNumericWavePointValue(res_H, ind, res); res++;
						MDSetNumericWavePointValue(Xres_H, ind, Xres); Xres++;
						MDSetNumericWavePointValue(Zres_H, ind, Zres); Zres++;
						MDSetNumericWavePointValue(Tres_H, ind, Tres); Tres++;
					}
					for (ind[0] = 0; ind[0] < DimNoise[0]; ind[0]++)
					{
						MDSetNumericWavePointValue(noise_H, ind, noise); noise++;
					}
					p->result = true;
					if (!Mes->GetQuiet())
					{
						XOPNotice("Field integral measurement." CR_STR);
						//strcat(Mes->last_err,CR_STR);
						Print_err(Mes->last_err);
					}
						sprintf(cmdstr, "MultiIntegralMeas(%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%d,%.3f)", Mes->PimStruct.X, Mes->PimStruct.Z, Mes->PimStruct.Len, 
							Mes->PimStruct.Angle, Mes->PimStruct.V, Mes->PimStruct.Acc, Mes->PimStruct.nb, Mes->PimStruct.tend);
					PutCStringInHandle(cmdstr, LastCmdstrH);
					v.strH = LastCmdstrH;
					SetDataFolderObject(MultipoleDF, "LastCmd", STR_OBJECT, &v);
				}
				else
				{
					p->result = false;
					XOPNotice("No Result wave created." CR_STR); return ERR_WAVECREATE;
				}
				break;
			case SECOND_INTEGRAL:
				if ((MDMakeWave(&res_H, "SecondFieldIntegral", MultipoleDF, Dimw, NT_FP64, 1) == 0) &&
					(MDMakeWave(&noise_H, "Noise", MultipoleDF, DimNoise, NT_FP64, 1) == 0) &&
					(MDMakeWave(&Xres_H, "XPosition", MultipoleDF, Dimw, NT_FP64, 1) == 0) &&
					(MDMakeWave(&Zres_H, "ZPosition", MultipoleDF, Dimw, NT_FP64, 1) == 0) &&
					(MDMakeWave(&Tres_H, "TrajAngle", MultipoleDF, Dimw, NT_FP64, 1) == 0))
				{
					for (ind[0] = 0; ind[0] < len; ind[0]++)
					{
						MDSetNumericWavePointValue(res_H, ind, res); res++;
						MDSetNumericWavePointValue(noise_H, ind, noise); noise++;
						MDSetNumericWavePointValue(Xres_H, ind, Xres); Xres++;
						MDSetNumericWavePointValue(Zres_H, ind, Zres); Zres++;
						MDSetNumericWavePointValue(Tres_H, ind, Tres); Tres++;
					}
					for (ind[0] = 0; ind[0] < DimNoise[0]; ind[0]++)
					{
						MDSetNumericWavePointValue(noise_H, ind, noise); noise++;
					}

					p->result = true;
					if (!Mes->GetQuiet())
					{
						XOPNotice("Field second integral measurement." CR_STR);
						//strcat(Mes->last_err,CR_STR);
						Print_err(Mes->last_err);
					}
					sprintf(cmdstr, "MultiDoubleIntegralMeas(%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%d)", Mes->PimStruct.X, Mes->PimStruct.Z, Mes->PimStruct.Len, 
							Mes->PimStruct.Angle, Mes->PimStruct.V, Mes->PimStruct.Acc, Mes->PimStruct.nb);
					PutCStringInHandle(cmdstr, LastCmdstrH);
					v.strH = LastCmdstrH;
					SetDataFolderObject(MultipoleDF, "LastCmd", STR_OBJECT, &v);
				}
				else
				{
					p->result = false;
					XOPNotice("No Result wave created." CR_STR); return ERR_WAVECREATE;
				}
				break;
			case LINEAR_SCAN:
				if ((MDMakeWave(&res_H, "FieldIntegral", MultipoleDF, Dimw, NT_FP64, 1) == 0) &&
					(MDMakeWave(&noise_H, "Noise", MultipoleDF, DimNoise, NT_FP64, 1) == 0) &&
					(MDMakeWave(&Xres_H, "XPosition", MultipoleDF, Dimw, NT_FP64, 1) == 0) &&
					(MDMakeWave(&Zres_H, "ZPosition", MultipoleDF, Dimw, NT_FP64, 1) == 0) &&
					(MDMakeWave(&Tres_H, "TrajAngle", MultipoleDF, Dimw, NT_FP64, 1) == 0))
				{
					for (ind[0] = 0; ind[0] < len; ind[0]++)
					{
						MDSetNumericWavePointValue(res_H, ind, res); res++;
						MDSetNumericWavePointValue(Xres_H, ind, Xres); Xres++;
						MDSetNumericWavePointValue(Zres_H, ind, Zres); Zres++;
						MDSetNumericWavePointValue(Tres_H, ind, Tres); Tres++;
					}
					for (ind[0] = 0; ind[0] < DimNoise[0]; ind[0]++)
					{
						MDSetNumericWavePointValue(noise_H, ind, noise); noise++;
					}

					//MultiIntegraleScan( XB,ZB,XE,ZE,NBPT,TINT,Acc,NBScan)
					p->result = true;
					if (!Mes->GetQuiet())
					{
						XOPNotice("Integral scan done." CR_STR);
						//strcat(Mes->last_err,CR_STR);
						Print_err(Mes->last_err);
					}
					sprintf(cmdstr, "MultiIntegralScan(%.3f,%.3f,%.3f,%.3f,%.0f,%.3f,%.3f,%.0f)", Mes->LISStruct.XB, Mes->LISStruct.ZB, Mes->LISStruct.XE, Mes->LISStruct.ZE
						, Mes->LISStruct.NBpt, Mes->LISStruct.TI, Mes->LISStruct.Acc, Mes->LISStruct.NBScan);
					PutCStringInHandle(cmdstr, LastCmdstrH);
					v.strH = LastCmdstrH;
					SetDataFolderObject(MultipoleDF, "LastCmd", STR_OBJECT, &v);
				}
				else
				{
					p->result = false;
					XOPNotice("No Result wave created." CR_STR); return ERR_WAVECREATE;
				}
				break;
			case CIRCLE_SCAN:
				if ((MDMakeWave(&res_H, "FieldIntegral", MultipoleDF, Dimw, NT_FP64, 1) == 0) &&
					(MDMakeWave(&noise_H, "Noise", MultipoleDF, DimNoise, NT_FP64, 1) == 0) && 
					(MDMakeWave(&Xres_H, "XPosition", MultipoleDF, Dimw, NT_FP64, 1) == 0) &&
					(MDMakeWave(&Zres_H, "ZPosition", MultipoleDF, Dimw, NT_FP64, 1) == 0) &&
					(MDMakeWave(&Tres_H, "TrajAngle", MultipoleDF, Dimw, NT_FP64, 1) == 0) && 
					(MDMakeWave(&VXres_H, "XVelocity", MultipoleDF, Dimw, NT_FP64, 1) == 0) &&
					(MDMakeWave(&VZres_H, "ZVelocity", MultipoleDF, Dimw, NT_FP64, 1) == 0))
				{
					for (ind[0] = 0; ind[0] < len; ind[0]++)
					{
						MDSetNumericWavePointValue(res_H, ind, res); res++;
						MDSetNumericWavePointValue(Xres_H, ind, Xres); Xres++;
						MDSetNumericWavePointValue(Zres_H, ind, Zres); Zres++;
						MDSetNumericWavePointValue(Tres_H, ind, Tres); Tres++;
						MDSetNumericWavePointValue(VXres_H, ind, VXres); VXres++;
						MDSetNumericWavePointValue(VZres_H, ind, VZres); VZres++;
					}
					for (ind[0] = 0; ind[0] < DimNoise[0]; ind[0]++)
					{
						MDSetNumericWavePointValue(noise_H, ind, noise); noise++;
					}

					//MultiCirculareScan( XC,ZC,R,NBPT,NBTURN,pTI,Acc)
					p->result = true;
					if (!Mes->GetQuiet())
					{
						XOPNotice("Circular scan done." CR_STR);
						//strcat(Mes->last_err,CR_STR);
						Print_err(Mes->last_err);
					}
					sprintf(cmdstr, "MultiCircularScan(%.3f,%.3f,%.3f,%.0f,%.0f,%.3f,%.3f)", Mes->CISStruct.XC, Mes->CISStruct.ZC, Mes->CISStruct.R,
						Mes->CISStruct.NBpt, Mes->CISStruct.NBturn, Mes->CISStruct.TI, Mes->CISStruct.Acc);
					PutCStringInHandle(cmdstr, LastCmdstrH);
					v.strH = LastCmdstrH;
					SetDataFolderObject(MultipoleDF, "LastCmd", STR_OBJECT, &v);
				}
				else
				{
					p->result = false;
					XOPNotice("No Result wave created." CR_STR); return ERR_WAVECREATE;
				}
				break;
			case MULTI_CIRCLE_SCAN:
				if ((MDMakeWave(&res_H, "FieldIntegral", MultipoleDF, Dimw, NT_FP64, 1) == 0) &&
					(MDMakeWave(&noise_H, "Noise", MultipoleDF, DimNoise, NT_FP64, 1) == 0) && 
					(MDMakeWave(&Xres_H, "XPosition", MultipoleDF, Dimw, NT_FP64, 1) == 0) &&
					(MDMakeWave(&Zres_H, "ZPosition", MultipoleDF, Dimw, NT_FP64, 1) == 0) &&
					(MDMakeWave(&Tres_H, "TrajAngle", MultipoleDF, Dimw, NT_FP64, 1) == 0) && 
					(MDMakeWave(&VXres_H, "XVelocity", MultipoleDF, Dimw, NT_FP64, 1) == 0) &&
					(MDMakeWave(&VZres_H, "ZVelocity", MultipoleDF, Dimw, NT_FP64, 1) == 0))
				{
					for (ind[0] = 0; ind[0] < len; ind[0]++)
					{
						MDSetNumericWavePointValue(res_H, ind, res); res++;
						MDSetNumericWavePointValue(Xres_H, ind, Xres); Xres++;
						MDSetNumericWavePointValue(Zres_H, ind, Zres); Zres++;
						MDSetNumericWavePointValue(Tres_H, ind, Tres); Tres++;
						MDSetNumericWavePointValue(VXres_H, ind, VXres); VXres++;
						MDSetNumericWavePointValue(VZres_H, ind, VZres); VZres++;
					}
					for (ind[0] = 0; ind[0] < DimNoise[0]; ind[0]++)
					{
						MDSetNumericWavePointValue(noise_H, ind, noise); noise++;
					}

					//MultiRepeatCirculareScan( XC,ZC,$Rw,NBPT,NBTURN,pTI,Acc)
					p->result = true;
					if (!Mes->GetQuiet())
					{
						XOPNotice("Multiple circular scan done." CR_STR);
						//strcat(Mes->last_err,CR_STR);
						Print_err(Mes->last_err);
					}
					sprintf(cmdstr, "MultiRepeatCircularScan(%.3f,%.3f,root:Multipole:Radius,%.0f,%.0f,%.3f,%.3f)", Mes->MCISStruct.XC, Mes->MCISStruct.ZC
						, Mes->MCISStruct.NBpt, Mes->MCISStruct.NBturn, Mes->MCISStruct.TI, Mes->MCISStruct.Acc);

					PutCStringInHandle(cmdstr, LastCmdstrH);
					v.strH = LastCmdstrH;
					SetDataFolderObject(MultipoleDF, "LastCmd", STR_OBJECT, &v);
				}
				else
				{
					p->result = false;
					XOPNotice("No Result wave created." CR_STR); return ERR_WAVECREATE;
				}
				break;
			case MULTI_COMP_CIRCLE_SCAN:
				if ((MDMakeWave(&res_H, "FieldIntegral", MultipoleDF, Dimw, NT_FP64, 1) == 0) &&
					(MDMakeWave(&noise_H, "Noise", MultipoleDF, DimNoise, NT_FP64, 1) == 0) && 
					(MDMakeWave(&Xres_H, "XPosition", MultipoleDF, Dimw, NT_FP64, 1) == 0) &&
					(MDMakeWave(&Zres_H, "ZPosition", MultipoleDF, Dimw, NT_FP64, 1) == 0) &&
					(MDMakeWave(&Tres_H, "TrajAngle", MultipoleDF, Dimw, NT_FP64, 1) == 0))
				{
					for (ind[0] = 0; ind[0] < len; ind[0]++)
					{
						MDSetNumericWavePointValue(res_H, ind, res); res++;
						MDSetNumericWavePointValue(Xres_H, ind, Xres); Xres++;
						MDSetNumericWavePointValue(Zres_H, ind, Zres); Zres++;
						MDSetNumericWavePointValue(Tres_H, ind, Tres); Tres++;
					}
					for (ind[0] = 0; ind[0] < DimNoise[0]; ind[0]++)
					{
						MDSetNumericWavePointValue(noise_H, ind, noise); noise++;
					}

					//MultiCompIntegraleScan(X0, Z0,$Rw, LEN,NBPT, NBAvg, Acc,CompOrder, Phi0)
					p->result = true;
					if (!Mes->GetQuiet())
					{
						XOPNotice("Compensated circular scan done." CR_STR);
						//strcat(Mes->last_err,CR_STR);
						Print_err(Mes->last_err);
					}
					sprintf(cmdstr, "MultiCompIntegralScan(%.3f,%.3f,root:Multipole:Radius,%.3f,%.0f,%.0f,%.3f,%.3f,%.3f,%.3f)", Mes->CCISStruct.XC, Mes->CCISStruct.ZC
						, Mes->CCISStruct.MeasLen, Mes->CCISStruct.NBPT, Mes->CCISStruct.NBAvg, Mes->CCISStruct.V, Mes->CCISStruct.Acc, Mes->CCISStruct.CompOrder, Mes->CCISStruct.Phi0);
					PutCStringInHandle(cmdstr, LastCmdstrH);
					v.strH = LastCmdstrH;
					SetDataFolderObject(MultipoleDF, "LastCmd", STR_OBJECT, &v);

				}
				else
				{
					p->result = false;
					XOPNotice("No Result wave created." CR_STR); return ERR_WAVECREATE;
				}
				break;
			case MULTI_QUAD_CENTER:
				if (MDMakeWave(&QCres_H, "MagCentreLastMeas", MultipoleDF, Dimw2, NT_FP64, 1) == 0)
				{
					for (ind[0] = 0; ind[0] < 2; ind[0]++)
					{
						MDSetNumericWavePointValue(QCres_H, ind, QCres); QCres++;
					}
					p->result = true;
					if (!Mes->GetQuiet())
					{
						XOPNotice("Multi quad center sequence done." CR_STR);
					}
					sprintf(cmdstr, "MultiQuadCenter(%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f)", Mes->QCStruct.x0, Mes->QCStruct.z0, Mes->QCStruct.TX0, Mes->QCStruct.TZ0, Mes->QCStruct.r0, Mes->QCStruct.nPoints, Mes->QCStruct.nAve);
					PutCStringInHandle(cmdstr, LastCmdstrH);
					v.strH = LastCmdstrH;
					SetDataFolderObject(MultipoleDF, "LastCmd", STR_OBJECT, &v);
				}
				else
				{
					p->result = false;
					XOPNotice("No Result wave created." CR_STR); return ERR_WAVECREATE;
				}
				break;
			case MULTI_QUAD_AXIS:
				if (MDMakeWave(&QTres_H, "MagAxisLastMeas", MultipoleDF, Dimw2, NT_FP64, 1) == 0)
				{
					for (ind[0] = 0; ind[0] < 2; ind[0]++)
					{
						MDSetNumericWavePointValue(QTres_H, ind, QTres); QTres++;
					}
					p->result = true;
					if (!Mes->GetQuiet())
					{
						XOPNotice("Multi quad axis sequence done." CR_STR);
					}
					sprintf(cmdstr, "MultiQuadAxis(%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f)", Mes->QCStruct.x0, Mes->QCStruct.z0, Mes->QCStruct.TX0, Mes->QCStruct.TZ0, Mes->QCStruct.r0, Mes->QCStruct.nPoints, Mes->QCStruct.nAve);
					PutCStringInHandle(cmdstr, LastCmdstrH);
					v.strH = LastCmdstrH;
					SetDataFolderObject(MultipoleDF, "LastCmd", STR_OBJECT, &v);
				}
				else
				{
					p->result = false;
					XOPNotice("No Result wave created." CR_STR); return ERR_WAVECREATE;
				}
				break;
			case MULTI_QUAD_CENTER_AXIS:
				p->result = true;
				XOPNotice("Multi quad center axis sequence not yet implemented." CR_STR);
				break;
			case MASTER_CIRCLE_SCAN:
				if ((MDMakeWave(&res_H, "FieldIntegral", MultipoleDF, Dimw, NT_FP64, 1) == 0) &&
					(MDMakeWave(&Xres_H, "XPosition", MultipoleDF, Dimw, NT_FP64, 1) == 0) &&
					(MDMakeWave(&Zres_H, "ZPosition", MultipoleDF, Dimw, NT_FP64, 1) == 0) &&
					(MDMakeWave(&Tres_H, "TrajAngle", MultipoleDF, Dimw, NT_FP64, 1) == 0))
				{
					for (ind[0] = 0; ind[0] < len; ind[0]++)
					{
						MDSetNumericWavePointValue(res_H, ind, res); res++;
						MDSetNumericWavePointValue(Xres_H, ind, Xres); Xres++;
						MDSetNumericWavePointValue(Zres_H, ind, Zres); Zres++;
						MDSetNumericWavePointValue(Tres_H, ind, Tres); Tres++;
					}
					//MasterCirculareScan( XC,ZC,R,NBPT,NBTURN,pTI,Acc)
					p->result = true;
					if (!Mes->GetQuiet())
					{
						XOPNotice("Master circular scan done." CR_STR);
						//strcat(Mes->last_err,CR_STR);
						Print_err(Mes->last_err);
					}
					sprintf(cmdstr, "MasterCircularScan(%.3f,%.3f,%.3f,%.0f,%.0f,%.3f,%.3f)", Mes->CISStruct.XC, Mes->CISStruct.ZC, Mes->CISStruct.R,
						Mes->CISStruct.NBpt, Mes->CISStruct.NBturn, Mes->CISStruct.TI, Mes->CISStruct.Acc);
					PutCStringInHandle(cmdstr, LastCmdstrH);
					v.strH = LastCmdstrH;
					SetDataFolderObject(MultipoleDF, "LastCmd", STR_OBJECT, &v);
				}
				else
				{
					p->result = false;
					XOPNotice("No Result wave created." CR_STR); return ERR_WAVECREATE;
				}
				break;
			case MULTI_QUAD_ALIGN:
				if ( (MDMakeWave(&Ares_H, "QuadAlignResults", MultipoleDF, Dimw, NT_FP64, 1) == 0 ) && (MDMakeWave(&QCres_H, "MagCentreLastMeas",
					MultipoleDF, Dimw2, NT_FP64, 1) == 0 ) && MDMakeWave(&QTres_H, "MagAxisLastMeas", MultipoleDF, Dimw2, NT_FP64, 1) == 0 )
				{
					for (ind[0] = 0; ind[0] < len; ind[0]++)
					{
						MDSetNumericWavePointValue(Ares_H, ind, Ares); Ares++;
					}
					Ares = Ares - 8;
					for (ind[0] = 0; ind[0] < 2; ind[0]++)
					{
						MDSetNumericWavePointValue(QCres_H, ind, Ares); Ares++;
					}
					for (ind[0] = 0; ind[0] < 2; ind[0]++)
					{
						MDSetNumericWavePointValue(QTres_H, ind, Ares); Ares++;
					}

					
					p->result = true;
					if (!Mes->GetQuiet())
					{
						XOPNotice("Quad alignment done." CR_STR);
						//strcat(Mes->last_err,CR_STR);
						Print_err(Mes->last_err);
					}
					sprintf(cmdstr, "MultiQuadAlignment(%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.0f)", Mes->QAStruct.X0, Mes->QAStruct.Z0, Mes->QAStruct.TX0,
						Mes->QAStruct.TZ0, Mes->QAStruct.DX, Mes->QAStruct.DT, Mes->QAStruct.MEASLEN,Mes->QAStruct.VEL,Mes->QAStruct.ACC,Mes->QAStruct.NAVE);
					PutCStringInHandle(cmdstr, LastCmdstrH);
					v.strH = LastCmdstrH;
					SetDataFolderObject(MultipoleDF, "LastCmd", STR_OBJECT, &v);

	
				}
				else
				{
					p->result = false;
					XOPNotice("No Result wave created." CR_STR); return ERR_WAVECREATE;
				}
				break;

			default:
				p->result = false;
				XOPNotice("No Result wave created." CR_STR); return ERR_UNKNOWMES;
				break;
			}

			return 0;
		}
		//		else
		//		{
		//			p->result = false;
		//			XOPNotice("No Result Wave Created\015"); return ERR_WAVECREATE;
		//		}
		//}
		else
		{
			p->result = false;
			Print_err(Mes->last_err); return ERR_GETLASTMES;
		}
	}
	else
	{
		p->result = false;
		XOPNotice("Measurement object not created." CR_STR); return NO_MEASCREATED;
	}

}

#pragma pack(2)		// All structures passed to Igor are two-byte aligned.
struct MultipoleDoubleIntegralMeasParams  {
	double nb;
	double acc;
	double V;
	double ZLen;
	double XLen;
	double Z;
	double X;
	double result;
};
typedef struct MultipoleDoubleIntegralMeasParams MultipoleDoubleIntegralMeasParams;
#pragma pack()		// Reset structure alignment to default.

extern "C" int
	MultipoleDoubleIntegralMeas(MultipoleDoubleIntegralMeasParams* p)
{
	if( Mes!=NULL)
	{
		if(Mes->IsMeasuring())
		{	p->result=false;XOPNotice("Measurement already running." CR_STR);
			return MEASALREADYRUNNING;
		}
		else
		{	Mes->SetMesType(SECOND_INTEGRAL);
			if(Mes->DoubleIntegralMeas(p->X,p->Z,p->XLen,p->ZLen,p->V,p->acc,p->nb))
			{	p->result=true;return 0;
			}
			else
			{	p->result=false;strcat(Mes->last_err,CR_STR);Print_err(Mes->last_err);;
				return NO_MEASCREATED;
			}
		}
	}
	else
	{	p->result=false;XOPNotice("Measurement object not created." CR_STR);
		return NO_MEASCREATED;
	}

}

#pragma pack(2)		// All structures passed to Igor are two-byte aligned.
struct MultipoleIntegralScanParams  {
	double NBScan;
	double Acc;
	double TI;
	double NB;
	double ZE;
	double XE;
	double ZB;
	double XB;
	double result;
};
typedef struct MultipoleIntegralScanParams MultipoleIntegralScanParams;
#pragma pack()		// Reset structure alignment to default.

extern "C" int
	MultipoleIntegralScan(MultipoleIntegralScanParams* p)
{
	if( Mes!=NULL)
	{
		if(Mes->IsMeasuring())
		{	p->result=false;XOPNotice("Measurement already running." CR_STR);
			return MEASALREADYRUNNING;
		}
		else
		{	Mes->SetMesType(LINEAR_SCAN);
			if(Mes->LinearIntegralScan(p->XB,p->ZB,p->XE,p->ZE,p->NB,p->TI,p->Acc,p->NBScan))
			{	p->result=true;return 0;
			}
			else
			{	p->result=false;strcat(Mes->last_err,CR_STR);Print_err(Mes->last_err);
				return NO_MEASCREATED;
			}
		}
	}
	else
	{	p->result=false;XOPNotice("Measurement object not created." CR_STR);
		return NO_MEASCREATED;
	}

}

#pragma pack(2)		// All structures passed to Igor are two-byte aligned.
struct MultipoleCircularScanParams  {
	double driftOrder;
	double Acc;
	double Velocity;
	double TI;
	double NBTURN;
	double NBPT;
	double R;
	double ZC;
	double XC;
	double result;
};
typedef struct MultipoleCircularScanParams MultipoleCircularScanParams;
#pragma pack()		// Reset structure alignment to default.

extern "C" int
	MultipoleCircularScan(MultipoleCircularScanParams* p)
{
	if( Mes!=NULL)
	{
		if(Mes->IsMeasuring())
		{	p->result=false;XOPNotice("Measurement already running." CR_STR);
			return MEASALREADYRUNNING;
		}
		else
		{	Mes->SetMesType(CIRCLE_SCAN);
			if(Mes->CircularScan(p->XC,p->ZC,p->R,p->NBPT,p->NBTURN,p->TI,p->Velocity,p->Acc, p->driftOrder))
			{	p->result=true;return 0;
			}
			else
			{	p->result=false;strcat(Mes->last_err,CR_STR);Print_err(Mes->last_err);
				return NO_MEASCREATED;
			}
		}
	}
	else
	{	p->result=false;XOPNotice("Measurement object not created." CR_STR);
		return NO_MEASCREATED;
	}
}

extern "C" int
MultipoleMasterCircularScan(MultipoleCircularScanParams* p)
{
	if (Mes != NULL)
	{
		if (Mes->IsMeasuring())
		{
			p->result = false; XOPNotice("Measurement already running." CR_STR);
			return MEASALREADYRUNNING;
		}
		else
		{
			Mes->SetMesType(MASTER_CIRCLE_SCAN);
			if (Mes->MasterCircularScan(p->XC, p->ZC, p->R, p->NBPT, p->NBTURN, p->TI, p->Velocity, p->Acc))
			{
				p->result = true; return 0;
			}
			else
			{
				p->result = false; strcat(Mes->last_err, CR_STR); Print_err(Mes->last_err);
				return NO_MEASCREATED;
			}
		}
	}
	else
	{
		p->result = false; XOPNotice("Measurement object not created." CR_STR);
		return NO_MEASCREATED;
	}
}

#pragma pack(2)		// All structures passed to Igor are two-byte aligned.
struct MultipoleMultiCircularScanParams  {
	double driftOrder;
	double Acc;
	double Velocity;
	double TI;
	double NBTURN;
	double NBPT;
	waveHndl  R;
	double ZC;
	double XC;
	double result;
};
typedef struct MultipoleMultiCircularScanParams MultipoleMultiCircularScanParams;
#pragma pack()		// Reset structure alignment to default.

extern "C" int
	MultipoleMultiCircularScan(MultipoleMultiCircularScanParams* p)
{
	if( Mes!=NULL)
	{
		if(Mes->IsMeasuring())
		{	p->result=false;XOPNotice("Measurement already running." CR_STR);
			return MEASALREADYRUNNING;
		}
		else
		{
			int nbpt=WavePoints(p->R);
			if (nbpt>1024)
			{
				p->result=false;
				Print_err("Number of radii limited to 1024." CR_STR);
				return ERR_NOTEXECUTE;
			}
			IndexInt Ind[MAX_DIMENSIONS];
			int errret;
			Rtable[0]=nbpt;
			for (int i=0;i<nbpt;i++)
			{
				double val[2];
				Ind[0]=i;Ind[1]=0;
				//MDGetNumericWavePointValue(p->R,Ind,val);
				if (errret = MDGetNumericWavePointValue(p->R, Ind, val))
					return errret;
				Rtable[i+1]=val[0];


			}
			Mes->SetMesType(MULTI_CIRCLE_SCAN);
			if(Mes->MultiCircularScan(p->XC,p->ZC,Rtable,p->NBPT,p->NBTURN,p->TI,p->Velocity,p->Acc,p->driftOrder))
			{	p->result=true;return 0;
			}
			else
			{	p->result=false;strcat(Mes->last_err,CR_STR);Print_err(Mes->last_err);
				return NO_MEASCREATED;
			}
		}
	}
	else
	{	p->result=false;XOPNotice("Measurement object not created." CR_STR);
		return NO_MEASCREATED;
	}

}

#pragma pack(2)		// All structures passed to Igor are two-byte aligned.
struct MultipoleGetLastErrParams  {

	Handle result;
};
typedef struct MultipoleGetLastErrParams MultipoleGetLastErrParams;
#pragma pack()		// Reset structure alignment to default.

extern "C" int
	MultipoleGetLastErr(MultipoleGetLastErrParams* p)
{
	Handle retstr;
	if( Mes!=NULL)
	{
		retstr=NewHandle((BCInt)strlen(Mes->last_err));
		memcpy(*retstr, Mes->last_err, strlen( Mes->last_err));
		p->result=retstr;
		return 0;
	}
	else
	{	
		retstr=NewHandle(strlen("Measurement object not created."));
		memcpy(*retstr, "Measurement object not created", strlen( "Measurement object not created."));
		
		p->result=retstr;

		return NO_MEASCREATED;
	}

}
#pragma pack(2)		// All structures passed to Igor are two-byte aligned.
struct MultipoleCompCircularScanParams  {
	double tend;
	double Phi;
	double Order;
	double Acc;
	double V;
	double NBAVG;
	double NBPT;
	double Len;
	waveHndl  R;
	double ZC;
	double XC;
	double result;
};
typedef struct MultipoleCompCircularScanParams MultipoleCompCircularScanParams;
#pragma pack()		// Reset structure alignment to default.

extern "C" int
	MultipoleCompCircularScan(MultipoleCompCircularScanParams* p)
{
	if( Mes!=NULL)
	{
		if(Mes->IsMeasuring())
		{	p->result=false;XOPNotice("Measurement already running." CR_STR);
			return MEASALREADYRUNNING;
		}
		else
		{
			int nbpt=WavePoints(p->R);
			if (nbpt>1024)
			{
				p->result=false;
				Print_err("Number of radii limited to 1024." CR_STR);
				return ERR_NOTEXECUTE;
			}
			IndexInt Ind[MAX_DIMENSIONS];
			int errret;
			Rtable[0]=nbpt;
			for (int i=0;i<nbpt;i++)
			{
				double val[2];
				Ind[0]=i;Ind[1]=0;
				//MDGetNumericWavePointValue(p->R,Ind,val);
				if (errret = MDGetNumericWavePointValue(p->R, Ind, val))
					return errret;
				Rtable[i+1]=val[0];
			}
			Mes->SetMesType(MULTI_COMP_CIRCLE_SCAN);
			if (Mes->CompCircularScan(p->XC,p->ZC,Rtable, p->Len,p->NBPT,p->NBAVG,p->V,p->Acc,p->Order,p->Phi,p->tend))
			{	p->result=true;return 0;
			}
			else
			{	p->result=false;strcat(Mes->last_err,CR_STR);Print_err(Mes->last_err);
				return NO_MEASCREATED;
			}
		}
	}
	else
	{	p->result=false;XOPNotice("Measurement object not created." CR_STR);
		return NO_MEASCREATED;
	}

}

#pragma pack(2)		// All structures passed to Igor are two-byte aligned.
struct MultipoleLineFitParams  {

	waveHndl Y;
	waveHndl  X;
	double result;
};
typedef struct MultipoleLineFitParams MultipoleLineFitParams;
#pragma pack()		// Reset structure alignment to default.

extern "C" int
MultipoleLineFit(MultipoleLineFitParams* p)
{
	if (Mes != NULL)
	{
		int NX = WavePoints(p->X);
		int NY = WavePoints(p->Y);
		if (NX!=NY)
		{
			p->result = false; XOPNotice("The two wave must have the same points number." CR_STR);
			return ERR_NOTSAMELEN;
		}
		double *VX = (double *)malloc(NX*sizeof(double));
		double *VY = (double *)malloc(NX*sizeof(double));

		IndexInt Ind[MAX_DIMENSIONS];
		int errret;
		for (int i = 0; i < NX; i++)
		{
			double val[2];
			Ind[0] = i; Ind[1] = 0;
			if (errret = MDGetNumericWavePointValue(p->X, Ind, val))
				return errret;
			VX[i] = val[0];
			if (errret = MDGetNumericWavePointValue(p->Y, Ind, val))
				return errret;
			VY[i] = val[0];
		}
		double a, b, X0;
		Mes->LinFit(VX, VY, NX, &a, &b, &X0);
		p->result = X0;
		char infoMsg[100];
		sprintf(infoMsg, " Y = %e X + %e \n", a, b);
		Print_err(infoMsg);
		return 0;
	}
	else
	{
		p->result = false; XOPNotice("Measurement object not created." CR_STR);
		return NO_MEASCREATED;
	}

}

#pragma pack(2)		// All structures passed to Igor are two-byte aligned.
struct MultipoleQuadCenterParams  {
	double nbavg;
	double nbpt;
	double R0;
	double TZ0;
	double TX0;
	double Z0;
	double  X0;
	double result;
};
typedef struct MultipoleQuadCenterParams MultipoleQuadCenterParams;
#pragma pack()		// Reset structure alignment to default.

extern "C" int
MultipoleQuadCenter(MultipoleQuadCenterParams* p)
{
	if (Mes != NULL)
	{
		Mes->SetMesType(MULTI_QUAD_CENTER);
		Mes->QuadCenter(p->X0, p->Z0,p->TX0,p->TZ0, p->R0, p->nbpt, p->nbavg);
		return 0;
	}
	else
	{
		p->result = false; XOPNotice("Measurement object not created" CR_STR);
		return NO_MEASCREATED;
	}

}
extern "C" int
MultipoleQuadAxis(MultipoleQuadCenterParams* p)
{
	if (Mes != NULL)
	{
		Mes->SetMesType(MULTI_QUAD_AXIS);
		Mes->QuadAxis(p->X0, p->Z0, p->TX0, p->TZ0, p->R0, p->nbpt, p->nbavg);
		return 0;
	}
	else
	{
		p->result = false; XOPNotice("Measurement object not created" CR_STR);
		return NO_MEASCREATED;
	}

}
#pragma pack(2)		// All structures passed to Igor are two-byte aligned.
struct MultipoleQuadAlignmentParams {
	double nbavg;
	double acc;
	double vel;
	double len;
	double dt;
	double dx;
	double tz0;
	double tx0;
	double z0;
	double x0;
	double result;
};
typedef struct MultipoleQuadAlignmentParams MultipoleQuadAlignmentParams;
#pragma pack()		// Reset structure alignment to default.

extern "C" int
MultipoleQuadAlignment(MultipoleQuadAlignmentParams* p)
{
	if (Mes != NULL)
	{
		Mes->SetMesType(MULTI_QUAD_ALIGN);
		Mes->QuadAlign(p->x0, p->z0, p->tx0, p->tz0, p->dx, p->dt, p->len, p->vel, p->acc, p->nbavg);
		return 0;
	}
	else
	{
		p->result = false; XOPNotice("Measurement object not created." CR_STR);
		return NO_MEASCREATED;
	}

}

#pragma pack(2)		// All structures passed to Igor are two-byte aligned.
struct MultipoleQuietParams {
	double QuietSwitch;
	double result;
};
extern "C" int
MultipoleQuiet(MultipoleQuietParams* p)
{
	if (Mes != NULL)
	{
		Mes->SetQuiet((bool)p->QuietSwitch);
		return 0;
	}
	else
	{
		XOPNotice("Measurement object not created." CR_STR);
		return NO_MEASCREATED;
	}
}


static XOPIORecResult
	RegisterFunction()
{
	int funcIndex;

	funcIndex = (int)GetXOPItem(0);	/* which function invoked ? */
	switch (funcIndex) {
	case 0:						/* MultipoleOpen(p1, p2) */
		return (XOPIORecResult)MultipoleOpen;
		break;
	case 1:						/* MultipoleClose(p1, p2) */
		return (XOPIORecResult)MultipoleClose;
		break;
	case 2:						/* MultipoleIsMesuring(p1) */
		return (XOPIORecResult)MultipoleIsMesuring;
		break;
	case 3 :
		return (XOPIORecResult)MultipoleIntegralMeas;
		break;
	case 4 :
		return (XOPIORecResult)GetLastMes;
		break;
	case 5 :
		return (XOPIORecResult)MultipoleDoubleIntegralMeas;
		break;
	case 6 :
		return (XOPIORecResult)MultipoleIntegralScan;
		break;
	case 7 :
		return (XOPIORecResult)MultipoleAbort;
		break;
	case 8 :
		return (XOPIORecResult)MultipoleCircularScan;
		break;	
	case 9:
		return (XOPIORecResult)MultipoleGetLastErr;
		break;
	case 10 :
		return (XOPIORecResult)MultipoleMultiCircularScan;
		break;	
	case 11 :
		return (XOPIORecResult)MultipoleCompCircularScan;
		break;	
	case 12:
		return (XOPIORecResult)MultipoleLineFit;
		break;
	case 13 :
		return (XOPIORecResult)MultipoleQuadCenter;
		break;
	case 14:
		return (XOPIORecResult)MultipoleQuadAxis;
		break;
	case 15:
		return (XOPIORecResult)MultipoleMasterCircularScan;
		break;
	case 16:
		return (XOPIORecResult)MultipoleQuadAlignment;
		break;
	case 17:
		return (XOPIORecResult)MultipoleQuiet;
		break;
	}
	return 0;
}



/*	DoFunction()

This will actually never be called because all of the functions use the direct method.
It would be called if a function used the message method. See the XOP manual for
a discussion of direct versus message XFUNCs.
*/
static int
	DoFunction()
{
	int funcIndex;
	void *p;				/* pointer to structure containing function parameters and result */
	int err;

	funcIndex = (int)GetXOPItem(0);	/* which function invoked ? */
	p = (void*)GetXOPItem(1);		/* get pointer to params/result */
	switch (funcIndex) {
	case 0:						/* MultipoleOpen(p1, p2) */
		err = MultipoleOpen((MultipoleOpenParams*)p);
		break;
	case 1:						/* MultipoleClose(p1, p2) */
		err = MultipoleClose((MultipoleCloseParams*)p);
		break;
	case 2:						/* MultipoleIsMesuring(p1) */
		err = MultipoleIsMesuring((MultipoleIsMesuringParams*)p);
		break;
	case 3 :
		err=MultipoleIntegralMeas((MultipoleIntegralMeasParams*) p);
		break;
	case 4 :
		err=GetLastMes((GetLastMesParams*) p);
		break;
	case 5 :
		err=MultipoleDoubleIntegralMeas((MultipoleDoubleIntegralMeasParams*) p);
		break;
	case 6 :
		err=MultipoleIntegralScan((MultipoleIntegralScanParams*) p);
		break;
	case 7:						/* MultipoleIsMesuring(p1) */
		err = MultipoleAbort((MultipoleAbortParams*)p);
		break;
	case 8:						/* MultipoleIsMesuring(p1) */
		err = MultipoleCircularScan((MultipoleCircularScanParams*)p);
		break;
	case 9:						/* MultipoleIsMesuring(p1) */
		err = MultipoleGetLastErr((MultipoleGetLastErrParams*)p);
		break;
	case 10:						/* MultipoleIsMesuring(p1) */
		err = MultipoleMultiCircularScan((MultipoleMultiCircularScanParams*)p);
		break;
	case 11:						/* MultipoleIsMesuring(p1) */
		err = MultipoleCompCircularScan((MultipoleCompCircularScanParams*)p);
		break;
//		MultipoleLineFit
	case 12:						/* MultipoleIsMesuring(p1) */
		err = MultipoleLineFit((MultipoleLineFitParams*)p);
		break;
	case 13:						/* MultipoleIsMesuring(p1) */
		err = MultipoleQuadCenter((MultipoleQuadCenterParams*)p);
		break;
	case 14:						/* MultipoleIsMesuring(p1) */
		err = MultipoleQuadAxis((MultipoleQuadCenterParams*)p);
		break;
	case 15:						/* MultipoleIsMesuring(p1) */
		err = MultipoleMasterCircularScan((MultipoleCircularScanParams*)p);
		break;
	case 16:						/* MultipoleIsMesuring(p1) */
		err = MultipoleQuadAlignment((MultipoleQuadAlignmentParams*)p);
		break;
	case 17:
		err = MultipoleQuiet((MultipoleQuietParams*)p);
		break;
	}
	return(err);
}

/*	XOPEntry()

This is the entry point from the host application to the XOP for all messages after the
INIT message.
*/

extern "C" void
	XOPEntry(void)
{	
	XOPIORecResult result = 0;

	switch (GetXOPMessage()) {
	case FUNCTION:								/* our external function being invoked ? */
		result = DoFunction();
		break;

	case FUNCADDRS:
		result = RegisterFunction();
		break;
	}
	SetXOPResult(result);
}

/*	XOPMain(ioRecHandle)

This is the initial entry point at which the host application calls XOP.
The message sent by the host must be INIT.

XOPMain does any necessary initialization and then sets the XOPEntry field of the
ioRecHandle to the address to be called for future messages.
*/

HOST_IMPORT void
	main(IORecHandle ioRecHandle)			// The use of XOPMain rather than main means this XOP requires Igor Pro 6.20 or later
{	

	XOPInit(ioRecHandle);					// Do standard XOP initialization
	SetXOPEntry(XOPEntry);					// Set entry point for future calls

	if (igorVersion < 620) 
		SetXOPResult(OLD_IGOR);
	else {
		XOPInit(ioRecHandle);					// Do standard XOP initialization
		SetXOPEntry(XOPEntry);					// Set entry point for future calls
	
		SetXOPResult(0);

		Mes = NULL;

	}

}
