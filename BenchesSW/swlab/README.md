# Stretched Wire Measurement Lab (SW lab)

## Introduction

The SWLab code implements stretched wire measurements for multipole accelerator magnets. In its present version, it works with Keithley voltmeters and Newport XPS motion controllers. The following interfaces are available:

- Igor XOP6, 7 and 8 Win32
- x64 XPS drivers not available

## Contents

The repository is as follows:

- *.vscode:* Visual Studio Code files
- *config*: configuration files needed for initialization
- *doc*: User manual
- *igor_proc*: Igor procedure files for bench interface and data analysis
  -  *esrf*: procedures that are specific to the ESRF site (with e.g. power supply control)
  - *generic*: the same without ESRF specificities
- *source*: C++ source code
- *vc*: Visual studio 2017 project files
- *xop:* Igor Pro extension files
  - *res:* Igor resource files
  - *xop6, xop7, xop8:* binaries for Igor extensions

## Dependencies

This project needs the following packages:

- The Igor XOP toolkit (www.wavemetrics.com)
- The Eigen matrix library (http://eigen.tuxfamily.org)
- The benches' shared files: https://gitlab.esrf.fr/IDM/benchesshared
- NI 448-2 drivers

These files are assumed to be organized as follows:

- *external_libs*
  - *eigen-eigen*
  - *XOPToolkit/XOPToolkit6*
  - *XOPToolkit/XOPToolkit7.01*
  - *XOPToolkit/XOPToolkit8*
- *BenchesShared*
- *This project*

The ESRF version of the procedures requires the VDT2 XOP for driving a RS-232 link. This XOP is available in the *Igor Pro Folder\More Extensions\Data Aquisition*.

## Pre-compiled binaries

Compiled Igor Pro extension binaries (*.xop* DLL file) are available in *xop* directory.

## Compilation

The project can be compiled with Microsoft Visual Studio 2017 and MSBuild 2017. 

After cloning the repository, please update the include directories according to the location of the external libraries (Igor XOPToolkit, etc.) on your computer.

### Visual Studio Code

Open the *swlab* folder and type Ctrl+Shift+B, then select the configuration you want.

### Visual Studio

Open the *swlab/vc/swlab.sln* file and switch to the configuration you want.

## Installation

There is no installer provided for this program. The installation must be done manually following the steps bellow.

**Note**: Generally speaking, Igor extensions and Igor procedures are installed by copy-paste in the WaveMetrics directories: 

* *C:\Program Files\WaveMetrics\Igor Pro 8 Folder\Igor Extensions* for XOP files (*.xop)

* *C:\Program Files\WaveMetrics\Igor Pro 8 Folder\Igor Procedures* for procedure files (*.ipf)

In most cases, it is convenient to paste a shortcut to the extensions and procedures directories.

Another Igor Pro Folder is located in *Documents\WaveMetrics*. Using this folder for installation may lead to strange behavior if using a generic account together with the Work Folders technology.

### Installation with Igor Pro 8

1. Clone the *BenchesShared* and the *BenchesSW* repositories at the locations indicated in the Dependencies section above
2. Create a shortcut to the *xop\xop8* folder, rename it as *swlab* and paste it in the *Igor Extensions* directory
3. Create a shortcut to the *igor_proc* folder, rename it as *swlab_proc* and paste it in the *Igor Procedures* directory
4. Create a shortcut to the *..\BenchesShared\axis\xop\xop8* folder, rename it as *axis* paste it in the *Igor Extensions* directory
5. Copy-paste the *\config\ConfigurationFolder* in the C:\ directory, or set the *ConfPath* environment variable (see the Configuration section below)
6. *For ESRF users:* move the *More Extension\Data Aquisition\VDT2.xop* and the associated help file in the Igor Extension folder

## Configuration

Configuration files are needed for initialization. The default path to configuration files is *c:\\ConfigurationFolder*. It can be set to another directory with the *ConfPath* environment variable.

### Common errors

In case of compilation or link errors, please check the following items:

- The XOPs directory must contain IGOR.lib and XOPSupport.lib.
- It may be necessary to recompile the XOPSupport libs
- The NI 488 drivers must be installed
- Be sure that the link in the project files corresponds to the directory structure above, or update the links
- The XPS drivers are 32 bits and the extension must be used with Igor Win32.

**Igor XOP 7 and 8 note:** It was necessary to replace the XOPMain() by the legacy main() in the xop.cpp files, in order to avoid a bug at Igor start. The cause of this problem is not well understood but it seems to work as it is now.

## License

This program is under a GNU license, see the copyright file attached.

## Citations

The measurement method is described in https://journals.aps.org/prab/abstract/10.1103/PhysRevSTAB.15.022401