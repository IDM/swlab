# Stretched Wire measurements

This repository contains source codes and binaries for the ESRF stretched wire measurement benches.

## Contents

The structure of the repository is as follows:

- *racetrack:* racetrack trajectory based measurements (i.e. "hypomeas" in IDM dialect...). See *racetrack\README* for details
- *swlab:* linear and circular trajectory based measurements. See *swlab\README* for details

## Dependencies

These projects needs the ESRF benches' shared files: https://gitlab.esrf.fr/IDM/benchesshared.

Other libraries are needed for compilation (see the README of the subprojects).

