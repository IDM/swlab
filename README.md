# SWLab Stretched Wire Magnetic Measurement program

## Important note

THIS REPOSITORY IS FOR EXTERNAL USE ONLY. FOR ANY INTERNAL USE PLEASE PREFER THE https://gitlab.esrf.fr/IDM/BenchesSWLab REPOSITORY.

## Licence

This program is under a GNU licence, see the copyright file attached.

## Contributors

Ch Penel, G Le Bec, J Chavanne, L Lefebvre, ESRF

## Dependencies

[Eigen] matrix library (http://eigen.tuxfamily.org)

[Igor], [XOP Toolkit] (Wavemetrics, www.wavemetrics.com)

[Newport XPS drivers] (.h, .lib and .dll) to be installed in the Axis2S/VC folder (www.newport.com) for compilation.  The XPS_xx_drivers.dll should be in the same folder as multipole.xop.

[NI-488.2] Needed for the GPIB interface (http://www.ni.com/download/). The following name is assumed for the NI directory: C:\Program Files (x86)\National Instruments. Please modify the KeithXop project properties if this is not the case.

## Structure

The repository is organized as follows:

- *BenchesShared:* files shared between different benches (e.g. voltmeter and linear stage controls )
- *BenchesSW:* files for stretched-wire measurement benches

## Installation

Please refer to *BenchesShared/README.md* and *BenchesSW/README.md*.
