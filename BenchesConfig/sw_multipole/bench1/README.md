# SW bench 1

## Present location

This bench is installed in ESRF01.

## History

- December 2019: installation in ESRF01
- November 2019: update to Windows 10, new PC
- Chartreuse hall before (dedicated to multipole measurements)

## Last calibration

- Linear stages: January 2020

## Contents

- *calibration:*  Calibration files
- *sw_config:* Stretched wire configuration files used by SW lab and racetrack measurements
- *xps_config:* Newport XPS configuration files